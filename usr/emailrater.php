<?php 
$msg="";

session_start();
if(empty($_SESSION['cid'])){
    die("Not Logged in.");
}

$cid=$_SESSION['cid'];
require_once "../meta/candidate.php";
require_once "../meta/rater.php";
require_once "candfn.php";

$msg="";

writeHead("Conflict Dynamics Profile - Candidates",false);
writeBody("Email Raters",$msg);

if ((isset($_POST['preview'])) && (strtolower($_POST['preview']) == "y")) {

	// --------------------------
	// Preview the email
	// --------------------------
  $type=$_POST['mailtype'];
  $msgbody=$_POST['mailbody'];
  $count=$_POST['count'];
  //echo $count;

	// Get rater ids
	if ($_POST["what"] == "sendall") {
		// Get all
		$rids = array();
		$query="select RID from RATER where CID = $cid";
		//echo "<hr>QUERY: $query<hr>";
		$rs=mysql_query($query)
		or die(mysql_error());
		while($row=mysql_fetch_row($rs)) {
			$rids[] = $row[0];
		};
	} else {
		// Get selected raters
	  $rids=array();
  	for ($i=0;$i<$count;$i++) {
			$val=$_POST["rcp".$i];
			if("N"!=$val){
				$rids[]=$val;
			}
  	}
  }

	// Get email address(es)
	$emails = array();
	$query="select EMAIL from RATER where RID in (" . implode(", ", $rids) . ") order by EMAIL";
	//echo "<hr>".$query."<hr>";
	$rs=mysql_query($query)
		or die(mysql_error());
	
	while($rw=mysql_fetch_row($rs)) {
		$emails[] = $rw[0];
	};
		
	// use this email address as sender
	$conn=dbConnect();
	$query="select EMAIL, FNAME, LNAME from CANDIDATE where CID=$cid";
	//echo "<hr>$query<hr>";
	$rs=mysql_query($query)
		or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	$sender=$rw[0];
	$candname = $rw[1] . " " . $rw[2];

	// get all the recipients
	$rc=array();
	$query="select a.EMAIL,FNAM,LNAM,RID,FNAME, LNAME from RATER a, CANDIDATE b where a.CID=b.CID and a.CID=$cid and RID in (" . implode(", ", $rids) . ")";
	//echo "<hr>".$query."<hr>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	while($rw){
    $rc[]=$rw[0];
    // build an email and send it
    $to=$rw[0];
    $subject="Conflict Dynamics Profile";
    $name=$rw[1]." ".$rw[2];
    $email = $rw[0];
		$lid=$rw[4];
    $mailbody="";

    if ("I"==$type) {
			// Initial 360
			$mailbody = initialRaterEmail($cid, $rid, $candname, $email, true);
    } elseif ("R"==$type) {
			// Reminder 360
			$mailbody = reminderRaterEmail($cid, $rid, $candname, $email, true);
	  } elseif ("C"==$type) {
			// Custom 360
			$mailbody = customRaterEmail($msgbody, $cid, $rid, true);
	  }
    $rw=mysql_fetch_row($rs);
	}

	if (count($emails) == 1) {
		$emailField = $emails[0];
		$emailWord = "email";
	} else {
		$emailField = '<span style="font:bold;" title="Email address will be substituted for each participant">[EMAILID]</span>';
		$emailWord = "emails";
	}
	
	?>
<form name="frmMailSend" id="frmMailSend" action="emailrater.php" method="POST" style="margin:0px;">
<input type="hidden" name="cid"      id="cid"      value="">
<input type="hidden" name="what"     id="what"     value="">
<input type="hidden" name="tmv"  id="tmv"  value="/cons/listpgm.php?^tid=<?=$tid?>">
<?php
if ((isset($_POST["what"])) && ($_POST["what"] == "sendall")) {
	$rs=raterList($cid);
	$count = 0;
	foreach ($rs as $row) {
		echo '<input type="hidden" name="rcp'.$count.'"     id="rcp'.$count.'"    value="' . $row[0] . '">' . "\n";
		$count++;
	}
	echo '<input type="hidden" name="count"" id="count" value="' . $count . '">' . "\n";

} else {
	if (isset($_POST["count"])) {
		$count = 0;
		for ($i=0; $i<$_POST["count"]; $i++) {
			if ((isset($_POST["rcp".$i])) && ($_POST["rcp".$i] != "N")) {
				echo '<input type="hidden" name="rcp'.$count.'" id="rcp'.$count.'" value="' . $_POST["rcp".$i] . '">' . "\n";
				$count++;
			}
		}
		echo '<input type="hidden" name="count"     id="count"     value="' . $count . '" />' . "\n";
	}
	
}
?>
<input type="hidden" name="mailtype" id="mailtype" value="<?php echo $_POST["mailtype"]; ?>">
<textarea name="mailbody"  id="mailbody"  style="display:none;"><?php echo $_POST["mailbody"]; ?></textarea>

<div style="width:500px;text-align:left;padding:8px 0px 4px 4px;">
<input type="button" name="btnSendEmail" id="btnSendEmail" value="Send <?php echo ucfirst($emailWord); ?>" onclick="javascript:sendEmail();" style="font-weight:bold;" title="Send <?php echo $emailWord; ?> to participants now" />
&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
if (($_POST["mailtype"] == "C") || ($_POST["mailtype"] == "SC")) {
	echo "\n" . '<input type="button" name="btnMakeChanges" id="btnMakeChanges" value="Make Changes" onclick="javascript:makeChanges();" title="Make changes to email before sending" />';
	echo "\n" . '&nbsp;&nbsp;&nbsp;&nbsp;';
}
?>
<input type="button" name="btnCancelSend" id="btnCancelSend" value="Cancel" onclick="javascript:cancelSend();" title="Cancel sending the <?php echo $emailWord; ?>" />
&nbsp;&nbsp;&nbsp;&nbsp;
<!--<input type="checkbox" name="ccCon" id="ccCon" value="1" /> Receive copy of <?php echo $emailWord; ?> -->
</p>
</div>
Below is a preview of the <?php echo $emailWord; ?> that will be sent.
<table border="0" cellspacing="3" style="width:500px;margin:0px;background-color:#fcfcfc;border:outset 1px #444444;">
<!-- Always sent from administrator@onlinecdp.org, not the trainer
	<tr>
		<td style="text-align:right;vertical-align:top;"><div style="margin-top:3px;font-weight:bold;">From:</div></td>
		<td><table style="border-width:0px;font-size:8pt;height:12px;"><tr><td width="500"><?php echo $sender; ?></td></tr></table></td>
	</tr>
-->
	<tr>
		<td style="text-align:right;vertical-align:top;"><div style="margin-top:3px;font-weight:bold;">To:</div></td>
		<td><table style="border-width:0px;font-size:8pt;height:12px;"><tr><td width="500"><?php echo implode(", ", $emails); ?></td></tr></table></td>
	</tr>
	<tr>
		<td style="text-align:right;vertical-align:top;"><div style="margin-top:3px;font-weight:bold;">Subject:</div></td>
		<td><table style="border-width:0px;font-size:8pt;height:24px;"><tr><td width="500"><?php echo $subject; ?></td></tr></table></td>
	</tr>
	<tr>
		<td style="text-align:right;vertical-align:top;"><div style="margin-top:3px;font-weight:bold;">Body:</div></td>
		<td><table style="border-width:0px;font-size:8pt;height:96px;"><tr><td width="500"><?php echo nl2br($mailbody); ?></td></tr></table></td>
	</tr>
</table>
<center>
</form>

<script language="javascript">

var validSelection = false;

window.onbeforeunload = confirmSelectionMade;

function confirmSelectionMade() {

  if (!validSelection) {
		return "You have not not yet sent the <?php echo $emailWord; ?>.\n\nYou must click on the \'Send <?php echo ucfirst($emailWord); ?>\' button to send the <?php echo $emailWord; ?>.";
  }
}

function cancelSend() {
	validSelection = true;
	frmMailSend.action = "emailrater.php";
	document.getElementById("what").value = "cancel";
	frmMailSend.submit();
}

function sendEmail() {
  var frmMailSend=document.getElementById('frmMailSend');
	validSelection = true;  
	document.getElementById("what").value = "send";
	frmMailSend.submit();
}

function makeChanges() {    
	frmMailSend.action = "emailrater.php";
	document.getElementById("what").value = "";
	validSelection = true;
	frmMailSend.submit();
}

</script>
	<?php
	writeFooter(false);

	exit;
		
	
} else {

	if("send"==$_POST['what']){
		// -----------------------------------------
		// Send the email to selected participants
		// -----------------------------------------
    $type=$_POST['mailtype'];
    $msgbody=$_POST['mailbody'];
    $rcp=array();
    $count=$_POST['count'];
    $cid=$_SESSION['cid'];
    //echo $count;
    for ($i=0;$i<$count;$i++) {
			$val=$_POST["rcp".$i];
			if ("N" != $val){
				$rcp[]=$val;
			}
    }

    $lst=sendRaterEmail($rcp,$type,$msgbody,$cid);
    if (!$lst) {
			echo "\n" . '<p style="color:#aa0000;">Unable to send email(s)</p>';
    } else {
			echo "\n" . '<p style="color:#00aa00;">Sent emails to '.trim($lst, ",").'.</p>';
			$_POST["count"] = 0;
			$_POST["mailtype"] = "";
			$_POST["mailbody"] = "";
    } 
	}

	if("sendall"==$_POST['what']){
		// -----------------------------------------
		// Send the email to ALL raters
		// -----------------------------------------
    $type=$_POST['mailtype'];
    $msgbody=$_POST['mailbody'];
    $rcp=array();
    $cid=$_SESSION['cid'];

		$rcp = array();
		$query="select RID from RATER where CID = $cid";
		//echo "<hr>QUERY: $query<hr>";
		$rs=mysql_query($query)
		or die(mysql_error());
		while($row=mysql_fetch_row($rs)) {
			$rcp[] = $row[0];
		};
    
    $lst=sendRaterEmail($rcp,$type,$msgbody,$cid);
    if (!$lst) {
			echo "\n" . '<p style="color:#aa0000;">Unable to send email(s)</p>';
    } else {
			echo "\n" . '<p style="color:#00aa00;">Sent emails to '.trim($lst, ",").'.</p>';
			$_POST["count"] = 0;
			$_POST["mailtype"] = "";
			$_POST["mailbody"] = "";
    } 
	}
	
}

if ($_POST["what"] == "cancel") echo "\n" . '<p style="color:#aa0000;">The email has been canceled</p>';

?>
<form name="mailfrm" action="emailrater.php" method=POST>
<input type="hidden" name="cid" value="">
<input type="hidden" name="what" value="">
<input type="hidden" name="preview" value="y">
<table border=1>
	<tr>
		<td style="width:100px;"><small>Name</small></td>
		<td style="width:150px;"><small>Email</small></td>
		<td><small> Include in mailing </small></td>
	</tr>
<?

$rcps = array();
if (isset($_POST["count"])) {
	for ($i=0; $i<$_POST["count"]; $i++) {
		if ((isset($_POST["rcp".$i])) && ($_POST["rcp".$i] != "N")) {
			$rcps[] = $_POST["rcp".$i];
		}
	}
}

raterMailList($cid, $rcps);

$iChecked = '';
$rChecked = '';
$cChecked = '';
if (($_POST["mailtype"] == "R") || ($_POST["mailtype"] == "SR")) {
	$rChecked = 'checked="checked"';
	$textAreaDisplayStyle = "none";
} elseif (($_POST["mailtype"] == "C") || ($_POST["mailtype"] == "SC")) {
	$cChecked = 'checked="checked"';
	$textAreaDisplayStyle = "inline";
} else {
	$iChecked = 'checked="checked"';
	$textAreaDisplayStyle = "none";
}


?>
	<tr>
		<td colspan=3>
			<center>
		
			<table border="1">
				<tr>
					<td bgcolor="#dddddd">
						<small>Initial Email <input type="radio" name="mailtype" value="I" <?php echo $iChecked; ?> onclick="javascript:selectEmailType('I');" /></small>
					</td>
					<td bgcolor="#ffffff">
						<small>Reminder Email <input type="radio" name="mailtype" value="R" <?php echo $rChecked; ?> onclick="javascript:selectEmailType('R');" /></small>
					</td>
					<td bgcolor="#dddddd">
						<small>Custom Email <input type="radio" name="mailtype" value="C" <?php echo $cChecked; ?> onclick="javascript:selectEmailType('C');" /></small>
					</td>
				</tr>
			</table>
			
			</center>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<center>
			<small>
<!-- Enter text for Custom Email:<br><textarea name="mailbody" cols=40 rows=5></textarea>
			<input type="button" value="Send!" onClick="javascript:submitFrm(mailfrm);">&nbsp;&nbsp;
			<input type="button" value="Cancel" onClick="javascript:mailfrm.action='home.php';mailfrm.submit();">
-->
			<div name="mailbodydescr" id="mailbodydescr" style="display:<?php echo $textAreaDisplayStyle; ?>;margin-top:8px;">Enter text for Custom Email:</div><div><textarea name="mailbody" id="mailbody" cols="40" rows="5" style="display:<?php echo $textAreaDisplayStyle; ?>;"><?php echo $_POST["mailbody"]; ?></textarea></div>
			<br />
			<input type="button" value="Send to selected!" onClick="javascript:submitFrm(mailfrm,false);">&nbsp;&nbsp;
			<input type="button" value="Send to all!" onClick="javascript:submitFrm(mailfrm,true);">
			
			</small>
			</center>
		</td>
	</tr>
</table>
</form>
<?php
writeFooter(false);
?>

<script language="JavaScript">
function submitFrm(frm, sendall){
	if(sendall){  
		frm.what.value="sendall";
		frm.submit();
		
	} else {
		// Verify at least one candidate has been selected
		var count = document.getElementById("count").value
		var atLeastOneSelected = false;
		for (var i=0; i<count; i++) {
			if (document.getElementById("rcp"+i).checked) {
				//alert(document.getElementById("rcp"+i).value);
				atLeastOneSelected = true;
				break;
			}
		}
		if (!atLeastOneSelected) {
			alert("You have not selected any recipients for the email.");
		} else {
			frm.what.value="send";
			frm.submit();
		}
	}
}

function selectEmailType( mtype ) {
	switch (mtype) {
		case "I" :
			document.getElementById("mailbodydescr").style.display = "none";
			document.getElementById("mailbody").style.display = "none";
			break;
		case "R" :
			document.getElementById("mailbodydescr").style.display = "none";
			document.getElementById("mailbody").style.display = "none";
			break;
		case "C" :
			document.getElementById("mailbodydescr").style.display = "inline";
			document.getElementById("mailbody").style.display = "inline";
			break
	}
}

</script>

<script language="JavaScript">
/*
function submitFrm(frm){
    if(confirm("Are you sure you want to send the emails?")){
	frm.what.value="send";
	frm.submit();
    }
}
*/
</script>

<?php 
$msg="";
session_start();
if(empty($_SESSION['cid'])){
    die("Not Logged in.");
}
require_once "../meta/candidate.php";
require_once "../meta/rater.php";
require_once "candfn.php";

$msg="";
$rid=$_POST['rid'];
$cid=$_SESSION['cid'];
$catid=$_POST['catid'];
$what=$_POST['what'];

if("add"==$what){
	$idx=$_POST['idx'];
	//$email=addslashes($_POST["email$idx"]);	// Not using indexes anymore
	$email=addslashes($_POST["email"]);
	if(!raterEmailExists($cid,$email)){
		$ratData=array();
		$ratData[0]=$cid;
		$ratData[1]=$catid;
		//$ratData[2]=addslashes($_POST["fname$idx"]);	// Not using indexes anymore
		//$ratData[3]=addslashes($_POST["lname$idx"]);	// Not using indexes anymore
		$ratData[2]=addslashes($_POST["fname"]);
		$ratData[3]=addslashes($_POST["lname"]);
		$ratData[4]=$email;
		$ratData[5]="2";
		$rid=addRater($ratData);
		if(!$rid){
			$msg="<font color=\"#aa0000\">Unable to add rater</font>";
		}
		else{
			$msg="<font color=\"#00aa000\">Successfully added rater</font>";
		}
	}
	else{
		$msg="<font color=\"#aa0000\">Email address already exists</font>";
	}
}
// Delete an existing rater
if("del"==$what){
	$i=deleteRater($rid);
	if(!$rid){
		$msg="<font color=\"#aa0000\">Unable to delete rater</font>";
	}
	else{
		$msg="<font color=\"#00aa000\">Successfully deleted rater</font>";
	}
}

// Save rater data
if("save"==$what){
	$idx=$_POST['idx'];
	//$email=addslashes($_POST["email$idx"]);
	$email=addslashes($_POST["email"]);	// Not using indexes anymore
	if(!raterEmailExists($cid,$email,$rid)){
		$ratData=array();
		$ratData[0]=$cid;
		$ratData[1]=$catid;
		//$ratData[2]=addslashes($_POST["fname$idx"]);
		//$ratData[3]=addslashes($_POST["lname$idx"]);
		$ratData[2]=addslashes($_POST["fname"]);	// Not using indexes anymore
		$ratData[3]=addslashes($_POST["lname"]);	// Not using indexes anymore
		$ratData[4]=$email;
		$ratData[5]=$rid;
		if(!saveRater($ratData)){
			$msg="<font color=\"#aa0000\">Unable to save data</font>";
		}
		else{
			$msg="<font color=\"#00aa000\">Successfully saved data</font>";
		}
	}
	else{
		$msg="<font color=\"#aa0000\">Email address already exists</font>";
	}
}
// send initial rater emails
if("send"==$what){
	$msg=sendInitialEmailToAllRaters($cid);
}

writeHead("Conflict Dynamics Profile - Candidates",false);
writeBody("Rater Management Console",$msg);

?>
<form name="frm1" id="frm1" action="raterhome.php" method=POST style="margin:0px;">
<input type="hidden" name="cid" value="<?=$cid?>">
<input type="hidden" name="rid" value="">
<input type="hidden" name="catid" value="">
<input type="hidden" name="what" value="">
<input type="hidden" name="idx" value="">
<input type="hidden" name="numUnsent" id="numUnsent" value="<?php echo count(getUnmailedRaters($cid)); ?>">
<!--<input type="button" value="Go Back" onClick="javascript:frm1.action='home.php';frm1.submit();">-->
<p>
You can add, edit and delete raters on this page. When you are done 
<input type=button onClick="javascript:showWarningMsg=false;frm1.what.value='send';frm1.submit();" value="Click Here"><br> to send initial email
to your raters. You may assign up to 30 Raters.
</p>
<p><?=listRaterFormsNEW($cid,"frm1")?>
</form>
<script language="JavaScript">
function subForm(frm,catid){
    if(frm.lname.value.length<1||frm.fname.value.length<1||frm.email.value.length<1){
	alert("You must provide First and Last Names and Email address.");
    }
    else{
	frm.catid.value=catid;
	frm.what.value="add";
	frm.submit();
    }
}

function subEditForm(frm,rid){
    if(frm.lname.value.length<1||frm.fname.value.length<1||frm.email.value.length<1){
	alert("You must provide First and Last Names and Email address.");
    }
    else{
	frm.rid.value=rid;
	frm.what.value="save";
	frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>


<?php 

function listRaterFormsNEW($cid,$frm){

	$minimumsMet = "#C3FDB8";
	$lessThanMinimum = "#FFF8C6";
	$titleStyle = "font-size:10pt;";
  $maxraters = 31; //Actually 30 or less
	$weCanAdd=getCanAdd($cid,$maxraters);

	echo <<<EOD
	
<script lang="javascript">

var showWarningMsg = true;

window.onbeforeunload = verifyOnExit;

function verifyOnExit() {
	if ((showWarningMsg) && (document.getElementById("numUnsent").value > 0)) {
		return "You have raters that have not been sent an initial email.";
	}
}

function addRater( catid ) {
  var $frm = document.getElementById("$frm");
	$frm.what.value = "add";
	$frm.catid.value = catid;
	$frm.rid.value = "";
	
	$frm.fname.value = "";
	$frm.lname.value = "";
	$frm.email.value = "";

	switch (catid) {
		case 2 :
			document.getElementById("btnRaterSubmit").value = "Add Boss";
			setRaterDivTitle("Add a Boss");
			showRaterDiv("btnAddBoss");
			break;
		case 3 :
			document.getElementById("btnRaterSubmit").value = "Add Peer";
			setRaterDivTitle("Add a Peer");
			showRaterDiv("btnAddPeer");
			break;
		case 4 :
			document.getElementById("btnRaterSubmit").value = "Add Direct Report";
			setRaterDivTitle("Add a Direct Report");
			showRaterDiv("btnAddDirect");
			break;
	}

}

function editRater(catid, rid, fname, lname, email ) {
  var $frm = document.getElementById("$frm");
	$frm.what.value = "save";
	$frm.catid.value = catid;
	$frm.rid.value = rid;
	
	$frm.fname.value = fname;
	$frm.lname.value = lname;
	$frm.email.value = email;

	switch (catid) {
		case 2 :
			document.getElementById("btnRaterSubmit").value = "Update Boss";
			setRaterDivTitle("Edit a Boss");
			break;
		case 3 :
			document.getElementById("btnRaterSubmit").value = "Update Peer";
			setRaterDivTitle("Edit a Peer");
			break;
		case 4 :
			document.getElementById("btnRaterSubmit").value = "Update Direct Report";
			setRaterDivTitle("Edit a Direct Report");
			break;
	}

	showRaterDiv("catid"+catid+"_rid"+rid);
	
}

function setRaterDivTitle( title ) {
	titleElem = document.getElementById("raterDivTitle");
	while (titleElem.hasChildNodes()) titleElem.removeChild(titleElem.lastChild);

	txtNode = document.createTextNode(title);
	titleElem.appendChild(txtNode);
	
}

function showRaterDiv( elemid ) {

	var elem = document.getElementById(elemid);
	var yPos = getYcoord(elem);
	
	yPos = yPos - 80;

	document.getElementById("raterDiv").style.left = "250px";
	//document.getElementById("raterDiv").style.top = "200px";
	document.getElementById("raterDiv").style.top = yPos+"px";
	document.getElementById("raterDiv").style.visibility = "visible";
	document.getElementById("fname").focus();

}

function cancelRaterDiv( catid ) {
	document.getElementById("raterDiv").style.left = "-1000px";
	document.getElementById("raterDiv").style.top = "-1000px";
	document.getElementById("raterDiv").style.visibility = "hidden";
}

function deleteRater( catid, rid, rname ) {
  var $frm = document.getElementById("$frm");
	if (confirm("Are you sure you want to delete `"+rname+"`.")) {
		$frm.idx.value = "";
		$frm.catid.value = catid;
		$frm.rid.value = rid;
		$frm.what.value = "del";
		showWarningMsg = false;
		$frm.submit();
	}
}

function submitRaterDiv( ) {
  var $frm = document.getElementById("$frm");
	showWarningMsg = false;
	$frm.submit();
}

//Get X position
function getXcoord(el){
  var x=0;
  while(el){
    x += el.offsetLeft;
    el=el.offsetParent;
  }
  return x;
}

//Get Y position
function getYcoord(el){
  var y=0;
  while(el){
    y += el.offsetTop;
    el=el.offsetParent;
  }
  return y;
}

</script>
	
EOD;
	
	echo "\n\n" . '<p>';
	echo "\n" . '<table border="1" cellpadding="4" cellspacing="1px">';

	// ------------------------
	// Boss
	// ------------------------
	$catCount=getCatCount($cid,"2");

	if ($catCount > 0) $bgColor = $minimumsMet;
		else $bgColor = $lessThanMinimum;
	
	echo "\n\t" . '<tr>';
	echo "\n\t\t" . '<td colspan="7" style="background-color:'.$bgColor.';">';
	echo "\n\t\t\t" . '<table width="100%" cellspacing="0" cellpadding="0">';
	echo "\n\t\t\t\t" . '<tr><td id="bosstitle" style="text-align:left;'.$titleStyle.'">';
	echo "\n\t\t\t\t\t" . 'Boss (1 recommended, 1 minimum required, '.$catCount.' assigned)';
	echo "\n\t\t\t\t" . '</td><td style="text-align:right;" width="1">';
	if ($weCanAdd) {
		echo "\n\t\t\t\t\t" . '<input type="button" name="btnAddBoss" id="btnAddBoss" value="Add a Boss" onclick="addRater(2);" />';
	} else {
		echo "\n\t\t\t\t\t" . '&nbsp;';
	}
	echo "\n\t\t\t" . '</td></tr></table>';
	echo "\n\t\t" . '</td>';
	echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr>';
	echo "\n\t\t" . '<td colspan="1" width="200px"><small>Name</small></td>';
	echo "\n\t\t" . '<td colspan="1" width="180px"><small>Email</small></td>';
	echo "\n\t\t" . '<td colspan="1" nowrap="nowrap" style="text-align:center;"><small>Survey Sent</small></td>';
	echo "\n\t\t" . '<td colspan="2" width="120px">&nbsp</td>';
	echo "\n\t" . '</tr>';

	$rows=getConsRaters($cid,"2");
	$idx=0;
	$cnt=0;
	if ($rows > 0) {
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid = $row[0];
			$fname = stripslashes($row[3]);
			$lname = stripslashes($row[4]);
			$rname = "$fname $lname";
			$email = stripslashes($row[5]);
			$exp = ("N" != $row[8]) ? "Yes" : '<span style="font-weight:bold;color:crimson;">No</span>';
			$id = "catid2_rid$rid";
			echo "\n\t" . '<tr>';
			echo "\n\t\t" . '<td colspan="1" id="'.$id.'">';
			echo "\n\t\t\t" . $rname;
			echo "\n\t\t" . '</td>';
			echo "\n\t\t" . '<td colspan="1">';
			echo "\n\t\t\t" . $row[5];
			echo "\n\t\t" . '</td>';
			echo "\n\t\t" . '<td colspan="1" style="text-align:center;"><small>'.$exp.'</small></td>';
			$js_fname = str_replace("'","\'",stripslashes($row[3]));
			$js_lname = str_replace("'","\'",stripslashes($row[4]));
			$js_email = str_replace("'","\'",stripslashes($row[5]));   
      $js_rname = "$js_fname $js_lname";			
			echo "\n\t\t" . '<td colspan="1"><input type="button" value="Edit"   onClick="javascript:editRater(2, ' . $rid . ', \''.$js_fname.'\', \''.$js_lname.'\', \''.$js_email.'\');"></td>';
			echo "\n\t\t" . '<td colspan="1"><input type="button" value="Delete" onClick="javascript:deleteRater(2, '.$rid.',\' '.$js_rname.'\');" /></td>';
			echo "\n\t" . '</tr>';
		}
  }
	echo "\n" . '</table>';
	echo "\n" . '<p>';

	// ------------------------
	// Peer
	// ------------------------
	$catCount=getCatCount($cid,"3");

	if ($catCount > 2) $bgColor = $minimumsMet;
		else $bgColor = $lessThanMinimum;

	echo "\n\n" . '<p>';
	echo "\n" . '<table border="1" cellpadding="4" cellspacing="1px">';
	echo "\n\t" . '<tr>';
	echo "\n\t\t" . '<td colspan="7" style="background-color:'.$bgColor.';">';
	echo "\n\t\t\t" . '<table width="100%" cellspacing="0" cellpadding="0">';
	echo "\n\t\t\t\t" . '<tr><td id="peertitle" style="text-align:left;'.$titleStyle.'">';
	echo "\n\t\t\t\t\t" . 'Peer (5 recommended, 3 minimum required, '.$catCount.' assigned)';
	echo "\n\t\t\t\t" . '</td><td style="text-align:right;" width="1">';
	if ($weCanAdd) {
		echo "\n\t\t\t\t\t" . '<input type="button" name="btnAddPeer" id="btnAddPeer" value="Add a Peer" onclick="addRater(3);" />';
	} else {
		echo "\n\t\t\t\t\t" . '&nbsp;';
	}
	echo "\n\t\t\t" . '</td></tr></table>';
	echo "\n\t\t" . '</td>';
	echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr>';
	echo "\n\t\t" . '<td colspan="1" width="200px"><small>Name</small></td>';
	echo "\n\t\t" . '<td colspan="1" width="180px"><small>Email</small></td>';
	echo "\n\t\t" . '<td colspan="1" nowrap="nowrap" style="text-align:center;"><small>Survey Sent</small></td>';
	echo "\n\t\t" . '<td colspan="2" width="120px">&nbsp</td>';
	echo "\n\t" . '</tr>';
  $rows=getConsRaters($cid,"3");
	$cnt=0;
  if($rows){
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid=$row[0];
			$exp=("N" != $row[8]) ? "Yes" : '<span style="font-weight:bold;color:crimson;">No</span>';
			$fname = stripslashes($row[3]);
			$lname = stripslashes($row[4]);
			$rname = "$fname $lname";
			$email = stripslashes($row[5]);
			$id = "catid3_rid$rid";
			echo "\n\t" . '<tr>';
			echo "\n\t\t" . '<td colspan="1" id="'.$id.'">';
			echo "\n\t\t\t" . "$fname $lname";
			echo "\n\t\t" . '</td>';
			echo "\n\t\t" . '<td colspan="1">';
			echo "\n\t\t\t" . $row[5];
			echo "\n\t\t" . '</td>';
			echo "\n\t\t" . '<td colspan="1" style="text-align:center;"><small>'.$exp.'</small></td>';
			$js_fname = str_replace("'","\'",stripslashes($row[3]));
			$js_lname = str_replace("'","\'",stripslashes($row[4]));
			$js_email = str_replace("'","\'",stripslashes($row[5]));   
      $js_rname = "$js_fname $js_lname";			
			echo "\n\t\t" . '<td colspan="1"><input type="button" value="Edit" onClick="javascript:editRater(3, '.$rid.', \''.$js_fname.'\', \''.$js_lname.'\', \''.$js_email.'\');" /></td>';
			echo "\n\t\t" . '<td colspan="1"><input type="button" value="Delete" onClick="javascript:deleteRater(3, '.$rid.', \''.$js_rname.'\');" /></td>';
			echo "\n\t" . '</tr>';
		}
	}
	echo "\n" . '</table>';
	echo "\n" . '<p>';

	// ------------------------
	// Direct Report
	// ------------------------
	$catCount=getCatCount($cid,"4");

	if ($catCount > 2) $bgColor = $minimumsMet;
		else $bgColor = $lessThanMinimum;

	echo "\n" . '<table border="1" cellpadding="4" cellspacing="1px">';
	echo "\n\t" . '<tr>';
	echo "\n\t\t" . '<td colspan="7" style="background-color:'.$bgColor.';">';
	echo "\n\t\t\t" . '<table width="100%" cellspacing="0" cellpadding="0">';
	echo "\n\t\t\t\t" . '<tr><td id="directtitle" style="text-align:left;'.$titleStyle.'">';
	echo "\n\t\t\t\t\t" . 'Direct Report (5 recommended, 3 minimum required, '.$catCount.' assigned)';
	echo "\n\t\t\t\t" . '</td><td style="text-align:right;" width="1">';
	if ($weCanAdd) {
		echo "\n\t\t\t\t\t" . '<input type="button" name="btnAddDirect" id="btnAddDirect" value="Add a Direct Report" onclick="addRater(4);" />';
	} else {
		echo "\n\t\t\t\t\t" . '&nbsp;';
	}
	echo "\n\t\t\t" . '</td></tr></table>';
	echo "\n\t\t" . '</td>';
	echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr>';
	echo "\n\t\t" . '<td colspan="1" width="200px"><small>Name</small></td>';
	echo "\n\t\t" . '<td colspan="1" width="180px"><small>Email</small></td>';
	echo "\n\t\t" . '<td colspan="1" nowrap="nowrap" style="text-align:center;"><small>Survey Sent</small></td>';
	echo "\n\t\t" . '<td colspan="2" width="120px">&nbsp</td>';
	echo "\n\t" . '</tr>';
  $rows=getConsRaters($cid,"4");
	$cnt=0;
  if($rows){
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid=$row[0];
			$fname = stripslashes($row[3]);
			$lname = stripslashes($row[4]);
			$rname = "$fname $lname";
			$email = stripslashes($row[5]);
			$exp=("N" != $row[8]) ? "Yes" : '<span style="font-weight:bold;color:crimson;">No</span>';
			$id = "catid4_rid$rid";
			echo "\n\t" . '</tr>';
			echo "\n\t\t" . '<td colspan="1" id="'.$id.'">';
			echo "\n\t\t\t" . $rname;
			echo "\n\t\t" . '</td>';
			echo "\n\t\t" . '<td colspan="1">';
			echo "\n\t\t\t" . $row[5];
			echo "\n\t\t" . '</td>';
			echo "\n\t\t" . '<td colspan="1" style="text-align:center;"><small>'.$exp.'</small></td>';
			$js_fname = str_replace("'","\'",stripslashes($row[3]));
			$js_lname = str_replace("'","\'",stripslashes($row[4]));
			$js_email = str_replace("'","\'",stripslashes($row[5]));   
      $js_rname = "$js_fname $js_lname";    			
      echo "\n\t\t" . '<td colspan="1"><input type="button" value="Edit"   onClick="javascript:editRater(4, '.$rid.', \''.$js_fname.'\', \''.$js_lname.'\', \''.$js_email.'\');" /></td>';
			echo "\n\t\t" . '<td colspan="1"><input type="button" value="Delete" onClick="javascript:deleteRater(4, '.$rid.', \''.$js_rname.'\');" /></td>';
			echo "\n\t" . '</tr>';
		}
  }
  echo "\n" . '</table>';
  echo "\n" . '<p>';
  
  echo <<<EOD

<div id="raterDiv" style="position:absolute;left:-1000px;top:-1000px;visibility:hidden;padding:0px;background-color:#fefefe;border:outset 2px #444444;width:550px;background-color:#fafafa;">
<center>
<h2 id="raterDivTitle" style="margin:0px;background-color:#4f8d97;color:#fdfdfd;padding:4px;">Add a Peer</h2>
<table border="0" cellpadding="4px" style="margin:12px;">
	<tr>
		<td>First Name</td>
		<td>Last Name</td>
		<td>Email</td>
	</tr>
	<tr>
		<td><input type="text" name="fname" id="fname" value="$fname" /></td>
		<td><input type="text" name="lname" id="lname" value="$lname" /></td>
		<td><input type="text" name="email" id="email" value="$email" /></td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:right;">
			<input type="button" name="btnRaterCancel" id="btnRaterCancel" value="Cancel" style="margin-top:8px;" onclick="javascript:cancelRaterDiv();" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" name="btnRaterSubmit" id="btnRaterSubmit" value="Save"   style="margin-top:8px;" onclick="javascript:submitRaterDiv();" />
		</td>
	</tr>
</table>
</center>
</div>

EOD;
  
}

?>

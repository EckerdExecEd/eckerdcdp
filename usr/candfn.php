<?php
// Common functions for the candidate site

@session_start();
require_once "/var/www/html/meta/MenuTree.class.php";

require_once "../meta/candidate.php";

function writeHead($title,$buffered){
    if($buffered){
	ob_start();
    }
  	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"; 
	  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">"; 
    echo "<head>"; 
    echo "<title>$title</title>"; 
    echo "<link href=\"../index_files/styles_backend.css\" rel=\"stylesheet\" type=\"text/css\" />"; 
    echo "</head>";    
}

/*
function writeHead($title,$buffered){
    if($buffered){
	ob_start();
    }
	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title>$title</title>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
	echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">";
	echo "<style>.ivanC10400569321381 {";
	echo "VISIBILITY: hidden; POSITION: absolute";
	echo "}</style></head>";
}
*/

function writeBody($title,$msg){
    $conn=dbConnect();
    // first see if we've completed the survey
    $rs=mysql_query("select * from RATER where RID=" . $_SESSION['cid'] . " and CID=" . $_SESSION['cid'] . " and CATID=1 and EXPIRED='N' and ENDDT is NULL");
    if(mysql_num_rows($rs)) $candidateComplete = false;
    	else $candidateComplete = true;
    	
  echo"<body class=\"internal\"> 
	       <div id=\"wrapper\"> 
		        <div id=\"main\"> 
 			        <div id=\"banner\"> 
                <h5><a href=\"../admin/home.php\">CCD Home</a></h5> 
                <h6>Home of the Conflict Dynamics Profile</h6> 
			        </div><!-- end banner --> 
			        <div id=\"content\">";
echo "\n\n" . '<table width="100%">';
echo "\n\t" . '<tr>';
echo "\n\t\t" . '<td style="vertical-align:top;background-color:#f0f0f0;" width="180">';
$excludeItems = array();
if (empty($_SESSION["conid"])) $excludeItems[] = "Done";
	else $excludeItems[] = "Logout";
if ($candidateComplete) $excludeItems[] = "Take Your Survey";
$data=checkCompletionStatus($_SESSION["cid"]);
if (count($data) > 0) $excludeItems[] = "Add Your Raters";
	else $excludeItems[] = "Manage Your Raters";
$tree = new MenuTree("cand", getRelativePath() . 'menu.txt', array(), $excludeItems);
$tree->show();
//$tree->dump();

	echo "\n\t" . '</td><td style="vertical-align:top;"><center>';
	
	echo "\n\t\t\t" . '<font face="Arial, Helvetica, Sans-Serif"><h2>' . $title . '</h2>' . $msg . '<br>&nbsp;<br>';
  			        
}
/*
function writeBody($title,$msg){

    $conn=dbConnect();
    // first see if we've completed the survey
    $rs=mysql_query("select * from RATER where RID=" . $_SESSION['cid'] . " and CID=" . $_SESSION['cid'] . " and CATID=1 and EXPIRED='N' and ENDDT is NULL");
    if(mysql_num_rows($rs)) $candidateComplete = false;
    	else $candidateComplete = true;

    echo "<body class=\".body\">";
//echo "<br>POST: </b>"; var_dump($_POST);
//echo "<br>SESSION: </b>"; var_dump($_SESSION); echo "<hr>";
    echo "\n\n" . '<table cellspacing="0" cellpadding="0" width="100%" border="0">';
    echo "\n\t" . '<tbody>';
		echo "\n\t" . '<tr valign="top" align="left" height="1">';
    echo "\n\t\t" . '<td colspan="3"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
    echo "\n\t" . '</tr>';
    echo "\n\t" . '<tr valign="top" align="right" bgcolor="#4f8d97" height="22">';
    echo "\n\t\t" . '<td colspan="3"></td>';
    echo "\n\t" . '</tr>';
		echo "\n\t" . '<tr valign="top" align="left" height="1">';
    echo "\n\t\t" . '<td colspan="3"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
    echo "\n\t" . '</tr>';
    echo "\n\t" . '<tr valign="center" height="42" bgcolor="#253355">';
    echo "\n\t\t" . '<td align="left"><img height="64" src="../index_files/eclogotop.gif" width="155" border="0"></td>';
    echo "\n\t\t" . '<td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
    echo "\n\t\t" . '<td align="right"><img height="64" src="../index_files/ldi-title.gif" width="430" border="0"></td>';
    echo "\n\t" . '</tr>';
    echo "\n\t" . '</table>';
    
echo "\n\n" . '<table width="100%">';
echo "\n\t" . '<tr>';
echo "\n\t\t" . '<td style="vertical-align:top;background-color:#f0f0f0;" width="180">';
$excludeItems = array();
if (empty($_SESSION["conid"])) $excludeItems[] = "Done";
	else $excludeItems[] = "Logout";
if ($candidateComplete) $excludeItems[] = "Take Your Survey";
$data=checkCompletionStatus($_SESSION["cid"]);
if (count($data) > 0) $excludeItems[] = "Add Your Raters";
	else $excludeItems[] = "Manage Your Raters";
$tree = new MenuTree("cand", getRelativePath() . 'menu.txt', array(), $excludeItems);
$tree->show();
//$tree->dump();

	echo "\n\t" . '</td><td style="vertical-align:top;"><center>';
	
	echo "\n\t\t\t" . '<font face="Arial, Helvetica, Sans-Serif"><h2>' . $title . '</h2>' . $msg . '<br>&nbsp;<br>';
}
*/
function writeFooter($buffered){
	if($buffered){
		ob_end_flush();
  }
	echo "\n\t\t" . '</td>';
	echo "\n\t" . '</tr>';
	echo "\n" . '</table>';
  echo "</font>"; 
  echo"</div><!-- end content --> 
		  </div><!-- end main -->
		  <div id=\"footer\"> 
      <div id=\"extras\"> 
    <div id=\"copyright\"> 
    <p>Conflict Dynamics Profile is a registered trademark of Eckerd College</p> 
    </div> 
    <!-- end copyright --> 
    <a href=\"http://www.eckerd.edu\"><img alt=\"Eckerd College Home\" class=\"left\" height=\"66\" src=\"../index_files/foot_logo.gif\" title=\"Eckerd College Home\" width=\"207\"/></a> 
    <p>4200 54th Avenue South<br/> 
    St. Petersburg, Florida 33711<br/> 
    888-359-9906 | <a href=\"mailto:cdp@eckerd.edu\">cdp@eckerd.edu</a></p> 
    </div><!-- end extras --> 
 		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>";  
}
/*
function writeFooter($buffered){
	if($buffered){
		ob_end_flush();
  }
	echo "\n\t\t" . '</td>';
	echo "\n\t" . '</tr>';
	echo "\n" . '</table>';
  echo "</font> </body> </html>";
}
*/
function menu($cid){
    $conn=dbConnect();
    // get pid
    $rs=mysql_query("select PID from CANDIDATE where CID=$cid");
		if($rs){
			$row=mysql_fetch_row($rs);
			$pid = $row[0];
		} else {
			$pid = 0;
		}

    echo "\n\n" . '<form name="frm1" method="POST">';
    echo "\n" . '<input type="hidden" name="pid" id="pid" value="'.$pid.'" />';
    echo "<table border=0 cellpadding=5>"; 
    candidateSummary($cid);
    if(empty($_SESSION['conid'])){
		echo '<tr><td onClick="javascript:frm1.action=\'index.php\';frm1.submit();"><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0><small>&nbsp;Log Out</small><td></tr>';
    }
    else{
		echo '<tr><td onClick="javascript:frm1.action=\'../cons/candhome.php\';frm1.submit();"><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0><small>&nbsp;Done</small><td></tr>';
    }
	echo "</table>"; 
	echo "</form>";
}

function showRaterCompletion($cid){
	$data=checkCompletionStatus($cid);
	$cat=array("","Self","Boss","Peer","Direct Report");	
	foreach($data as $row){
		echo "<tr>";
		//CID,CATID,LNAM, FNAM, IFNULL(ENDDT,'Incomplete')
		echo "<td>$row[2], $row[3]</td>";
		echo "<td>".$cat[$row[1]]."</td>";
		echo "<td>$row[4]</td>";
		echo "</tr>";
	}
}

function getRelativePath() {
	/**
	*
	*	This method returns the relative path of this file with reference to the main PHP file.
	*
	**/

	// File Include Path		
	if (strpos(dirname(__FILE__), '/') !== false) {
		// Linux, etc.
		$includePath = explode('/', dirname(__FILE__));
	} else {
		// Windows
		$includePath = explode("\\", dirname(__FILE__));
	}
	if ((count($includePath) > 0) && (strlen($includePath[0]) == 0)) array_shift($includePath);
		
	// File Base Path
	$basePath = explode("/", $_SERVER['SCRIPT_FILENAME']);
	if (count($basePath) > 0) array_pop($basePath);

	if ((count($basePath) > 0) && (strlen($basePath[0]) == 0)) array_shift($basePath);

	while (((count($includePath) > 0) && (count($basePath) > 0)) && ($includePath[0] == $basePath[0])) {
		array_shift($includePath);
		array_shift($basePath);
	}

	$done = false;
	$i = 0;		// To prevent endless loop
	while ((count($includePath) > 0) && (count($basePath) > 0) && !$done && ($i <40)) {
		if ($includePath[0] == $basePath[0]) {
			array_shift($includePath);
			array_shift($basePath);
		} else { 
			$done = true;
		}
		$i++;
	}

	$relativePath = array();
	for ($i=0; $i<count($basePath); $i++) $relativePath[] = "..";
	for ($i=0; $i<count($includePath); $i++) $relativePath[] = $includePath[$i];

	if (count($relativePath) > 0) {
		return implode("/", $relativePath) . "/";
	} else {
		return "";
	}

	return true;
		
}

?>

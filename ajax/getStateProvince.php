<?php
/**-----------------------------------------------------------------------------
 * This code will take an Ajax request from orderForm.php and return an HTML
 * select list according to parameters
-----------------------------------------------------------------------------**/
//Step 1 - Review the POSTed Data
$countryID=(isset($_POST['countryid']))? $_POST['countryid'] : false;
$stateid=(isset($_POST['stateid']))? $_POST['stateid'] : false;
$eleid=(isset($_POST['eleid']))? $_POST['eleid'] : 'state';

  if($countryID){
    include_once "../meta/dbfns.php";
    require_once "../meta/formfns.php";
    $msg=($countryID=="CA")? "SELECT A PROVINCE" : "SELECT A STATE";
    $ctrySelect=selectList( $eleid, fetchArray("select SUBCOUNTRYID,NAME from SUBCOUNTRY where COUNTRYID = '$countryID' AND DISPLAY = 'Y' ORDER BY NAME ASC"), $default,$msg,'id="'.$eleid.'"');
    
    echo $ctrySelect;
  }else{
    echo "Error";
  }
?>

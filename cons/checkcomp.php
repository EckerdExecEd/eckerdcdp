<?php 
$msg="";
session_start();

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$conid=$_SESSION['conid'];
require_once "../meta/candidate.php";
require_once "consfn.php";
$pid=$_POST['pid'];
$tid=$_POST['tid'];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}
$msg="";

writeHead("Conflict Dynamics Profile - Consultants",false);
if ($tid == 1) $crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "Check Completion Status For Program"=>"");
	else $crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "Check Completion Status For Program"=>"");
writeBody("Check Completion Status For Program",$msg,$crumbs);
?>
<form name="frm1" action="listpgm.php" method=POST>
<input type="hidden" name="tid" value="<?=$tid?>">
<table class="sort-table" id="table-1" cellspacing="1">
	<col>
	<col>
	<col>
	<col>
	<thead>
		<tr>
			<td class="head" title="Sort by Rater Name"><a href="#" onclick="return false;"><small>Rater</small></a></td>
			<td class="head" title="Sort by Participant Name"><a href="#" onclick="return false;"><small>Participant</small></a></td>
			<td class="head" title="Sort by Category"><small><a href="#" onclick="return false;">Category</small></a></td>
			<td class="head" title="Sort by Completion Status"><a href="#" onclick="return false;"><small>Completion Status</small></a></td>
		</tr>
	</thead>
	<tbody>
<?
//showCompletion($pid);
showCompletionWithCandidate($pid);
?>
	</tbody>
</table>
</form>

<form name="listfrm" action="pgmdetail.php" method=POST>
<input type="hidden" name="what" value="save">
<input type="hidden" name="pid" value="<?php echo $pid; ?>">
<input type="hidden" name="tid" value="<?php echo $tid; ?>">
<table border=0 cellpadding=5>
	<tr>
		<td onClick="javascript:listfrm.action='listpgm.php';listfrm.submit();"><img src="../images/r.gif" onMouseOver="this.src='../images/b.gif';" onMouseOut="this.src='../images/r.gif';" border=0> &nbsp;Back</td>
	</tr>
</table>
</form>

<?php
menu(array(), array("Back"),"");
?>
<script language="javascript">
var st1 = new SortableTable(document.getElementById("table-1"), ["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"]);
</script>
<?php
writeFooter(false);
?>


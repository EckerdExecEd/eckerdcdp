<?php
include_once "../meta/dbfns.php";
require_once "consfn.php";
require_once "../meta/orderfns.php";
require_once "../meta/table.class.php";
require_once "../meta/orders.class.php";
require_once "../meta/licfns.php";
session_start();
setlocale(LC_MONETARY, 'en_US');

$headertext="<link rel=\"stylesheet\" type=\"text/css\" href=\"../script/js/orderrpts.css\" />\n";

$showPage="H"; //Default to show order history 
$msg=""; 
$log="Order Review Log ".date("d-M-Y H:i:s")."\n";

foreach($_POST as $key=>$val){
  //Show Non tree menu variables
  if(substr($key,0,3)!='tmv')
    $msg.="$key => $val \n";
}

if(empty($_SESSION['conid'])){
    die("Not Logged in. $msg");
}else
  $conid=$_SESSION['conid']; 
//Who is directing the user to this page?
$refPage=$_SERVER['HTTP_REFERER'];
$inRef=strpos($refPage, "192.168.88.122");

$log.="CONID = $conid\n";
$log.="REFERED FROM = $refPage\n";
$log.="POST Variables:\n".$msg;
 
//Check to see if we should show order Detail
if(isset($_REQUEST['show'])&&substr($_REQUEST['show'],0,1)=='D'){
  $showPage="D";
  $ordid=substr($_REQUEST['show'],1);
}
//Check to see if coming back from Eckerd Payment Gateway
if(isset($_POST['ApprovalCode'])&&(isValidFormat($_POST['ApprovalCode']))){
  $log.="Valid Approval Code sent.\n";
  if(!isset($_POST['uniqueId'])||(!isValidOrderId($conid,$_POST['uniqueId']))){
    $msg.="Unable to properly complete Order. Incorrect or no order ID provided for approval code ".$_POST['ApprovalCode'].".";
    $log.="Unable to properly complete Order. Incorrect or no order ID provided for approval code ".$_POST['ApprovalCode'].".\n";
    $showPage="E";
  }else{
    //Yahoo!! We have an approved transaction
    $ordid=$_POST['uniqueId'];
    $approvalCode=$_POST['ApprovalCode'];
    $PaymentId=$_POST['PaymentId'];
    //We have an approval code SO
     // 1 - UPDATE ORDER MASTER
     if(updateOrderMaster(array('ORDID'=>$ordid,'APPROV'=>$approvalCode,'PAYID'=>$PaymentId,'COMPLETETS'=>'NOW'))){
       insertOrderStatus($ordid,'APPROVED');
       //Add Licenses to consultant's account
       $licMsg=insertPurchasedLicenses($ordid,$conid);
       //Gets the con info row
       $cons  = fetchOne('SELECT EMAIL, FNAME, LNAME from CONSULTANT where CONID = ' . $conid,'A');
       //Send email to Order support personnel
       sendGenericMail("cdp@eckerd.edu", //"michaldm@eckerd.edu,rundece@eckerd.edu,hammercl@eckerd.edu,pcheely@discoverylearning.com",
                       //"No-Reply@onlinecdp.org","Order #".$ordid." Approved",
                       "cdp@eckerd.edu","Order #".$ordid." Approved",
                       showOrderDetail($conid,$ordid),
                       "",
                       1,
                       array(),
                       "MIME-Version: 1.0\n"."Content-type: text/html; charset=utf-8\n");
		//Send email to Order support personnel
		sendGenericMail($cons->EMAIL,
			//"No-Reply@onlinecdp.org","Order #".$ordid." Approved",
			"cdp@eckerd.edu","Order #".$ordid." Approved",
			showOrderDetail($conid,$ordid),
			$cons->FNAME . ' ' . $cons->LNAME,
			1,
			array(),
			"MIME-Version: 1.0\n"."Content-type: text/html; charset=utf-8\n");
     }else{
       $log.="Error: Unable to update ORDERMASTER!!\n";
     }
     $showPage="D";     
  }
}

if($inRef===false){
  //If the refering page is not from 192.168.88.122 we want to create a log email
  sendLogEmail("pcheely@discoverylearning.com",
              //"No-Reply@onlinecdp.org","Order Log",
              "cdp@eckerd.edu","Order Log",
                nl2br($log),
                "",
                1,
                array(),
                "MIME-Version: 1.0\n"."Content-type: text/html; charset=utf-8\n");
}

//DISPLAY SECTION BEGIN=========================================================
writeHead("CDP Order History",false,$headertext);

switch($showPage){
  case "D":
    $text="CDP Online Order Detail";
    $crumb="ordersreview.php";  
    if($OK2see=isValidOrderId($_SESSION['conid'],$ordid)){  
      $HTML=renderOrderDetails($ordid);
      $msg="";
    }else
      $HTML="<h3>Invalid Credentials!</h3>";
   break;
  case "E":
    $text="Error Displaying Online Order";
    $crumb="";
    $HTML="<span style=\"color:red;\">$msg</span>";
   break;   
  case "O":
    $crumb="";
    $HTML="";  
   break;
  default:
    $text="Previous CDP Online Orders";
    $crumb="";
    $HTML=showOrderHistory($conid,false);
   break;
}
$crumbs = array("Home"=>"home.php","CDP Online Ordering"=>"orders.php","CDP Order History"=>"$crumb");

writeBody($text,$msg,$crumbs);

$urls=array('home.php');
$txts=array();
menu($urls,$txts,"");
 


echo $HTML;


writeFooter(false);
//DISPLAY SECTION END===========================================================

function showOrderHistory($conid,$limit=5){
  $orders=getLastOrderInfo($conid,$limit);
  $dispCnt=count($orders);
  $cnts=getOrderCount($conid);
  //die(print_r($cnts));
  $total=$cnts['ACTIVE'];
  $HTML ="<center>";
  
  $tbl = new table("700px","","orders");
  $tbl->addRow("", "", "head");
  $hdr="<span style=\"font-size:12px;\">ORDER HISTORY</span><span style=\"float:right;\">($dispCnt of $total)</span>";
  $tbl->rows[0]->addCell($hdr,"h","","","","7");
  
  $hdrs=array("&nbsp;","ORDER #","CREATED","TOTAL","STATUS","STATUS DATE","VIEW DETAILS");  
  $tbl->addRow("","");
  foreach($hdrs as $label){ $tbl->rows[1]->addCell("<b>$label</b>"); }
  $i=1;
  
  if($total < 1){
   $tbl->addRow("","");
   $tbl->rows[2]->addCell("You Currently have no orders.","h","","","","7"); 
  }else{
    foreach($orders as $order){
      $ordID = $order['ORDID'];
      $ordTtl=money_format('%.2n', $order['TOTAL']);
      $createTS=date("m/d/o h:i:s a T",$order['ORDERTS']);
      $ordStatus=$order['STATUS'];
      $statusTS=date("m/d/o h:i:s a T",$order['STATUSTS']);
      $link="<a href=\"ordersreview.php?show=D".$ordID."\" title=\"Review this order\" class=\"detailLnk\">ORDER DETAIL</a>";
      $trclass=(($i%2)!=0)? "" : "alt";
      $tbl->addRow($trclass, "");
      $tbl->rows[($i+1)]->addCell($i,"d","rt");         //Col 1
      $tbl->rows[($i+1)]->addCell($ordID,"d","rt");     //Col 2
      $tbl->rows[($i+1)]->addCell($createTS);           //Col 3
      $tbl->rows[($i+1)]->addCell($ordTtl,"d","rt");    //Col 4
      $tbl->rows[($i+1)]->addCell($ordStatus);          //Col 5
      $tbl->rows[($i+1)]->addCell($statusTS);           //Col 6       
      $tbl->rows[($i+1)]->addCell($link);               //Col 7      
      $i++;
    } 
  }
  /**/          
  $HTML.=$tbl->draw(true)."</center>";
  return $HTML;                
}

function showOrderDetail($conid,$ordid){
$ordObj = new orders;
$ordObj->setConID($conid);
$ordObj->setOrdID($ordid);
$ordObj->getOrdMaster();
$ordObj->getOrdDetails();
$ordObj->getOrderShipTo();
$ordObj->getOrderStatus();

$conName=$ordObj->_conName;
$ordid=$ordObj->_ordid;
//Order Master Data
$subtotal=money_format('%.2n',$ordObj->_ordMaster['SUBTOTAL']);
$shipping=money_format('%.2n',$ordObj->_ordMaster['SHIPPING']);
$handling=money_format('%.2n',$ordObj->_ordMaster['HANDLING']);
$taxes=money_format('%.2n',$ordObj->_ordMaster['TAXES']);
$ordtotal=money_format('%.2n',$ordObj->_ordMaster['TOTAL']);
$orddate=$ordObj->_ordMaster['ORDERTS'];
//Order Details Data
$detailRows="";
$i=1;
foreach($ordObj->_ordDetails as $detail){
  $rowClass=($i%2)? 'ordRowOdd' : 'ordRowEven';
  $detailRows.="<tr class=\"$rowClass\"><td>".$detail['QTY']."</td>";  //QTY
  $detailRows.="<td>".$detail['PRDNAME']."</td>"; //PRDNAME
  $detailRows.="<td align=\"center\">".$detail['TAXED']."</td>"; //TAXED
  $detailRows.="<td align=\"right\">".money_format('%.2n',$detail['PRICE'])."</td>"; //PRICE
  $detailRows.="<td align=\"right\">".money_format('%.2n',($detail['QTY']*$detail['PRICE']))."</td></tr>\n"; //TOTAL
  $i++; 
}
//Order Shipping Data
$upsServices=getUpsServices('A');
$ship2=$ordObj->_ordShip['SHIPTO'];
$addr1=$ordObj->_ordShip['ADDR1'];
$addr2=$ordObj->_ordShip['ADDR2'];
$ctstzp=$ordObj->_ordShip['CITY'].", ".$ordObj->_ordShip['STATE']." ".$ordObj->_ordShip['ZIP'];
$phone=format_phone($ordObj->_ordShip['PHONE']);
$email=$ordObj->_ordShip['EMAIL']; 
$attn=$ordObj->_ordShip['ATTN'];
$service=$upsServices[$ordObj->_ordShip['SHIPSERVCODE']];
//Order Status Data
$ordStatusRows="";
//die(print_r($ordObj->_ordStatusDetails));
if($ordObj->_ordStatusDetails){
  $i=1;
/**/
  foreach($ordObj->_ordStatusDetails as $stat=>$ts){
    $rowClass=($i%2)? 'ordRowOdd' : 'ordRowEven';
    $ordStatusRows.=($ts)?"<tr class=\"$rowClass\"><td>$stat</td><td>$ts</td></tr>" : "";
    $i++;
  }
}

$HTML=<<<EOD
<table class="ordHist">
<tr class="ordHead"><th colspan="2">ORDER REVIEW</th></tr>
<tr class="ordRowNorm"><td>CONSULTANT</td><td>$conName</td></tr>
<tr class="ordRowNorm"><td>ORDER #</td><td>$ordid</td></tr>
<tr class="ordRowNorm"><td>ORDER DATE</td><td>$orddate</td></tr>
<tr><td colspan="2">
	<table class="ordHist">
		<tr class="ordHead"><th colspan="5">ORDER DETAILS</th></tr>
		<tr class="ordHead"><td>QTY</td><td>PRODUCT/SERVICE</td><td>TAXED</td><td>PRICE</td><td>TOTAL</td></tr>
    $detailRows
	</table>
</td></tr>
<tr class="ordRowNorm"><td>SUBTOTAL</td><td style="text-align:right">$subtotal</td></tr>
<tr class="ordRowNorm"><td>SHIPPING</td><td style="text-align:right">$shipping</td></tr>
<tr class="ordRowNorm"><td>HANDLING</td><td style="text-align:right">$handling</td></tr>
<tr class="ordRowNorm"><td>TAXES</td><td style="text-align:right">$taxes</td></tr>
<tr class="ordRowNorm"><td>TOTAL</td><td style="text-align:right">$ordtotal</td></tr>
<tr><td colspan="2">
	<table class="ordHist">
		<tr class="ordHead"><th colspan="5">SHIPPING DETAILS</th></tr>
		<tr class="ordRowNorm"><td>SHIP TO</td><td>$ship2</td></tr>
        <tr class="ordRowNorm"><td>ADDR1</td><td>$addr1</td></tr> 
        <tr class="ordRowNorm"><td>ADDR2</td><td>$addr2</td></tr>         
        <tr class="ordRowNorm"><td>CITY, STATE ZIP</td><td>$ctstzp</td></tr> 
        <tr class="ordRowNorm"><td>PHONE</td><td>$phone</td></tr>  
        <tr class="ordRowNorm"><td>EMAIL</td><td>$email</td></tr>                 
		<tr class="ordRowNorm"><td>ATTN.</td><td>$attn</td></tr> 
		<tr class="ordRowNorm"><td>SHIPPING SERVICE</td><td>$service</td></tr>                             
	</table>
</td></tr> 
<tr><td colspan="2">
	<table class="ordHist">
		<tr class="ordHead"><th colspan="5">ORDER STATUS TRACKING</th></tr>
    $ordStatusRows                    
	</table>
</td></tr>         
</table>
EOD;
return $HTML;
}

function insertPurchasedLicenses($ordid,$conid){
  $ordObj = new orders;
  $ordObj->setConID($conid);
  $ordObj->setOrdID($ordid);
  $ordObj->getOrdDetails();
  //die(print_r($ordObj->_ordDetails));
   
  $msg="";
  foreach($ordObj->_ordDetails as $item){
    if($item['LICREQ']=='Y'){
      //May want to add messaging for this.
      if(($item['QTY']<1)||($item['QTY']==""))
        die(print_r($item));
      if($ok=insertLicense($conid,$item['QTY'],2,$item['INSTR'],"Online Order #$ordid")){
        $msg.=$item['QTY']." ".$item['PRDNAME']." licenses inserted<br>\n";
      }else{
        $msg.="Error: unable to insert ".$item['QTY']." ".$item['PRDNAME']." licenses<br>\n";
        //die($msg);
      }
    }
  }
  return $msg;
}

function isValidFormat($approvalCode){
  //Must be > 0 and < 7 characters
  //Must be alpha-numeric
  if((strlen($approvalCode) > 0)&&(strlen($approvalCode) < 7)&&(ctype_alnum($approvalCode)))
    return true;
  else
    return false;  
}


?>


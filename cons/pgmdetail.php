<?php 
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$pid=$_POST['pid'];
$tid=$_POST['tid'];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}
$msg="";

if("save"==$_POST['what']){
    $pid=$_POST['pid'];
    $descr=addslashes($_POST['descr']);
    $startDt=date('Y-m-d',strtotime($_POST['startdt']));
    $endDt=date('Y-m-d',strtotime($_POST['enddt']));
    $exp=$_POST['exp'];
    $compEmail=($_POST['cemail']);  //added 3/14/2011 JPC
    $pgmData=array($pid,$descr,$startDt,$endDt,$exp,$compEmail);
    if(updatePgm($pgmData)){
	$msg="<font color='#00aa00'>Successfully saved data</font>";
    }
    else{
	$msg="<font color='#aa0000'>Error saving data</font>";
    }
}
writeHead("Conflict Dynamics Profile - Consultant",false);
if ($tid == 1) $crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid", "Edit Program"=>"");
	else $crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid", "Edit Program"=>"");
writeBody("Edit Program",$msg,$crumbs);
?>
<form name="listfrm" action="pgmdetail.php" method=POST>
<input type="hidden" name="what" value="save">
<input type="hidden" name="pid" value="<?=$pid?>">
<input type="hidden" name="tid" value="<?=$tid?>">
<table border=1 cellpadding=5>

<?php
if(!showPgm($pid)){
    echo "<font color='#aa0000'>Error displaying program data</font></br>";
}
?>
<tr><td colspan=2><input type="submit" value="Save Changes"></td></tr>
</table>

<?php
$urls=array('listpgm.php');
$txts=array('Back');
menu($urls,$txts,"listfrm");
?>
</form>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>

<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');
/**
 * There is a lot going on here, we are looking at both the CDP individual and
 * the CDP 360 for scoring purposes. The $_POST['what'] variable tells us what
 * to do with the following options:
 *  
 *    
 **/  
$msg="";
session_start();

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$conid=$_SESSION['conid'];
require_once "../meta/candidate.php";
require_once "../meta/program.php";
require_once "consfn.php";
$pid=$_POST['pid'];
$tid=$_POST['tid'];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}
$msg="";
$what=$_POST['what'];
if('score'==$what){
	if ($tid == 3) {
		// Individual program's are scored immediately
		require_once "../meta/scoreandreportselfonly.php";
		$msg = '<span style="color:green;">' . scoreProgram($pid) . '</span>';
		$okToScore = false;

	} else {
		// All other programs send an email request to the administrator
		//$msg=sendScoreRequestEmail($pid);
		require_once "../meta/score.php";	
		$msg = '<span style="color:green;">' . score360onDemand($conid,$pid,$tid) . '</span>';
		$okToScore = false;
	}
} elseif ('close' == $what) {
	require_once "../meta/program.php";
	$msg = '<span style="color:green;">' . closeProgram($pid) . '</span>';
	$okToScore = false;
}

writeHead("Conflict Dynamics Profile - Consultants",false);
if ($tid == 3) {
	// Self-Only (Individual) Survey
	$crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "Score Program"=>"");
	writeBody("Score Program (ID: $pid)",$msg,$crumbs);
} else {
	// 360 Survey
	$crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "Request Score For Program"=>"");
	writeBody("Request Score For Program (ID: $pid)",$msg,$crumbs);
}

if ($what != "score") {
	$licNeeded =($tid==3)? numSelfOnlyLicensesNeeded($pid) : num360LicensesNeeded($pid);
	$licAvailable = numLicensesAvailable($conid, $tid);
	//echo "<li>Needed / Available = $licNeeded / $licAvailable";

	// -------------------------------------------------------------------------
	// Only display licensing info for self-only (individual) survey
	// because that is scored immediately
	// -------------------------------------------------------------------------
	if (($_POST['what'] != "score") && ($_POST['what'] != "close")) {
//		if ($tid == 3) {
			// Individual (selfonly) survey
			if ($licNeeded == 0) {
				// No licenses needed
				$okToScore = true;
			} else {
				if ($licAvailable < $licNeeded) {
					if ($licAvailable > 0) {
						echo "\n" . '<h4 style="color:crimson;">You need ' . $licNeeded . ($licNeeded > 1 ? ' licenses' : ' license') . ', but only have ' . $licAvailable . ($licAvailable > 1 ? ' licenses' : ' license') . ' available.</h4>';
					} else {
						echo "\n" . '<h4 style="color:crimson;">You need ' . $licNeeded . ($licNeeded > 1 ? ' licenses' : ' license') . ', but have a negative ' . (-$licAvailable) . ($licAvailable > 1 ? ' licenses' : ' license') . ' balance.</h4>';
					}
					$okToScore = false;
				} else {
					echo "\n" . '<h4 style="color:green;">Scoring will consume ' . $licNeeded . ($licNeeded > 1 ? ' licenses' : ' license') . '. (You have ' . $licAvailable . ($licAvailable > 1 ? ' licenses' : ' license') . ' available.)</h4>';
					$okToScore = true;
				}
			}
//		}
	}
}

?>
<form name="frm1" action="listpgm.php" method="POST" style="margin:0px;">
<input type="hidden" name="pid" value="<?=$pid?>">
<input type="hidden" name="tid" value="<?=$tid?>">
<input type="hidden" name="what" value="">
<input type="hidden" name="tmv"  id="tmv"  value="/cons/listpgm.php?^tid=<?=$tid?>">
<table border=1>
	<tr>
		<td style="text-align:center;"><small>Name</small></td>
		<td style="text-align:center;"><small>Category</small></td>
		<td style="text-align:center;"><small>Completion Status</small></td>
<?php
if ($tid == 3) echo "\n\t\t" . '<td style="text-align:center;"><small>License<br />Used</small></td>';
?>
	</tr>
	
<?php

$numComplete = showCompletion($pid, $tid);

?>

	<tr>
		<td colspan="100%">
			<center>
			

<?php
if (($_POST['what'] == "score") || ($_POST['what'] == "close")) {
	echo "\n\t\t\t" . '<input type="submit" value="Go Back" />';
} else {
	if ($tid == 3) {
		// Individual (self-only) report
		if ($okToScore) {
			if ($numComplete == 0) {
				echo "\n\t\t\t" . '<h4 style="color:crimson;">There is nothing to score because no participants are complete.</h4>';
				echo "\n\t\t\t" . '<input type="submit" value="Go Back" />';
				echo "\n\t\t\t" . '<input type="button" value="Close" onClick="javascript:frm1.what.value=\'close\';frm1.action=\'requestscore.php\';frm1.submit();" title="Close the program" />';
			} else {
				echo "\n\t\t\t" . '<input type="submit" value="Go Back" />';
				echo "\n\t\t\t" . '&nbsp;';
				echo "\n\t\t\t" . '<input type="button" value="Score" onClick="javascript:frm1.what.value=\'score\';frm1.action=\'requestscore.php\';frm1.submit();" title="Score the program" />';
			}
		} else {
			echo "\n\t\t\t" . '<input type="submit" value="Go Back" />';
		}
	} else {
		// 360 Report
		echo "\n\t\t\t" . "<input type=\"submit\" value=\"Go Back\" />";
		echo "\n\t\t\t" . '&nbsp;';
		echo "\n\t\t\t" . '<input type="button" value="Score" onClick="javascript:frm1.what.value=\'score\';frm1.action=\'requestscore.php\';frm1.submit();" title="By selecting the score option, you now can score this program." />';
	}
}
?>

			</center>
		</td>
	</tr>
</table>
</form>
<?php
writeFooter(false);
?>

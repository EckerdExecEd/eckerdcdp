<?php
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$pid=$_POST['pid'];
$tid=$_POST['tid'];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}
$msg="";

writeHead("Conflict Dynamics Profile - Consultant",false);
if ($tid == 1) $crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "View/Print Reports"=>"");
	else $crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "View/Print Reports"=>"");
writeBody("View/Print Reports",$msg,$crumbs);
?>
<form name="listfrm" action="pgmdetail.php" method=POST>
<table border=1 cellpadding=5>
<input type="hidden" name="tid" value="<?=$tid?>">
<?php
// Potentuially all reports can be multilingual
listAllReportsML($pid,$tid);

/* This is the old way
if("3"==$tid){
	// Survey(s) with multilingual support
	listAllReportsML($pid,$tid);
}
else{
	// No multilingual support
	listAllReports($pid,$tid);
}
*/
?>
</table>

<?php
$urls=array('listpgm.php');
$txts=array('Back');
menu($urls,$txts,"listfrm");
?>
</form>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>


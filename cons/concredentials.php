<?php
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
require_once "../meta/table.class.php";
require_once "../classes/consultant.class.php";
require_once "consfn.php";
session_start();

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$conid=$_SESSION['conid'];

$con = new consultant;

if("save"==$_POST['what']){
    $pwd=$_POST['pwd'];
    if($chngd=$con->changeConPwd($conid,$pwd)){
	     $msg="<font color='#00aa00'>Successfully changed password</font>";
    }
    else{
	     $msg="<font color='#aa0000'>Error changing password</font>";
    }
}

$headertext= <<<EOT
<link rel="stylesheet" type="text/css" href="../ptl/js/orderrpts.css" />
<script type="text/javascript" src="../meta/jquery/jquery.js"></script>
EOT;

writeHead("Conflict Dynamics Profile - Consultant",false,$headertext);
$crumbs = array("Home"=>"home.php", "Change Password"=>"");
writeBody("Change Password",$msg,$crumbs);
$urls=array('home.php');
$txts=array();
menu($urls,$txts,"");


//Check if Consultant exists
if(!$found=$con->getConsultant($conid)){
  echo "Unable to find Consultant info for CONSULTANT ID $conid";
}else{
$html= <<<EOT
<script language="Javascript">
$(document).ready(function(){

  $( "#svPWD" ).click(function() {
    if(!$('#pwd').val().length){
      alert("Please provide a valid password!");
      return false;
    }
    if($('#pwd').val()!=$('#pwdrt').val()){
      alert("The two passwords don't match!");
      return false;
    }
    $("#what").val("save");
    $("#pwdfrm").submit();
  });


});
</script>
<form id="pwdfrm" name="pwdfrm" action="concredentials.php" method=POST>
<input type="hidden" id="what" name="what" value="">
<input type="hidden" id="conid" name="conid" value="<?=$conid?>">
<table border=1 cellpadding=5>
<tr><td>Type New Password</td><td><input type="text" id="pwd" name="pwd"></td></tr>
<tr><td>Retype New Password</td><td><input type="text" id="pwdrt"  name="pwdrt"></td></tr>
<tr><td colspan=2 align="center"><input type="button" id="svPWD" value="Save Changes"></td></tr>
</table>
<div id="iSM">&nbsp;</div>
</form>
EOT;
echo $html;
}

writeFooter(false);
?>
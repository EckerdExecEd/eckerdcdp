<?php
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$msg="";

$tid=$_POST['tid'];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}

if("archive" == $_POST['what']){
	// Archive a program
  if(chgProgramStatus($_POST['pid'],"Y")){
		$msg="<font color='#00aa00'>Successfully archived program</font>";
  } else {
		$msg="<font color='#aa0000'>Error archiving program</font>";
  }
} elseif("re-open" == $_POST['what']){
	// Re-open a program
	$msg=reopenProgram($_POST['pid']);	
}

writeHead("Conflict Dynamics Profile - Consultant",false);
if ($tid == 1) $crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "Manage Active $testName Programs"=>"");
	else $crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "Manage Active $testName Programs"=>"");
writeBody("Manage Active $testName Programs",$msg,$crumbs);
?>

<!--
<center>
<div style="margin:20px;background-color:#ffff00;width:500px;font:bold 12pt arial;">This page is really cluttered with all of the buttons so I have included a sample display at the bottom of the page that we could use that looks a lot cleaner. What do you think?</div>
</center>
-->


<form name="listfrm" id="listfrm" action="listpgm.php" method=POST>
<input type="hidden" name="what" id="what" value="">
<input type="hidden" name="pid"  id="pid"  value="">
<input type="hidden" name="tid"  id="tid"  value="<?=$tid?>">
<input type="hidden" name="tmv"  id="tmv"  value="/cons/listpgm.php?^tid=<?=$tid?>">
<table border=1 cellpadding=5>

<tr>
<td colspan="100%" align="left">
Search:&nbsp;
<input type="text" name="narrow" value="" maxlength="255">&nbsp;
<input type="submit" value="Refresh View!">
</td>
</tr>

<?php
    $narrow=$_POST['narrow'];
    $conid=$_SESSION['conid'];

//    if (!listPrograms($narrow,"N","listfrm",$conid,$tid)) {
    if (!listProgramsWithDropdown($narrow,"N","listfrm",$conid,$tid)) {
 			echo "<font color='#aa0000'>Error listing programs</font></br>";
    }

?>

</table>
</form>
<?php
/* 4/4/8 removed because we now have a left menu (cdz)
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
*/
?>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>

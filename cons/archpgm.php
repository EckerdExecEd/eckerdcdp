<?php 
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$msg="";

$tid=$_POST['tid'];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}

if("restore"==$_POST['what']){
    if(chgProgramStatus($_POST['pid'],"N")){
	$msg="<font color='#00aa00'>Successfully restored program</font>";
    }
    else{
	$msg="<font color='#aa0000'>Error restoring program</font>";
    }
}
writeHead("Conflict Dynamics Profile - Consultant",false);
if ($tid == 1) $crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "Manage Archived $testName Programs"=>"");
	else $crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "Manage Archived $testName Programs"=>"");
writeBody("Manage Archived $testName Programs",$msg,$crumbs);
?>
<form name="listfrm" action="archpgm.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="pid" value="">
<input type="hidden" name="tid" value="<?=$tid?>">
<table border=1 cellpadding=5>

<tr>
<td colspan=5 align="left">
Search:&nbsp;
<input type="text" name="narrow" value="">&nbsp;
<input type="submit" value="Refresh View!">
</td>
</tr>

<?php
    $narrow=$_POST['narrow'];
    $conid=$_SESSION['conid'];
    if(!listPrograms($narrow,"Y","listfrm",$conid,$tid)){
//    if (!listProgramsWithDropdown($narrow,"Y","listfrm",$conid,$tid)) {
			echo "<font color='#aa0000'>Error listing programs</font></br>";
    }
?>

</table>

<?php
/* 4/9/08 no longer needed because of side menu (cdz)
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"listfrm");
*/
?>
</form>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>

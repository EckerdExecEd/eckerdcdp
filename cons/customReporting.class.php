<?php
class customReporting {
  var $_pid;
  var $_cid;
  var $_conid;
  var $_lid;
  var $_orgid;
  
  function customReporting($pid, $cid, $conid, $orgid, $lid=1){
    $this->setVar('pid',$pid);
    $this->setVar('cid',$cid);
    $this->setVar('conid',$conid);
    $this->setVar('lid',$lid); 
    $this->setVar('orgid',$orgid);     
               
  }
  
  function getRptInclude($pdf){
    // Language switch for custom reporting
    if(($this->_orgid==3)||($this->_orgid==4)){
      switch($this->_lid){
        case "10":    //French
        case "12":    //French-CA        
          include_once "../custom/ICD/frScrAndRptselfonly.php";
          break;  
        case "11":    //German
          include_once "../custom/ICD/deScrAndRptselfonly.php";
          break; 
        case "8" :    //Spanish
        case "9" :    //Portuguese           
        default:      //English 
          include_once "../custom/ICD/scoreandreportselfonly.php";
          break;
      }
      pdf_set_parameter($pdf,"textformat", "utf8");
      pdf_set_parameter($pdf,"FontOutline", "georgia=/usr/share/fonts/georgia.ttf");
      pdf_set_parameter($pdf,"FontOutline", "georgiab=/usr/share/fonts/georgiab.ttf");
      pdf_set_parameter($pdf,"FontOutline", "georgiai=/usr/share/fonts/georgiai.ttf");   
    }else{
      switch($this->_lid){
        case "9" :    //Portuguese
          include_once "../meta/ptScrAndRptselfonly.php";
          break;
        case "10":    //French
        case "12":    //French-CA        
          include_once "../custom/EKD/frScrAndRptselfonly.php";
          //pdf_set_parameter($pdf,"textformat", "utf8");
          pdf_set_parameter($pdf,"FontOutline", "georgia=/usr/share/fonts/arial.ttf");
          pdf_set_parameter($pdf,"FontOutline", "georgiab=/usr/share/fonts/arial.ttf");
          pdf_set_parameter($pdf,"FontOutline", "georgiai=/usr/share/fonts/arial.ttf");   
          break; 
        case "11":    //German
	  
include_once "../custom/ICD/de2ScrAndRptselfonly.php";
pdf_set_parameter($pdf,"textformat", "utf8");
pdf_set_parameter($pdf,"FontOutline", "georgia=/usr/share/fonts/georgia.ttf");
pdf_set_parameter($pdf,"FontOutline", "georgiab=/usr/share/fonts/georgiab.ttf");
pdf_set_parameter($pdf,"FontOutline", "georgiai=/usr/share/fonts/georgiai.ttf");
break;

        case "8": //Spanish
	default:      //English 
          include_once "../meta/scoreandreportselfonly.php";
          break;
      }
     }  
  }
  
  function setVar($id,$val){
    $var="_".$id;
    $this->$var=$val;
  }
  
  function getVar($id){
    $var="_".$id;
    return $this->$var;
  }  
}
?>

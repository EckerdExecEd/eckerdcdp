<?php 
$msg="";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$conid=$_SESSION['conid'];
require_once "../meta/candidate.php";
require_once "consfn.php";
$msg="";

$tid="??????";
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}

if("add"==$_POST['what']){
    $fnam=addslashes($_POST['fnam']);
    $lnam=addslashes($_POST['lnam']);
    $email=addslashes($_POST['email']);
    $conid=$_SESSION['conid'];
	// -- Note: The Program is always 0 for self-only candidates
    $candData=array("0",$fnam,$lnam,$email,"Y","N",$conid);
	$i=candInsert($candData);
    if($i){
		$msg="<font color='#00aa00'>Successfully added $fnam $lnam ($email)<font>";
	}
	else{
		$msg="<font color='#aa0000'>Error adding participant.</font>";
	}
}
elseif("email"==$_POST['what']){
    $cid=addslashes($_POST['cid']);
	$rcp=array();
	$rcp[0]=$cid;
	$email="";
	if(false!=($email=sendCandidateEmail($rcp,"S","",$conid))){
		$msg="<font color='#00aa00'>Email sent to $email</font>";
	}
	else{
		$msg="<font color='#aa0000'>Error sending email to $email</font>";
	}
}

writeHead("Conflict Dynamics Profile - Consultants",false);
$crumbs = array("Home"=>"home.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "Self-Only Participant Management"=>"");
writeBody("Self-Only Participant Management",$msg,$crumbs);
?>
<form name="frm1" action="selfcandhome.php" method=POST>
<input type="hidden" name="cid" value="">
<input type="hidden" name="what" value="">
<input type="hidden" name="conid" value="<?=$conid?>">
<table border=1>
<tr>
<td bgcolor="#dddddd"><small>First Name</small></td><td bgcolor="#dddddd"><small>Last Name</small></td><td bgcolor="#dddddd"><small>Email</small></td>
<td bgcolor="#dddddd" colspan=2><small>Actions</small></td>
</tr>
<tr>
<td><input type="text" name="fnam" value=""></td>
<td><input type="text" name="lnam" value=""></td>
<td><input type="text" name="email" value=""></td>
<td><input type="button" value="Add Self Only Candidate" onClick="javascript:frm1.what.value='add';frm1.submit();"></td>
</tr>
<tr>
<td bgcolor="#dddddd"><small>Name</small></td><td bgcolor="#dddddd"><small>Email</small></td><td bgcolor="#dddddd"><small>PIN</small></td>
<td bgcolor="#dddddd"><small>Status</small></td>
<td bgcolor="#dddddd"><small>Actions</small></td>
</tr>
<?php
listSelfOnlyCandidates($conid,"frm1");
?>
</table>
<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"frm1");
writeFooter(false);
?>


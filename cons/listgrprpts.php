<?php 
require_once "../meta/groups.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$pid=$_POST['pid'];
$tid=$_POST['tid'];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}
$msg="";

writeHead("Conflict Dynamics Profile - Consultant",false);
if ($tid == 1) $crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "View/Print $testName Reports"=>"");
	else $crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "View/Print $testName Reports"=>"");
writeBody("View/Print $testName Reports",$msg,$crumbs);
?>
<form name="listfrm" action="home.php" method=POST>
<table border=1 cellpadding=5>
<?php
	$conid=$_SESSION['conid'];
	echo listGroupReportsByConsultant($conid,"listfrm",$tid); 
?>
</table>

<?php
/* 4/9/08 no longer needed because of side menu (cdz)
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"listfrm");
*/
?>
</form>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>


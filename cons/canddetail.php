<?php 
require_once "../meta/program.php";
require_once "../meta/candidate.php";
require_once "consfn.php";
session_start();

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$msg="Edit participant Information, click 'Save!'";
$cid=$_POST['cid'];
$pid=$_POST['pid'];
$tid=$_POST['tid'];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}
$fnam=addslashes($_POST['fnam']);
$lnam=addslashes($_POST['lnam']);
$email=addslashes($_POST['email']);
$lid="";

$qry="select CONID from PROGCONS where PID = $pid";
$conid = fetchOne($qry,'O');

if("save"==$_POST['what']){
    $cid=$_POST['cid'];
    $pid=$_POST['pid'];
    $fnam=$_POST['fnam'];
    $lnam=$_POST['lnam'];
    $email=$_POST['email'];
	// language support
	$lid=addslashes($_POST['lid']);
	if(strlen($lid)<1){
		$lid="1";	
	}
	
    $candData=array($cid,$fnam,$lnam,$email,$lid);
    if(checkCandEmail($email,$pid,$cid)){
        if(saveCandidate($candData)){
			$msg="<font color='#00aa00'>Successfully saved $fnam $lnam ($email)</font>";
		}
		else{
			$msg="<font color='#aa0000'>Error saving participant info.</font>";
		}
    }
    else{
		$msg="<font color='#aa0000'>Email already exists.</font>";
    }
}

$rs=getCandInfo($cid);
if($rs){
    $fnam=stripslashes($rs[1]);
    $lnam=stripslashes($rs[2]);
    $email=stripslashes($rs[3]);
	// language support
	$lid=stripslashes($rs[10]);
}


writeHead("Conflict Dynamics Profile - Consultant",false);
$crumbs = array("Home"=>"home.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid", "Participant Management"=>"candhome.php?^tid=$tid&^pid=$pid", "Edit Participant"=>"");
writeBody("Edit $testName Participant",$msg, $crumbs);
?>
<form name="edfrm" action="canddetail.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="pid" value="<?=$pid?>">
<input type="hidden" name="cid" value="<?=$cid?>">
<input type="hidden" name="tid" value="<?=$tid?>">
<input type="hidden" name="tmv"  id="tmv"  value="/cons/listpgm.php?^tid=<?=$tid?>">

<table border=1 cellpadding=5>

<tr>
<td align=left>First Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="fnam" value="<?=$fnam?>"></td>
</tr>

<tr>
<td align=left>Last Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="lnam" value="<?=$lnam?>"></td>
</tr>

<tr>
<td align=left>Email<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="email" value="<?=$email?>"></td>
</tr>

<tr>
<td align=left>Language<font color="#ff0000">*</font></td>
<td align=left><?=conLanguageBox($tid,$conid,$lid);?></td>
</tr>

<tr>
<td colspan=2 align="left">
<input type="button" onClick="javascript:chkForm(edfrm);" value="Save!">
</td>
</tr>

</table>
<small>Mandatory fields are marked </small><font color="#ff0000">*</font>.
<?php
$urls=array('listpgm.php');
$txts=array('Back');
menu($urls,$txts,"edfrm");
?>
</form>
<script language="Javascript">
function chkForm(frm){
    if(frm.fnam.value.length<1||frm.lnam.value.length<1||frm.email.value.length<1){
	alert("Please provide all mandatory fields!");
    }
    else{
	frm.what.value='save';
        frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>

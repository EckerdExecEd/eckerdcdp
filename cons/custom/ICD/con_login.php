<?php
function loginPg($msg,$tempid=4){
global $systemON, $systemMSG;
$systemON=true;
$surl="home.php";
$Xuid=(isset($_GET["uid"]))? trim(htmlspecialchars($_GET["uid"])) : "";
$HTML=(!$systemON)? $systemMSG : "Please enter email address:
<br>
<input type=\"text\" name=\"uid\" value=\"$Xuid\">
<br>
Password:
<br>
<input type=\"password\" value=\"\" name=\"pwd\">
<br>
<input type=\"submit\" value=\"Log In\">";
$html = <<<EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<HTML xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
  <TITLE>Institute for Conflict Dynamics</TITLE>
  <META content="text/html; charset=utf-8" http-equiv=Content-Type>
  <LINK rel=stylesheet type=text/css href="custom/ICD/styles_backend.css">
<BODY>
  <DIV id=wrapper>
    <DIV id=main>
      <DIV id=banner>
        <!-- <H5  ><A href="#" id="headerlogo" >Home</A></H5> -->
        <!-- <DIV id=features>
          <IMG class=right title="Name of Organization" alt="Name of Organization" src="topright.png" width=306 height=178>
        </DIV>--> <!-- end features -->
      </DIV><!-- end banner -->
      <DIV id=content>
        <div id="left">
          <h1>CCC Login<span style="font-size:60%;">&nbsp;</span></h1> 
        <p>
        <div>$msg</div>
        <form name="frm1" action="$surl" method="post">
        <input type="hidden" name="s" value="1">
        <input type="hidden" name="tempid" value="$tempid">        
        $HTML
        </form></p>
    </div><!-- end left -->
        <DIV id=right class="right"><!--
          <DIV class=feature>
          <H3>Customer Service</H3>
          <P>Monday - Friday<BR>8:00 am - 4:00 pm</P>
          <P>123-456-7890<BR><A href=""></A></P>
          </DIV>--><!-- .features -->
        </DIV><!-- end right -->
      </DIV><!-- end content -->
    </DIV><!-- end main -->
    <DIV id=footer>
      <DIV id=extras>
        <DIV id=copyright>
        <P>Authorized exploitation of the English edition © 1999 Center <br />
           for Conflict Dynamics / USA for this edition, published and<br />
           sold by permission of Center for Conflict Dynamics, the <br />
           owner of all rights to publish and sell the name.</P></DIV><!-- end copyright -->
          <!--<A href="">
            <IMG class=left title="Name of Organization Home" alt="Name of Organization Home" src="foot_logo.gif" width=207 height=66>
          </A>--> 
          <P>© Institute for Conflict Dynamics Europe GmbH<br />
          Kurfürstendamm 184<br />
          10707 Berlin, Germany;<br />
          Contact : <a href="mailto:service@conflict-institute.com?Subject=Assessment%20Center">service@conflict-institute.com</a><br />
          Info : <a href="www.conflict-institute.com" target="_blank">www.conflict-institute.com</a></P>
      </DIV><!-- end extras -->
    </DIV><!-- end footer -->
  </DIV><!-- end wrapper -->
</BODY>
</HTML>
EOD;

/*$html = "<h1>Hello World</h1>";  */
return $html;
}
?>

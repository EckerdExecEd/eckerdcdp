<?php
/*=============================================================================*
* 04-22-2005 TRM: Added support for "Self Only" licensing.
*
*=============================================================================*/
$msg="";
session_start();
require_once("consfn.php");
if("1"==$_POST['s']){
    // if we're here it's a login request
    require_once "../meta/dbfns.php";
    $uid=$_POST['uid'];
    $uid=strip_tags($uid);
    
    $pwd=$_POST['pwd'];  
    $tempid=(isset($_POST['tempid'])&& $_POST['tempid']!="")? $_POST['tempid'] : 1;    
    /*$conn=dbConnect();
    // removed 'quote_smart' function call wrapped around email value. not needed. wbs 2/7/2007
    $query=sprintf("select a.CONID,FNAME,LNAME,ORGID from CONSULTANT a, CONS b where a.CONID=b.CONID and UPPER(EMAIL)='%s' and PWD=PASSWORD('%s')",
        mysql_real_escape_string(strtoupper($uid)),
        mysql_real_escape_string($pwd));
    $rs=mysql_query($query);
    // wbs 12/7/2006
    //$rs=mysql_query("select a.CONID,FNAME,LNAME from CONSULTANT a, CONS b where a.CONID=b.CONID and EMAIL=".quote_smart($uid)." and PWD=PASSWORD('".$pwd."') and ACTIVE='Y'");
    if(!($row=mysql_fetch_row($rs))){
			// Login error - go back to the login page
	    $rs=mysql_query("select a.CONID,FNAME,LNAME,ORGID from CONSULTANT a, CONS b where a.CONID=b.CONID and UPPER(EMAIL)='".strtoupper($uid)."' and ACTIVE='Y'");
	    if(!($row=mysql_fetch_row($rs))){ 
				header("Location: index.php?lerr=1&uid=$uid&tempid=$tempid");
	    } else {
				header("Location: index.php?lerr=2&uid=$uid&tempid=$tempid");
		}
    }
    else{
		$_SESSION['conid']=$row[0];
		$_SESSION['consname']=$row[1]." ".$row[2];
		$_SESSION['tempid']= $row[3]; 
    }*/
    $mysqli=dbiConnect();
    if (!($query = $mysqli->prepare("select a.CONID,FNAME,LNAME,ORGID from CONSULTANT a, CONS b where a.CONID=b.CONID and UPPER(EMAIL)=? and PWD=PASSWORD(?)"))) {
    	echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    if (!$query->bind_param("ss", strtoupper($uid), $pwd)) {
    	echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    if (!$query->execute()) {
    	echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    if (!$query->bind_result($out_CONID, $out_FNAME, $out_LNAME, $out_ORGID)) {
    	echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    $row = $query->fetch();
    $query->close();
    if(!($row)){
    	if (!($query2 = $mysqli->prepare("select a.CONID,FNAME,LNAME,ORGID from CONSULTANT a, CONS b where a.CONID=b.CONID and UPPER(EMAIL)=? and ACTIVE='Y'"))) {
    		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
    	}
    	if (!$query2->bind_param("s", strtoupper($uid))) {
    		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    	}
    	if (!$query2->execute()) {
    		echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
    	}
    	if (!$query2->bind_result($out_CONID2, $out_FNAME2, $out_LNAME2, $out_ORGID2)) {
    		echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    	}
    	$row2 = $query2->fetch();
    	$query2->close();
    	if (!$row2) {
    		header("Location: index.php?lerr=1&uid=$uid&tempid=$tempid");
    	} else {
    		header("Location: index.php?lerr=2&uid=$uid&tempid=$tempid");
    	}
    }else{
    	$_SESSION['conid']=$out_CONID;
    	$_SESSION['consname']=$out_FNAME." ".$out_LNAME;
    	$_SESSION['tempid']=$out_ORGID;
    }
}else{
    $orgid=getOrgID($_SESSION['conid']);
    $_SESSION['tempid']=((isset($orgid))&&($orgid!=""))?   $orgid : 1;       
}

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

// ???? This is for testing and debugging MenuTree class - CDZ (3/31/2008)
// conids: thor@presenstech.com 7790432
//         kiwi@presensit.com   4212078
if ($_SESSION['conid'] == 4212078) {
	$_SESSION["MenuTreeTest"] = true;
} else {
	$_SESSION["MenuTreeTest"] = false;
}

$msg="Welcome ".$_SESSION['consname']."<br>";

writeHead("Conflict Dynamics Profile - Consultants",false);
$crumbs = array("Home"=>"");
writeBody("Consultant Home",$msg,$crumbs);
?>
<table border=1>
<tr><td> CDP 360 </td><td> CDP Individual </td></tr>

<tr>
<td valign=top>
<?php
$urls=array('addpgm.php','listpgm.php','archpgm.php','../upload/upload1.php','listgrprpts.php','');
$txts=array('Add New Program','Manage Active Programs','Archived Programs','Upload Scanned Surveys','View Group Reports','');
echo "<form name='frm1' method=POST>\n";
echo "<input type=hidden name='tid' value='1'>\n";
menu($urls,$txts,"frm1");
echo "</form>\n";
?>
</td>
<td valign=top>
<?php
$urls=array('addpgm.php','listpgm.php','archpgm.php','');
$txts=array('Add New Program','Manage Active Programs','Archived Programs','');
echo "<form name='frm2' method=POST>\n";
echo "<input type=hidden name='tid' value='3'>\n";
menu($urls,$txts,"frm2");
echo "</form>\n";
?>
</td>
</tr>

<tr>
<td colspan=2>
Common functions
</td>
</tr>
<tr>
<td colspan=2>
<?php
// The log out works different for cons and impersonating admin
if(empty($_SESSION['admid'])){
    $urls=array('lichist.php','concredentials.php','index.php');
    $txts=array('License History','Change Password','Log out');
}
else{
    $urls=array('lichist.php','concredentials.php','../admin/home.php');
    $txts=array('License History','Change Password','Done');
}
echo "<form name='frm3' method=POST>";
menu($urls,$txts,"frm3");
echo "</form>";
?>
</td>
</tr>
</table>

<?php
writeFooter(false);
?>


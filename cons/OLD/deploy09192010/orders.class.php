<?php
setlocale(LC_MONETARY, 'en_US');

  class orders {
    var $_conid;
    
    function setConID($conid){
      if($conData=$this->getConInfo($conid)){
        //die(print_r($conData));
        $this->_conid=$conData[0]['CONID'];
        $this->_conName=$conData[0]['FNAME']." ".$conData[0]['LNAME'];
        $this->_conEmail=$conData[0]['EMAIL'];
        $this->_conTaxExempt=$conData[0]['TAXEXEMPT'];
        $this->_conState=$conData[0]['STATE'];
      }else{
        $this->_conid="";
      }
    }
    
    function setOrdID($ordid){
      
      $this->_ordid=$ordid;
    }
    
    function getConOrders($limit=false,$type=false,$orderby=false){
      if((!isset($this->_conid))||($this->_conid=="")){
        return false;
      }
      
      $sql="SELECT mst1.CONID,mst1.TOTAL,UNIX_TIMESTAMP(mst1.ORDERTS) AS ORDERTS,st1.ORDID, st1.STATUS, st1.STATUSTS
              FROM ORDERMASTER mst1, ORDERSTATUS st1
              JOIN (SELECT st2.ORDID, MAX(st2.STATUSTS) AS LASTSTATUS
                      FROM ORDERSTATUS st2
                    GROUP BY st2.ORDID ) AS stM
              ON st1.ORDID = stM.ORDID AND st1.STATUSTS = stM.LASTSTATUS
             WHERE mst1.ORDID = st1.ORDID 
             AND mst1.CONID = ".$this->_conid." ";
      $sql.=($type)? " AND st1.STATUS IN (".$type.") " : "";
      $sql.=($orderby)? "ORDER BY ".$orderby : "ORDER BY st1.ORDID asc"; 
      $sql.=($limit)? " LIMIT 0,$limit" : "";  
      //die($sql); 
      $res=fetchArray($sql);       
    }
    
    function getConInfo($conid){
      $sql="select CONID,FNAME,LNAME,EMAIL,STATE,TAXEXEMPT from CONSULTANT where CONID=$conid";
      $res=fetchArray($sql);
      //die(print_r($res));
      if(count($res)<1){
        return false;
      }else               
        return $res;
    }
    
   
    function getOrdCon($ordid){
      $sql="select CONID from ORDERMASTER where ORDID = $ordid";
      $res=fetchOne($sql);
      //die($sql);
      return $res;
    }
    
    function getOrdMaster(){
      $ordid=$this->_ordid;
      $conid=$this->_conid;
      $sql="SELECT ORDID, SUBTOTAL , TAXES , TAXRATE , TAXSTATE, TAXEIN, SHIPPING , HANDLING , TOTAL , 
                   DATE_FORMAT(ORDERTS,'%m/%d/%Y %h:%i%p') AS ORDERTS , PAYID , APPROV , 
                   DATE_FORMAT(COMPLETETS,'%m/%d/%Y %h:%i%p') AS COMPLETETS  
              FROM ORDERMASTER 
             WHERE ORDID = $ordid AND CONID = $conid";
      $res=fetchArray($sql);
      if(count($res[0])==13){
        $this->_ordMaster=array();
        $this->_ordMaster=$res[0];
      }else{
        $this->_ordMaster=false;
      }
    }
    
    function getOrdDetails(){
      $ordid=$this->_ordid;
      $sql="SELECT a.DETAILID, a.ORDID, a.PRDKEY, b.PRDID, b.PRDNAME, b.WEIGHT, b.TAXABLE, a.TAXED, a.QTY, a.PRICE, b.LICREQ, b.INSTR
              FROM ORDERDETAIL a, CDPPRODUCTS b
             WHERE a.PRDKEY = b.PRDKEY
               AND a.ORDID = $ordid
          ORDER BY a.SEQUENCE ASC";
      $res=fetchArray($sql);
     
      if($res>0){
         $this->_ordDetails=array();
         foreach($res as $detail)
          $this->_ordDetails[]=$detail; 
      }else{
        $this->_ordDetails=false;
      }
    }

    function getOrderShipTo(){
      $orderid=$this->_ordid;
      $sql="SELECT SHIPID,ORDID,SHIPTO,ADDR1,ADDR2,CITY,STATE,ZIP,COUNTRY,PHONE,EMAIL,SHIPSERVCODE,ATTN,QTY,WEIGHT 
              FROM ORDERSHIP 
             WHERE ORDID = $orderid";
      $res=fetchArray($sql);
      if(count($res[0])==15){
        $this->_ordShip=array();
        $this->_ordShip=$res[0];
      }else
        $this->_ordShip=false;
    }

    function getOrderStatus(){
      $orderid=$this->_ordid;
      $this->_ordStatusDetails=array('CREATED'=>false,'APPROVED'=>false,'SHIPPED'=>false,'CANCELLED'=>false); 
      $sql="SELECT STATUS, DATE_FORMAT(STATUSTS,'%m/%d/%Y %h:%i%p') AS STATUSTS
              FROM ORDERSTATUS
             WHERE ORDID = $orderid
             ORDER BY STATUSTS ASC";
      //die($sql);
      $res=fetchArray($sql);
      //die(print_r($res));
      if(count($res[0])==2){
        foreach($res as $status){
          $this->_ordStatusDetails[$status['STATUS']]=$status['STATUSTS']; 
        }
      }else
        $this->_ordStatusDetails=false;
    }
    
    function getOrderNotes($publicOnly=false,$activeOnly=true){
      $orderid=$this->_ordid;
      $sql="SELECT NOTEID, ORDID, NOTE, CREATEDBY, DATE_FORMAT(CREATEDDT,'%m/%d/%Y %h:%i %p') AS CREATEDDT, PUBLIC, ACTIVE
              FROM ORDERNOTES 
             WHERE ORDID = $orderid";
      $sql.=($publicOnly)? " AND PUBLIC='Y'" : "";
      $sql.=($activeOnly)? " AND ACTIVE='Y'" : "";
      $sql.=" ORDER BY CREATEDDT Asc";
      //die($sql);
      $res=fetchArray($sql);
      if(count($res[0])==7){       
        $this->_ordNotes=$res;
      }else
        $this->_ordNotes=false;
              
    }
  }

function buildWhere($params){
  $ordid=false;
  $bdate=false;
  $edate=false;
  $whr=false;
  foreach($params as $vars){
    $var=explode("^",$vars);
    ${$var[0]}=$var[1];    
  }
  if($ordid){
   $whr="where ORDID = $ordid";
   return $whr;
  }
  if(($bdate)&&(!$edate)){
   $dt=explode("/",$bdate);
   $fdate=mktime(0, 0, 0,$dt[0],$dt[1],$dt[2]);
   $tdate=mktime(23, 59, 59,$dt[0],$dt[1],$dt[2]);   
   $whr="where ORDERTS > $fdate AND ORDERTS <  $tdate"; 
   return $whr; 
  }
  if(($bdate)&&($edate)){
    $dt=explode("/",$bdate);  
    $fdate=mktime(0, 0, 0,$dt[0],$dt[1],$dt[2]);
    $dt2=explode("/",$edate);     
    $tdate=mktime(23, 59, 59,$dt2[0],$dt2[1],$dt2[2]);
    $whr="where ORDERTS > $fdate AND ORDERTS < $tdate"; 
    return $whr; 
  }
  
  return $whr;  
}

function renderSearchResult($params){
  
  $srchWhr=buildWhere($params);
  $qry="SELECT ORDID, CONID, FNAME, LNAME, TOTAL, FROM_UNIXTIME(ORDERTS,'%m/%d/%Y %h:%i:%s %p') as ORDERTS, LASTSTATUS, DATE_FORMAT(STATUSTS,'%m/%d/%Y %h:%i:%s %p') as STATUSTS FROM ORDERINFO  ".$srchWhr;
  $res=fetchArray($qry);
  //die( "Chk 1" );
  if(count($res[0])==8){
    $headers=array("Order #","Consultant","Amount","Ordered","Status","&nbsp;");
    $HTML="";
    $table = new table("600px","","orders");
    $table->addRow("", "", "head");
    $table->rows[0]->addCell("", "h");
    foreach($headers as $h){ $table->rows[0]->addCell($h, "h"); }
    $i=1;
    foreach($res as $o){
      $trclass=(($i%2)!=0)? "" : "alt";
      $link="<a href=\"orderssearch.php?detail=D".$o['ORDID']."\" class=\"detailLnk\">View Details</a>";
      $table->addRow($trclass, "");
      $table->rows[$i]->addCell("$i");
      $table->rows[$i]->addCell($o['ORDID'],"d","rt");
      $table->rows[$i]->addCell($o['FNAME']." ".$o['LNAME']);
      $table->rows[$i]->addCell(money_format('%.2n',$o['TOTAL']),"d","rt");
      $table->rows[$i]->addCell($o['ORDERTS']);
      $table->rows[$i]->addCell($o['LASTSTATUS']);
      $table->rows[$i]->addCell($link);
      $i++;                                          
    }
     
    $HTML.=$table->draw(true);
    return $HTML;
  }else{
    return false;
  }
}

function renderOrderDetails($ordid,$refPg=false,$retTbl=false){
  $ordObj = new orders;
  $conid=$ordObj->getOrdCon($ordid);
  $ordObj->setConID($conid);
  $ordObj->setOrdID($ordid);
  $ordObj->getOrdMaster();
  $ordObj->getOrdDetails();
  $ordObj->getOrderShipTo();
  $ordObj->getOrderStatus();
  $publicOnly=($refPg)? false : true;
  $ordObj->getOrderNotes($publicOnly);
  
  $conName=$ordObj->_conName;
  //Order Master Data    
  $orddate=$ordObj->_ordMaster['ORDERTS'];
  $ordtaxstate=$ordObj->_ordMaster['TAXSTATE'];
  $ordtaxein=$ordObj->_ordMaster['TAXEIN']; 
  $oC=array(); //Order Charges
  $oC['SUBTOTAL']=money_format('%.2n',$ordObj->_ordMaster['SUBTOTAL']);
  $oC['SHIPPING']=money_format('%.2n',$ordObj->_ordMaster['SHIPPING']);
  $oC['HANDLING']=money_format('%.2n',$ordObj->_ordMaster['HANDLING']);
  $oC['TAXES']=money_format('%.2n',$ordObj->_ordMaster['TAXES']);
  $oC['TOTAL']=money_format('%.2n',$ordObj->_ordMaster['TOTAL']);
   
  /**/ 
  $r=0;    
  $tbl = new table("600px","","orders");
  $tbl->addRow("", "", "head");
  $tbl->rows[$r]->addCell("ORDER REVIEW","h","","","","2");
  $tbl->addRow("", ""); $r++;
  $tbl->rows[$r]->addCell("CONSULTANT");
  $tbl->rows[$r]->addCell($conName);
  $tbl->addRow("", ""); $r++;
  $tbl->rows[$r]->addCell("ORDER #");
  $tbl->rows[$r]->addCell($ordid);  
  $tbl->addRow("", ""); $r++;
  $tbl->rows[$r]->addCell("ORDER DATE");
  $tbl->rows[$r]->addCell($orddate);
  if($refPg){
    $tbl->addRow("", ""); $r++;
    $tbl->rows[$r]->addCell("ORDER STATE");
    $tbl->rows[$r]->addCell($ordtaxstate);  
    $tbl->addRow("", ""); $r++;
    $tbl->rows[$r]->addCell("TAX EIN");
    $tbl->rows[$r]->addCell($ordtaxein);   
  }   
  $tableA=$tbl->draw(true); 
  if($retTbl=="A")
    return $tableA;
    
  $tbl = new table("600px","","orders");
  $tbl->addRow("", "", "head");
  $tbl->rows[0]->addCell("ORDER DETAILS","h","","","","5");
  $tbl->addRow("", "");
  $headers=array("QTY","PRODUCT/SERVICE","TAXED","PRICE","TOTAL");        
  foreach($headers as $h){ $tbl->rows[1]->addCell("<b>$h</b>","d","ct"); }
  $i=1;
  foreach($ordObj->_ordDetails as $o){
    $trclass=(($i%2)!=0)? "" : "alt";
    $tbl->addRow($trclass, "");
    $tbl->rows[($i+1)]->addCell($o['QTY'],"d","rt");
    $tbl->rows[($i+1)]->addCell($o['PRDNAME']);
    $tbl->rows[($i+1)]->addCell($o['TAXED'],"d","ct");
    $tbl->rows[($i+1)]->addCell(money_format('%.2n',$o['PRICE']),"d","rt");
    $tbl->rows[($i+1)]->addCell(money_format('%.2n',($o['QTY']*$o['PRICE'])),"d","rt");
    $i++;
  }
  $tableB=$tbl->draw(true);
  
  $tbl = new table("600px","","orders");
  $i=0;
  foreach($oC as $label=>$amt){
    $tbl->addRow("", "");
    $tbl->rows[$i]->addCell($label);
    $tbl->rows[$i]->addCell($amt,"d","rt");    
    $i++;
  }
  $tableC=$tbl->draw(true);
  
  $upsServices=getUpsServices('A');
  $oS=array();
  $oS['SHIP TO']=$ordObj->_ordShip['SHIPTO'];
  $oS['ADDR1']=$ordObj->_ordShip['ADDR1'];
  $oS['ADDR2']=$ordObj->_ordShip['ADDR2'];
  $oS['CITY, STATE ZIP']=$ordObj->_ordShip['CITY'].", ".$ordObj->_ordShip['STATE']." ".$ordObj->_ordShip['ZIP'];
  $oS['PHONE']=format_phone($ordObj->_ordShip['PHONE']);
  $oS['EMAIL']=$ordObj->_ordShip['EMAIL'];
  $oS['ATTN.']=$ordObj->_ordShip['ATTN'];
  $oS['SHIPPING SERVICE']=$service=$upsServices[$ordObj->_ordShip['SHIPSERVCODE']];

  $tbl = new table("600px","","orders");
  $tbl->addRow("", "", "head");
  $tbl->rows[0]->addCell("SHIPPING DETAILS","h","","","","2"); 
  $i=1; 
  foreach($oS as $label=>$data){
    $tbl->addRow("","");
    $tbl->rows[$i]->addCell($label);
    $tbl->rows[$i]->addCell($data);    
    $i++;
  }
  $tableD=$tbl->draw(true);

  $tbl = new table("600px","","orders");
  $tbl->addRow("", "", "head");
  $tbl->rows[0]->addCell("ORDER STATUS TRACKING","h","","","","2");

  if($ordObj->_ordStatusDetails){
    $i=1;

    foreach($ordObj->_ordStatusDetails as $stat=>$ts){
      $trclass=(($i%2)!=0)? "" : "alt";
      if($ts){
        $tbl->addRow($trclass, "");      
        $tbl->rows[$i]->addCell($stat);
        $tbl->rows[$i]->addCell($ts);      
        $i++;
      }
    }
  }
  $tableE=$tbl->draw(true);
  
  if($retTbl=="E")
    return $tableE;
 
  if($ordObj->_ordNotes){
    $tbl = new table("600px","","orders");
    $tbl->addRow("", "", "head");
    $tbl->rows[0]->addCell("ORDER NOTES","h","","","","2");
    $i=1;
    foreach($ordObj->_ordNotes as $notes){
      $trclass=(($i%2)!=0)? "" : "alt";
      $tbl->addRow($trclass, "");
      $tbl->rows[$i]->addCell($i);
      $note=$notes['NOTE'].' - ( '.$notes['CREATEDBY'].' '.$notes['CREATEDDT'].')';
      $tbl->rows[$i]->addCell($note);
      $i++;
    } 
    $tableF=$tbl->draw(true);    
  }else
    $tableF="";
    
  $tableG="";
  if($refPg){
    $tbl = new table("600px","","orders");
    $tbl->addRow("", "", "head");
    $tbl->rows[0]->addCell("ADMIN OPTIONS","h","","","","3");
    $tbl->addRow("", "");
    $linkA="<a href=\"$refPg\" class=\"detailLnk\">Return to Order Search</a>";
    $tbl->rows[1]->addCell($linkA,"d","ct"); 
    $linkB="<a href=\"orderstatus.php?status=O".$ordid."\" class=\"detailLnk\">Update Status</a>";
    $tbl->rows[1]->addCell($linkB,"d","ct");
    $linkC="<a href=\"ordernotes.php?notes=N".$ordid."\" class=\"detailLnk\">Add Note</a>";
    $tbl->rows[1]->addCell($linkC,"d","ct"); 
    $tableG=$tbl->draw(true);            
  }
  
   
  return $tableA.$tableB.$tableC.$tableD.$tableE.$tableF.$tableG;     
}

function getAvailableOrderStatus($ordid,$name){ 
      $noMore=array('CANCELLED','FAILED');
      $statusTypes=array('SHIPPED'=>false,'CANCELLED'=>false,'FAILED'=>false); 
      $sql="SELECT STATUS, DATE_FORMAT(STATUSTS,'%m/%d/%Y %h:%i%p') AS STATUSTS
              FROM ORDERSTATUS
             WHERE ORDID = $ordid
             ORDER BY STATUSTS ASC";
      //die($sql);
      $res=fetchArray($sql);
      if(count($res[0])==2){
        foreach($res as $status){
          if(in_array($status['STATUS'],$noMore))
            return false;
            
          if (array_key_exists($status['STATUS'], $statusTypes))
              $statusTypes[$status['STATUS']]=$status['STATUSTS']; 
        }
      }else
        return false;
      
      $valpairs=array();
      
      foreach($statusTypes as $k=>$v){
        if(!$v)
          $valpairs[]=array('statID'=>$k,'statDESCR'=>$k); 
      }

      if(count($valpairs)>0){
        $statusSelect=selectList($name,$valpairs,"",'Select a Status',"id=\"$name\""); 
        return $statusSelect;
      }else
        return false;
}

function format_phone($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);
 
	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
	else
		return $phone;
} 
 
?>
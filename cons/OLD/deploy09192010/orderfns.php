<?php
/**=============================================================================
 * Contains functions specific to the shopping cart and order review functionality
 * 
=============================================================================**/

function insertOrderMaster($params){
/**
 * $params array:
 *    'conid'=>$conid,
 *    'subtotal'=>$subtotal,
 *    'taxes'=>$taxes,
 *    'taxrate'=>$taxrate,
 *    'taxstate'=>$taxstate,
 *    'taxein'=>$taxein,                     
 *    'shipping'=>$shipping,
 *    'handling'=>$handling,
 *    'total'=>$total  
 **/  
  //SQL for insert
  $taxein=(strlen($params['taxein']))? "'".$params['taxein']."'" : "NULL";
  $sql="INSERT INTO ORDERMASTER (CONID, SUBTOTAL, TAXES, TAXRATE, TAXSTATE, TAXEIN, SHIPPING, HANDLING, TOTAL, ORDERTS, PAYID, APPROV, COMPLETETS) VALUES
        (".$params['conid'].",
         ".$params['subtotal'].", 
         ".$params['taxes'].", 
         ".$params['taxrate'].",
         '".$params['taxstate']."',
         $taxein, 
         ".$params['shipping'].", 
         ".$params['handling'].", 
         ".$params['total'].", 
         now(), 
         NULL, 
         NULL, 
         NULL)";
  //die($sql);
  if(!$ordid=insertAutoNumRecord($sql)){
    return false;
  }else{
    insertOrderStatus($ordid,"CREATED");
    return $ordid;
  }        
}

function updateOrderMaster($params){
/**
 * params array:
 *        'ORDID'=>$canOrdID,
 *        'APPROV'=>'CANCELLED',
 *        'COMPLETETS'=>'NOW'
 **/  
  //die(print_r($params));
  if(!isset($params['ORDID'])||$params['ORDID']=="")
    return false;
   
  $sql ="UPDATE ORDERMASTER SET ";
  
  $sql.=((isset($params['PAYID']))&&$params['PAYID']!="")? "PAYID='".$params['PAYID']."',": "";
  $sql.=((isset($params['APPROV']))&&$params['APPROV']!="")? "APPROV='".$params['APPROV']."',": ""; 
  $sql.=((isset($params['COMPLETETS']))&&$params['COMPLETETS']!="")? "COMPLETETS=now(),": "";
  $sql=(substr($sql,-1)==",")? substr($sql, 0, -1) : $sql;  
  $sql.=" WHERE ORDID = ".$params['ORDID']." AND APPROV IS NULL";
  
  
  if($affected=executeQurey($sql,true))
    return true;
  else
    return false;  
}

function insertOrderDetial($params){
/**
  * $params array:
  *     'ordid'=>$ordid,
  *     'prdkey'=> prodkey,
  *     'qty'=> prod,
  *     'price'=> false,
  *     'sequence'=> $seq,
  *     'taxed'= >$taxed
  **/    
  $price = $params['price'];
  $sql="INSERT INTO ORDERDETAIL (ORDID, PRDKEY, QTY, PRICE, SEQUENCE, TAXED, TRANSTS) ";
  
  //Should INSERT using a SELECT from the CDPPRODUCTS table   
  $sql.=(!$price)? "(SELECT ".$params['ordid']." as ordid,
                            ".$params['prdkey']." as prdkey,
                            ".$params['qty']." as qty,
                            PRICE as price,
                            ".$params['sequence']." as sequence,
                           '".$params['taxed']."' as taxed,
                           now() as transts
                      FROM CDPPRODUCTS 
                     WHERE PRDKEY = ".$params['prdkey']."
                       AND ACTIVE = 'Y')"
     : " VALUES (".$params['ordid']." as ordid,
                 ".$params['prdkey']." as prdkey,
                 ".$params['qty']." as qty,
                 ".$params['price']." as price, 
                 ".$params['sequence']." as sequence,
                '".$params['taxed']."' as taxed,
                  now() as transts)";
        
  //die($sql);     
  if(!$detailid=insertAutoNumRecord($sql)){
    die("Failed to create detail record <br>".$sql);
    return false;
  }else
    return $detailid;         
}

function insertShippingData($params){
/**
  * $params array:
  *     'ordid'=>$ordid,
  *     'addr1'=>$addr1,
  *     'addr2'=>$addr2,
  *     'country'=>$country,
  *     'city'=>$city,
  *     'state'=>$state,
  *     'zip'=>$zip,
  *     'attn'=>$attn,
  *     'phone'=>$phone,
  *     'email'=>$email,
  *     'shipservcode'=>$shipservcode,
  *     'qty'=>$ordQty,
  *     'weight'=>$ordweight
  **/        
  $sql ="INSERT INTO ORDERSHIP (ORDID, SHIPTO, ADDR1, ADDR2, CITY, STATE, ZIP, COUNTRY, PHONE, EMAIL, SHIPSERVCODE, ATTN, QTY, WEIGHT ) VALUES
        (".$params['ordid'].",";
  $sql.=($params['shipto']=="")? "NULL," : "'".$params['shipto']."',";        
  $sql.="'".$params['addr1']."',"; 
  $sql.=($params['addr2']=="")? "NULL," : "'".$params['addr2']."',"; 
  $sql.= "'".$params['city']."', 
         '".$params['state']."', 
         '".$params['zip']."', 
         '".$params['country']."', 
         '".$params['phone']."',";
  $sql.=($params['email']=="")? "NULL," : "'".$params['email']."',";           
  $sql.= "'".$params['shipservcode']."',";
  $sql.=($params['attn']=="")? "NULL," : "'".$params['attn']."',";
  $sql.=   $params['qty'].", 
         ".$params['weight'].")";

  //die($sql); 
  if(!$shipid=insertAutoNumRecord($sql))
    return false;
  else
    return $shipid;              
}

function insertOrderStatus($ordid,$status,$statusTS=false){
  switch($status){
    case 'CREATED' :
      $useTS = "ORDERTS";
      break;
    case 'CANCELLED' :
      $useTS = "COMPLETETS";
      break;
    default :
      $useTS = "now()";
      break;
  }
  $useTS=($statusTS)? $statusTS : $useTS;
  //$useTS=date('Y-m-d H:i:s',strtotime($useTS));
  
  $ckSQL="SELECT count(*) FROM ORDERSTATUS WHERE ORDID = $ordid AND STATUS = '$status'";
  if(!$cnt=fetchOne($ckSQL)){
    $sql="INSERT INTO ORDERSTATUS (ORDID, STATUS, STATUSTS)";
    $sql.=(!$statusTS)? "(SELECT ORDID,'$status',$useTS FROM ORDERMASTER WHERE ORDID = $ordid)":
                        "(SELECT ORDID,'$status','$useTS' FROM ORDERMASTER WHERE ORDID = $ordid)";
    //die($sql);
    executeQurey($sql);
  }  
}

function getLastOrderInfo($conid,$limit=5){

  $sql="SELECT mst1.CONID,mst1.TOTAL,UNIX_TIMESTAMP(mst1.ORDERTS) AS ORDERTS,st1.ORDID, st1.STATUS, st1.STATUSTS
          FROM ORDERMASTER mst1, ORDERSTATUS st1
          JOIN (SELECT st2.ORDID, MAX(st2.STATUSTS) AS LASTSTATUS
                  FROM ORDERSTATUS st2
                GROUP BY st2.ORDID ) AS stM
          ON st1.ORDID = stM.ORDID AND st1.STATUSTS = stM.LASTSTATUS
         WHERE mst1.ORDID = st1.ORDID
           AND st1.STATUS NOT IN ('CANCELLED','FAILED')        
           AND mst1.CONID = $conid
      ORDER BY st1.STATUSTS Desc, st1.ORDID Asc";
  $sql.=($limit)? " LIMIT 0,$limit" : "";
  //die($sql);  
  $res=fetchArray($sql);
  return $res;       
}

function getOrderCounts($conid){
  $sql="SELECT count(st1.STATUSTS)
          FROM ORDERMASTER mst1, ORDERSTATUS st1
          JOIN (SELECT st2.ORDID, MAX(st2.STATUSTS) AS LASTSTATUS
                  FROM ORDERSTATUS st2
                GROUP BY st2.ORDID ) AS stM
          ON st1.ORDID = stM.ORDID AND st1.STATUSTS = stM.LASTSTATUS
         WHERE mst1.ORDID = st1.ORDID
           AND st1.STATUS NOT IN ('CANCELLED','FAILED')
           AND mst1.CONID = $conid";
  //die($sql);
  $cnt=fetchOne($sql);          
  return $cnt;      
}

function getOrderCount($conid,$type="ALL"){
 $sql ="SELECT st1.STATUS, count(st1.STATUS) AS SCOUNT
          FROM ORDERMASTER mst1, ORDERSTATUS st1
         WHERE mst1.ORDID = st1.ORDID
           AND mst1.CONID = $conid
         GROUP BY st1.STATUS";
 $res=fetchArray($sql);
 $statCnts=array();
 //die(print_r($res));
 foreach($res as $statCnt){
     $statCnts[$statCnt['STATUS']]=$statCnt['SCOUNT'];
     if($type==$statCnt['STATUS'])
      return array($type=>$statCnt['SCOUNT']); 
 }
  $statCnts['ACTIVE']=getOrderCounts($conid);
 return $statCnts;
} 

function isValidOrderId($conid,$ordid){
  $qry="SELECT count(ORDID) FROM ORDERMASTER WHERE ORDID=$ordid AND CONID=$conid";
  $cnt=fetchOne($qry);
  if($cnt>0)
    return true;
  else
    return false;
}

function insertOrderNote($ordid,$note,$creator,$public){
  $sql="INSERT INTO ORDERNOTES(ORDID, NOTE, CREATEDBY, CREATEDDT, PUBLIC) VALUES
  ($ordid,'$note','$creator',now(),'$public')";
  //die($sql);
  if(!$noteid=insertAutoNumRecord($sql)){
    return false;
  }else{
    return $noteid;
  }   
}

function getUpsServices($A=false){
  $upsSrvsA=array();
  $upsSrvsA['01']='UPS Next Day Air';
  $upsSrvsA['02']='UPS Second Day Air';
  $upsSrvsA['03']='UPS Ground';
  //$upsSrvsA['07']='UPS Worldwide ExpressSM';
  //$upsSrvsA['08']='UPS Worldwide ExpeditedSM';
  //$upsSrvsA['11']='UPS Standard'; 
  $upsSrvsA['12']='UPS Three-Day Select';
  $upsSrvsA['14']='UPS Next Day Air Early A.M. SM';
  //$upsSrvs['54']='UPS Worldwide Express PlusSM';
  //$upsSrvs['59']='UPS Second Day Air A.M.';
  //$upsSrvs['65']='UPS Saver'; 

  if($A)
    return $upsSrvsA;
  else{
    $upsSrvs=array();
    foreach($upsSrvsA as $k => $v){
     $upsSrvs[]=array($k,$v);
    }
    return $upsSrvs;
  }
}

function validateDate( $date, $format='YYYY-MM-DD')
{
  switch( $format )
  {
      case 'YYYY/MM/DD':
      case 'YYYY-MM-DD':
      list( $y, $m, $d ) = preg_split( '/[-\.\/ ]/', $date );
      break;

      case 'YYYY/DD/MM':
      case 'YYYY-DD-MM':
      list( $y, $d, $m ) = preg_split( '/[-\.\/ ]/', $date );
      break;

      case 'DD-MM-YYYY':
      case 'DD/MM/YYYY':
      list( $d, $m, $y ) = preg_split( '/[-\.\/ ]/', $date );
      break;

      case 'MM-DD-YYYY':
      case 'MM/DD/YYYY':
      list( $m, $d, $y ) = preg_split( '/[-\.\/ ]/', $date );
      break;

      case 'YYYYMMDD':
      $y = substr( $date, 0, 4 );
      $m = substr( $date, 4, 2 );
      $d = substr( $date, 6, 2 );
      break;

      case 'YYYYDDMM':
      $y = substr( $date, 0, 4 );
      $d = substr( $date, 4, 2 );
      $m = substr( $date, 6, 2 );
      break;

      default:
      throw new Exception( "Invalid Date Format" );
  }
  return checkdate( $m, $d, $y );
}
?>

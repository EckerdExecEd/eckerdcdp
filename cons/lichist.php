<?php 
/*=============================================================================*
* 04-22-2005 TRM: Added support for "Self Only" licensing.
*
*=============================================================================*/
require_once "../meta/licfns.php";
require_once "../meta/table.class.php";
require_once "consfn.php";
session_start();

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$headertext="<link rel=\"stylesheet\" type=\"text/css\" href=\"../script/js/orderrpts.css\" />\n";

writeHead("Conflict Dynamics Profile - Consultant",false,$headertext);
$crumbs = array("Home"=>"home.php", "License History"=>"");
writeBody("License History",$msg,$crumbs);
$urls=array('home.php');
//$txts=array('Back');  // 4/3/08 removed by cdz
$txts=array();
menu($urls,$txts,"");
$conid=$_SESSION['conid'];

$licTable=renderLicenseHistory($conid);
echo $licTable."<br />";


writeFooter(false);

function renderLicenseHistory($conid){
   $ttlPurStd=totPurchased($conid);
   $ttlConStd=totConsumed($conid);
   $ttlRemStd= $ttlPurStd - $ttlConStd;
   $color1=($ttlRemStd > 0)? "#003399" : "#CC0000";   
   $dtlPurStd= purchased($conid,1,false);
   $dtlConStd= consumed($conid,1,false);
   $tbl = new table("600px","","orders"); $r=0;
   $tbl->addRow("", "", "head");
   $tbl->rows[$r]->addCell("LICENSE HISTORY - STANDARD","h","","","","3");
   $tbl->addRow("alt", ""); $r++;
   $tbl->rows[$r]->addCell("Licenses Purchased (Total)","d","ct");
   $tbl->rows[$r]->addCell("Licenses Consumed (Total)","d","ct");
   $tbl->rows[$r]->addCell("Remaining License Balance","d","ct"); 
   $tbl->addRow("", ""); $r++;
   $tbl->rows[$r]->addCell("<span style=\"color:#003399;font-weight:bold;\" >".$ttlPurStd."</span>","d","ct");
   $tbl->rows[$r]->addCell("<span style=\"color:#003399;font-weight:bold;\" >".$ttlConStd."</span>","d","ct");
   $tbl->rows[$r]->addCell("<span style=\"color:$color1;font-weight:bold;\" >".$ttlRemStd."</span>","d","ct");
   $tbl->addRow("alt", ""); $r++;
   $tbl->rows[$r]->addCell("Licenses Purchased (Detail)","d","ct");
   $tbl->rows[$r]->addCell("Licenses Consumed (Detail)","d","ct");
   $tbl->rows[$r]->addCell("","d","ct");
   $tbl->addRow("", ""); $r++;
   $tbl->rows[$r]->addCell($dtlPurStd,"d","lt","","","","top");
   $tbl->rows[$r]->addCell($dtlConStd,"d","lt","","","","top");
   $tbl->rows[$r]->addCell("&nbsp;","d","lt");             
   $table=$tbl->draw(true); 
   $table.="<hr />";

   $ttlPurSelf=totPurchased($conid,3);
   $ttlConSelf=totConsumed($conid,3);
   $ttlRemSelf= $ttlPurSelf - $ttlConSelf;
   $color2=($ttlRemSelf > 0)? "#003399" : "#CC0000";  
   $dtlPurSelf= purchased($conid,3,false);
   $dtlConSelf= consumed($conid,3,false); 
   $tbl = new table("600px","","orders"); $r=0;
   $tbl->addRow("", "", "head");
   $tbl->rows[$r]->addCell("LICENSE HISTORY - SELF-ONLY","h","","","","3");
   $tbl->addRow("alt", ""); $r++;
   $tbl->rows[$r]->addCell("Licenses Purchased (Total)","d","ct");
   $tbl->rows[$r]->addCell("Licenses Consumed (Total)","d","ct");
   $tbl->rows[$r]->addCell("Remaining License Balance","d","ct"); 
   $tbl->addRow("", ""); $r++;
   $tbl->rows[$r]->addCell("<span style=\"color:#003399;font-weight:bold;\" >".$ttlPurSelf."</span>","d","ct");
   $tbl->rows[$r]->addCell("<span style=\"color:#003399;font-weight:bold;\" >".$ttlConSelf."</span>","d","ct");
   $tbl->rows[$r]->addCell("<span style=\"color:$color2;font-weight:bold;\" >".$ttlRemSelf."</span>","d","ct"); 
   $tbl->addRow("alt", ""); $r++;
   $tbl->rows[$r]->addCell("Licenses Purchased (Detail)","d","ct");
   $tbl->rows[$r]->addCell("Licenses Consumed (Detail)","d","ct");
   $tbl->rows[$r]->addCell("","d","ct"); 
   $tbl->addRow("", ""); $r++;
   $tbl->rows[$r]->addCell($dtlPurSelf,"d","lt","","","","top");
   $tbl->rows[$r]->addCell($dtlConSelf,"d","lt","","","","top");
   $tbl->rows[$r]->addCell("&nbsp;","d","lt");         
   $table.=$tbl->draw(true);     
   return $table;
}
?>

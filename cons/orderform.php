<?php


include_once "../meta/dbfns.php";
require_once "consfn.php";
require_once "../meta/formfns.php";
require_once "../meta/orderfns.php";
require_once "../meta/orders.class.php";

session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$conid=$_SESSION['conid'];
$ordObj = new orders;
$ordObj->setConID($conid);
$conName=$ordObj->_conName;
$conTaxExempt=$ordObj->_conTaxExempt;
$conState=$ordObj->_conState;

$SCRIPTNAME = $_SERVER['PHP_SELF'];
$headertext="<script type=\"text/javascript\" src=\"../meta/jquery/jquery.js\"></script>\n
             <script type=\"text/javascript\" src=\"../meta/jquery/jquery.validate.js\"></script>\n
             <script type=\"text/javascript\" src=\"../meta/jquery/jquery.numeric.js\"></script>\n             
             <script type=\"text/javascript\" src=\"../script/js/orderForm.js\"></script>\n
             <link rel=\"stylesheet\" type=\"text/css\" href=\"../script/js/orderFormCSS.php?conid=$conid\" />\n";

$msg="conid =".$_SESSION['conid']."<br>";

//Check to see if we have a CANCELLED order
if(isset($_GET['paymentStatus'])&&($_GET['paymentStatus']=='canceled')){
  if(isset($_GET['uniqueId'])){
    $canOrdID=$_GET['uniqueId'];
    if($cancelled=updateOrderMaster(array('ORDID'=>$canOrdID,'APPROV'=>'CANCELLED','COMPLETETS'=>'NOW')))
      insertOrderStatus($canOrdID,"CANCELLED");
  }
}

//Check to see if order has been posted             
if((isset($_POST))&&($_POST['cnfOrder']=='Y')){
  //Step 1 - Check that all data is valid (work on after testing Eckerd payment gateway!!!)
  
  //Step 2 - Insert records into database
  list($orderCreated,$ordid,$amount)=processSubmittedOrder($_POST);
  if($orderCreated){
    //Send to credit card form by Eckerd
    $msg="Order Created!! Now what??";
    //Lets get ready to POST data for payment gateway.
  
    $paymentGateway=send2gateway($ordid,$amount);
    echo  $paymentGateway;
    exit; 
  
  /*foreach($_POST as $k=>$v) $msg.="$k = $v<br>";*/
  }else{
    $msg.="Problem with processSubmittedOrder function!!<br>";
  }
}


writeHead("CDP Products and Services Order Form",false,$headertext);
$crumbs = array("Home"=>"home.php","CDP Online Ordering"=>"orders.php","CDP Order Form"=>"");

$text="CDP Products and Service Order Form";

writeBody($text,$msg,$crumbs);
$urls=array('home.php');
$txts=array();
menu($urls,$txts,"");


//==============================================================================
  //Purchase Licenses Fieldset +++++++++++++++++++++++++++++++++++++++++++++++++
  $plParams=array();
  //Qualify and pass parameters
  $plParams['conid']=$conid;
  $plParams['defaultPkgID']='1'; //Change to whatever package you want to be standard
  //Render HTML for the plFieldSet section
  $plFieldSet=plFieldSet($plParams);
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  //Development Guides Fieldset ++++++++++++++++++++++++++++++++++++++++++++++++
  $dgParams=array();
  
  $dgFieldSet = dgFieldSet();
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  //SHIPPING Fieldset ---------------------------------------------------
  //Address 1
  $ship2IN=textInput("ship2","$conName","71","250",'id="ship2" class="required"');  
  //Address 1
  $addr1IN=textInput("addr1","","71","250",'id="addr1" class="required"');
  //Address 2
  $addr2IN=textInput("addr2","","71","250",'id="addr2" class="required"');  
  //Country
  $ctrySelect=countrySelect("country");
  //City
  $cityIN=textInput("city","","25","50",'id="city" class="required"');
  //State/Province
  $stateSelect=stateSelect("state");
  //Zip
  $zipIN=textInput("zip","","10","20",'id="zip" class="required"'); 
  //Attention
  $attnIN=textInput("attn","","35","250",'id="attn"');
  //Phone
  $phoneIN=textInput("phone","","10","20",'id="phone"');
  //Email
  $emailIN=textInput("email","","25","50",'id="email"');  
  //UPS services
  $upsSrvcs=upsSelect("upsSrvc","03");
  
//==============================================================================

  $ordTtlFieldSet=orderTotalsFieldset($conTaxExempt,$conState);
  $enable=(($conTaxExempt=='Y')&&($conState=='FL'))? "" : "disabled";

?>
<div id="shipfrm" style="width:700px;">
<form id="cdpOrdFrm" class="cdpOrderForm" method="post" action="orderform.php" name="orderfrm">
  <?php echo $plFieldSet;?>
  <?php echo $dgFieldSet;?>
  <fieldset  id="shipFieldSet" class="shipfieldset">
    <legend>Shipping</legend>
    <table style="text-align: left;" width="680px" border="0" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td  style="width: 25%;"><label id="noShipLbl" for="noShip" style="font-weight:bold;font-size:110%;" title="Is shipping of printed development guides required for this order.">Required?</label></td>
          <td  style="width: 75%;" colspan="3" rowspan="1">
            <input type="radio" name="noShip" value="Y" checked/> Yes &nbsp;
            <input type="radio" name="noShip" value="N" /> No &nbsp;
            <span style="color:red;">(Is shipping of printed development guides required for this order?)</span>
          </td>
        </tr>       
        <tr>        
          <td style="width: 25%;"><label id="ship2Lbl" for="ship2">Ship To *</label></td>
          <td style="width: 75%;" colspan="3" rowspan="1"><?php echo $ship2IN; ?></td>
        </tr>      
        <tr>
          <td style="width: 25%;"><label id="addr1Lbl" for="addr1">Address 1 *</label></td>
          <td style="width: 75%;" colspan="3" rowspan="1"><?php echo $addr1IN; ?></td>
        </tr>
        <tr>
          <td style="width: 25%;"><label id="addr1Lb2" for="addr2">Address 2</label></td>
          <td style="width: 75%;" colspan="3" rowspan="1"><?php echo $addr2IN; ?></td>
        </tr>
        <tr>
          <td style="width: 25%;"><label id="countryLbl" for="country">Country *</label></td>
          <td style="width: 25%;"><?php echo $ctrySelect;?></td>
          <td style="width: 25%;"><label for="city">City *</label></td>
          <td style="width: 25%;"><?php echo $cityIN;?></td>
        </tr>
        <tr>
          <td style="width: 25%;"><label id="stateLbl" for="state">State/Province *</label></td>
          <td style="width: 25%;"><span id="stateSEL"><?php echo $stateSelect;?></span></td>
          <td style="width: 25%;"><label for="zip">Zip *</label></td>
          <td style="width: 25%;"><?php echo $zipIN;?></td>
        </tr>
        <tr>
          <td style="width: 25%;"><label  id="attnLbl" for="attn">Attention</label></td>
          <td style="width: 25%;"><?php echo $attnIN;?></td>
          <td style="width: 25%;"><label  id="taxExLbl" for="taxExId" title="Employer Identification Number needed if located in the state of FL and considered Tax Exempt">Tax EIN</label></td>
          <td style="width: 25%;"><input type="text" name="taxExId" id="taxExId" maxlength="10" size="10" <?php echo $enable;?>></td>
        </tr>
        <tr>
          <td style="width: 25%;"><label  id="phoneLbl" for="phone">Phone *</label></td>
          <td style="width: 25%;"><?php echo $phoneIN;?></td>
          <td style="width: 25%;"><label  id="emailLbl" for="email">Email</label></td>
          <td style="width: 25%;"><?php echo $emailIN;?></td>
        </tr> 
        <tr>
          <td style="width: 25%;"><label id="shippingLbl" for="upsSrvc">Shipping *</label></td>
          <td colspan="2" style="width: 50%;">
          <?php echo $upsSrvcs;?>
          </td>
          <td style="width: 25%;" align="center"><input type="button" value="Calculate Shipping and Taxes" class="calcShip" /></td>
        </tr>
      </tbody>
    </table>
    </fieldset>
  <?php echo $ordTtlFieldSet;?>   
  <input type="hidden" id="conid" name="conid" value="<?php echo $conid;?>" size="8" />         
 </form>
</div>  
<?php
writeFooter(false);
/**/
function countrySelect($selName){
  $qry="select COUNTRYID,NAME from COUNTRY where COUNTRYID in ('CA','US') ORDER BY NAME DESC";
  $countries=fetchArray($qry);
  //die(print_r($countries));
  $ctrySelect=selectList( $selName, $countries, $default,'Select a Country','id="'.$selName.'" ');
  return $ctrySelect;
}

function stateSelect($selName,$country='US',$default=""){
  $current=array('CA','US');
  $states=(in_array($country,$current))? $ctrySelect=selectList( $selName, fetchArray("select SUBCOUNTRYID,NAME from SUBCOUNTRY where COUNTRYID = '$country' AND DISPLAY = 'Y' ORDER BY NAME ASC"), $default,'SELECT A STATE/PROVINCE','id="'.$selName.'"')
   : textInput($selName, $default,"25","50",'id="'.$selName.'"');
  return $states;
}

function upsSelect($selName, $default='GND'){
  $upsSrvs=array();  
  $upsSrvs[]=array('03','UPS Ground');
  $upsSrvs[]=array('01','UPS Next Day Air');
  $upsSrvs[]=array('02','UPS Second Day Air');
  //$upsSrvs[]=array('07','UPS Worldwide ExpressSM');
  //$upsSrvs[]=array('08','UPS Worldwide ExpeditedSM');
  //$upsSrvs[]=array('11','UPS Standard'); Not Valid
  $upsSrvs[]=array('12','UPS Three-Day Select');
  $upsSrvs[]=array('14','UPS Next Day Air Early A.M. SM');
  //$upsSrvs[]=array('54','UPS Worldwide Express PlusSM');
  //$upsSrvs[]=array('59','UPS Second Day Air A.M.');
  //$upsSrvs[]=array('65','UPS Saver'); /**/
  //die(print_r($upsSrvs));
  $srvSelect=selectList( $selName, $upsSrvs, $default,'','id="'.$selName.'"'); 
  
  return $srvSelect;
}

function plFieldSet($plParams){
  //unpack parameters ----------------------------------------------------------
  $conid=$plParams['conid'];
  $defaultPkgID=$plParams['defaultPkgID'];
  $DG='<span style="position:relative;float:right;color:#5F5F5F;font-family: Arial, Helvetica, sans-serif;font-size:80%;font-weight:bold;" title="Development Guide included">&nbsp; &nbsp;(DG)</span>';
  //----------------------------------------------------------------------------
  //STEP 1 - Identify rate package for consultant
  if($defaultPkgID=="")
    die("No defaultPkgID");  
 
  $query="select IFNULL(b.PKGID,".$defaultPkgID.") from CONSULTANT a 
          LEFT JOIN CONSPECIALPKGS b ON a.CONID = b.CONID and b.INACTIVETS is null 
          WHERE a.CONID=$conid 
      ORDER BY b.CREATETS DESC LIMIT 0,1";
      
  $pkgID=fetchOne($query);
  //die($query);
  //$pkgID=1;
  $sixcoldisp=($pkgID==1||$pkgID==6)? true : false;
  $numCols=($sixcoldisp)? 6 : 4 ;
  //STEP 2 - Get Products and prices for rate package
  $plProducts=getPackageDetails($pkgID); 
error_log(print_r($plProducts, true));
  //STEP 3 - Render Table Header
  $headCols=array();
  if($sixcoldisp){ // 6 Column layout
    $headCols[]=array('<span class="orderFormHeader">Qty</span>','align="left"');
    $headCols[]=array('<span class="orderFormHeader">Item</span>','align="left"');
    $headCols[]=array('<span class="orderFormHeader">Qty Pricing</span>','align="center"');
    $headCols[]=array('<span class="orderFormHeader">(1 - 49)</span>','align="center"');
    $headCols[]=array('<span class="orderFormHeader">(50 - 99)</span>','align="center"');
    $headCols[]=array('<span class="orderFormHeader">(100+)</span>','align="center"');
  }else{ // 4 Column layout
    $headCols[]=array('<span class="orderFormHeader">Qty</span>','align="left"');
    $headCols[]=array('<span class="orderFormHeader">Item</span>','align="left"');    
    $headCols[]=array('<span class="orderFormHeader">&nbsp;</span>','align="center"');
    $headCols[]=array('<span class="orderFormHeader">Price</span>','align="center"');
  }
  
  $plHeaderRow=renderTR($headCols);
  
  //STEP 4 - Render 360 license
  
  $item1name=$plProducts[0]['PRDNAME'];
  $licCDP360=textInput("licCDP360","0","2","3",'id="licCDP360" class="plInput"');
  $licCDP360.="\n<input type=\"hidden\" id=\"licCDP360key\" name=\"licCDP360key\" value=\"0\" size=\"3\" />";
  $row1=array();  
  $row1[]=array($licCDP360,''); 
  $row1[]=array($item1name.$DG,'');
  $row1[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"');
  $item1weight=$plProducts[0]['WEIGHT'];
  $prdkey1=$plProducts[0]['PRDKEY'];        
  if($sixcoldisp){ // 6 column format
    $qtyPrice1=$plProducts[0]['PRICE'];
    $row1[]=array('<span id="cdp360price1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item1weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"');
    $qtyPrice50=$plProducts[1]['PRICE'];
    $prdkey50=$plProducts[1]['PRDKEY'];          
    $row1[]=array('<span id="cdp360price50" class="activeQty" prdkey="'.$prdkey50.'" weight="'.$item1weight.'" price="'.$qtyPrice50.'">$'.$qtyPrice50.'</span>','align="center"');
    $qtyPrice100=$plProducts[2]['PRICE']; 
    $prdkey100=$plProducts[2]['PRDKEY'];         
    $row1[]=array('<span id="cdp360price100" class="activeQty" prdkey="'.$prdkey100.'" weight="'.$item1weight.'" price="'.$qtyPrice100.'">$'.$qtyPrice100.'</span>','align="center"');        
  }else{   // 4 column format
    $qtyPrice1=$plProducts[0]['PRICE'];
    $row1[]=array('<span id="cdp360price1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item1weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"'); 
  }
  
  $plRow1=renderTR($row1);  
  
  //STEP 5 Render Individual license 
  $item2name=($sixcoldisp)? $plProducts[3]['PRDNAME'] : $plProducts[1]['PRDNAME'];
  $licCDPI=textInput("licCDPI","0","2","3",'id="licCDPI" class="plInput"');
  $licCDPI.="\n<input type=\"hidden\" id=\"licCDPIkey\" name=\"licCDPIkey\" value=\"0\" size=\"3\" />";  
  $row2=array();  
  $row2[]=array($licCDPI,''); 
  $row2[]=array($item2name.$DG,'');
  $row2[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"'); 
  $item2weight=($sixcoldisp) ? $plProducts[3]['WEIGHT'] : $plProducts[1]['WEIGHT'];        
  if($sixcoldisp){ // 6 column format 
    $qtyPrice1=$plProducts[3]['PRICE']; 
    $prdkey1=$plProducts[3]['PRDKEY'];         
    $row2[]=array('<span id="cdpIprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item2weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"');
    $qtyPrice50=$plProducts[4]['PRICE'];
    $prdkey50=$plProducts[4]['PRDKEY'];         
    $row2[]=array('<span id="cdpIprice50" class="activeQty" prdkey="'.$prdkey50.'" weight="'.$item2weight.'" price="'.$qtyPrice50.'">$'.$qtyPrice50.'</span>','align="center"');
    $qtyPrice100=$plProducts[5]['PRICE']; 
    $prdkey100=$plProducts[5]['PRDKEY'];         
    $row2[]=array('<span id="cdpIprice100" class="activeQty" prdkey="'.$prdkey100.'" weight="'.$item2weight.'" price="'.$qtyPrice100.'">$'.$qtyPrice100.'</span>','align="center"');        
  }else{   // 4 column format
    $qtyPrice1=$plProducts[1]['PRICE']; 
    $prdkey1=$plProducts[1]['PRDKEY'];       
    $row2[]=array('<span id="cdpIprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item2weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"'); 
  }
          
  $plRow2=renderTR($row2);  
  
  //STEP 6 Render CDP-I Paper/Pencil
  $item3name=($sixcoldisp)? $plProducts[6]['PRDNAME'] : $plProducts[2]['PRDNAME'];
  $licCDPIPP=textInput("licCDPIPP","0","2","3",'id="licCDPIPP" class="plInput"');
  $licCDPIPP.="\n<input type=\"hidden\" id=\"licCDPIPPkey\" name=\"licCDPIPPkey\" value=\"0\" size=\"3\" />";  
  $row3=array();  
  $row3[]=array($licCDPIPP,''); 
  $row3[]=array($item3name.$DG,'');
  $row3[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"'); 
  $item3weight=($sixcoldisp)? $plProducts[6]['WEIGHT'] : $plProducts[2]['WEIGHT'];        
  if($sixcoldisp){ // 6 column format 
    $qtyPrice1=$plProducts[6]['PRICE']; 
    $prdkey1=$plProducts[6]['PRDKEY'];         
    $row3[]=array('<span id="cdpIPPprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item3weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"');
    $qtyPrice50=$plProducts[7]['PRICE'];
    $prdkey50=$plProducts[7]['PRDKEY'];         
    $row3[]=array('<span id="cdpIPPprice50" class="activeQty" prdkey="'.$prdkey50.'" weight="'.$item3weight.'" price="'.$qtyPrice50.'">$'.$qtyPrice50.'</span>','align="center"');
    $qtyPrice100=$plProducts[8]['PRICE']; 
    $prdkey100=$plProducts[8]['PRDKEY'];         
    $row3[]=array('<span id="cdpIPPprice100" class="activeQty" prdkey="'.$prdkey100.'" weight="'.$item3weight.'" price="'.$qtyPrice100.'">$'.$qtyPrice100.'</span>','align="center"');        
  }else{   // 4 column format
    $qtyPrice1=$plProducts[2]['PRICE']; 
    $prdkey1=$plProducts[2]['PRDKEY'];       
    $row3[]=array('<span id="cdpIPPprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item3weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"'); 
  }
          
  $plRow3=renderTR($row3); 
  

  //STEP 7 Render 360 Group
  $item4name=($sixcoldisp)? $plProducts[9]['PRDNAME'] : $plProducts[3]['PRDNAME'];
  $licCDP360G=textInput("licCDP360G","0","2","3",'id="licCDP360G" class="plInput"');
  $licCDP360G.="\n<input type=\"hidden\" id=\"licCDP360Gkey\" name=\"licCDP360Gkey\" value=\"0\" size=\"3\" />";    
  $row4=array();   
  $row4[]=array($licCDP360G,''); 
  $row4[]=array($item4name,'');  
  $item4weight=($sixcoldisp)? $plProducts[9]['WEIGHT'] : $plProducts[3]['WEIGHT'];   
  if($sixcoldisp){ // 6 column format 
    $qtyPrice1=$plProducts[9]['PRICE']; 
    $prdkey1=$plProducts[9]['PRDKEY'];     
    $row4[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"'); 
    $row4[]=array('<span id="cdp360Gprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item4weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"'); 
    $row4[]=array('<span style="color:#cccccc;">&nbsp;</span>','align="center"');
    $row4[]=array('<span style="color:#cccccc;">&nbsp;</span>','align="center"');        
  }else{
    $qtyPrice1=$plProducts[3]['PRICE'];
    $prdkey1=$plProducts[3]['PRDKEY'];      
    $row4[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"');   
    $row4[]=array('<span id="cdp360Gprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item4weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"');
  }    
  $plRow4=renderTR($row4);  
   
  //STEP 8 Render Individual Group
  $item5name=($sixcoldisp)? $plProducts[10]['PRDNAME'] : $plProducts[4]['PRDNAME'];  
  $licCDPIG=textInput("licCDPIG","0","2","3",'id="licCDPIG" class="plInput"');
  $licCDPIG.="\n<input type=\"hidden\" id=\"licCDPIGkey\" name=\"licCDPIGkey\" value=\"0\" size=\"3\" />";  
  $row5=array();
  $row5[]=array($licCDPIG,''); 
  $row5[]=array($item5name,''); 
  $item5weight=($sixcoldisp)?  $plProducts[10]['WEIGHT'] : $plProducts[4]['WEIGHT'];    
  if($sixcoldisp){ // 6 column format
    $qtyPrice1=$plProducts[10]['PRICE']; 
    $prdkey1=$plProducts[10]['PRDKEY'];      
    $row5[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"'); 
    $row5[]=array('<span id="cdpIGprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item5weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"'); 
    $row5[]=array('<span style="color:#cccccc;">&nbsp;</span>','align="center"');
    $row5[]=array('<span style="color:#cccccc;">&nbsp;</span>','align="center"');        
  }else{
    $qtyPrice1=$plProducts[4]['PRICE']; 
    $prdkey1=$plProducts[4]['PRDKEY'];     
    $row5[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"');   
    $row5[]=array('<span id="cdpIGprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item5weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"');
  }       
  $plRow5=renderTR($row5,'style="visibility:hidden;"'); 
  
  //STEP 9 Render BCC Module 
  //die(print_r($plProducts));
  $item6name = ($sixcoldisp)? $plProducts[11]['PRDNAME'] : $plProducts[5]['PRDNAME'];
  $licBCC=textInput("licBCC","0","2","3",'id="licBCC" class="plInput"');
  $licBCC.="\n<input type=\"hidden\" id=\"licBCCkey\" name=\"licBCCkey\" value=\"0\" size=\"3\" />"; 
  $row6=array();     
  $row6[]=array($licBCC,''); 
  $row6[]=array($item6name,''); 
  $item6weight=($sixcoldisp)?  $plProducts[11]['WEIGHT'] : $plProducts[5]['WEIGHT'];
  if($sixcoldisp){ // 6 column format
    $qtyPrice1=$plProducts[11]['PRICE']; 
    $prdkey1=$plProducts[11]['PRDKEY'];
    $qtyPrice100=$plProducts[12]['PRICE']; 
    $prdkey100=$plProducts[12]['PRDKEY'];    
    $row6[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"'); 
    $row6[]=array('<span id="cdpBCCprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item6weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"'); 
    $row6[]=array('<span style="color:#cccccc;">&nbsp;</span>','align="center"');
    $row6[]=array('<span id="cdpBCCprice100" class="activeQty" prdkey="'.$prdkey100.'" weight="'.$item6weight.'" price="'.$qtyPrice100.'">$'.$qtyPrice100.'</span>','align="center"'); 
           
  }else{
    $qtyPrice1=$plProducts[5]['PRICE']; 
    $prdkey1=$plProducts[5]['PRDKEY'];  
    $row6[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"');   
    $row6[]=array('<span id="cdpBCCprice1" class="activeQty" prdkey="'.$prdkey1.'" weight="'.$item6weight.'" price="'.$qtyPrice1.'">$'.$qtyPrice1.'</span>','align="center"');
 
  }      
  $plRow6=($pkgID < 7)? renderTR($row6) : "";    
  //PURCHASE LICENSES Fieldset ------------------------------------------
  
  
  
$HTML =<<<EOD
<fieldset class="plfieldset">
    <legend>CDP Licenses</legend>
    $msg
    
    <table style="text-align: left;" width="680px" border="0" cellpadding="0" cellspacing="0">
      <tbody>
        $plHeaderRow
        $plRow1
        $plRow2
        $plRow3
        $plRow6         
        $plRow4
        $plRow5 
        <tr><td colspan="$numCols" align="center"><b>(DG)</b> - Development Guide is included. </td></tr>                            
      </tbody>
    </table>
  </fieldset>
EOD;

return $HTML;

}

function dgFieldSet(){
  //STEP 1 - Get Products and prices for fieldset ------------------------------
  $dgProducts=getPackageDetails(5);
  
  //Row 1 
  $fgCDP360=textInput("fgCDP360","0","2","3",'id="fgCDP360" class="fgInput"');
  $prd10 = getProductDetails(10,$dgProducts);
  $prd10desc=$prd10['PRDNAME'];
  $prd10price=$prd10['PRICE'];
  $prd10weight=$prd10['WEIGHT'];
  $prd10key=$prd10['PRDKEY'];
  $fgCDP360.="\n<input type=\"hidden\" id=\"fgCDP360key\" name=\"fgCDP360key\" value=\"".$prd10key."\" size=\"3\" />";    
  $row1=array();
  $row1[]=array($fgCDP360,'');
  $row1[]=array($prd10desc,'');
  $row1[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"');  
  $row1[]=array('<span id="cdp360FGprice" class="activeQty" weight="'.$prd10weight.'" price="'.$prd10price.'">$'.$prd10price.'</span>','align="center"');      
  $dgRow1=renderTR($row1,'style="visibility:hidden;"');

  //Row 2 
  $fgCDPI=textInput("fgCDPI","0","2","3",'id="fgCDPI" class="fgInput"');
  $prd21 = getProductDetails(21,$dgProducts);
  $prd21desc=$prd21['PRDNAME'];
  $prd21price=$prd21['PRICE'];
  $prd21weight=$prd21['WEIGHT'];  
  $prd21key=$prd21['PRDKEY'];  
  $fgCDPI.="\n<input type=\"hidden\" id=\"fgCDPIkey\" name=\"fgCDPIkey\" value=\"".$prd21key."\" size=\"3\" />";  
  $row2=array();
  $row2[]=array($fgCDPI,'');
  $row2[]=array($prd21desc,'');
  $row2[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"');  
  $row2[]=array('<span id="cdpIFGprice" class="activeQty" weight="'.$prd21weight.'"  price="'.$prd21price.'">$'.$prd21price.'</span>','align="center"');      
  $dgRow2=renderTR($row2,'style="visibility:hidden;"');  

  //Row 3 
  $fgCDPCTK=textInput("fgCDPCTK","0","2","3",'id="fgCDPCTK" class="fgInput"');
  $prd28 = getProductDetails(28,$dgProducts);
  $prd28desc=$prd28['PRDNAME'];
  $prd28price=$prd28['PRICE'];
  $prd28weight=$prd28['WEIGHT'];
  $prd28key=$prd28['PRDKEY'];   
  $fgCDPCTK.="\n<input type=\"hidden\" id=\"fgCDPCTKkey\" name=\"fgCDPCTKkey\" value=\"".$prd28key."\" size=\"3\" />";     
  $row3=array();
  $row3[]=array($fgCDPCTK,'');
  $row3[]=array($prd28desc,'');
  $row3[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"');  
  $row3[]=array('<span id="cdpCTKFGprice" class="activeQty" weight="'.$prd28weight.'" price="'.$prd28price.'">$'.$prd28price.'</span>','align="center"');      
  $dgRow3=renderTR($row3);    
  
  //Row 4 
  $fgCDPTTK=textInput("fgCDPTTK","0","2","3",'id="fgCDPTTK" class="fgInput"');
  $prd29 = getProductDetails(29,$dgProducts);
  $prd29desc=$prd29['PRDNAME'];
  $prd29price=$prd29['PRICE'];
  $prd29weight=$prd29['WEIGHT'];
  $prd29key=$prd29['PRDKEY']; 
  $fgCDPTTK.="\n<input type=\"hidden\" id=\"fgCDPTTKkey\" name=\"fgCDPTTKkey\" value=\"".$prd29key."\" size=\"3\" />";      
  $row4=array();
  $row4[]=array($fgCDPTTK,'');
  $row4[]=array($prd29desc,'');
  $row4[]=array('<span style="font-weight:bold;font-size:9px;">each</span>','align="right"');  
  $row4[]=array('<span id="cdpTTKFGprice" class="activeQty" weight="'.$prd29weight.'" price="'.$prd29price.'">$'.$prd29price.'</span>','align="center"');      
  $dgRow4=renderTR($row4);  
  
  //Row 5 
  $fgCDPCTTK=textInput("fgCDPCTTK","0","2","3",'id="fgCDPCTTK" class="fgInput"');
  $prd30 = getProductDetails(30,$dgProducts);
  $prd30desc=$prd30['PRDNAME'];
  $prd30price=$prd30['PRICE'];
  $prd30weight=$prd30['WEIGHT']; 
  $prd30key=$prd30['PRDKEY'];   
  $fgCDPCTTK.="\n<input type=\"hidden\" id=\"fgCDPCTTKkey\" name=\"fgCDPCTTKkey\" value=\"".$prd30key."\" size=\"3\" />";   
  $row5=array();
  $row5[]=array($fgCDPCTTK,'');
  $row5[]=array($prd30desc,'');
  $row5[]=array('<span style="font-weight:bold;font-size:9px;">set</span>','align="right"');  
  $row5[]=array('<span id="cdpCTTKFGprice" class="activeQty" weight="'.$prd30weight.'" price="'.$prd30price.'">$'.$prd30price.'</span>','align="center"');      
  $dgRow5=renderTR($row5,'style="visibility:hidden;"');         
    
$dgHTML =<<<EOD
<!-- <fieldset class="dgfieldset">
  <legend>Additional Products</legend>
    <table style="text-align: left;" width="680px" border="0" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
           <td style="" align="left"><span class="orderFormHeader">Qty</span></td>
           <td style="" align="left"><span class="orderFormHeader">Item</span></td>
           <td style="" align="center"><span class="orderFormHeader">&nbsp;</span></td> 
           <td style="" align="center"><span class="orderFormHeader">Price</span></td>                                                    
        </tr>
        $dgRow3
        $dgRow4
        $dgRow5
        $dgRow1
        $dgRow2                        
      </tbody>
    </table>
    <input type="hidden" id="fgTotal" name="fgTotal" value="0" />
</fieldset> -->
EOD;

return $dgHTML;
}

function orderTotalsFieldset($taxExempt='Y',$conState){
$ordTtlHTML =<<<EOD
  <fieldset class="ordTtlfieldset">
  <legend>Totals</legend>
  <table style="text-align: left;" width="680px" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td>Subtotal</td>
      <td align="right"><span id="ordSubtotal">$0.00</span>
          <input type="hidden" id="plTTLck" name="plTTLck" value="0" size="8" />
          <input type="hidden" id="fgTTLck" name="fgTTLck" value="0" size="8" />
          <input type="hidden" id="ordSubTTLamt" name="ordSubTTLamt" value="0" size="8" />
      </td>          
    <tr>
    <tr>
      <td>Shipping</td>
      <td align="right"><span id="ordShipping">$0.00</span>
          <input type="hidden" id="shipPLwgt" name="shipPLwgt" value="0" size="8" />          
          <input type="hidden" id="shipFGwgt" name="shipFGwgt" value="0" size="8" />          
          <input type="hidden" id="shipTTLwgt" name="shipTTLwgt" value="0" size="8" />
          <input type="hidden" id="shipTTLamt" name="shipTTLamt" value="0" size="8" />
      </td>
    <tr>   
    <tr>
      <td>Handling</td>
      <td align="right"><span id="ordHandling">$0.00</span></td>
          <input type="hidden" id="handTTLamt" name="handTTLamt" value="0" size="8" />      
    <tr>
    <tr>
      <td>Taxes</td>
      <td align="right"><span id="ordTaxes">$0.00</span></td>
          <input type="hidden" id="conState" name="conState" value="$conState" size="2" />
          <input type="hidden" id="taxExempt" name="taxExempt" value="$taxExempt" size="1" />
          <input type="hidden" id="taxTTLamt" name="taxTTLamt" value="0" size="8" />    
          <input type="hidden" id="taxRate" name="taxRate" value="0" size="8" />             
    <tr> 
    <tr>
      <td  style="border-top:1px solid #000000;">Total</td>
      <td align="right" style="border-top:1px solid #000000;"><span id="ordTotal" style="font-weight:bold;">$0.00</span>
      <input type="hidden" id="ordTTLamt" name="ordTTLamt" value="0" size="8" />
      <input type="hidden" id="cnfOrder" name="cnfOrder" value="N" size="8" /> 
      </td>
    <tr> 
    <tr>
      <td colspan=2 align="center">
        <input type="submit" value="Submit Order" class="orderBut" />&nbsp; &nbsp; &nbsp; 
        <button type="button" class="resetBut">Reset</button><br>
        <span style="position:relative;float:right;">
        <a href="documents/CDPreturnpolicy.pdf" title="Online CDP return policy - May 15, 2014" target="_blank">Return Policy</a>
        </span></td>
    <tr>             
  </table>
</fieldset>
EOD;

return $ordTtlHTML;
}

function getPackageDetails($pkgID){

  $query="select a.PRDKEY, a.PRDID, a.PRDNAME, a.PRDDESC, a.PRICE, a.WEIGHT, a.TAXABLE 
            from CDPPRODUCTS a, PKGPRODUCTS b
           where a.PRDKEY = b.PRDKEY
             and a.ACTIVE = b.ACTIVE
             and b.ACTIVE = 'Y'
             and b.PKGID=$pkgID
        order by b.SEQUENCE asc";

  $products=fetchArray($query); 
  
  //if($pkgID==5) die('Test 5<br>'.print_r($products));
     
  return $products; 
}

function getProductDetails($prdKey,$products){
  //die(print_r($products));
  foreach($products as $product){
   
    if($product['PRDKEY']==$prdKey)
      return $product;
  }
  
  return false;
}
 
function renderTR($datacols, $params=""){
  $row ="<tr  $params>";
  
  foreach($datacols as $col){
    if(!is_array($col)){
      $row.="<td>$col</td>";
    }else{
      $row.="<td ";
      $row.=($col[1]!="")? $col[1].">" : ">";
      $row.= $col[0];
      $row.="</td>\n";
    }
  }
  
  $row.="<tr>\n";
  return $row;
}

function processSubmittedOrder($post_vars){
    //Validate data (come back to!!!)
    $orderOK=true;
    //temp for testing comment out!!!!   
    //return array($orderOK,"1",$post_vars['ordTTLamt']);
    
 
    //Order Master variables ===================================================
    $conid=$post_vars['conid'];
    $subtotal=$post_vars['ordSubTTLamt'];
    $taxes=$post_vars['taxTTLamt'];
    $taxed=($post_vars['taxTTLamt'] > 0)? 'Y' : 'N';
    $taxrate=$post_vars['taxRate'];
    $taxstate=$post_vars['conState'];
    $taxein=$post_vars['taxExId'];
    $shipping=$post_vars['shipTTLamt'];
    $handling=$post_vars['handTTLamt'];
    $total=$post_vars['ordTTLamt'];
    $ordMaster=array('conid'=>$conid,
                     'subtotal'=>$subtotal,
                     'taxes'=>$taxes,
                     'taxrate'=>$taxrate,
                     'taxstate'=>$taxstate,
                     'taxein'=>$taxein,                     
                     'shipping'=>$shipping,
                     'handling'=>$handling,
                     'total'=>$total);
    
    //Order detail variables ===================================================
    //Any time you add a new product, you need to add its ID to this array!!
    $productlist=array('licCDP360','licCDPI','licCDPIPP','licCDP360G','licCDPIG','licBCC','fgCDP360','fgCDPI','fgCDPCTK','fgCDPTTK','fgCDPCTTK');
    
    //Shipping variables =======================================================
    $need2ship=($post_vars['noShip']=="Y")? true : false;
    $ship2=$post_vars['ship2'];
    $addr1=$post_vars['addr1'];
    $addr2=$post_vars['addr2']; 
    $country=$post_vars['country'];
    $city=$post_vars['city'];
    $state=$post_vars['state']; 
    $zip=$post_vars['zip']; 
    $attn=$post_vars['attn'];
    $phone=$post_vars['phone']; 
    $email=$post_vars['email'];
    $shipservcode=($need2ship)? $post_vars['upsSrvc'] : "00";   
    $ordweight=$post_vars['shipTTLwgt'];
    
    //INSERT Order Master Record
    if(($orderOK)&&(!$ordid=insertOrderMaster($ordMaster))){
      $orderOK=false;
      die("failed to create order master record");
      return array($orderOK,false,false);
    }
   
   //$ordid=1; 
    //INSERT Order Detail Record
    $orderDetalis=array();
    $totalQty=0;
    $seq=1;
    foreach($productlist as $prod){
      if($post_vars[$prod] > 0){
        $totalQty+=$post_vars[$prod];
        $detail=array('ordid'=>$ordid,
                      'prdkey'=>$post_vars[$prod.'key'],
                      'qty'=>$post_vars[$prod],
                      'price'=>false,
                      'sequence'=>$seq,
                      'taxed'=>$taxed);
/**/                      
        if(!$detailid=insertOrderDetial($detail)){
          die("failed to generate an order detail record"); 
        } 
              
      }
    }
    //Need to determine amount of quantity!!
    $ordQty=calcCartonQty($totalQty,$break);
    
    $shipparams=array('ordid'=>$ordid,
                      'shipto'=>$ship2,
                      'addr1'=>$addr1,
                      'addr2'=>$addr2,
                      'country'=>$country,
                      'city'=>$city,
                      'state'=>$state,
                      'zip'=>$zip,
                      'attn'=>$attn,
                      'phone'=>$phone,
                      'email'=>$email,
                      'shipservcode'=>$shipservcode,
                      'qty'=>$ordQty,
                      'weight'=>$ordweight);
                      
    //INSERT Shipping Data
    if(!$shipid=insertShippingData($shipparams)){
       die("failed to generate an initial shipping record"); 
    } 
   
   return array($orderOK,$ordid,$total); 
}

function send2gateway($invoice,$amount){
/**
 *  Function (Needs lots of work!!!!)
 *  VARs  callingPage   =>  cdpOrderForm
 *        bannerCode    =>  ???
 *        ownerEmail    =>  'michaldm@eckerd.edu'
 *        amount        =>  $orderAmount
 *        returnPage    =>  'https://www.onlinecdp.org/script/cons/ordersreview.php'
 *        invoiceNumber =>  $ordid
 *        uniqId        =>  ???      
 **/
  $InvoiceCd="CDP".str_pad($invoice,5,'0',STR_PAD_LEFT);
  $bnrCd="CDP";
  $testParams=array('callingPage'=>'https://www.onlinecdp.org/cons/orderform.php',
                    'bannerCode'=>$bnrCd,
                    'ownerEmail'=>'pheilrl@eckerd.edu,michaldm@eckerd.edu',
                    'amount'=>$amount,
                    'returnPage'=>'https://www.onlinecdp.org/cons/ordersreview.php',
                    'invoiceNumber'=>$InvoiceCd,
                    'uniqueId'=>$invoice);
 
  //$pmtGateway="https://www2.eckerd.edu/webpay2/creditcard.php";
  // Old payment processing page was hosted on Eckerd server 
  // New payment processing page hosted locally
  $pmtGateway="https://www.onlinecdp.org/webpay2/creditcard.php";

  //$pmtGateway="https://www.onlinecdp.org/webpay2/creditcardtest.php";
  $inputs = "";
  foreach($testParams as $name => $value){
      $inputFlds.="<input type=\"hidden\" id=\"$name\" name=\"$name\" value=\"$value\" size=\"50\" />\n";
  }

$formHTML=<<<EOD
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=windows-1250">
  <title>CDP Order Processing</title>
  </head>
  <body style="background-color:lightblue;color:slategray;font-family:arial;" onLoad="sendForm();">
  <form name="cdpOrderForm" action="$pmtGateway" method="post">
   $inputFlds
  </form>
  <script type="text/javascript">
    function sendForm(){
        document.forms['cdpOrderForm'].submit();
    }
  </script>
  </body>
</html>
EOD;

return $formHTML;
}

function calcCartonQty($qty,$break){
  if($qty<$break){
    $cartons=1;
  }elseif(is_int(($qty / $break))){
    $cartons=$qty / $break;
  }else{
    $cartons=ceil($qty / $break);
  }
  return $cartons;
}

?>

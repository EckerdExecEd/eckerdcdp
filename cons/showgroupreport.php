<?php
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$gid=$_GET['gid'];

include_once "../meta/groupreport.php";

$pdf=pdf_new();
pdf_set_parameter($pdf,"license","L700102-010500-734253-ZZ4ND2-FMQ922");
pdf_open_file($pdf,"");

pdf_set_info($pdf,'Creator','conflictdynamics.org');
pdf_set_info($pdf,'Author','conflictdynamics.org');
pdf_set_info($pdf,'Title','Conflict Dynamics Profile');

$page=1;
renderPage1($gid,$pdf,$page);
renderPage2($gid,$pdf,$page);
renderPage3($gid,$pdf,$page);
renderPage4($gid,$pdf,$page);
renderPage5($gid,$pdf,$page);
renderPage6($gid,$pdf,$page);
renderPage7($gid,$pdf,$page);
renderPage8($gid,$pdf,$page);
renderPage9($gid,$pdf,$page);

pdf_close($pdf);

header("Content-Type: application/pdf");
header("Content-Disposition: inline; filename=$pid-$cid.pdf");

echo pdf_get_buffer($pdf);
pdf_delete($pdf);
?>


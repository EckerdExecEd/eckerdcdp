<?php

require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$tid=$_POST["tid"];
switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}

$msg="Enter Program Information, click 'Add!'";
if($_POST['what']=="add" && (($tid != 1)||($tid != 3))){
  $msg="<font color='#aa0000'>WARNING!! - No test ID provided. Return to menu and begin again to initialize test ID</font>";
}

if("add"==$_POST['what']){
    $descr=addslashes($_POST['descr']);
    $startDt=date('Y-m-d',strtotime($_POST['startdt']));
    $endDt=date('Y-m-d',strtotime($_POST['enddt']));
    $compEmail=($_POST['cemail']);  //added 3/14/2011 JPC    
    $conid=$_SESSION['conid'];
    $pgmData=array($descr,$startDt,$endDt,$conid,$tid,$compEmail);
    $i=pgmInsert($pgmData);
    if($i){
		// $tid is the isntrument ID
		// 1 = 360
		// 3 = Inidvidual
		if("1"==$tid){
			// 360
			if(instrInsert($i,"1")&&instrInsert($i,"2")){
				$msg="<font color='#00aa00'>Successfully added program '" . stripslashes($descr) . "'.</font>";
			}
			else{
				$msg="<font color='#aa0000'>Error adding instrument ($tid).</font>";
			}
		}
		elseif("3"==$tid){
			// Individual
			if(instrInsert($i,"3")){
				$msg="<font color='#00aa00'>Successfully added program '" . stripslashes($descr) . "'.</font>";
			}
			else{
				$msg="<font color='#aa0000'>Error adding instrument ($tid).</font>";
			}
		}
		else{
			$msg="<font color='#aa0000'>Invalid instrument ($tid).</font>";
		}
    }
    else{
		$msg="<font color='#aa0000'>Error adding program. ($tid)</font>";
    }
}

writeHead("Conflict Dynamics Profile - Consultant",false);
if ($tid == 1) $crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "Add $testName Program"=>"");
	else $crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "Add $testName Program"=>"");
writeBody("Add $testName Program",$msg, $crumbs);
$now=date('m/d/Y');
$end=date('m/d/Y',strtotime("$now + 1 months"));
?>
<form name="addfrm" action="addpgm.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="tid" value='<?=$tid?>'>
<table border=1 cellpadding=5>

<tr>
<td align=left>Program Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="descr" value="" maxlength="255"></td>
</tr>

<tr>
<td align=left>Start Date<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="startdt" value="<?=$now?>"></td>
</tr>

<tr>
<td align=left>End Date<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="enddt" value="<?=$end?>"></td>
</tr>

<tr>
<td align=left><span title="Recieve an email each time a candidate completes their assessment.">Send Completion Email</span></td>
<td align=left><input name="cemail" type="radio" value="N" /> No    
               <input name="cemail" type="radio" value="Y" checked/> Yes</td>
</tr>

<tr>
<td colspan=2 align="left">
<input type="button" onClick="javascript:chkForm(addfrm);" value="Add!">
</td>
</tr>

</table>
</form>
<small>Mandatory fields are marked </small><font color="#ff0000">*</font>.
<?php
/* 4/4/08 commented out because we now have a left menu (cdz)
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
*/
?>
<script language="Javascript">
function chkForm(frm){
    if(frm.descr.value.length<1||frm.startdt.value.length<1||frm.enddt.value.length<1){
	alert("Please provide all mandatory fields!");
    }
    else{
	frm.what.value='add';
        frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>

<?php
$msg="";

session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$conid=$_SESSION['conid']; // consultant id

require_once "../meta/program.php";
require_once "../meta/candidate.php"; 

if(!isset($pid)) {
  if(isset($_REQUEST['pid'])){
    $pid=$_REQUEST['pid'];
  }else{
  //die('We need a program ID!');
  }
}
$ckPost="";
foreach($_POST as $key=>$val){
  $ckPost.="$key => $val <br>";
}

if((!isset($_POST['tid'])) && (isset($_SESSION['tid']))) {
	$_POST["tid"] = $_SESSION['tid'];
	$_POST["tmv"] = "/cons/listpgm.php?^tid=" . $_POST["tid"];
	//unset($_SESSION['tid']);
}

$tid=$_POST['tid'];

$mode = ((isset($_REQUEST['mode']))&&($_REQUEST['mode']!=""))?  $_REQUEST['mode'] : 0;
 
$progname=$_POST['progname'];

switch ($tid) {
	case 1 : $testName = "CDP 360"; break;
	case 3 : $testName = "CDP Individual"; break;
	default : $testName = ""; break;
}
/* if(!($pgmData=pgmInfo($pid)))
    die("Invalid program code");
*/

$msg="";

require_once "consfn.php";

writeHead("Conflict Dynamics Profile - Consultants",false);
  if ($tid == 1) {
    $crumbs = array("Home"=>"home.php", "CDP 360"=>"cdp360.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "Participant Management for Program"=>"candhome.php?^tid=$tid&^pid=$pid", "Upload Participants"=>"");
  } else {
    $crumbs = array("Home"=>"home.php", "CDP Individual"=>"cdpindividual.php", "Manage Active $testName Programs"=>"listpgm.php?^tid=$tid&^pid=$pid", "Participant Management for Program"=>"candhome.php?^tid=$tid&^pid=$pid", "Upload Participants"=>"");
  }

    writeBody("<h2>Import Participants from an Excel Spreadsheet</h2><h3>Program $pid</h3><h3>$progname</h3>".stripslashes($pgmData[1]),$msg,$crumbs);
  
  
		if (count($emails) == 1) {
			$emailField = $emails[0];
			$emailWord = "email";
		} else {
			$emailField = '<span style="font:bold;" title="Email address will be substituted for each participant">[EMAILID]</span>';
			$emailWord = "emails";
		}


//***** START UPLOAD FUNCTIONALITY *****//

error_reporting(E_ALL);
//set_time_limit(0);

// $pid="111222333";


$fileStr="";
$errMsg="";

header('Content-Type: text/html; charset=utf-8');
//=======================================================================================
// View Logic - Begin
//=======================================================================================

switch($mode){
  case 2 :  // File upload preview
    $errMsg=uploadFile($pid,$tid);
    break;
  case 3 :  // Process selected records to be inserted
    $errMsg=addParticipants($ckPost);
    break;
  case 1 :  // Initial state to select file for upload
  default : // Same as case 1
    echo renderUploadForm($pid,$tid,$errMsg);
    drawExample();
    break;
}

?>

<form name="resetfrm" id="resetfrm" action="candupload.php" method=POST>
  <input type="hidden" name="pid" value="<?=$pid?>">
  <input type="hidden" name="tid" value="<?=$tid?>">
</form>

<form name="frm1" action="candhome.php" method=POST>
  <input type="hidden" name="pid" value="<?=$pid?>">
  <input type="hidden" name="tid" value="<?=$tid?>">
  <input type="hidden" name="cid" value="">
  <input type="hidden" name="uid" value="">
  <input type="hidden" name="pin" value="">
  <input type="hidden" name="what" value="">
  <input type="hidden" name="tmv"  id="tmv"  value="/cons/listpgm.php?^tid=<?=$tid?>">
  <input type="hidden" name="conid" value="<?=$conid?>">
</form>

<?php
echo "<hr />$errMsg <hr />"; 
//============================================================================================
// View Logic - End
//============================================================================================

$urls=array('candhome.php');
$txts=array('Back');
menu($urls,$txts, "frm1");

// Display variables for development and testing
//print_r('<pre>'.get_defined_vars().'</pre>'); 


writeFooter(false);


//============================================================================================
// Functions
//============================================================================================
function uploadFile($pid,$tid){
  $retMsg="";
  if($_FILES['pfile']['name']!=""){
  
    include '../classes/PHPExcel/IOFactory.php';
  
    $allowedExts = array("xlsx", "xls", "csv");
  
    $extension = end(explode(".", $_FILES['pfile']['name']));
  
    $fileStr = showFileData($extension);
  
    //Check to see if the extension is one we can use.
  
    if(in_array(strtolower($extension),$allowedExts)){
  
      //Process File
      $bdir = "/home/ptl/upload/participants";
      
      $uploadedFile = $bdir.'/participants_' . $pid .'.'.strtolower($extension);
$uploadedFile = '/tmp/participants_' . $pid . '.' . strtolower($extension);     
      //Is this an uploadable file?
      
      if(is_uploaded_file($_FILES['pfile']['tmp_name'])){
        //Have we moved it over to the server directory?
//copy($_FILES['pfile']['tmp_name'], '/home/ptl/testfile.tmp');
        if (!move_uploaded_file($_FILES['pfile']['tmp_name'], $uploadedFile)) {
    			$retMsg.='<div style="color:red;">File '.$_FILES['pfile']['tmp_name'].' could not be uploaded to '.$uploadedFile.'.</div>';
    		} else {
        
    		$newParticipants=getUploadParticipants($uploadedFile, true);
        
        // Check number of new participants
        // adjust count to remove header row
        $numNewParticipants = count($newParticipants) - 1;
  
        if ($numNewParticipants == 0) {
          echo("There are no participants to import in the file.");
        }else{
          buildUploadTable($newParticipants, $numNewParticipants, $pid,$tid);
        }
       }
    	}else{
    	   $retMsg.='<div style="color:red;">File '.$_FILES['pfile']['tmp_name'].' is not an uploadable file.</div>';
    	}
  		
    }else{
      $retMsg.="$extension files are not able to be used for upload.<br /> Please use a CSV, XLS, or XLSX file.";
    }
  }
  return $retMsg; 
}

function renderUploadForm($pid,$tid,$errMsg=''){
  $post_action=basename(__FILE__,'');
  $UpForm="<p>$errMsg</p>
    <form action=\"$post_action\" method=\"post\" enctype=\"multipart/form-data\">
  <label for=\"file\">Filename:</label><br />
  <input type=\"file\" name=\"pfile\" size=\"40\" id=\"file\"><br>
  <input type=\"hidden\" name=\"pid\" value=\"$pid\" >
  <input type=\"hidden\" name=\"tid\" value=\"$tid\" > 
  <input type=\"hidden\" name=\"mode\" value=\"2\">   
  <input type=\"submit\" name=\"what\" value=\"Import\">
  </form>";
  return $UpForm;  
}

function drawExample(){
  $example= <<<EOT
  
  <div id="uploadExample" style="width:65%; background-color:#f8f8f8;margin: 10px; padding:10px;  border: 1px double #999;"><!-- xls example -->
  <p>The spreadsheet should contain the three columns as show below:</p>
  <p>To download an Excel template file right click on the participants.xls link below and then select the "Save Target As" option.
  <p><a href="../upload/samples/participants.xls">participants.xls</a></p>

  <table cellspacing="0" cellpadding="2px" style="font-family: calibri, sans serif !important; border-top: 2px solid #999; background-color:#FFF;">
    <tr style="background-color:#F0E4E2; " >
      <th style="border-bottom: 1px solid #999; border-right: 1px solid #999;"></th>
      <th style="border-bottom: 1px solid #999; border-left: 1px solid #999; border-right: 1px solid #999;">A</th>
      <th style="border-bottom: 1px solid #999; border-left: 1px solid #999; border-right: 1px solid #999;">B</th>
      <th style="border-bottom: 1px solid #999; border-left: 1px solid #999;">C</th>
    </tr>
    
    <tr >
      <td style="background-color:#F0E4E2; border-top: 1px solid #999; border-bottom: 1px solid #999; border-right: 1px solid #999;">1</td>
      <td style="border: 1px solid #999;">First Name</td>
      <td style="border: 1px solid #999;">Last Name</td>
      <td style="border-top: 1px solid #999; border-bottom: 1px solid #999;">Emailid</td>
    </tr>    
    <tr>
      <td style="background-color:#F0E4E2; border-top: 1px solid #999; border-bottom: 1px solid #999; border-right: 1px solid #999; ">2</td>
      <td style="border: 1px solid #999;" >Ward</td>
      <td style="border: 1px solid #999; ">Cleaver</td>
      <td style="border-top: 1px solid #999; border-bottom: 1px solid #999; border-left: 1px solid #999;"><a href="">ward@thebeaver.com</a></td>
    </tr>    
    <tr>
      <td style="background-color:#F0E4E2; border-top: 1px solid #999; border-bottom: 1px solid #999; border-right: 1px solid #999; ">3</td>
      <td style="border: 1px solid #999;">Beaver</td>
      <td style="border: 1px solid #999;">Cleaver</td>
      <td style="border-top: 1px solid #999; border-bottom: 1px solid #999; border-left: 1px solid #999;"><a href="">beaver@thebeaver.com</a></td>
    </tr>    
    <tr>
      <td style="background-color:#F0E4E2; border-top: 1px solid #999; border-bottom: 1px solid #999; border-right: 1px solid #999; ">4</td>
      <td style="border: 1px solid #999;">June</td>
      <td style="border: 1px solid #999;">Cleaver</td>
      <td style="border-top: 1px solid #999; border-bottom: 1px solid #999; border-left: 1px solid #999;"><a href="">june@thebeaver.com</a></td>
    </tr>    
    <tr>
      <td style="background-color:#F0E4E2; border-top: 1px solid #999; border-bottom: 2px solid #999; border-right: 1px solid #999; ">5</td>
      <td style="border-top: 1px solid #999; border-bottom: 2px solid #999; border-left: 1px solid #999; border-right: 1px solid #999;">Wally</td>
      <td style="border-top: 1px solid #999; border-bottom: 2px solid #999; border-left: 1px solid #999; border-right: 1px solid #999;">Cleaver</td>
      <td style="border-top: 1px solid #999; border-bottom: 2px solid #999; border-left: 1px solid #999;"><a href="">wally@thebeaver.com</a></td>
    </tr>    
  </table>

</div><!-- END uploadExample -->
EOT;
 echo $example;

}

function getUploadParticipants($uploadedFile,$dbug=false){
  //Now lets try reading the file.

  $inputFileType = PHPExcel_IOFactory::identify($uploadedFile);  
  $objReader = PHPExcel_IOFactory::createReader($inputFileType);
  $objPHPExcel = $objReader->load($uploadedFile);
  $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

  $lbs=false;
  $dbugTxt='<hr />'.$inputFileType.'<hr />';
  $r=0;
  $columns=array('A'=>'firstname','B'=>'lastname','C'=>'emailid','D'=>'error'); 
  $newParticipants = array();
  $newEmailids = array();
          
  foreach($sheetData as $row){
    $newDuplicateEmail = false;

    //clean data from file removing spaces, new lines, and tabs
    $fname=ereg_replace("[\r\n\t\ ?]", "", trim($row['A']));
    $lname=ereg_replace("[\r\n\t\ ?]", "", trim($row['B']));
    $emailid=ereg_replace("[\r\n\t\ ?]", "", trim($row['C']));
    //die("fname = $fname | lname = $lname | emailid = $emailid ");
    
    if((strlen(trim($fname)) > 0)||(strlen(trim($lname)) > 0)||(strlen(trim($emailid)> 0))){
      $dbugTxt.="<div>";  
      //Add to newParticipants array
      $newParticipants[$r][$columns['A']]=$fname;
      $newParticipants[$r][$columns['B']]=$lname;
      $newParticipants[$r][$columns['C']]=$emailid;
      $newParticipants[$r][$columns['D']]='';
     
      //die("<hr />".print_r($newParticipants));
                           
      //Check for duplicate email addresses in previous records
      if (in_array(strtolower($emailid), $newEmailids)) {
  				$newDuplicateEmail = true;
  		}
      
      $newEmailids[] = strtolower($emailid);
   
      // Missing first name  
  		// if(!strlen($newParticipants[$r]['firstname'])) $newParticipants[$r]['error'] .= 'f';
      if(!strlen($newParticipants[$r]['firstname'])) $newParticipants[$r]['error'] .= 'No first name. ';
      // Missing last name
	    // if(!strlen($newParticipants[$r]['lastname'])) $newParticipants[$r]['error'] .= 'l';
	    if(!strlen($newParticipants[$r]['lastname'])) $newParticipants[$r]['error'] .= 'No last name. ';
      // Missing emailid
    	// if(!strlen($newParticipants[$r]['emailid'])) $newParticipants[$r]['error'] .= 'e';
      if(!strlen($newParticipants[$r]['emailid'])) $newParticipants[$r]['error'] .= 'No email. ';
  		// Invalid emailid
 /*  		if(!validEmail($newParticipants[$r]['emailid'])) $newParticipants[$r]['error'] .= 'i';
 */     // Duplicate email
      // if($newDuplicateEmail) $newParticipants[$r]['error'] .= 'd'; 
      if($newDuplicateEmail) $newParticipants[$r]['error'] .= 'Duplicate email. '; 

      $dbugTxt.="<span style=\"color:#0000FF;font-weight:normal;\">[".$columns['A']."] => ".$newParticipants[$r]['firstname']."</span> |";
      $dbugTxt.="<span style=\"color:#0000FF;font-weight:normal;\">[".$columns['B']."] => ".$newParticipants[$r]['lastname']."</span> |";
      $dbugTxt.="<span style=\"color:#0000FF;font-weight:normal;\">[".$columns['C']."] => ".$newParticipants[$r]['emailid']."</span> |";
      $dbugTxt.="<span style=\"color:#0000FF;font-weight:normal;\">[".$columns['D']."] => ".$newParticipants[$r]['error']."</span> |";            
      $dbugTxt.="</div>";
      $r++;
    }else{
    }
  }
  if($dbug)
    // Display array of uploaded xls for testing
    // echo $dbugTxt;
    
    //die(print_r($newParticipants));
  return $newParticipants;
}

function showFileData($extension){
  $str="";
  if ($_FILES["pfile"]["error"] > 0){
    $str.= "Error: " . $_FILES["pfile"]["error"] . "<br>";
  }else{
    $str.= "Upload: " . $_FILES["pfile"]["name"] . "<br>";
    $str.= "Type: " . $_FILES["pfile"]["type"] . "<br>";
    $str.= "Size: " . ($_FILES["pfile"]["size"] / 1024) . " kB<br>";
    $str.= "Stored in: " . $_FILES["pfile"]["tmp_name"]. "<br>";
    $str.= "File extension is: ".$extension. "<br>";
  }
  return $str;  
}

function buildUploadTable($newParticipants, $numNewParticipants, $pid, $tid) {
$conid=$_SESSION['conid'];
$uploadInstructions = <<<EOD
<h1 style="max-width: 30em;">Please select the participants you wish to copy and then click the "Import Selected Participants" button.</h1>
EOD;


echo $uploadInstructions;

$newTable = <<<EOD

<div>
  
  <form name="frmSaveImport" id="frmSaveImport" action="candupload.php?i=2&pid=$pid" method = "post">
    <input type="hidden" name="bastardvariable" value="Import">
    <input type="button" name="btnContinue" value="Import Selected Participants" title="Import the selected participants to Program $pid." onclick="importParticipants();" style="margin: 20px;" />
    <input type="button" name="btnCancelImport" value="Cancel" onclick="cancelImport()" />
    
EOD;


$newTable .= <<<EOD

  <table cellspacing=0 style="width: 75%; border: 1px solid black">
    <thead>
      <tr style="border: 1px solid black">
        <td style="border: 1px solid black; width: 40px; " >&nbsp;</td>
        <td style="border: 1px solid black; width: 40px; text-align: center; font-weight: 800; " >Select</td>
        <td style="border: 1px solid black; text-align: center; font-weight: 800; " >Name</td>
        <td style="border: 1px solid black; text-align: center; font-weight: 800; " >Email</td>
        <td style="border: 1px solid black; text-align: center; font-weight: 800; " >Language</td>        
      </tr>
    </thead>
    <tbody>
EOD;

for ($i = 1; $i<=$numNewParticipants; $i++) {
  
  $firstname = $newParticipants[$i]['firstname'];
  $lastname = $newParticipants[$i]['lastname'];
  $emailid = $newParticipants[$i]['emailid'];
  $errorStr = $newParticipants[$i]['error'];
  $langSelct = conLanguageBox($tid,$conid,'I'.$i);
  $hiddenVars = '<input type="hidden" name="firstname' . $i . '" value="' . htmlentities($firstname) . '" />';
  $hiddenVars .= "\n\t\t\t" . '<input type="hidden" name="lastname' . $i . '" value="' . htmlentities($lastname) . '" />';
  $hiddenVars .= "\n\t\t\t" . '<input type="hidden" name="emailid' . $i . '" value="' . htmlentities($emailid) . '" />';
  $cbx="cbx".$i;
  $ckd=(!strlen($errorStr))? "checked " : "";
  $newTable .= <<<EOD
    <tr>
      <td style="border: 1px solid black; text-align: center; ">$i</td>
      <td style="border: 1px solid black; text-align: center; "><input type="checkbox" name="$cbx" value="OK" $ckd/>$hiddenVars</td>
      <td style="border: 1px solid black; padding-left: 5px;">$firstname $lastname</td>
      <td style="border: 1px solid black; padding-left: 5px;">$emailid <span style="color: red">$errorStr</span></td>
      <td style="border: 1px solid black; padding-left: 5px;">$langSelct</td>
    </tr>
EOD;
  
}
  
$newTable .= <<<EOD
    </tbody>
  </table>
  <input type="hidden" name="pid" value="$pid" >
  <input type="hidden" name="tid" value="$tid" >   
  <input type="hidden" name="mode" value="3">   
</form><!-- frmSaveImport -->
</div>
EOD;

print_r($newTable);

$uploadJS = <<<EOD
<script type="text/javascript">

var validSelection = false;

window.onbeforeunload = confirmSelectionMade;

function confirmSelectionMade() {

  if (!validSelection) {
		return "You have not clicked the \"Import Selected Participants\" button to import the participants.\\nPlease click Cancel to go back and import the participants or OK to continue without importing.";
  }
}

function cancelImport() {
	validSelection = true;
  document.getElementById("resetfrm").submit();
}

function importParticipants() {
  validSelection = true;
  document.getElementById("frmSaveImport").submit();
}

function clickErrorEntry( id, msg ) {
	alert(msg);
	document.getElementById(id).checked = false;
	return false;
}

</script>

EOD;

echo $uploadJS;

}

function addParticipants($ckPost) {
  //die(print_r($ckPost));
  $t=0; $x=0;
  $pid=$_POST['pid'];
  $tid=$_POST['tid'];
  $conid=$_SESSION['conid'];
  $hasOth=($tid == 3)? "N" : "Y";  
  //Step 1 - Verify that we have Participant Data
  //Step 2 - Loop through Participant(s) date from POST and pass to candInsert()
  foreach($_POST as $k=>$v){
    if(substr($k,0,3)=='cbx'){
      $idx=substr($k,3,strlen($k)-3);  
      $fnam='firstname'.$idx;
      $lnam='lastname'.$idx;
      $email='emailid'.$idx;
      $lang='lid'.$idx;
      $candData=array($pid,$_POST[$fnam],$_POST[$lnam],$_POST[$email],"Y",$hasOth,$conid,$_POST[$lang]);
      if(candInsert($candData))
        $t++;
      else
        $x++;
    }  
  }
  //Step 3 - Return Status message
  $retMsg="";
  $retMsg.=($t>0)? "<font color='#00aa00'>Successfully added $t participant(s)<font>" : "";
  $retMsg.=($x>0)? "<font color='#aa0000'>Error when adding $x participant(s).</font>" : "";
  if(strlen($retMsg))
    return $retMsg;
  else
    return false;
}

?>

<?php

// 4/1/8 - Added for left menu - CDZ
@session_start();
require_once "/var/www/html/meta/MenuTree.class.php";
require_once "/var/www/html/meta/dbfns.php";

$showOrderForm=true; //Turn orderform linkage On/Off

// Common functions for the consultant site
function writeHead($title,$buffered=false,$script=false){
    if($buffered){
	   ob_start();
    }

    $tempid =(isset($_SESSION['tempid']))? $_SESSION['tempid'] : 1;
    switch($tempid){
     case 4 :
     case 3 :
      $css="custom/ICD/styles_backend.css";
      break;
     default :
      $css="../index_files/styles_backend.css";
      break;
    }
 
  	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"; 
	  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">"; 
    echo "<head>"; 
    echo "<title>$title</title>"; 
    echo "<link href=\"$css\" rel=\"stylesheet\" type=\"text/css\" />"; 
	  if($script)
	    echo "$script"; 
	  echo "<script type=\"text/javascript\" src=\"../meta/sortabletable/sortabletable.js\"></script>\n";
	  echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../meta/sortabletable/sortabletable.css\" />\n";
    echo "</head>";	  
}

function writeBody($title,$msg,$crumbs=array()){
  $showCart=array(5732010,307734);  
  
  $conid=((isset($_SESSION['conid']))&&($_SESSION['conid']!=""))? $_SESSION['conid'] : 0;
  $orgid=getOrgID($conid);
  
  $excludeTtls=($orgid > 2)? array(14,15,16) : array();  
  echo"<body class=\"internal\"> 
	       <div id=\"wrapper\"> 
		        <div id=\"main\"> 
 			        <div id=\"banner\"> 
                <h5><a href=\"../admin/home.php\">CCD Home</a></h5> 
                <h6>Home of the Conflict Dynamics Profile</h6> 
			        </div><!-- end banner --> 
			        <div id=\"content\">";
    
		// 4/1/8 - Added for left menu - CDZ
		echo "\n\n" . '<table width="100%">';
		echo "\n\t" . '<tr>';
		echo "\n\t\t" . '<td style="vertical-align:top;background-color:#f0f0f0;" width="200">';
    //$menu=(in_array($conid,$showCart))? 'menuCart.txt' : 'menuNoCart.txt';  //Turn orderform linkage On/Off
    $menu='menuCart.txt'; 
		if (empty($_SESSION['admid'])) {
			// Administrator impersonating a consultant
      $excludeTtls[]=19; 
      $exclude=getMenuTitles($excludeTtls); 
      //die(print_r($exclude));	       
			$tree = new MenuTree("cons", getRelativePath() . $menu, '', $exclude);
		} else {
			// Consultant logged in (ie, trainer is not impersonating the consultant)
			$excludeTtls[]=20; 
			$exclude=getMenuTitles($excludeTtls);	
      //die(print_r($exclude));		
			$tree = new MenuTree("cons", getRelativePath() . $menu, '', $exclude);
		}
		$tree->show();
		//$tree->dump();
		echo "\n\t\t" . '</td>';
		echo "\n\t\t" . '<td style="vertical-align:top;">';

    if (count($crumbs) > 0) echo '<div style="padding-left:6px;">' . breadCrumbs($crumbs) . '</div>';
    echo "\n" . '<center>';
    echo "<font face='Arial, Helvetica, Sans-Serif'><h2>$title</h2>$msg<br />\n";   			        
}

function writeFooter($buffered){
    if($buffered){
    	ob_end_flush();
    }
    $tempid =(isset($_SESSION['tempid']))? $_SESSION['tempid'] : 1;
        
		// 4/1/8 - Added for left menu - CDZ
		echo "\n\t\t" . '</td>';
		echo "\n\t" . '</tr>';
		echo "\n" . '</table>';
 if($tempid<=2){		
  echo"</div><!-- end content --> 
		  </div><!-- end main -->
		  <div id=\"footer\"> 
      <div id=\"extras\"> 
    <div id=\"copyright\"> 
    <p>Conflict Dynamics Profile is a registered trademark of Eckerd College</p> 
    </div> 
    <!-- end copyright --> 
    <a href=\"http://www.eckerd.edu\"><img alt=\"Eckerd College Home\" class=\"left\" height=\"66\" src=\"../index_files/foot_logo.gif\" title=\"Eckerd College Home\" width=\"207\"/></a> 
    <p>4200 54th Avenue South<br/> 
    St. Petersburg, Florida 33711<br/> 
    888-359-9906 | <a href=\"mailto:cdp@eckerd.edu\">cdp@eckerd.edu</a></p> 
    </div><!-- end extras --> 
 		</div><!-- end footer -->	
	 </div><!-- end wrapper --> 
  </body> 
  </html>"; 
 }else{
echo "      </DIV><!-- end content -->
    </DIV><!-- end main -->
    <DIV id=footer>
      <DIV id=extras>
        <DIV id=copyright>
        <P>Authorized exploitation of the English edition © 1999 Center <br />
           for Conflict Dynamics / USA for this edition, published and<br />
           sold by permission of Center for Conflict Dynamics, the <br />
           owner of all rights to publish and sell the name.</P></DIV><!-- end copyright -->
          <!--<A href=\"\">
            <IMG class=left title=\"Name of Organization Home\" alt=\"Name of Organization Home\" src=\"custom\ICD\foot_logo.gif\" width=207 height=66>
          </A>--> 
          <P>© Institute for Conflict Dynamics Europe GmbH<br />
          Kurfürstendamm 184<br />
          10707 Berlin, Germany;<br />
          Contact : <a href=\"mailto:service@conflict-institute.com?Subject=Assessment%20Center\">service@conflict-institute.com</a><br />
          Info : <a href=\"www.conflict-institute.com\" target=\"_blank\">www.conflict-institute.com</a></P>
      </DIV><!-- end extras -->
    </DIV><!-- end footer -->
  </DIV><!-- end wrapper -->
</BODY> 
</HTML>";
 }       
}

function menu($url,$txt,$frm){
    # if no form is passed in, well create one called frm1
    $len=strlen($frm);
    if(0==$len){
		echo "<form name='frm1' method=POST>";
		$frm="frm1";
    }

    echo "<table border=0 cellpadding=5>";
    $cnt=count($url);
    $cnt-=1;
    for($i=0;$i<=$cnt;$i++){
		if(strlen($txt[$i])>0){
			// only if we have text to go with it
			echo '<tr> <td align="left" onClick="javascript:'.$frm.'.action=\''.$url[$i].'\';'.$frm.'.submit();">';
			if($i!=$cnt){
			   echo '<img src="../images/g.gif" onMouseOver="this.src=\'../images/b.gif\';"';
			   echo ' onMouseOut="this.src=\'../images/g.gif\';" border=0> &nbsp;'.$txt[$i].'</td></tr>';
			}
			else{
			   # last option has a red bullet for log out, back etc.
			   echo '<img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
			   echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp;'.$txt[$i].'</td></tr>';
			}
		}
    }
    echo "</table>";

    if(0==$len){
		echo "</form>";
    }
}

function breadCrumbs( $crumbs ) {
	/**
	*
	*	This function takes an array of link/label breadcrumbs and returns them in a HTML string.
	* The link can contain request variables.  If the request variable name begins with the character ^
	* it will be used as a POST variable.  For example, for the link:
	*          prgmlist.php?tid=666&pid=12345&^testid=8
	* In this example the variables `tid` and `pid` are GET variables and `testid` is a POST variable.
	*
	*	@param $crumbs  : breadcrumb array
	*
	*	@return HTML breadcrumb string
	*
	**/
	 
	$postVars = "";

	$str = "\n" . '<div class="breadcrumbs">';
	$str .= "\n\t" . '<table width="98%" border="0" cellpadding="0" cellspacing="0">';
	$str .= "\n\t\t" . '<tr>';
	$str .= "\n\t\t\t" . '<td width="99%" nowrap="nowrap">';

	$count = 0;
	foreach ($crumbs as $label => $link) {
		$link = str_replace('"', '&quot;', $link);
		if ($count != 0) $str .= ' &gt; ';
		if (strlen($link) == 0) {
			$str .= $label;
		} else {
			if (strpos($link, "?") === false) {
				// No variables
				$str .= "<a href=\"$link\">$label</a>";
				
			} else {
				// Check variables
				$url = trim(substr($link, 0, strpos($link, "?")));
				$vars = trim(substr($link, strpos($link, "?") + 1));
				if (strpos($link, "^") === false) {
					// Simple link with $_GET variables
					$str .= "<a href=\"$link\">$label</a>";
					
				} else {
					// Has post variables
					$vars = explode("&", $vars);
					$getList = "";
					for ($i=0; $i<count($vars); $i++) {
						$vals = explode("=", $vars[$i]);
						$var = str_replace('"', "&quot;", trim($vals[0]));
						if (isset($vals[1])) $val = str_replace('"', "&quot;", $vals[1]); else $val = "";
						
						if ((strlen($var) > 0) && (substr($var, 0, 1) == "^")) {
							$var = substr($var, 1);
							$postVars .= "<input type=\"hidden\" name=\"$var\" value=\"$val\" />";
						} else {
							if (strlen($getList) > 0) $getList .= "&";
							$getList .= "$var=$val";
						}
					}
					if (strlen($getList) > 0) $str .= "<a href=\"#\" onclick=\"javascript:submitCrumb('$url?$getList');\">$label</a>";
						else $str .= "<a href=\"#\" onclick=\"javascript:submitCrumb('$url');\">$label</a>";
					
				}
			}
		}
		$count++;
	}
	$str .= "\n\t\t\t" . '</td>';	

	// -----------------------------------------
	// Add a button at far right
	// -----------------------------------------
	$buttonStyle = 'color:#fcfcfc;background-color:#4f8d97;font:bold 7pt tahoma;text-decoration:none;padding:0px 3px 1px 3px;margin:0px 4px 0px 0px;';
	if (empty($_SESSION['admid'])) {
		$str .= "\n\t\t\t" . '<td>';
		$str .= "\n\t\t\t\t" . '<form name="frmBCLogout" id="frmBCLogout" action="" method="POST" style="margin:0px;"><a href="#" onclick="javascript:frmBCLogout.action=\'index.php\';frmBCLogout.submit();" style="' . $buttonStyle . '">Logout</a></form>';
		$str .= "\n\t\t\t" . '</td>';
	} else {
		$str .= "\n\t\t\t" . '<td >';
		$str .= "\n\t\t\t\t" . '<form name="frmBCDone" id="frmBCDone" action="" method="POST" style="margin:0px;"><a href="#" onclick="javascript:frmBCDone.action=\'../admin/listcons.php\';frmBCDone.submit();" style="' . $buttonStyle . '" title="Return to Administrator Menu">Done</a></form>';
		$str .= "\n\t\t\t" . '</td>';
	}
	$str .= "\n\t\t" . '</tr>';
	$str .= "\n\t" . '</table>';
	$str .= "\n" . '</div>';

	if (strlen($postVars) > 0) {
		$str .= <<<EOD

<form name="frmSubmitCrumb" id="frmSubmitCrumb" action="" method="POST" style="margin:0px;">
$postVars
</form>

<script language="Javascript">
function submitCrumb(action) {
	document.frmSubmitCrumb.action = action;
	document.frmSubmitCrumb.submit();
	return false;
}
</script>

EOD;
	}
	
	return $str;
	
}

function getRelativePath() {
	/**
	*
	*	This method returns the relative path of this file with reference to the main PHP file.
	*
	**/

	// File Include Path		
	if (strpos(dirname(__FILE__), '/') !== false) {
		// Linux, etc.
		$includePath = explode('/', dirname(__FILE__));
	} else {
		// Windows
		$includePath = explode("\\", dirname(__FILE__));
	}
	if ((count($includePath) > 0) && (strlen($includePath[0]) == 0)) array_shift($includePath);
		
	// File Base Path
	$basePath = explode("/", $_SERVER['SCRIPT_FILENAME']);
	if (count($basePath) > 0) array_pop($basePath);

	if ((count($basePath) > 0) && (strlen($basePath[0]) == 0)) array_shift($basePath);

	while (((count($includePath) > 0) && (count($basePath) > 0)) && ($includePath[0] == $basePath[0])) {
		array_shift($includePath);
		array_shift($basePath);
	}

	$done = false;
	$i = 0;		// To prevent endless loop
	while ((count($includePath) > 0) && (count($basePath) > 0) && !$done && ($i <40)) {
		if ($includePath[0] == $basePath[0]) {
			array_shift($includePath);
			array_shift($basePath);
		} else { 
			$done = true;
		}
		$i++;
	}

	$relativePath = array();
	for ($i=0; $i<count($basePath); $i++) $relativePath[] = "..";
	for ($i=0; $i<count($includePath); $i++) $relativePath[] = $includePath[$i];

	if (count($relativePath) > 0) {
		return implode("/", $relativePath) . "/";
	} else {
		return "";
	}

	return true;
		
}

function numSelfOnlyLicensesAlreadyConsumed( $pid ) {
	// This method returns the number of licenses already consumed for a self-only program
	$conn=dbConnect();
	$rs=mysql_query("select count(b.CID) from CANDIDATE a, SELFLIC b where a.PID = $pid AND b.CID = a.CID");
	$row=mysql_fetch_row($rs);
	return $row[0];
}

function numSelfOnlyLicensesNeeded( $pid ) {
	// This method returns the number of licenses needed to score a self-only program
	$conn=dbConnect();
	
	// Get number of candidates complete
	$rs=mysql_query("select count(b.CID) from CANDIDATE a, RATER b where a.PID = $pid and b.EXPIRED = 'Y' and b.CID = a.CID");
	//echo "<hr>select count(b.CID) from CANDIDATE a, RATER b where a.PID = $pid and b.EXPIRED = 'Y' and b.CID = a.CID<hr>";
	$row=mysql_fetch_row($rs);
	$numComplete = $row[0];
	
	// Get number of licenses already consumed
	$rs=mysql_query("select count(b.CID) from CANDIDATE a, SELFLIC b where a.PID = $pid and b.CID = a.CID");
	//echo "<hr>select count(b.CID) from CANDIDATE a, SELFLIC b where a.PID = $pid and b.CID = a.CID<hr>";
	$row=mysql_fetch_row($rs);
	$numAlreadyConsumed = $row[0];

	//echo "<li> $numComplete - $numAlreadyConsumed";
	$numNeeded = $numComplete - $numAlreadyConsumed;

	return $numNeeded;
	
}

function num360LicensesNeeded( $pid ){
	// This method returns the number of licenses needed to score a CDP 360 program
	$conn=dbConnect();
	
	// Get number of SELF raters complete	
	$sql="select count(b.CID) from CANDIDATE a, RATER b where a.PID = $pid and b.EXPIRED = 'Y' and b.CID = a.CID and b.CATID = 1";
	$rs=mysql_query($sql);
	$row=mysql_fetch_row($rs);
	$numComplete = $row[0];
	
	// Get number of licenses already consumed	
  $sql="select count(CID) from CANDLIC where TID = $pid";
	$rs=mysql_query($sql);
	$row=mysql_fetch_row($rs);
	$numConsumed = $row[0]; 
  
  $numNeeded = $numComplete - $numConsumed; 
  
  return $numNeeded;	
}

function numLicensesAvailable( $conid, $tid ) {
	// This method returns the number of licenses that a consultant has available for
	// a specific survey.
	
	$conn=dbConnect();
	
	// Get number of licenses bought
	$rs=mysql_query("select sum(CNT) from LICENSE where CONID = $conid and INSTR = $tid");

	$row=mysql_fetch_row($rs);
	$numBought = $row[0];
	
	// Get number of licenses consumed
	if ($tid == 3) {
		// Self-Only survey (Individual)
		$rs=mysql_query("select COUNT(CID) from SELFLIC where CONID = $conid and TID = $tid");
		$row=mysql_fetch_row($rs);
		$numConsumed = $row[0];
	} else if ($tid == 1) {
		// 360 Survey
		$rs=mysql_query("select COUNT(CID) from CANDLIC where CONID = $conid");
		$row=mysql_fetch_row($rs);
		$numConsumed = $row[0];
	} else {
		$numConsumed = 0;
	}
	
	//echo "<li>Bought / Consumed = $numBought - $numConsumed";
	$numAvailable = $numBought - $numConsumed;
	
	return $numAvailable;
	
}

function hasSpecialLanguagePrivs($conid,$testid,$desc=false){
  $qry ="select a.LID, b.DESCR from CONSPECIALLANG a, LANG b
          where a.LID = b.LID
            and CONID = $conid AND TESTID = $testid";
  //die($qry);
	$rows=fetchArray($qry);
	if(!$rows){
	  //die("No Rows");
		return false;
	}
  elseif($desc){
    $ret=($desc=="multiple")? $rows : $rows[0];
    return $ret;
  }else{
    $retval=""; $c=0;
    foreach($rows as $row){
      $retval.=($c==0)? $row['LID'] : ','.$row['LID'];
      $c++;
    }
    return $retval;
  }
}

function getOrgID($conid){

  $qry="SELECT ORGID FROM CONSULTANT WHERE CONID = $conid";
  $orgid=fetchOne($qry);
  return $orgid;  
}

function getMenuTitles($ids=array()){
  // Temporary function until we are ready to put the whole menu into the database
  // Titles are found on cons/menu.txt
    
  $titles=array("");
  $titles[] ="Home";                                  //  1
  $titles[] ="Products";                              //  2
  $titles[] ="CDP 360";                               //  3
  $titles[] ="Add a New Program";                     //  4
  $titles[] ="Manage Active Programs";                //  5
  $titles[] ="Archived Programs";                     //  6
  $titles[] ="Upload Scanned Survey";                 //  7
  $titles[] ="View Group Reports";                    //  8
  $titles[] ="CDP Individual";                        //  9
  $titles[] ="Add a New Program";                     // 10
  $titles[] ="Manage Active Programs";                // 11 
  $titles[] ="Archived Programs";                     // 12
  $titles[] ="View Group Reports";                    // 13
  $titles[] ="CDP Orders";                            // 14
  $titles[] ="CDP Order Form";                        // 15
  $titles[] ="CDP Order History";                     // 16
  $titles[] ="License History";                       // 17
  $titles[] ="Change Password";                       // 18  
  $titles[] ="Done";                                  // 19
  $titles[] ="Logout";                                // 20
/**/   
  $excludes = array();

  if(count($ids)>0){
    foreach($ids as $k=>$v)
      $excludes[]=$titles[$v];
  }

  return $excludes;       
}
?>

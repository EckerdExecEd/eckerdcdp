<?php
// Data collection for the Credit Card Gateway Interface
// Change History
// 07-27-2009: Added fields for Return page, Eckerd Owner Email and Invoice Number
// 07-31-2009: Rather extensive re-write to accomodate new requirement for self post-back, essentially
// incorporating all the functionality from receipt.php on this page itself
// The logic proposed by Eckerd's web team invloves a hidden field like so:
// Is this value present in the post array?
//	No - display the form
//	Yes but validation failed - display the form with submitted values in fields and with relevant errors.
//	Yes, no errors - move to the process form section of the code - where the gateway submission, email, etc. functions would finally be called.

session_start();
require_once "functionstest.php";
$showForm=true;

// Logic: Is this value present in the post array?
if(!isset($_POST['what'])||strlen($_POST['what'])<1){ 
	//	No - display the form
	$amount=stripslashes($_POST['amount']);
	//$amount = "1"; //<-- uncomment for testing
	$uniqueId=stripslashes($_POST['uniqueId']);
	$callingPage=stripslashes($_POST['callingPage']);
	$bannerCode=stripslashes($_POST['bannerCode']);
	$returnPage=stripslashes($_POST['returnPage']);
	$ownerEmail=stripslashes($_POST['ownerEmail']);
	$invoiceNumber=stripslashes($_POST['invoiceNumber']);
	$reg_type = stripslashes($_POST['type']);
	$firstName="";
	$mi="";
	$lastName="";
	$email1="";
	$email2="";
	$address1="";
	$address2="";
	$city="";
	$state="";
	$zip="";
	$errMsg="";
}
else{
	$amount=stripslashes($_SESSION['amount']);
	$uniqueId=stripslashes($_SESSION['uniqueId']);
	$callingPage=stripslashes($_SESSION['callingPage']);
	$bannerCode=stripslashes($_SESSION['bannerCode']);
	$returnPage=stripslashes($_SESSION['returnPage']);
	$ownerEmail=stripslashes($_SESSION['ownerEmail']);
	$invoiceNumber=stripslashes($_SESSION['invoiceNumber']);
	$reg_type = '';
	$firstName=stripslashes($_POST['firstName']);
	$mi=stripslashes($_POST['mi']);
	$lastName=stripslashes($_POST['lastName']);
	$email1=stripslashes($_POST['email1']);
	$email2=stripslashes($_POST['email2']);
	$address1=stripslashes($_POST['address1']);
	$address2=stripslashes($_POST['address2']);
	$city=stripslashes($_POST['city']);
	$state=stripslashes($_POST['state']);
	$zip=stripslashes($_POST['zip']);
	$data=array(
		'amount' => $amount,
		'uniqueId' => $uniqueId,
		'callingPage' => $callingPage,
		'bannerCode' => $bannerCode,
		'returnPage' => $returnPage,
		'ownerEmail' => $ownerEmail,
		'invoiceNumber' => $invoiceNumber,
		'firstName' => $firstName,
		'mi' => $mi,
		'lastName' => $lastName,
		'email1' => $email1,
		'email2' => $email2,
		'address1' => $address1,
		'address2' => $address2,
		'city' => $city,
		'state' => $state,
		'zip' => $zip,
		'cardType' => stripslashes($_POST['cardType']),
		'creditCardNumber' => stripslashes($_POST['creditCardNumber']),
		'month' => stripslashes($_POST['month']),
		'year' => stripslashes($_POST['year']),
		'ccv2' => stripslashes($_POST['ccv2'])
	);

	$errMsg=validateData($data);
	if(1>strlen($errMsg)){
		// Logic: Yes, no errors - move to the process form section of the code - where the gateway submission, email, etc. functions would finally be called.	
		$rs=processCC($data);
//print_r($rs);
//die();	
		if($rs['ssl_result']=="0"){
			// Everything went well
			$data['paymentId']=$rs['ssl_txn_id'];
			$data['approvalCode']=$rs['ssl_approval_code'];
			$data['name']=$data['firstName']." ".$data['lastName'];
			$data['last4']=substr($data['creditCardNumber'],strlen($data['creditCardNumber'])-4);
			$ccType=$data['cardType'];
			switch(intval($ccType)){
				case 1:
					$card="VISA";
					break;
				case 2:
					$card="Master Card";
					break;
				case 3:
					$card="Discover";
					break;
				case 4:
					$card="American Express";
					break;
				default:
					$card="";
				break;
			}
			$data['card']=$card;
			date_default_timezone_set('America/New_York');
			$data['transactionDate']=date('m/d/Y');
		
			// Store these in SESSION for generation printer friendly receipt in the absence of a MySQL database
			$_SESSION['paymentId']=$data['paymentId'];
			$_SESSION['approvalCode']=$data['approvalCode'];
			$_SESSION['name']=$data['name'];
			$_SESSION['last4']=$data['last4'];
			$_SESSION['card']=$data['card'];
			$_SESSION['transactionDate']=$data['transactionDate'];
		
			// 1. Send email if we have email address for the purchaser
			if(strlen($data['email1'])!=0){
				// we can safely assume that this is a valid email address since it passed the validation tests above
				sendConfirmation($data,$data['email1']);			
			}
			// 2. Always end copy of email to Eckerd owner, if we have a valid email address
			if(true==isValidEmailAddress($data['ownerEmail'])){
				sendConfirmation($data,$data['ownerEmail']);			
			}
			
			if ($reg_type == "EMP"){
				sendEmailReceiptToEMPAdmins($data);
			}else{
				sendEmailReceiptToCDPAdmins($data);
			}
			
			
			// 3. and we're done
			echo showReceipt($data);
			$showForm=false;

		}
		else{
			// Everything else is an error, get as much info as we can
			$errMsg="Sorry, we're unable to process your credit card<br />";
			if(0<strlen($rs['errorName'])){
				$errMsg.=$rs['errorName'];
			}
			if(0<strlen($rs['errorCode'])){
				$errMsg.=" (Code: ".$rs['errorCode'].")";
			}
			if(0<strlen($rs['errorMessage'])){
				$errMsg.="<br />".$rs['errorMessage'];
			}
			$errMsg.="<br />";
		}
	}		
//	else{
		// Logic: Yes but validation failed - display the form with submitted values in fields and with relevant errors.
//	}
}

// Placeholder for future expansion with other payment options, such as debit, bank account etc.
$paymentOption=1;

// The session value is used for actual payment etc.
$_SESSION['amount']=$amount;
$_SESSION['uniqueId']=$uniqueId;
$_SESSION['callingPage']=$callingPage;
$_SESSION['bannerCode']=$bannerCode;
$_SESSION['returnPage']=$returnPage;
$_SESSION['ownerEmail']=$ownerEmail;
$_SESSION['invoiceNumber']=$invoiceNumber;

if($showForm){
	// Show the form
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?=renderHead("Eckerd College Credit Card Processing") ?>

<script src="https://www.onlinecdp.org/scripts/jquery-1.4.js" type="text/javascript"></script>
<script src="https://www.onlinecdp.org/scripts/jquery.tooltip.min.js" type="text/javascript"></script>

<link href="https://www.onlinecdp.org/scripts/jquery.tooltip.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

$(document).ready(function(){
	$("a.show_cvv2").tooltip({ 
	    bodyHandler: function() { 
	        return $('div#cvv_tooltip').html(); 
	    }, 
	    showURL: false ,
	    delay: 0
	});
	$("a.show_cvv2").click(function(e){
			e.preventDefault();
		});

	var z = 0;
    var brwzer="";
    var brwzerIn=document.getElementById('brwsr');
    jQuery.each(jQuery.browser, function(i, val) {
      if(z==0)
        brwzer=val;
      if((z>0)&&(val===true))
        brwzer= i+" "+brwzer; 
      z++;
    });
    brwzerIn.value=brwzer;

    setConfirmUnload(true);
    
    $('form#ccForm').submit(function() {
        $('#navOK').val('Y');
         setConfirmUnload(false);
         return true;
     });

    $('#cancel').click(function() {
    	$('form#ccForm').attr('action','cancel.php');
        $('#navOK').val('Y');
         setConfirmUnload(false);
         $('form#ccForm').submit();
     });

     $(window).unload(function() {
       if( $('#navOK').val()=='N'){
        var dataStr="";
        dataStr+="uniqueId="+ $('#uniqueId').val();
        dataStr+=($('#ApprovalCode').val().length > 0)? "&ApprovalCode="+$('#ApprovalCode').val() : ""; 
        dataStr+=($('#PaymentId').val().length > 0)? "&PaymentId="+$('#PaymentId').val() : ""; 
        dataStr+=($('#brwsr').val().length > 0)? "&brwsr="+$('#brwsr').val() : "";
        dataStr+=($('#page').val().length > 0)? "&page="+$('#page').val() : "";                       
         
        $.ajax({
           type: "POST",
           url: "https://www.onlinecdp.org/ptl/ajax/updateOrderStatus.php",
           data: dataStr,
           success: function(msg){
             //alert( "Update Order Status " + msg );
           }
         });
       }
     });
});

  function setConfirmUnload(on) {
    window.onbeforeunload = (on) ? unloadMessage : null;
  }

  function unloadMessage() {
    var navMsg =($('#page').val() == "ccBefore")? "'Pay Now' or 'Cancel'" : "'Return'";
    return 'If you navigate away from this page by any means other than the '+navMsg+' button(s) provided, your online order status will not be updated.';
  } 

</script>

</head>
<body>



<?=renderHeader() ?>
<p><font color="#ff0000"><?=$errMsg ?></font></p>
<p>Please complete the following payment information and select "Pay Now" to make a<br />
payment. Please Note: Once you have submitted your payment, it may take an additional<br />
business day for payment to reflect on your account. To void this transaction, select<br />
"Cancel".</p>
<form name="ccForm" id="ccForm" action="creditcardtest.php" method="POST">
<input type="hidden" name="what" value="save" />
<input type="hidden" name="callingPage" value="<?=$callingPage ?>" />
<input type="hidden" name="page" id="page" value="ccBefore" />
<input type="hidden" name="uniqueId" id="uniqueId" value="<?=$uniqueId ?>" />
<input type="hidden" name="navOK" id="navOK" value="N" /> 
<input type="hidden" name="brwsr" id="brwsr" value="" />
<input type="hidden" name="ApprovalCode" id="ApprovalCode" value="" />
<input type="hidden" name="PaymentId" id="PaymentId" value="" />
<table>
<tr><td>*</td><td>Name on Account</td><td>
	<table cellpadding="0" cellspacing="0">
		<tr><td><input type="text" name="lastName" value="<?=$lastName ?>" /></td><td><input type="text" name="firstName" value="<?=$firstName ?>" /></td><td><input type="text" size="2" name="mi" value="<?=$mi ?>" /></td></tr>
		<tr><td>Last</td><td>First</td><td>M.I.</td></tr>
	</table>
</td></tr>
<tr><td></td><td>Email Address</td><td><input type="text" name="email1" value="<?=$email1 ?>" /></td></tr>
<tr><td></td><td>Repeat Email Address</td><td><input type="text" name="email2" value="<?=$email2 ?>" /></td></tr>
<tr><td>*</td><td>Address 1</td><td><input type="text" name="address1" value="<?=$address1 ?>" /></td></tr>
<tr><td></td><td>Address 2</td><td><input type="text" name="address2" value="<?=$address2 ?>" /></td></tr>
<tr><td>*</td><td>City</td><td><input type="text" name="city" value="<?=$city ?>" /></td></tr>
<tr><td>*</td><td>State</td><td><?= stateDropDown("state") ?></td></tr>
<tr><td>*</td><td>Zip Code</td><td><input type="text" name="zip" value="<?=$zip ?>" /></td></tr>
<tr><td>*</td><td>Card Type</td><td><?=ccDropDown()?></td></tr>
<tr><td>*</td><td>Credit Card Number</td><td><input type="text" name="creditCardNumber" value="<?=$creditCardNumber ?>" /></td></tr>
<tr><td>*</td><td>Expiration Date</td><td><?=monthDropDown() ?> / <?=yearDropDown() ?></td></tr>
<tr><td>*</td><td>CCV2 (<a href="#" class="show_cvv2">What's this?</a>)</td><td><input type="text" name="ccv2" value="<?=$ccv2 ?>" /></td></tr>
<tr><td>*</td><td>Amount</td><td>USD <?=$amount ?></td></tr>
<tr><td></td><td>&nbsp;</td><td></td></tr>
<tr><td>*</td><td colspan="2">Indicates a required field</td></tr>
<tr><td></td><td colspan="2">Processing may take approximately 30 seconds or more. Please don't click "Pay Now"<br />multiple times, as it may result in you being charged more than once.</td></tr>
<tr><td></td><td></td><td><input type="submit" name="payNow" id="paynow" value="Pay Now" />&nbsp;<input type="button" name="cancel" id="cancel" value="Cancel" /></td></tr>
</table>
</form>
<div id="cvv_tooltip" style="display:none;">
	<div style="width:300px;">
	<p>
		<img src="https://www.onlinecdp.org/images/cvv_card.gif" />
		<br />
		Your Credit / Debit Card Verification number is located on the reverse side of your VISA or MasterCard, on the signature line, just to the right of your card number. For American Express the code will appear above the account number on the face of the card. 
	</p>
	</div>
</div>
<?=renderFooter() ?>
</body>
</html>
<?php
}
?>

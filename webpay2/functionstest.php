<?php
// Stuff that should be part of the <head> section goes here 
// 07/27/2009 - We are no longer using a SQL database, changed multiple functions accordingly
// Changed telephoen number to email address
// Inlcuded Banner Code and Invoice Number in CC process
// Included result code in receipt 
function renderHead($title){
	return "<title>$title</title>
	<link title='standard' media='screen' href='index_files/eckerd-1.css' type='text/css' rel='stylesheet'>";
}

// Stuff that should be part of the page header goes here
function renderHeader(){
//	return "<h3>Eckerd College (Logo or CMS Wrapper)&nbsp;<img src='./v-mc-d-amex-small.gif' height='22' width='132' alt='We accept Visa/MC/Amex/Discover' /></h3>";
	return renderHeaderSmall()."&nbsp;&nbsp;&nbsp;&nbsp;<img src='./v-mc-d-amex-small.gif' height='22' width='132' alt='We accept Visa/MC/Amex/Discover' /></h3>";
}

// Stuff that should be part of the smaller version of the page header goes here
function renderHeaderSmall(){
	return "<a href='http://www.eckerd.edu/'><img height='51' alt='Eckerd College' src='index_files/site-logo.gif' width='159' border='0'></a>";
}

// Stuff that should be page of the page footer goes here
function renderFooter(){
return "<table cellspacing='0' cellpadding='0' width='750' border='0'>
        <tbody>
        <tr valign='top' align='left'>
          <td width='100%' colspan='2'><a href='http://www.eckerd.edu/'><img height='51' alt='Eckerd College' src='index_files/site-logo.gif' width='159' border='0'></a></td>
          <td width='9' rowspan='3'><img height='1' src='index_files/pixel.gif' width='9' border='0'></td>
                <td align='right' width='81' rowspan='3'>
				</td>
              </tr>
        <tr valign='top' align='left' height='8'>
          <td width='100%' colspan='2'><img height='8' src='index_files/pixel.gif' width='1' border='0'></td></tr>
        <tr valign='top' align='left'>
          <td width='4'><img height='1' src='index_files/pixel.gif' width='4' border='0'></td>
          <td class='footertext' width='100%'>4200 54th Avenue South . St.
            Petersburg, Florida 33711<br>(800) 456-9009 or (727) 867-1166<br><a href='http://www.eckerd.edu/about/index.php?f=contact'>E-mail</a> |
            <a href='http://www.eckerd.edu/departments/index.php?f=home'>Directory</a>
            | <a href='http://www.eckerd.edu/its/index.php?f=pprivacy'>Privacy
            Policy</a><br><br>Copyright Eckerd College. All rights
            reserved.</td></tr></tbody></table>";
}

// Standard drop downs
function ccDropDown(){
	return "<select name='cardType'>
		<option value='' selected>--</option>
		<option value='1'>VISA</option>
		<option value='2'>Master Card</option>
		<option value='3'>Discover</option>
		<option value='4'>American Express</option>
	</select>";
}

function monthDropDown(){
	return "<select name='month'>
		<option value='' selected>--</option>
		<option value='01'>Jan</option>
		<option value='02'>Feb</option>
		<option value='03'>Mar</option>
		<option value='04'>Apr</option>
		<option value='05'>May</option>
		<option value='06'>Jun</option>
		<option value='07'>Jul</option>
		<option value='08'>Aug</option>
		<option value='09'>Sep</option>
		<option value='10'>Oct</option>
		<option value='11'>Nov</option>
		<option value='12'>Dec</option>
	</select>";
}

function yearDropDown(){
	return "<select name='year'>
		<option value='' selected>--</option>
		<option value='10'>2010</option>
		<option value='11'>2011</option>
		<option value='12'>2012</option>
		<option value='13'>2013</option>
		<option value='14'>2014</option>
		<option value='15'>2015</option>
		<option value='16'>2016</option>
		<option value='17'>2017</option>
		<option value='18'>2018</option>
		<option value='19'>2019</option>
		<option value='20'>2020</option>
	</select>";
}


function stateDropDown($name){
	return "<select name='$name' size='1'>
	<option value='' selected>--</option>
	<option value='INTL'>INTL</option>
	<option value='AK'>AK</option>
	<option value='AL'>AL</option>
	<option value='AR'>AR</option>
	<option value='AZ'>AZ</option>
	<option value='CA'>CA</option>
	<option value='CO'>CO</option>
	<option value='CT'>CT</option>
	<option value='DC'>DC</option>
	<option value='DE'>DE</option>
	<option value='FL'>FL</option>
	<option value='GA'>GA</option>
	<option value='HI'>HI</option>
	<option value='IA'>IA</option>
	<option value='ID'>ID</option>
	<option value='IL'>IL</option>
	<option value='IN'>IN</option>
	<option value='KS'>KS</option>
	<option value='KY'>KY</option>
	<option value='LA'>LA</option>
	<option value='MA'>MA</option>
	<option value='MD'>MD</option>
	<option value='ME'>ME</option>
	<option value='MI'>MI</option>
	<option value='MN'>MN</option>
	<option value='MO'>MO</option>
	<option value='MS'>MS</option>
	<option value='MT'>MT</option>
	<option value='NC'>NC</option>
	<option value='ND'>ND</option>
	<option value='NE'>NE</option>
	<option value='NH'>NH</option>
	<option value='NJ'>NJ</option>
	<option value='NM'>NM</option>
	<option value='NV'>NV</option>
	<option value='NY'>NY</option>
	<option value='OH'>OH</option>
	<option value='OK'>OK</option>
	<option value='OR'>OR</option>
	<option value='PA'>PA</option>
	<option value='RI'>RI</option>
	<option value='SC'>SC</option>
	<option value='SD'>SD</option>
	<option value='TN'>TN</option>
	<option value='TX'>TX</option>
	<option value='UT'>UT</option>
	<option value='VA'>VA</option>
	<option value='VT'>VT</option>
	<option value='WA'>WA</option>
	<option value='WI'>WI</option>
	<option value='WV'>WV</option>
	<option value='WY'>WY</option>
	</select>";
}

// Validate email addresses
function isValidEmailAddress($email){
        $qtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
        $dtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
        $atom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c'.
            '\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
        $quoted_pair = '\\x5c[\\x00-\\x7f]';
        $domain_literal = "\\x5b($dtext|$quoted_pair)*\\x5d";
        $quoted_string = "\\x22($qtext|$quoted_pair)*\\x22";
        $domain_ref = $atom;
        $sub_domain = "($domain_ref|$domain_literal)";
        $word = "($atom|$quoted_string)";
        $domain = "$sub_domain(\\x2e$sub_domain)*";
        $local_part = "$word(\\x2e$word)*";
        $addr_spec = "$local_part\\x40$domain";
        return preg_match("!^$addr_spec$!", $email);
}

// Validates the various data fields
function validateData($data){
	$rc="";
	if(0==strlen($data['amount'])){
		$rc.="<br />Invalid amount";
	}		
	if(0==strlen($data['firstName'])){
		$rc.="<br />Invalid first name";
	}		
	if(0==strlen($data['lastName'])){
		$rc.="<br />Invalid last name";
	}
	if(0==strlen($data['address1'])){
		$rc.="<br />Invalid address line1";
	}
	if(0==strlen($data['city'])){
		$rc.="<br />Invalid city";
	}
	if(0==strlen($data['state'])){
		$rc.="<br />Invalid state";
	}
	if(0==strlen($data['zip'])){
		$rc.="<br />Invalid zip code";
	}
	if(0==strlen($data['creditCardNumber'])){
		$rc.="<br />Missing credit card number";
	}
	if(0==strlen($data['month'])){
		$rc.="<br />Missing expiration date month";
	}
	if(0==strlen($data['year'])){
		$rc.="<br />Missing expiration date year";
	}
	if(0==strlen($data['ccv2'])){
		$rc.="<br />Missing CCV2";
	}
	if(0!=strlen($data['email1'])&&$data['email1']!=$data['email2']){
		$rc.="<br />The two email addresses don't match";
	}
	if(0!=strlen($data['email1'])&&false==isValidEmailAddress($data['email1'])){
		$rc.="<br />The email address has invalid format";
	}
			
	return strlen($rc)==0?"":"<u>The following items must be corrected before you can continue:</u>".$rc;
}

// Returns the url for the calling page
// if no URL take caller back to the 
// Eckerd college home page
function getCallerURL($callingPage){
	return (strlen($callingPage)>0)?$callingPage:"http://www.eckerd.edu"; // <--this is the original aab 8/7/2013
/*
	$url = "https://www.onlinecdp.org/";
	if(strlen($callingPage)>0){
		$url = $callingPage;
	}
	return $url;*/
}

// Show the CC receipt
function showReceipt($data){
	// Display the receipt
	$url=getCallerURL($data['returnPage']);
	$card=$data['card'];
	$amount=$data['amount'];
	$last4=$data['last4'];
	$name=$data['name'];
	$appCode=$data['approvalCode'];
	$paymentId=$data['paymentId'];
	$uniqueId=$data['uniqueId'];
	return "<html><head>".renderHead("Credit Card Receipt").
	'<script src="https://www.onlinecdp.org/scripts/jquery-1.4.js" type="text/javascript"></script>
	<script type="text/javascript">
	
$(document).ready(function(){
		
	var z = 0;
    var brwzer="";
    var brwzerIn=document.getElementById("brwsr");
    jQuery.each(jQuery.browser, function(i, val) {
      if(z==0)
        brwzer=val;
      if((z>0)&&(val===true))
        brwzer= i+" "+brwzer; 
      z++;
    });
    brwzerIn.value=brwzer;

    setConfirmUnload(true);
    
    $("form").submit(function() {
        $("#navOK").val("Y");
         setConfirmUnload(false);
         return true;
     });
     
     $(window).unload(function() {
       if( $("#navOK").val()=="N"){
        var dataStr="";
        dataStr+="uniqueId="+ $("#uniqueId").val();
        dataStr+=($("#ApprovalCode").val().length > 0)? "&ApprovalCode="+$("#ApprovalCode").val() : ""; 
        dataStr+=($("#PaymentId").val().length > 0)? "&PaymentId="+$("#PaymentId").val() : ""; 
        dataStr+=($("#brwsr").val().length > 0)? "&brwsr="+$("#brwsr").val() : "";
        dataStr+=($("#page").val().length > 0)? "&page="+$("#page").val() : "";                       
         
        $.ajax({
           type: "POST",
           url: "https://www.onlinecdp.org/ptl/ajax/updateOrderStatus.php",
           data: dataStr,
           success: function(msg){
             //alert( "Update Order Status " + msg );
           }
         });
       }
     });
	});
	
	function setConfirmUnload(on) {
    window.onbeforeunload = (on) ? unloadMessage : null;
  }

  function unloadMessage() {
    if ($("#page").val() == "ccBefore")
    {
    	return "You must click \'Pay Now\' or \'Cancel\' to update your order status!"; 
    }
    else
    {
        return "You must click \'Complete your Order\' to complete your order!";
    }
  } 
	</script>'.
	"</head><body><table><tr><td>".renderHeaderSmall()."</td>
	<td><!--<a href='printerfriendly.php' target='PF'><img src='printerfriendly.jpg' border='0' /></a>--></td></tr> 
	<tr><td><p><h4>Receipt</p>
	<form action='$url' method='POST'>
	<input type='hidden' name='ApprovalCode' id='ApprovalCode' value='$appCode' />
	<input type='hidden' name='PaymentId' id='PaymentId' value='$paymentId' />
	<input type='hidden' name='uniqueId' id='uniqueId' value='$uniqueId' />
	<input type='hidden' name='page' id='page' value='ccAfter' />
	<input type='hidden' name='brwsr' id='brwsr' value='' />
	<input type='hidden' name='navOK' id='navOK' value='N' />
	<table cellpadding='0' cellspacing='0'>
	<tr><td><input type='submit' value='Complete your Order' /></td><td></td></tr>
	<tr><td><br /></td><td><br /></td></tr>
	<tr><td>Payment Id</td><td>$paymentId</td></tr>
	<tr><td>Approval Code</td><td>$appCode</td></tr>
	<tr><td>Payment Amount</td><td>\$ $amount</td></tr>
	<tr><td>Credit Card Number</td><td>xxxxxxxxxxxx$last4</td></tr>
	<tr><td>Credit Card Type</td><td>$card</td></tr>
	<tr><td>Name on Account</td><td>$name</td></tr>
	<tr><td><br /></td><td><br /></td></tr>
	<tr><td> </td><td> </td></tr>
	<tr><td colspan='2'>This payment will be processes in approximately 1-2 business days.</td></tr>
	<tr><td><br /></td><td><br /></td></tr>
	<tr><td colspan='2'>If you have any questions contact  cdp@eckerd.edu</td></tr>
	<tr><td><br /></td><td><br /></td></tr>
	<tr><td><input type='submit' value='Complete your Order' /></td><td></td></tr>
	</table>
	</form></td></tr></table></body></html>";
}

// CC functions
function ccGetMerchantId(){
	return "532874";
}

function ccGetUserId(){
	return "developer";
}

function ccGetPin(){
	return "CSYTSC";
}

function getCCUrl(){
	return "https://www.myvirtualmerchant.com/VirtualMerchant/process.do";
}

function getCCReferer(){
// TODO: For production
	return 'https://www.onlinecdp.org/webpay2/creditcard.php';
// For testing
//	return "http://www.crocophant.com/";
}

// this is the actual credit card call
// 07/27/2009: Added banner code to ssl_customer_number and invoice number to ssl_invoice_number
function processCC($data){
	if(strlen($data['invoiceNumber'])<1){
		$data['invoiceNumber']=" ";
	}
	$values=array(
		"ssl_test_mode" => "TRUE",	// SET TO "FALSE" FOR PRODUCTION USE!!! 
		"ssl_merchant_id" => ccGetMerchantId(),
		"ssl_user_id" => ccGetUserId(),
		"ssl_pin" => ccGetPin(),
		"ssl_amount" => $data['amount'],
		"ssl_transaction_type" => "ccsale",
		"ssl_show_form" => "false",
		"ssl_result_format" => "ASCII",
		"ssl_cvv2cvc2_indicator" => "1",
		"ssl_cvv2cvc2" => $data['ccv2'],
		"ssl_card_number" => $data['creditCardNumber'],
		"ssl_exp_date" => "".$data['month'].$data['year'],
		"ssl_salestax" => "0.00",			// Not sure why this is a required field, the doc says it isn't, but we get 4009 error if omitted
		"ssl_avs_address" => $data['address1'],
		"ssl_avs_zip" => "".$data['zip'],
		"ssl_customer_code" => $data['bannerCode'], 
		"ssl_invoice_number" => $data['invoiceNumber'],
		"ssl_first_name" => $data['firstName'],		// 07/30/2009: Added this field per Frank Abney's request
		"ssl_last_name" => $data['lastName'],			// 07/30/2009: Added this field per Frank Abney's request
		"ssl_description" => $data['bannerCode'].';'.$data['firstName']." ".$data['lastName']		// 07/30/2009: Added this field per Frank Abney's request
	);
//print_r($values);
//die();
	
	$fields = "";
	foreach($values as $key => $value){
  		$fields .= "$key=".urlencode($value)."&";
	}
	$fields = rtrim($fields, "& ");
	$ch=curl_init(getCCUrl());
	curl_setopt($ch, CURLOPT_HEADER, 0);               // set to 0 to eliminate header info from response
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);       // Returns response data instead of TRUE(1)
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);     // use HTTP POST to send form data
	
	// Note: This is a funky workaround for 4003 POST REFERER Error problem
	$refer = getCCReferer();
	curl_setopt($ch, CURLOPT_REFERER, $refer);
//die($refer);		
	
	$resp=curl_exec($ch);                            	//execute post and get results
	curl_close($ch);
	$tmp=explode("\n",$resp);									// get an array with the format [n] => "key=value"
	foreach($tmp as $line){										// turn it into associative array [key] => value
		$pair=explode("=",$line);
		$rc[$pair[0]]=$pair[1];
	} 
	return $rc;
}

// gets the sender email address
function getSender(){
	//return "www@eckerd.edu";
	return "cdp@eckerd.edu";
}

// Generuc mailer function
function sendGenericMail($to,$from,$subject,$body,$doNotReply=true){
	// Note: We always send from this account
   $hdrs="From: $from\n";
   $hdrs=$hdrs."Reply-To:  $from\n";
   $dnr=$doNotReply?"\n\nThis is an automatically generated email. Please do not reply.\n\n":"";
	return mail($to,$subject,$body.$dnr,$hdrs);
}

// sends confirmation email to the customer
function sendConfirmation($data,$to){
	$from=getSender();
	$subject="Eckerd College CDP Receipt";
	$body="This e-mail is to confirm that a credit card payment was submitted to Eckerd College on ".$data['transactionDate']."\n\n";
	$body.="Payment Id:          ".$data['paymentId']."\n";
	$body.="Approval Code:          ".$data['approvalCode']."\n";
	$body.="Payment Amount:      ".floatval($data['amount'])."\n";
	$body.="Credit Card Number:  xxxxxxxxxxxx".$data['last4']."\n";
	$body.="Credit Card Type:    ".$data['card']."\n";
	$body.="Name on Account:     ".$data['name']."\n\n";
	$body.="This payment will be processed in approximately 1-2 business days.\n\n";		
	$body.="Please do not reply to this email. If you have any questions, contact cdp@eckerd.edu";
	sendGenericMail($to,$from,$subject,$body);
}

function sendEmailReceiptToCDPAdmins($data){
	
	// Cdp group email
/*	$recipientList .= 'cdp@eckerd.edu' . ',';
	
	// Patricia Viscomi
	$recipientList .= 'viscompe@eckerd.edu' . ',';

	// Margaret Albury
	$recipientList .= 'hammercl@eckerd.edu' . ','; 

	// Runde
	$recipientList .= 'rundece@eckerd.edu';
	
	//$recipientList .= 'bedardaa@eckerd.edu';*/

	$recipientList = 'nfleming@rampant.tech' . ',';
	$recipientList .= 'carter@rampant.tech';
	
	
	$from=getSender();
	$subject="www.onlinecdp.org purchase by - " . $data['firstName'] . ' '. $data['lastName'];
	$body = "Here are the user details:\n\n";
	$body .= "Name: " . $data['firstName'] . " " . $data['lastName'] . "\n";
	$body .= "Date: " . $data['transactionDate'] . "\n";
	$body .= "Email Address: " . $data['email1'] . "\n";
	$body .= "Address 1: " . $data['address1'] . "\n";
	$body .= "Address 2: " . $data['address2'] . "\n";
	$body .= "City: " . $data['city'] . "\n";
	$body .= "State: " . $data['state'] . "\n";
	$body .= "Zip: " . $data['zip'] . "\n";
	$body .= "Here are the payment details:\n\n";
	$body.="Payment Id:          ".$data['paymentId']."\n";
	$body.="Approval Code:          ".$data['approvalCode']."\n";
	$body.="Payment Amount:      ".floatval($data['amount'])."\n";
	$body.="Credit Card Number:  xxxxxxxxxxxx".$data['last4']."\n";
	$body.="Credit Card Type:    ".$data['card']."\n";
	$body.="Name on Account:     ".$data['name']."\n\n";
	$body.="This payment will be processed in approximately 1-2 business days.\n\n";
	$body.="Please do not reply to this email. If you have any questions, contact cdp@eckerd.edu";
	sendGenericMail($recipientList,$from,$subject,$body);
}

function sendEmailReceiptToEMPAdmins($data){

	// Craig Zearfoss
/*	$recipientList .= 'craigzearfoss@yahoo.com' . ',';

	// Christine Hammerschmidt
	$recipientList .= 'hammercl@eckerd.edu';

	// Patricia Viscomi
	$recipientList .= 'viscompe@eckerd.edu' . ',';

	// Jennifer Hall
	$recipientList .= 'hallja@eckerd.edu';*/
	$recipientList .= 'nfleming@rampant.tech';

	$from=getSender();
	$subject="EMP purchase by - " . $data['firstName'] . ' '. $data['lastName'];
	$body = "Here are the user details:\n\n";
	$body .= "Name: " . $data['firstName'] . " " . $data['lastName'] . "\n";
	$body .= "Date: " . $data['transactionDate'] . "\n";
	$body .= "Email Address: " . $data['email1'] . "\n";
	$body .= "Address 1: " . $data['address1'] . "\n";
	$body .= "Address 2: " . $data['address2'] . "\n";
	$body .= "City: " . $data['city'] . "\n";
	$body .= "State: " . $data['state'] . "\n";
	$body .= "Zip: " . $data['zip'] . "\n";
	$body .= "Here are the payment details:\n\n";
	$body.="Payment Id:          ".$data['paymentId']."\n";
	$body.="Approval Code:          ".$data['approvalCode']."\n";
	$body.="Payment Amount:      ".floatval($data['amount'])."\n";
	$body.="Credit Card Number:  xxxxxxxxxxxx".$data['last4']."\n";
	$body.="Credit Card Type:    ".$data['card']."\n";
	$body.="Name on Account:     ".$data['name']."\n\n";
	$body.="This payment will be processed in approximately 1-2 business days.\n\n";
	$body.="Please do not reply to this email. If you have any questions, contact cdp@eckerd.edu";
	sendGenericMail($recipientList,$from,$subject,$body);
}



function completeTransaction($data){
	
}

?>

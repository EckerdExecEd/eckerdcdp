<?php
session_start();
require_once "functions.php";
$callingPage=stripslashes($_POST['callingPage']);
$uid=stripslashes($_SESSION['uniqueId']);
$url=getCallerURL($callingPage);
session_unset();
header("Location: {$url}?uniqueId={$uid}&paymentStatus=canceled");
exit();
?>

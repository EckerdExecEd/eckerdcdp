<?php
// 07/27/2009: Multiple changes to use session instead of the MySQL database
// Added approval code
session_start();

require_once "functions.php";

?>
<html>
<head>
<title>Print Credit Card Receipt</title>
</head>
<body>
<table>
<tr>
<td><img height='51' alt='Eckerd College' src='index_files/site-logo.gif' width='159' border='0'></td>
<td><input type="button" onClick="window.print()" value="Print This Page"/>
</td>
</tr>
<tr>
<td>
<pre>
Date:                <?=$_SESSION['date']."\n" ?>
Payment Id:          <?=$_SESSION['paymentId']."\n" ?>
Approval Code:       <?=$_SESSION['approvalCode']."\n" ?>
Payment Amount:      $<?=floatval($_SESSION['amount'])."\n" ?>
Credit Card Number:  xxxxxxxxxxxx<?=$_SESSION['last4']."\n" ?>
Credit Card Type:    <?=$_SESSION['card']."\n" ?>
Name on Account:     <?=$_SESSION['name']."\n" ?>
</pre>
</td>
<td>
</td>
</tr>
</table>
</body>
</html>

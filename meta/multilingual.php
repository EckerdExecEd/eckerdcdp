<?php
require_once "dbfns.php";
//---------------------------------------------
//-- General Multilingual support
//---------------------------------------------

// get the File ID for the file we're using
function getFLID($path,$filename){
	$conn=dbConnect();
	$query="select FLID from MLFile where Path='$path' and Descr='$filename'";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	return $row[0];

}

// get text for specific location(s) within a file
// is returned as an array if multiple values
// or as a single value if a single location was requested
function getMLText($flid,$tid,$lid,$pos=""){
	$single=false;
	$conn=dbConnect();
	$query="select Descr, Pos from MLText where FLID=$flid and LID=$lid and TID=$tid";
	if(strlen($pos)>0){
		$query=$query." and Pos=$pos";
		$single=true;
	}
	else{
		$query=$query." order by Pos";
	}
	//if($flid=='3005') die($query);
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	
	if(true==$single){
		// we only asked for one value
		$row=mysql_fetch_array($rs);
		return $row[0]; //removed utf8_decode() - JPC 05/23/08
	}
	
	// potentially multiple values
	$rc=array();
	$rows=dbRes2Arr($rs);
	foreach($rows as $row){
		// Stuff in the correctt position of the array
		// Note that we decode UTF-8 multibyte characters
		$idx=$row[1];
		$rc[$idx]=$row[0];  //removed utf8_decode() - JPC 05/23/08
	}
	return $rc;
}

// Does a rater lang row exist for this Rater (Candidate)?
function doesRaterLangExist($rid){
	$conn=dbConnect();
	$query="select count(*) from RATERLANG where RID=$rid";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	$cnt=$row[0];
	return $cnt>0;
}

// Get the Program ID for a Candidate
function getPID4Candidate($cid){
	$conn=dbConnect();
	$query="select PID from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	return $row[0];
}

// Get the Candidate ID for a Rater
function getCID4RATER($rid){
	$conn=dbConnect();
	$query="select CID from RATER where RID=$rid";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	return $row[0];
}

// Is this a valid language choice for this program?
function isLIDValidForPID($lid,$pid){
	$conn=dbConnect();
	$query="select count(*) from PROGLANG where PID=$pid and LID=$lid";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	$cnt=$row[0];
	return $cnt>0;
}

//----------------------------------------------------------
//-- Candidates --------------------------------------------
//----------------------------------------------------------

// Ensure a RATERLANG Row Exists for a Candidate
// Note: default is English
// Note: the third parameter dictates the behavior if language
// has not been specified for a PROGRAM
function insertCandidateLanguage($cid,$lid,$addProgLang=false){
	
	if("8"!=$lid&&"1"!=$lid){
		$lid="1";
	}
	
	$conn=dbConnect();
	// get the program id
	$pid=getPID4Candidate($cid);
	
	if(false==isLIDValidForPID($lid,$pid)){
		// This language is not valid for this program
		if(true==$addProgLang){
			// this means we should add a PROGLANG if it
			// doesn't already exist
			$query="insert into PROGLANG (PID,LID) values ($pid,$lid)";
			mysql_query($query);
		}
		else{
			// if we're here we shouldn't add a PROGLANG
			return false;
		}
	}
	// if we're here we have a valid program language
	// and can go ahead and add the RATERLANG for the Candidate
	// unless we already have it
	if(false==doesRaterLangExist($cid)){
		$query="insert into RATERLANG (RID,PID,LID) values ($cid,$pid,$lid)";
		mysql_query($query);
	}
	
	return true;
}


function ck4decode($string){
  if(is_array($string)){
    foreach($string as $k=>$v){
      $v = (detectUTF8($v))? utf8_decode($v) : $v;
      $string[$k]=$v;
    }
    $retval = $string;
  }else{
    $retval = (detectUTF8($string))? utf8_decode($string) : $string;
  }
  return $retval;
}

function detectUTF8($string)
{
    return preg_match('%(?:
        [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
        |\xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
        |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2} # straight 3-byte
        |\xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
        |\xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
        |[\xF1-\xF3][\x80-\xBF]{3}         # planes 4-15
        |\xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
        )+%xs', 
    $string);
}
//----------------------------------------------------------
//-- Raters -----------------------------------------------
//---------------------------------------------------------

?>

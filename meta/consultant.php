<?php
require_once 'dbfns.php';
require_once 'mailfns.php';
require_once "formfns.php";

    // Displays consultant details
    function showConsultant($conid){
	$conn=dbConnect();
	$query="select CONID,FNAME,LNAME,EMAIL,UID,PWD,ACTIVE,ORGID,STATE,TAXEXEMPT from CONSULTANT where CONID=$conid";
	$rs=mysql_query($query)
	    or die(mysql_error());

	if($row=mysql_fetch_row($rs)){
	    $fnm=stripslashes($row[1]);
	    $lnm=stripslashes($row[2]);
	    $st=($row[8]=='AC')? '' : $row[8];
	    $state=selectList('state', fetchArray("select SUBCOUNTRYID,SUBCOUNTRYID as NAME from SUBCOUNTRY where COUNTRYID = 'US' AND DISPLAY = 'Y' ORDER BY NAME ASC"), $st,'STATE?','id="state"');
	    $exempt=renderTaxExempt($row[9]);	    
	    echo "<input type='hidden' name='conid' value='$row[0]'>";
	    echo "<tr><td colspan=2>Consultant ID: $row[0]</td>";
	    echo "<tr><td>First Name</td><td><input type='text' name='fnam' value=\"$fnm\">";
	    echo "<tr><td>Last Name</td><td><input type='text' name='lnam' value=\"$lnm\">";
	    echo "<tr><td>E-Mail</td><td><input type='text' name='email' value='$row[3]'>";
	    echo "<tr><td><span title=\"State $fnm $lnm uses for tax purposes\">Select A State</span></td>
            <td>$state</td></tr>";
	    echo "<tr><td><span title=\"Select No if consultants orders should be taxed.\">Tax Exempt?</span></td>
            <td>$exempt</td></tr>"; 	    
	    return true;
	}
	return false;
    }

    // Check to see if a Consultant with a specific user id already exists
    // Note: Email is the same as UID for consultants
    function consExists($uid,$conid=NULL){
	$conn=dbConnect();
	// No row returned - uid doesn't exist, row returned - uid already exists
	$query="select * from CONSULTANT where UID='$uid'";
	if(!is_null($conid)){
	    $query=$query." and CONID<>$conid";
	}
	$rs=mysql_query($query);
	if(!$rs){
	    return false;
	}
	return mysql_fetch_row($rs);
    }

    // Insert a new Consultant into the database
    // Note: The key is generated as a large, unique random number, not as a sequence
    function consInsert($consData){ 
	$conn=dbConnect();
	$i=getKey("CONSULTANT","CONID");
	
	// Insert the CONSULTANT row
    	$query="insert into CONSULTANT (CONID,FNAME,LNAME,EMAIL,UID,PWD,ACTIVE,ORGID,STATE,TAXEXEMPT) values 
      ($i,'$consData[0]','$consData[1]','$consData[2]','$consData[3]',PASSWORD('$consData[4]'),'$consData[5]',$consData[6],"; 
      $query.=(isset($consData[7]))? "'$consData[7]'," : "NULL,";
      $query.=(isset($consData[8]))? "'$consData[8]')" : "'Y')";

 	if(!mysql_query($query)){
	    //echo $query."<br>";
	    return false;
	}
	
	// Insert the CONS row
	$query="insert into CONS (CONID) values ($i)";
	if(!mysql_query($query)){
	    //echo $query."<br>";
	    return false;
	}

	return true;
    }

    // Returns an array of arrays with consultant records
    function consList($narrow,$active){
	$crit=$narrow."%";
	$conn=dbConnect();
	$query="select CONID,FNAME,LNAME,EMAIL,ACTIVE from CONSULTANT where LNAME like '$crit' and ACTIVE='$active' order by LNAME";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
    }

    // Returns an array containg a consultant record
    function consInfo($conid){
	$conn=dbConnect();
	$query="select CONID,FNAME,LNAME,EMAIL,ACTIVE from CONSULTANT where CONID=$conid";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return mysql_fetch_array($rs);
    }

    // displays a list of all consultants meeting specific criteria
    function listConsultants($narrow,$active,$frm){
	$rows=consList($narrow,$active);

	echo "<tr><td>Name</td><td>Email</td><td>Actions</td></tr>";
	foreach($rows as $row){
	    echo "<tr><td>".stripslashes($row[2]).",".stripslashes($row[1])."</td><td>".$row[3]."</td><td>";
	    if("Y"==$active){
		echo "<input type='Button' value='Edit' onClick=\"javascript:$frm.what.value='edit';$frm.conid.value='$row[0]';$frm.action='consdetail.php';$frm.submit();\">&nbsp;";
		echo "<input type='Button' value='Change Password' onClick=\"javascript:$frm.action='conspwd.php';$frm.conid.value='$row[0]';$frm.submit();\">&nbsp;";
		echo "<input type='Button' value='Use' onClick=\"javascript:$frm.action='../cons/adminhome.php';$frm.conid.value='$row[0]';$frm.submit();\">&nbsp;";
		echo "<input type='Button' value='Archive' onClick=\"javascript:$frm.what.value='archive';$frm.conid.value='$row[0]';$frm.submit();\">&nbsp;";
	    }
	    else{
		echo "<input type='Button' value='Restore' onClick=\"javascript:$frm.what.value='restore';$frm.conid.value='$row[0]';$frm.submit();\">&nbsp;";
	    }
	    echo "</td></tr>";
	}
	return true;
    }

    // displays email addresses for consultants
    function listMailAddresses($narrow,$active,$frm){
	$rows=consList($narrow,$active);
	$i=0;
	echo "<tr><td>Name</td><td>Email</td><td>Include as Recipient</td></tr>";
	foreach($rows as $row){
	    echo "<tr><td>".stripslashes($row[2]).",".stripslashes($row[1])."</td><td>".$row[3]."</td><td>";
	    echo "Yes <input type='radio' name='rcp".$i."' value='Y'> No <input type='radio' name='rcp".$i."' value='N' checked>";
	    echo "<input type='hidden' name='conid".$i."' value='".$row[0]."'>";
	    echo "</td></tr>";
	    $i++;
	}
	echo "<input type='hidden' name='items' value='".$i."'>";
	return true;
    }

    // Toggle between Active and Archived status
    function chgConsultantStatus($conid,$status){
	$conn=dbConnect();
	return mysql_query("update CONSULTANT set ACTIVE='$status' where CONID=$conid");
    }

    // Creates an encrypted password for consultant
    function chgConsultantPwd($conid,$pwd){
	$conn=dbConnect();
	$query="update CONSULTANT set PWD=PASSWORD('$pwd') where CONID=$conid";
	return mysql_query($query);
    }

    // Changes editable attributes 
  function updateCons($consData){
	   $conn=dbConnect();

    	$query ="update CONSULTANT set FNAME='$consData[1]',LNAME='$consData[2]',EMAIL='$consData[3]'";
    	$query.=(isset($consData[4]))? ",STATE='$consData[4]'" : "";
    	$query.=(isset($consData[5]))? ",TAXEXEMPT='$consData[5]'" : "";    	
      $query.=" where CONID=$consData[0]";
      //die($query);

	return mysql_query($query);
    }
    
    // send an email to a group of consultants
    function sendConsEmail($conids,$subject,$msgBody){
	// use this email address as sender
	$conn=dbConnect();
	$query="select EMAIL from CONSULTANT where CONID=".$_SESSION['admid'];
	$rs=mysql_query($query)
	    or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	$sender=$rw[0];
	
	// send one email to each selected recipient, i.e. no spamming
	$rc;
	$dc=getDisclaimer();
	foreach($conids as $conid){
	    $row=consInfo($conid);
	    if(sendGenericMail($row[3],$sender,$subject,$msgBody.$dc,$row[1]." ".$row[2])){
			if(isset($rc)){
				$rc=$rc.", ".$row[3];
			}
			else{
				$rc=$row[3];
			}
	    }
	}
	return $rc;
    }
	
	function getAllConids(){
		$conn=dbConnect();
		$query="select a.CONID from CONSULTANT a, CONS b where a.ACTIVE='Y' and a.CONID=b.CONID";
		//echo $query."<br>";
		$rs=mysql_query($query);
		$rc=array();
		$rows=dbRes2Arr($rs);
		foreach($rows as $row){
			//echo $row[0],"<br>";
			$rc[]=$row[0];	
		}
		return $rc;
	}
	
	function showAllOrganizations(){
		$conn=dbConnect();
		$query="select ORGID,DESCR from ORGANIZATION order by DESCR";
		//echo $query."<br>";
		$rs=mysql_query($query);
		$sel="selected";
		$rc="";
		$rows=dbRes2Arr($rs);
		foreach($rows as $row){
			$rc=$rc."<option value='$row[0]' $sel>$row[1]</option>";
			$sel="";
		}
		return $rc;
	}

function renderTaxExempt($default='Y'){
  $vPairs=array();
  $vPairs[]=array('VAL'=>'Y','DISP'=>'Yes');
  $vPairs[]=array('VAL'=>'N','DISP'=>'No');  
  return radioButtons('taxexempt', '<br />', $vPairs, $default);
}		
?>

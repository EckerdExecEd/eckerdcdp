<?php
// Pulls in dbfns.php as well
require_once "multilingual.php";
require_once "pdffns.php";
// A standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);
define("MARGIN_LEFT",50);
define("MARGIN_RIGHT",565);
define("MARGIN_TOP",765);
define("MARGIN_BOTTOM",27);

// RGB values for the report
define("R_",0.85);
define("G_",0.85);
define("B_",1.0);


//----------------------------------------------------
//--- Scoring
//----------------------------------------------------

// Has the candidate responded to enough questions?
function isItComplete($cid,$pid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from RATERRESP where RID=$cid and TID=3 and VAL is not NULL");
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	// We allow 3 missing answers currently i.e. must have 96 of 99 to score
	return $row[0]>=96;
}

// get rid of all pre-existing scores for this candidate
function getRidOfOldScores($cid,$pid){
	$conn=dbConnect();
	$query="delete from SELFONLYREPORTSCORE where CID=$cid";
	mysql_query($query);
	$query="delete from SCALESCORE where RID=$cid and CID=$cid and PID=$pid";
	mysql_query($query);
}

// Compute the raw and standardized scale scores for a single self only rater
function computeSelfOnlyScaleScores($cid,$pid){
	$conn=dbConnect();
	
	// Raw score
	$query="insert into SCALESCORE (SID,RID,PID,CID,CATID,RAWSCORE,STDSCORE) ";
	$query=$query."select b.SID, a.RID, $pid, $cid, 1, AVG(VAL), 0 from RATERRESP a , SCALEITEM b, RATER c ";
	$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and a.RID=$cid and VAL is not NULL group by b.SID,a.RID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;	
	}
	
	// Standardized score
	$rows=getScaleMeanAndStd($cid,$pid);
	if(!$rows){
		return false;	
	}
	foreach($rows as $row){
		if(0!=$row[2]){
			$std=50+(10*(($row[0]-$row[1])/$row[2]));
			$query="update SCALESCORE set STDSCORE=$std where SID=$row[3] and RID=$cid and CID=$cid and PID=$pid";
			mysql_query($query);
		}
	}
	return "Calculated scale scores - OK";	
}

// function to return the appropriate Mean and Standard deviation etc for a Scale
// for self only
function getScaleMeanAndStd($cid,$pid){
	$conn=dbConnect();
	$query="select b.RAWSCORE,a.MEANVAL1,a.DEVVAL1,a.SID from SCALE a,SCALESCORE b where a.SID=b.SID and b.RID=$cid and b.CID=$cid and b.RID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// calculates the appropriate report scores for self only
function calculateSelfOnlyReportScores($cid,$pid){
	$conn=dbConnect();
	// always calculate for Self
	$query="insert into SELFONLYREPORTSCORE (SID,CID,AVGSCORE) select SID,CID,STDSCORE from SCALESCORE where RID=$cid and CID=$cid and PID=$pid and CATID=1 and STDSCORE is not NULL";
	if(false===mysql_query($query)){
		return false;
	}
	return "Calculated individual report scores - OK";
}

// checks to see if a license has been used for this candidate
function alreadyConsumedLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from SELFLIC where CONID=$conid and CID=$cid and TID=$tid");
	$row=mysql_fetch_row($rs);
	return $row[0]>0?true:false;
}

// Consumes a self-only license
function consumeSelfOnlyLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("insert into SELFLIC (CONID,CID,TID,TS) values ($conid,$cid,$tid,NOW())");
	return $rs;
}

// Score all the candidates in a program
function scoreProgram($pid){
	$conn=dbConnect();
	$msg="Scoring...";
	
	$rs=mysql_query("select CONID from PROGCONS where PID=$pid");
	$row=mysql_fetch_row($rs);
	$conid=$row[0];
	
	$rs=mysql_query("select CID,FNAME,LNAME from CANDIDATE where PID=$pid");
	$rows=dbRes2Arr($rs);
	if(false==$rows){
		return "<font color=\"#ff0000\">Unable to score program: unable to retrive participants</font>";
	}
	
	$ok=false;
	foreach($rows as $row){
		// we really don't need to score here since there's no group report
		// so let's just iterate over each candidate
		
		// Only consume a license if they are complete
		$query="select IFNULL(a.ENDDT,'Incomplete') from RATER a, CANDIDATE b,PROGRAM c where c.PID=$pid and a.CID=" . $row[0] . " and a.CID=b.CID and b.PID=c.PID";
		$rs=mysql_query($query);
		$rw=mysql_fetch_row($rs);
		if ($rw[0] != "Incomplete") {
		
			$query="select count(CID) from SELFLIC where CID=" . $row[0] . " and TID=3 and CONID=" . $_SESSION["conid"];
			$rs=mysql_query($query);
			$rw=mysql_fetch_row($rs);
			$ok=true;
		
			if ($rw[0] == 0) {
				// Only consume a license if one has not already been consumed
				$msg=$msg."<br> $row[1] $row[2]";
				$cid=$row[0];
				consumeSelfOnlyLicense($cid,$conid,3);
			}
		}
	}

	if(false==$ok){
		return "<font color=\"#ff0000\">Unable to score program: no participants</font>";
	} else {
		$msg .= '<p style="color:green;font-weight:bold;margin-bottom:0px;">The program has now been scored.<br />If you need to re-score the program you will need to re-open it first.</p>';
	}
	
	// Set status to "Scored"
	$query="update PROGRAM set EXPIRED='S' where PID=$pid";
	//echo $query."<br>";
	mysql_query($query);
	
	// Send email to consultant (added 7/6/08 by CDZ)
	sendScoreEmail($pid);
	
	return $msg."<br>";
}

//----------------------------------------------------
//------ Reporting
//----------------------------------------------------
function getCandidateName($cid){
	$conn=dbConnect();
	$query="select FNAME, LNAME from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return utf8_decode($row[0])." ".utf8_decode($row[1]);	
}

function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function writeHeader($cid,&$pdf,$lid){
	$name=getCandidateName($cid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);
	
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	
/*	
	pdf_rect($pdf,50,750,515,15);
	pdf_fill_stroke($pdf);
	// Note: (R) is hex AE, so well use sprintf to format properly
//	$what="Conflict Dynamics Profile ".sprintf("%c",0xae);
*/
	$what=$mltxt[1];
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,centerXcord($pdf, $what, (PAGE_WIDTH/2)),MARGIN_TOP+2);

  $colors = rgb2cmyk(hex2rgb('#aeaaa7'));	
  pdf_setcolor($pdf, "both", "cmyk", $colors['c'], $colors['m'], $colors['y'], $colors['k']);
  	
	pdf_moveto($pdf,MARGIN_LEFT,MARGIN_TOP);
	pdf_lineto($pdf,MARGIN_RIGHT,MARGIN_TOP);
	pdf_lineto($pdf,MARGIN_RIGHT,MARGIN_BOTTOM);
	pdf_stroke($pdf);
	
	$img=pdf_open_image_file($pdf,"PNG","../images/convirgente_logo.png","",0);
  pdf_place_image($pdf,$img,MARGIN_LEFT,MARGIN_TOP-20,0.25);
   
  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	return $name;	
}

function writeReportFooter(&$pdf,$name,&$page,$lid){
	$flid=getFLID("meta","ind0");
	$mltxt=getPTMLText($flid,"3",$lid);
	
	$rptdt=getPTdate('D, j d Y');
	
  $colors = rgb2cmyk(hex2rgb('#b9b9b9'));	
  pdf_setcolor($pdf, "both", "cmyk", $colors['c'], $colors['m'], $colors['y'], $colors['k']);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,8.0);	
	// 1 - Show Date 
  pdf_show_xy($pdf,$rptdt,MARGIN_LEFT,MARGIN_BOTTOM);
  // 2 - Show prepared for statement
  $strCntr=$mltxt[2]." ".$name;
  pdf_show_xy($pdf,$strCntr,centerXcord($pdf, $strCntr, (PAGE_WIDTH/2)),MARGIN_BOTTOM);
   
  // 3 - Draw and number page circle
   magic_circle($pdf, $page, "#D34817", "#ffffff", 12, MARGIN_RIGHT, MARGIN_BOTTOM-10, $outline="#ffffff");

	pdf_end_page($pdf);
	// Increment the page number
	$page+=1;
}	

function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");
	
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}
	writeReportFooter($pdf,$name,$page,"1");
}

function renderPage1($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);
	
	$flid=getFLID("meta","ind0");
	$mltxt=getPTMLText($flid,"3",$lid);

	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,600,0.2);
		
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);
	$size=20.0;
	pdf_setfont($pdf,$font,$size);

	
	
  $colors = rgb2cmyk(hex2rgb("#D34817"));	
  pdf_setcolor($pdf, "both", "cmyk", $colors['c'], $colors['m'], $colors['y'], $colors['k']);	
	//$title="Individual Version";
  $title=$mltxt[11];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$bfont,$size),525);
	//$title="Feedback Report";
	$title=$mltxt[12];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$bfont,$size),500);
	
  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);	  
	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	$title="Sal Capobianco, Ph.D.               Mark Davis, Ph.D.               Linda Kraus, Ph.D.";
	pdf_show_xy($pdf,$title,centerXcord($pdf, $title, (PAGE_WIDTH/2))-10,350);

	// 6 inches form top of 11 inch page
	$size=12.0;
	$ypos=290;
	$linehgth=1.75*$size;
	
	pdf_setfont($pdf,$font,$size-2);
//	$title="Prepared for:";
	$title=$mltxt[13];
	pdf_show_xy($pdf,$title,centerXcord($pdf, $title, (PAGE_WIDTH/2)),$ypos);

  $colors = rgb2cmyk(hex2rgb("#AEAAA7"));	
  pdf_setcolor($pdf, "both", "cmyk", $colors['c'], $colors['m'], $colors['y'], $colors['k']);	
  pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,$name,centerXcord($pdf, $name, (PAGE_WIDTH/2)),$ypos-$linehgth);

  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
  pdf_setfont($pdf,$font,$size-2);	
	//$title=date("F j, Y");
	$title=getPTdate('d M Y');
	pdf_show_xy($pdf,$title,centerXcord($pdf, $title, (PAGE_WIDTH/2)),$ypos-$linehgth-$linehgth);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage2($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	
	$flid=getFLID("meta","ind0");
	$mltxt=getPTMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$size=16.0;
	pdf_setfont($pdf,$bfont,$size);
	
	// Table of contents
	$title=$mltxt[21];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),650);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$y=650;
	$x=55;
	$x2=475;
	
//	Introduction
	pdf_show_xy($pdf,$mltxt[22],$x,$y-=30);	
	pdf_show_xy($pdf,"3",$x2,$y);

//	Guide to Your Feedback Report
	pdf_show_xy($pdf,$mltxt[23],$x,$y-=30);	
	pdf_show_xy($pdf,"4",$x2,$y);

//	Constructive Response Profile
	pdf_show_xy($pdf,$mltxt[24],$x,$y-=30);	
	pdf_show_xy($pdf,"5",$x2,$y);

//	Destructive Response Profile
	pdf_show_xy($pdf,$mltxt[25],$x,$y-=30);	
	pdf_show_xy($pdf,"6",$x2,$y);

//	Hot Buttons Profile
	pdf_show_xy($pdf,$mltxt[26],$x,$y-=30);	
	pdf_show_xy($pdf,"7",$x2,$y);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage3($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind3");
	$mltxt=getPTMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	// Introduction
	$title=$mltxt[1];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);
	
// 11 => Por conflito entende-se qualquer situação em que as pessoas têm interesses, objectivos, princípios
// 12 => ou sentimentos incompatíveis. Esta é, obviamente, uma definição abrangente e engloba muitas
// 13 => situações distintas. O conflito pode surgir, por exemplo, ligado a um conjunto de problemas que se
// 14 => agravam no tempo, uma diferença de opinião em relação à estratégia ou táctica para a consecução
// 15 => de um objectivo económico, convicções divergentes, disputa para obtenção recursos, etc. Também
// 16 => pode ocorrer quando uma pessoa age de uma maneira que outra considera insensível, irreflectida
// 17 => ou violenta. Um conflito, em suma, pode resultar de qualquer coisa que o coloque a si e à outra
// 18 => pessoa em oposição.	
	$size=11.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,$mltxt[11],50,640);
	
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
  pdf_continue_text($pdf,$mltxt[18]);	
  pdf_continue_text($pdf,"   ");   
  pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	
// 21 => Assim, o conflito é inerente à vida em sociedade. Apesar de tentarmos naturalmente evitar o conflito,
// 22 => por vezes encontramo-nos inevitavelmente, em desacordo com outras pessoas. Mas isso não é
// 23 => necessariamente negativo. Certos tipos de conflitos podem ser produtivos -- pontos de vista
// 24 => divergentes podem levar a soluções criativas para os problemas. O que distingue em grande medida
// 25 => o conflito útil do conflito destrutivo é a forma como as pessoas reagem numa situação conflitual.
// 26 => Assim, embora o conflito em si mesmo seja inevitável, é possível evitar respostas ineficazes e
// 27 => prejudiciais e aprender respostas eficazes e benéficas. Essa proposta constitui o cerne do Relatório
// 28 => de Feedback sobre o Conflict Dynamics Profile (CDP).
	pdf_continue_text($pdf,$mltxt[21]);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,$mltxt[26]);
	pdf_continue_text($pdf,$mltxt[27]);
	pdf_continue_text($pdf,$mltxt[28]);
  pdf_continue_text($pdf,"   ");	
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	
// 31 => Algumas reacções ao conflito, quer ocorram na fase inicial ou depois de o mesmo deflagrar podem  
// 32 => ser consideradas respostas construtivas. Têm o efeito de não provocar a escalada do conflito. 
// 33 => Tendem a reduzir a tensão e a manter o conflito centrado nas ideias e não nas pessoas. As respostas 
// 34 => destrutivas, por outro lado, tendem a agravar o problema – não contribuem para atenuar o conflito 
// 35 => e permitem que este continue centrado nas pessoas. Se pensarmos no conflito como um incêndio, as 
// 36 => respostas construtivas ajudam a extinguir o incêndio, ao passo que as destrutivas o agravam. 
// 37 => Obviamente, é melhor reagir ao conflito com respostas construtivas do que destrutivas.
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf,$mltxt[32]);
	pdf_continue_text($pdf,$mltxt[33]);
	pdf_continue_text($pdf,$mltxt[34]);
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf,$mltxt[37]);
	pdf_continue_text($pdf,"   ");	
  pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");  

// 41 => Também é possível entender as respostas ao conflito não nesta perspectiva simplista - construtivas  
// 42 => ou destrutiva - mas como diferentes abordagens, ou seja, mais activas ou mais passivas. As respo- 
// 43 => stas activas são aquelas em que o indivíduo toma uma medida clara em resposta ao conflito ou  
// 44 => provocação. Podem ser construtivas ou destrutivas - o que as torna activas é que exigem um esforço 
// 45 => visível da parte do indivíduo. As respostas passivas, em contrapartida, não exigem grande esforço. 
// 46 => Como são passivas, envolvem principalmente a decisão de não tomar qualquer tipo de medida. 
// 47 => Também estas podem ser construtivas ou destrutivas - isto é, podem melhorar ou piorar a situação.
	pdf_continue_text($pdf,$mltxt[41]);
	pdf_continue_text($pdf,$mltxt[42]);
	pdf_continue_text($pdf,$mltxt[43]);
	pdf_continue_text($pdf,$mltxt[44]);
	pdf_continue_text($pdf,$mltxt[45]);
	pdf_continue_text($pdf,$mltxt[46]);
	pdf_continue_text($pdf,$mltxt[47]);
	pdf_continue_text($pdf,"   ");	
  pdf_continue_text($pdf,"   ");

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage4($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind5");   //3005
	$mltxt=ck4decode(getMLText($flid,"3",$lid)); 

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
		
	$size=16.0;
  $dY=680;     
	pdf_setfont($pdf,$font,$size);
  $title = "Guide to your Feedback Report";
  pdf_show_xy($pdf,$mltxt[1],calcCenterStartPos($pdf,$mltxt[1],$font,$size),$dY);

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$mltxt[2],50,$dY-=34);
  pdf_setfont($pdf,$font,$size);
  $dY-=20;   
  for($i=5; $i<=40; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],94,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }
  
  $dY-=22;   
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,$mltxt[45],50,$dY);
  pdf_setfont($pdf,$font,$size);  
  $dY-=24;   
  for($i=50; $i<=90; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],94,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }     
	
	writeReportFooter($pdf,$name,$page,$lid);
}

function getSelfOnlyResponses($cid,$loSid,$hiSid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from SELFONLYREPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID between $loSid and $hiSid order by a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function drawSelfOnlyGraph(&$pdf,$x,$y,$width,$height,$data,$lid){
	$flid=getFLID("meta","indgraph");
	$mltxt=getPTMLText($flid,"3",$lid);

	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=count($data);
	$ystep=round($height/9);
	$xstep=round($width/($scales+1));
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
  
  $shade=array(0,3,6);	
	// 1. Horizontal divisions
	$i;
	for($i=0;$i<8;$i++){
	  if(in_array($i,$shade)){
    $colors = rgb2cmyk(hex2rgb("#cdcdcd"));	
    pdf_setcolor($pdf, "fill", "cmyk", $colors['c'], $colors['m'], $colors['y'], $colors['k']);			 
    //pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
     pdf_rect($pdf,($x+$xstep),$y+$ystep+round($i*$ystep),($width-$xstep),(2*$ystep));
     pdf_fill($pdf);
     pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);	  
	  }
		pdf_moveto($pdf,($x+$xstep),$y+$ystep+round($i*$ystep));
		pdf_lineto($pdf,($x+$width),$y+$ystep+round($i*$ystep));
		pdf_stroke($pdf);
	}

	// 2. the box outline 	
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);
	
	// divide the first box
	pdf_moveto($pdf,($x+$xstep),($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));
	
	// little box that juts out
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x,$y);
	pdf_lineto($pdf,$x,$y+round($ystep/2));
	pdf_lineto($pdf,$x+$xstep,$y+round($ystep/2));
	
	
	// 3. Vertical divisions
	for($i=$x+round(2*$xstep);$i<$x+$width;$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,$y+$ystep);
	}
	pdf_stroke($pdf);
	
	// 4. Vertical Scale
	pdf_setfont($pdf,$font,10.0);
  $hYs=array();
	$vs=array("30","35","40","45","50","55","60","65");
	$j=0;
	for($i=($y+$ystep);($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$hYs[]= $i;
		if(4==$j){
			pdf_setfont($pdf,$font,10.0);
			// Average
			pdf_show_xy($pdf,$mltxt[31],$x-2,$i);
			pdf_setfont($pdf,$font,10.0);
		}
		$j++;
	}
  //die(print_r($hYs));
	pdf_show_xy($pdf,"70",($x+round(2*$xstep/3)),$y+$height-2);
	pdf_setfont($pdf,$font,10.0);
	
	//"Very","Low","","","High","Very"
	//"Low","","","","","High"
	$vs=array("",$mltxt[32],$mltxt[33],"","","",$mltxt[34],$mltxt[32],"");
	$vs1=array("",$mltxt[33],"","","","","",$mltxt[34],"");
	$j=0;
	//for($i=($y+round(1.5*$ystep));($i<$y+$height);$i+=$ystep){
	 foreach($hYs as $hY){
	  switch($j){
	   case 1:
	   case 7:	   
	     $adjY = $hY+10;
	     break;
	   case 2: 
	     $adjY = $hY+25;
	     break;	 
	   case 6: 
	     $adjY = $hY-25;
	     break;
     default:
       $adjY = $hY; 
       break;       
	  }
	  
		pdf_show_xy($pdf,$vs[$j],$x-3,$adjY);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}

	// which scale do we have? we can determine from the SID
	switch($data[0][3]){
		case 1:
			//"Perspective Taking","Creating Solutions","Expressing Emotions","Reaching Out","Reflective Thinking","Delay Responding","Adapting"
			$vs=array($mltxt[1],$mltxt[2],$mltxt[3],$mltxt[4],$mltxt[5],$mltxt[6],$mltxt[7]);
			break;
		case 8:
			//"Winning","Displaying Anger","Demeaning Others","Retaliating","Avoiding","Yielding","Hiding Emotions","Self- Criticizing"
			$vs=array($mltxt[11],$mltxt[12],$mltxt[13],$mltxt[14],$mltxt[15],$mltxt[16],$mltxt[17],$mltxt[18]);
			break;
		case 22:
			//"Unrely","Over Analyze","Unapp","Aloof","Micro Manage","Self Centered","Abrasive","Untrust","Hostile"
			$vs=array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]);
			break;
	}
	
	// 5. Draw the scale

	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$offset=round(0.30*$ystep)+2;	
    if(($vs[$i] != "Criar Soluções")&&(count(explode(" ",$vs[$i])) > 1)){
      $offset=round(0.40*$ystep)+2;
    }
		if($data[0][3]==22) 	
      pdf_setfont($pdf,$font,7.0);
    else
      pdf_setfont($pdf,$font,9.0);
      
		pdf_show_boxed($pdf,$vs[$i],$j,($y+round($ystep/2)),$xstep,$offset,"center","");
		$j+=$xstep;
	}
	pdf_setfont($pdf,$font,10.0);
	
	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,$y,$xstep,round(0.35*$ystep),"center","");
		$j+=$xstep;
	}

	// Value
	pdf_show_xy($pdf,$mltxt[35],$x+5,$y+5);
	
	// 7. Draw the graph
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val<0){
		$clip2=true;
		$val=0;
		// 01-25-2005: Fixing a border condition
		$n1=0;
	}
	elseif($val>40){
		$clip2=true;
		$val=40;
		// 01-25-2005: Fixing a border condition
		$n1=40;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=30;
		if($val<0){
			$clip2=true;
			$val=0;
			$n1=0;
		}
		elseif($val>40){
			$clip2=true;
			$val=40.5;
			$n1=40.5;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+$ystep+($n1*($ystep/5)));
		}
		
		$yc=$y+$ystep+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_moveto($pdf,$x+5,$y+round($ystep/3));
	pdf_lineto($pdf,$x+$xstep-5,$y+round($ystep/3));
	pdf_stroke($pdf);	
  $colors = rgb2cmyk(hex2rgb("#D34817"));	
  pdf_setcolor($pdf, "both", "cmyk", $colors['c'], $colors['m'], $colors['y'], $colors['k']);	
	//pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);

	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val>=0&&$val<=40){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=30;
		if($val>=0&&$val<=40){
			$yc=$y+$ystep+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	/*	*/
	pdf_circle($pdf,$x+round($xstep/2),$y+round($ystep/3),4);
	pdf_fill_stroke($pdf);
}

function renderPage5($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getPTMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Constructive Responses
	$title=$mltxt[51];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	Higher numbers are more desirable
	$title=$mltxt[52];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getSelfOnlyResponses($cid,"1","7");
	if($rows){
		drawSelfOnlyGraph($pdf,50,150,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage6($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getPTMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//  Destructive Responses
	$title=$mltxt[61];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=$mltxt[62];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getSelfOnlyResponses($cid,"8","15");
	if($rows){
		drawSelfOnlyGraph($pdf,50,150,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage7($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind7");
	$mltxt=getPTMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title=$mltxt[1];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	
// 11 => Esta parte do Relatório de Feedback do Conflict Dynamics Profile (CDP) é um pouco diferente das 
// 12 => restantes. Em vez de indicar o modo como você responde normalmente a situações de conflito, dá-
// 13 => lhe uma percepção dos tipos de pessoas e situações que podem perturbá-lo e causar potencialmente 
// 14 => a ocorrência do conflito. Resumindo, os seus "Gatilhos". 	
	pdf_show_xy($pdf,$mltxt[11],70,660);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
// 15 => Segue-se uma breve descrição de cada um dos seus "Gatilhos" medidos pelo CDP e na página seguinte 
// 16 => um gráfico que ilustra até que ponto considera perturbadora cada situação comparativamente com a 
// 17 => generalidade das pessoas. Obviamente, estes não representam todos os seus "Gatilhos" possíveis; 
// 18 => são simplesmente alguns dos mais comuns. Em cada caso, uma pontuação mais elevada na escala  
// 19 => indica que se irrita e perturba facilmente com a situação.	
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	pdf_continue_text($pdf,$mltxt[18]);
	pdf_continue_text($pdf,$mltxt[19]);
	pdf_continue_text($pdf,"  ");


	//You get especially irritated and upset when working with people who are
	//unreliable, miss deadlines, and cannot be counted on.
	//You get especially irritated and upset when working with people who are
	//perfectionists, overanalyze things, and focus too much on minor issues.
	//You get especially irritated and upset when working with people who fail to
	//give credit to others or seldom praise good performance.
	//You get especially irritated and upset when working with people who
	//isolate themselves, do not seek input from others, or are hard to approach.
	//You get especially irritated and upset when working with people who
	//constantly monitor and check up on the work of others.
	//You get especially irritated and upset when working with people who are
	//self-centered, or believe they are always correct.
	//You get especially irritated and upset when working with people who are
	//arrogant, sarcastic, and abrasive.
	//You get especially irritated and upset when working with people who
	//exploit others, take undeserved credit, or cannot be trusted.
	//You get especially irritated and upset when working with people who lose
	//their tempers, become angry, or yell at others.

	$size=10.0;
	pdf_setfont($pdf,$bfont,$size);
  pdfUnderline($pdf,$mltxt[40],180,530); 
  	
  pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,$mltxt[21],180,500); 
  pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
		
	//pdf_continue_text($pdf,"Those who are perfectionists, over-analyze things and focus too much ");
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf," ");  	
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[25]); 
	pdf_continue_text($pdf,$mltxt[26]); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[27]); 
	pdf_continue_text($pdf,$mltxt[28]); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
  pdf_continue_text($pdf,$mltxt[29]);
//	pdf_continue_text($pdf,$mltxt[30]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,$mltxt[31]);  
//	pdf_continue_text($pdf,$mltxt[32]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,$mltxt[33]); 
//	pdf_continue_text($pdf,$mltxt[34]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,$mltxt[37]);
	//pdf_continue_text($pdf,$mltxt[38]); //???

	pdf_setfont($pdf,$bfont,$size);
	

	//Unreliable
	//Overly-Analytical
	//Unappreciative
	//Aloof
	//Micro-Managing
	//Self-Centered
	//Abrasive
	//Untrustworthy
	//Hostile
  pdfUnderline($pdf,$mltxt[39],70,530); //???
  
	pdf_show_xy($pdf,$mltxt[2],70,500);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	$label=explode(" ",$mltxt[3]);
	pdf_continue_text($pdf,$label[0]);
	pdf_continue_text($pdf,$label[1]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[4]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[5]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[6]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[7]);
	pdf_continue_text($pdf,$mltxt[67]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[8]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[9]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[10]);
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage8($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getPTMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Hot Buttons
	$title=$mltxt[81];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=$mltxt[82];
  //"(Higher numbers indicate greater frustration or irritation in response to this kind of behavior.)";
  // ???
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);
  pdf_show_xy($pdf,$mltxt[83],calcCenterStartPos($pdf,$mltxt[83],$font,$size),647);
	$rows=getSelfOnlyResponses($cid,"22","30");
	if($rows){			
		drawSelfOnlyGraph(&$pdf,50,150,500,450,$rows,$lid);	
	}
	else{			
		pdf_show_xy($pdf,"Cannot graph hot buttons",200,500);	
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function getPTdate($format){
  //$rptdt=date('D M d, Y');
  $rptdt=date('D-M-j-d-Y');
  $dtStrs=explode("-",$rptdt);
  //die(print_r($dtStrs));
  $day=array();
    $day['Mon']='Segunda-Feira';
    $day['Tue']='Terça-Feira';  
    $day['Wed']='Quarta-Feira';
    $day['Thu']='Quinta-Feira';
    $day['Fri']='Sexta-Feira';
    $day['Sat']='Sábado';
    $day['Sun']='Domingo';
                          
  $month=array();
    $month['Jan']='Janeiro';
    $month['Feb']='Fevereiro';
    $month['Mar']='Março';
    $month['Apr']='Abril';
    $month['May']='Maio';
    $month['Jun']='Junho';
    $month['Jul']='Julho';
    $month['Aug']='Agosto';
    $month['Sep']='Setembro';
    $month['Oct']='Outubro';
    $month['Nov']='Novembro';
    $month['Dec']='Dezembro';
  
  switch($format){
    case "d M Y"    :
      $retVal =$dtStrs[2]." ";
      $retVal.="de ".$month[$dtStrs[1]]." ";  
      $retVal.="de ".$dtStrs[4];
      break;              
    case "D, j d Y" :
    default :
      $retVal =$day[$dtStrs[0]].", ";
      $retVal.=$dtStrs[2]." ";
      $retVal.="de ".$month[$dtStrs[1]]." ";
      $retVal.="de ".$dtStrs[4];           
      break;
  }
  
  return $retVal;
                          
}

function getPTMLText($flid,$tid,$lid,$pos=""){
	$single=false;
	$conn=dbConnect();
	$query="select Descr, Pos from MLText where FLID=$flid and LID=$lid and TID=$tid";
	if(strlen($pos)>0){
		$query=$query." and Pos=$pos";
		$single=true;
	}
	else{
		$query=$query." order by Pos";
	}
	//die($query);
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	
	if(true==$single){
		// we only asked for one value
		$row=mysql_fetch_array($rs);
		return utf8_decode($row[0]); 
	}
	
	// potentially multiple values
	$rc=array();
	$rows=dbRes2Arr($rs);
	foreach($rows as $row){
		// Stuff in the correctt position of the array
		// Note that we decode UTF-8 multibyte characters
		$idx=$row[1];
		$rc[$idx]=utf8_decode($row[0]);  
	}
	return $rc;
}

function magic_circle($pdf, $num, $hexCircle, $hexNum, $r, $x, $y, $outline="#ffffff"){
  /**
  * This function creates a colored, numbered circle.
  *
  * @param $pdf       : pdf resource object
  * @param $num       : number to display
  * @param $hexCircle : hexadecimal color of circle
  * @param $hexNum    : hexadecimal color of number
  * @param $r         : radius of circle
  * @param $x         : horizontal offset
  * @param $y         : vertical offset
  * @param $outline   : hex color of circle outline; default is white
  *
  **/

  // font style for number
  $font = PDF_findfont($pdf, "Helvetica", "winansi", 0);

  // calculate cmyk color of circle
  $rgbCircle = hex2rgb($hexCircle);
  $cmykCircle = rgb2cmyk($rgbCircle);

  // calculate cmyk color of outline
  $rgbOutline = hex2rgb($outline);
  $cmykOutline = rgb2cmyk($rgbOutline);

  // calculate cmyk color of number
  $rgbNum = hex2rgb($hexNum);
  $cmykNum = rgb2cmyk($rgbNum);

  // display a circle with provided color values
  // display a circle with provided color values
	pdf_setColor($pdf, "fill", "cmyk", $cmykCircle['c'], $cmykCircle['m'], $cmykCircle['y'], $cmykCircle['k']);
	pdf_setcolor($pdf,"stroke","cmyk",$cmykOutline['c'],$cmykOutline['m'],$cmykOutline['y'],$cmykOutline['k']);
	pdf_setlinewidth($pdf, 0.50);
  pdf_circle($pdf, $x, $y, $r);
  pdf_fill_stroke($pdf);

  // the size ratio between the circle and number
  $ratio = 1.2222222222222;
  //$ratio = 1.0000000000000;
  // calculate the fontsize for the number
  $fontSize = $r / $ratio;
  $fontSize = round($fontSize);

  // calculations to position the number in the center of circle
  $x_pos = (($r / $fontSize)+($r - $fontSize))+1;  // for one-digit number
  $y_pos = ((($r*2) - $fontSize) / 5)+ 1;
  $x2_pos = $x_pos * 1.666667;                 // for two-digit number

  // display a number inside the circle...
  pdf_setcolor($pdf, "both", "cmyk", $cmykNum['c'], $cmykNum['m'], $cmykNum['y'], $cmykNum['k']);
  PDF_setfont($pdf, $font, $fontSize);
  if($num < 10){
    PDF_show_xy($pdf, $num, $x-$x_pos, $y-$y_pos);  // for one-digit number
  }
  else{
    PDF_show_xy($pdf, $num, $x-$x2_pos, $y-$y_pos); // for two-digit number
  }
}
?>

<?php
// Multilingual support added
function sendGenericMail($to,$from,$subject,$body,$name,$lid="1",$ccEmailids=array()){
  /**
   * returnpath added 01/27/2009, thanks Jake Carr @ rackspace for this suggestion.
   * JPC
   * */
   $returnpath = "<apache@onlinecdp.org>";
              
	// Note: We always send from this account
	$hdrs="From: Conflict Dynamics <administrator@onlinecdp.org>\n";
//	$hdrs .= "Reply-To: cdpcomments@eckerd.edu\r\n";
	$hdrs .= "Reply-To: $from\n";

	if (count($ccEmailids) > 0) {
		$hdrs .="Cc: " . implode(", ", $ccEmailids) . "\n";
	}

	// wbs 9/27/2007
	// patty gets ALL emails, not just scoring emails...
//    if(strstr($subject, "Scoring Request") === true){
	$hdrs .="Bcc: viscompe@eckerd.edu,alburymg@eckerd.edu,dtiservices@thediscoverytec.com\n";
//    }

	$bod='';
    
  if(!is_null($name)){
		$bod.= getEmailGreeting($name, $lid) . "\n\n".$body;
	} else {
		$bod.=$body;
	}
	
	// But we always include the sender in the email address
	if($lid=="8"){
		mail($to,$subject,$bod."\n\n( Emisor : ".$from." )\n\n",$hdrs, "-f$returnpath");
	}
	else{
		mail($to,$subject,$bod."\n\n( Sender : ".$from." )\n\n",$hdrs, "-f$returnpath");
	}
    // next line for debugging
    error_log("[MAIL][TO: $to]");
    return true;
}


function getURLRoot(){
	// production
	// temporary Rackspace URL
  $rc= "https://www.onlinecdp.org";
	//$rc= "https://98.129.134.138";
	return $rc;
}

/**
 * 03/04/2008 - JPC
 * Replaced the above function so it could be used for the main and 
 * aliased directories.
 **/  
/*function getURLRoot($alias=0){
	// production
	$dirs=explode("/",$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
	$URLroot ="https://";
	for($x=0; $x<=$alias; $x++){
	$URLroot.=($x==0)? $dirs[$x] : "/".$dirs[$x];	
  }
	return $URLroot;
}*/

function getReportURLRoot($alias=false){
	// production
	// temporary Rackspace URL
	$rc= "http://www.onlinecdp.org/";
	//$rc= "http://98.129.134.138/";
  $rc.=($alias)? $alias."/cons/" : "cons/";
	return $rc;
}

/* Same as above, but for port 80, not port 443
// This has to do with an IE bug for reporting
function getReportURLRoot(){
	// production
	// temporary Rackspace URL
	$rc= "http://www.onlinecdp.org/cons/";
	return $rc;
}
*/

// multilingual support added
function getDisclaimer($lid="1"){
	if($lid=="8"){
		return "  POR FAVOR NO RESPONDA A ESTE MENSAJE.\n\n  LAS RESPUESTAS A ESTE MENSAJE POR VIA ELECTRONICA, SON ENVIADAS A UNA DIRECCION DE CORREOS NO FUNCIONAL, Y NO RECIBIRAN RESPUESTA.";
	}
	return "  PLEASE DO NOT REPLY TO THIS MESSAGE.\n\n  REPLIES TO THIS MESSAGE ARE SENT TO A NON-OPERATIONAL MAIL LOCATION AND WILL NOT RECEIVE A RESPONSE.";
}

function getEmailGreeting( $name=null, $lid=1, $previewOnly=false ) {
	// Returns the greeting for the email body
	
	if ($previewOnly) $name = '________, <i style="background-color:#FFFC17;">(Participant\'s name will be inserted when email is sent.)</i>';
		else $name .= ",";

	if (!is_null($name)) {
		if ($lid=="8") {
			$str="Estimado(a) $name";
		} else {
			$str="Dear $name";
		}
  }

  return $str;

}
?>

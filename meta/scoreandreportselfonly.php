<?php
// Pulls in dbfns.php as well
require_once "multilingual.php";
require_once "pdffns.php";
// A standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);
define("MARGIN_LEFT",50);
define("MARGIN_RIGHT",565);
define("MARGIN_TOP",765);
define("MARGIN_BOTTOM",27);

// RGB values for the report
define("R_",0.85);
define("G_",0.85);
define("B_",1.0);


//----------------------------------------------------
//--- Scoring
//----------------------------------------------------

// Has the candidate responded to enough questions?
function isItComplete($cid,$pid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from RATERRESP where RID=$cid and TID=3 and VAL is not NULL");
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	// We allow 3 missing answers currently i.e. must have 96 of 99 to score
	return $row[0]>=96;
}

// get rid of all pre-existing scores for this candidate
function getRidOfOldScores($cid,$pid){
	$conn=dbConnect();
	$query="delete from SELFONLYREPORTSCORE where CID=$cid";
	mysql_query($query);
	$query="delete from SCALESCORE where RID=$cid and CID=$cid and PID=$pid";
	mysql_query($query);
}

// Compute the raw and standardized scale scores for a single self only rater
function computeSelfOnlyScaleScores($cid,$pid){
	$conn=dbConnect();
	
	// Raw score
	$query="insert into SCALESCORE (SID,RID,PID,CID,CATID,RAWSCORE,STDSCORE) ";
	$query=$query."select b.SID, a.RID, $pid, $cid, 1, AVG(VAL), 0 from RATERRESP a , SCALEITEM b, RATER c ";
	$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and a.RID=$cid and VAL is not NULL group by b.SID,a.RID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;	
	}
	
	// Standardized score
	$rows=getScaleMeanAndStd($cid,$pid);
	if(!$rows){
		return false;	
	}
	foreach($rows as $row){
		if(0!=$row[2]){
			$std=50+(10*(($row[0]-$row[1])/$row[2]));
			$query="update SCALESCORE set STDSCORE=$std where SID=$row[3] and RID=$cid and CID=$cid and PID=$pid";
			mysql_query($query);
		}
	}
	return "Calculated scale scores - OK";	
}

// function to return the appropriate Mean and Standard deviation etc for a Scale
// for self only
function getScaleMeanAndStd($cid,$pid){
	$conn=dbConnect();
	$query="select b.RAWSCORE,a.MEANVAL1,a.DEVVAL1,a.SID from SCALE a,SCALESCORE b where a.SID=b.SID and b.RID=$cid and b.CID=$cid and b.RID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// calculates the appropriate report scores for self only
function calculateSelfOnlyReportScores($cid,$pid){
	$conn=dbConnect();
	// always calculate for Self
	$query="insert into SELFONLYREPORTSCORE (SID,CID,AVGSCORE) select SID,CID,STDSCORE from SCALESCORE where RID=$cid and CID=$cid and PID=$pid and CATID=1 and STDSCORE is not NULL";
	if(false===mysql_query($query)){
		return false;
	}
	return "Calculated individual report scores - OK";
}

// checks to see if a license has been used for this candidate
function alreadyConsumedLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from SELFLIC where CONID=$conid and CID=$cid and TID=$tid");
	$row=mysql_fetch_row($rs);
	return $row[0]>0?true:false;
}

// Consumes a self-only license
function consumeSelfOnlyLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("insert into SELFLIC (CONID,CID,TID,TS) values ($conid,$cid,$tid,NOW())");
	return $rs;
}

// Score all the candidates in a program
function scoreProgram($pid){
	$conn=dbConnect();
	$msg="Scoring...";
	
	$rs=mysql_query("select CONID from PROGCONS where PID=$pid");
	$row=mysql_fetch_row($rs);
	$conid=$row[0];
	
	$rs=mysql_query("select CID,FNAME,LNAME from CANDIDATE where PID=$pid");
	$rows=dbRes2Arr($rs);
	if(false==$rows){
		return "<font color=\"#ff0000\">Unable to score program: unable to retrive participants</font>";
	}
	
	$ok=false;
	foreach($rows as $row){
		// we really don't need to score here since there's no group report
		// so let's just iterate over each candidate
		
		// Only consume a license if they are complete
		$query="select IFNULL(a.ENDDT,'Incomplete') from RATER a, CANDIDATE b,PROGRAM c where c.PID=$pid and a.CID=" . $row[0] . " and a.CID=b.CID and b.PID=c.PID";
		$rs=mysql_query($query);
		$rw=mysql_fetch_row($rs);
		if ($rw[0] != "Incomplete") {
		
			$query="select count(CID) from SELFLIC where CID=" . $row[0] . " and TID=3 and CONID=" . $_SESSION["conid"];
			$rs=mysql_query($query);
			$rw=mysql_fetch_row($rs);
			$ok=true;
		
			if ($rw[0] == 0) {
				// Only consume a license if one has not already been consumed
				$msg=$msg."<br> $row[1] $row[2]";
				$cid=$row[0];
				consumeSelfOnlyLicense($cid,$conid,3);
			}
		}
	}

	if(false==$ok){
		return "<font color=\"#ff0000\">Unable to score program: no participants</font>";
	} else {
		$msg .= '<p style="color:green;font-weight:bold;margin-bottom:0px;">The program has now been scored.<br />If you need to re-score the program you will need to re-open it first.</p>';
	}
	
	// Set status to "Scored"
	$query="update PROGRAM set EXPIRED='S' where PID=$pid";
	//echo $query."<br>";
	mysql_query($query);
	
	// Send email to consultant (added 7/6/08 by CDZ)
	sendScoreEmail($pid);
	
	return $msg."<br>";
}

//----------------------------------------------------
//------ Reporting
//----------------------------------------------------
function getCandidateName($cid){
	$conn=dbConnect();
	$query="select FNAME, LNAME from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0]." ".$row[1];	
}

function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function writeHeader($cid,&$pdf,$lid){
	$name=getCandidateName($cid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));
	
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,750,515,15);
	pdf_fill_stroke($pdf);
	// Note: (R) is hex AE, so well use sprintf to format properly
//	$what="Conflict Dynamics Profile ".sprintf("%c",0xae);
	$what=$mltxt[1];
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),754);

	return $name;	
}

function writeReportFooter(&$pdf,$name,&$page,$lid){
	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));
	
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,35,515,15);
	pdf_fill_stroke($pdf);
	$rptdt=date('D M d, Y');
	//$what=$rptdt."             This report was prepared for: ".$name."               Page ".$page;
	$what=$rptdt."             $mltxt[2] ".$name."               $mltxt[3] ".$page;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),39);	
	pdf_end_page($pdf);
	// Increment the page number
	$page+=1;
}	

function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");
	
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}
	writeReportFooter($pdf,$name,$page,"1");
}

function renderPage1($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);
	$name=ck4decode($name);
	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);

if ($img == 0){
   throw new Exception("Error: " . pdf_get_errmsg($pdf));
}
	pdf_place_image($pdf,$img,150,600,0.2);
		
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	$title="Individual Version";  
	$title=$mltxt[11];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),525);
	$title="Feedback Report";
	$title=$mltxt[12];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),500);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="Sal Capobianco, Ph.D.               Mark Davis, Ph.D.               Linda Kraus, Ph.D.";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),450);

	// 6 inches form top of 11 inch page
	$size=14.0;
	$ypos=round((5.25*PAGE_HEIGHT)/11);
	$linehgth=1.5*$size;
	
	pdf_setfont($pdf,$font,$size);
//	$title="Prepared for:";
	$title=$mltxt[13];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos);

	pdf_show_xy($pdf,strtoupper($name),calcCenterStartPos($pdf,strtoupper($name),$font,$size),$ypos-$linehgth);

	$title=date("F j, Y");
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos-$linehgth-$linehgth);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage2($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	
	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=16.0;
	pdf_setfont($pdf,$bfont,$size);
	
	// Table of contents
	$title=$mltxt[21];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),650);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$y=650;
	$x=55;
	$x2=475;
	
//	Introduction
	pdf_show_xy($pdf,$mltxt[22],$x,$y-=30);	
	pdf_show_xy($pdf,"3",$x2,$y);

//	Guide to Your Feedback Report
	pdf_show_xy($pdf,$mltxt[23],$x,$y-=30);	
	pdf_show_xy($pdf,"4",$x2,$y);

//	Constructive Response Profile
	pdf_show_xy($pdf,$mltxt[24],$x,$y-=30);	
	pdf_show_xy($pdf,"5",$x2,$y);

//	Destructive Response Profile
	pdf_show_xy($pdf,$mltxt[25],$x,$y-=30);	
	pdf_show_xy($pdf,"6",$x2,$y);

//	Hot Buttons Profile
	pdf_show_xy($pdf,$mltxt[26],$x,$y-=30);	
	pdf_show_xy($pdf,"7",$x2,$y);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage3($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind3");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	// Introduction
	$title=$mltxt[1];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);
	
	$size=11.0;
	pdf_setfont($pdf,$font,$size);
//	Conflict refers to any situation in which people have incompatible interests, goals, principles, or
//	feelings. This is, of course, a broad definition and encompasses many different situations. A conflict
//	could arise, for instance, over a long-standing set of issues, a difference of opinion about strategy or
//	tactics in the accomplishment of some business goal, incompatible beliefs, competition for resources,
//	and so on. Conflicts can also result when one person acts in a way that another individual sees as
//	insensitive, thoughtless, or rude. A conflict, in short, can result from anything that places you and
//	another person in opposition to one another.
	pdf_show_xy($pdf,$mltxt[11],50,640);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	pdf_continue_text($pdf,$mltxt[18]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	
//	Thus, conflict in life is inevitable. Despite our best efforts to prevent it, we inevitably find ourselves
//	in disagreements with other people at times. This is not, however, necessarily bad. Some kinds of
//	conflict can be productive--differing points of view can lead to creative solutions to problems. What
//	largely separates useful conflict from destructive conflict is how the individuals respond when the
//	conflict occurs. Thus, while conflict itself is inevitable, ineffective and harmful responses to conflict can
//	be avoided, and effective and beneficial responses to conflict can be learned. That proposition is at
//	the heart of the Conflict Dynamics Profile (CDP) Feedback Report you have received.
	pdf_continue_text($pdf,$mltxt[21]);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,$mltxt[26]);
	pdf_continue_text($pdf,$mltxt[27]);
	pdf_continue_text($pdf,$mltxt[28]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	
//	Some responses to conflict, whether occurring at its earliest stages or after it develops, can be
//	thought of as constructive responses. That is, these responses have the effect of not escalating the
//	conflict further. They tend to reduce the tension and keep the conflict focused on ideas, rather than
//	personalities. Destructive responses, on the other hand, tend to make things worse--they do little to
//	reduce the conflict, and allow it to remain focused on personalities. If conflict can be thought of as a
//	fire, then constructive responses help to put the fire out, while destructive responses make the fire
//	worse. Obviously, it is better to respond to conflict with constructive rather than destructive responses.
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf,$mltxt[32]);
	pdf_continue_text($pdf,$mltxt[33]);
	pdf_continue_text($pdf,$mltxt[34]);
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf,$mltxt[37]);
	pdf_continue_text($pdf,$mltxt[38]);
	pdf_continue_text($pdf,$mltxt[39]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

//	It is also possible to think of responses to conflict not simply as constructive or destructive, but as
//	differing in terms of how active or passive they are. Active responses are those in which the individual
//	takes some overt action in response to the conflict or provocation. Such responses can be either
//	constructive or destructive--what makes them active is that they require some overt effort on the part
//	of the individual. Passive responses, in contrast, do not require much in the way of effort from the
//	person. Because they are passive, they primarily involve the person deciding to not take some kind of
//	action. Again, passive responses can be either constructive or destructive--that is, they can make
//	things better or they can make things worse.
	pdf_continue_text($pdf,$mltxt[41]);
	pdf_continue_text($pdf,$mltxt[42]);
	pdf_continue_text($pdf,$mltxt[43]);
	pdf_continue_text($pdf,$mltxt[44]);
	pdf_continue_text($pdf,$mltxt[45]);
	pdf_continue_text($pdf,$mltxt[46]);
	pdf_continue_text($pdf,$mltxt[47]);
	pdf_continue_text($pdf,$mltxt[48]);
	pdf_continue_text($pdf,$mltxt[49]);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderNewPage4($cid,&$pdf,&$page,$pid,$lid){
  $name=writeHeader($cid,$pdf,$lid); 
	$flid=getFLID("meta","ind5");   //3005
	$mltxt=ck4decode(getMLText($flid,"3",$lid));    
	$font=pdf_findfont($pdf,"Helvetica","winansi",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","winansi",0);
	//$ifont=pdf_findfont($pdf,"Helvetica-Italic","host",0);	
	$size=16.0;
  $dY=680;     
	pdf_setfont($pdf,$font,$size);
  $title = "Guide to your Feedback Report";
  pdf_show_xy($pdf,$mltxt[1],calcCenterStartPos($pdf,$mltxt[1],$font,$size),$dY);

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$mltxt[2],50,$dY-=24);
  pdf_setfont($pdf,$font,$size);
  $dY-=20;   
  for($i=5; $i<=40; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],102,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }
    
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,$mltxt[45],50,$dY);
  pdf_setfont($pdf,$font,$size);  
  $dY-=24;   
  for($i=50; $i<=90; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],102,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }     
/*  
  $ptext=array();
  $ptext[]="Constructive Response Profile";
  $ptext[]="Seven ways of responding to conflict that have the effect of reducing conflict which are:";
  $ptext[]="Perspective Taking - putting yourself in the other person's position and trying to understand that";
  $ptext[]="person's point of view.";
  $ptext[]="Creating Solutions - brainstorming with the other person, asking questions, and trying to create";
  $ptext[]="solutions to the problem.";
  $ptext[]="Expressing Emotions - talking honestly with the other person and expressing your thoughts and";
  $ptext[]="feelings.";
  $ptext[]="Reaching Out - reaching out to the other person, making the first move, and trying to make amends.";
  $ptext[]="Reflective Thinking - analyzing the situation, weighing the pros and cons, and thinking about the";
  $ptext[]="best response.";
  $ptext[]='Delay Responding - waiting things out, letting matters settle down, or taking a "time out" when';
  $ptext[]="emotions are running high.";
  $ptext[]="Adapting - staying flexible, and trying to make the best of the situation.";

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$ptext[0],50,640);
	pdf_continue_text($pdf," ");  
  
  pdf_setfont($pdf,$font,$size);
  pdf_continue_text($pdf,$ptext[1]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[2]);
  pdf_continue_text($pdf,$ptext[3]);
	pdf_continue_text($pdf," ");
  pdf_continue_text($pdf,$ptext[4]);
  pdf_continue_text($pdf,$ptext[5]);
	pdf_continue_text($pdf," ");
  pdf_continue_text($pdf,$ptext[6]);
  pdf_continue_text($pdf,$ptext[7]);
	pdf_continue_text($pdf," ");	
  pdf_continue_text($pdf,$ptext[8]);
	pdf_continue_text($pdf," ");
  pdf_continue_text($pdf,$ptext[9]);
  pdf_continue_text($pdf,$ptext[10]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[11]);
  pdf_continue_text($pdf,$ptext[12]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[13]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
    
	pdf_setfont($pdf,$bfont,$size); 
  $ptext=array();
  $ptext[]="Destructive Response Profile";
  $ptext[]="Eight ways of responding to conflict that have the effect of escalating conflict which are:"; 
  $ptext[]="Winning at All Costs - arguing vigorously for your own position and trying to win at all costs."; 
  $ptext[]="Displaying Anger - expressing anger, raising your voice, and using harsh, angry words.";
  $ptext[]="Demeaning Others - laughing at the other person, ridiculing the other's ideas, and using sarcasm."; 
  $ptext[]="Retaliating - obstructing the other person, retaliating against the other, and trying to get revenge."; 
  $ptext[]="Avoiding - avoiding or ignoring the other person, and acting distant and aloof."; 
  $ptext[]="Yielding - giving in to the other person in order to avoid further conflict."; 
  $ptext[]="Hiding Emotions - concealing your true emotions even though feeling upset."; 
  $ptext[]="Self-Criticizing - replaying the incident over in your mind, and criticizing yourself for not handling it"; 
  $ptext[]="better.";
    
  pdf_continue_text($pdf,$ptext[0]);
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$font,$size);
  pdf_continue_text($pdf,$ptext[1]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[2]);	
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[3]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[4]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[5]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[6]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[7]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[8]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[9]);
  pdf_continue_text($pdf,$ptext[10]);  	  	  	  	  	  	  	  
*/                              
  writeReportFooter($pdf,$name,$page,$lid);    
}

function renderPage4($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind4");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	
//	Guide to your Feedback Report
	$title=$mltxt[1];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);
	
	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$mltxt[11],50,640);
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,$mltxt[21]);
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,$mltxt[32]);
	
	writeReportFooter($pdf,$name,$page,$lid);
}

function getSelfOnlyResponses($cid,$loSid,$hiSid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from SELFONLYREPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID between $loSid and $hiSid order by a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function drawSelfOnlyGraph(&$pdf,$x,$y,$width,$height,$data,$lid){
	$flid=getFLID("meta","indgraph");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=count($data);
	$ystep=round($height/9);
	$xstep=round($width/($scales+1));
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
  
  $shade=array(0,3,6);	
	// 1. Horizontal divisions
	$i;
	for($i=0;$i<8;$i++){
	  if(in_array($i,$shade)){
		 pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
     pdf_rect($pdf,($x+$xstep),$y+$ystep+round($i*$ystep),($width-$xstep),(2*$ystep));
     pdf_fill($pdf);
     pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);	  
	  }
		pdf_moveto($pdf,($x+$xstep),$y+$ystep+round($i*$ystep));
		pdf_lineto($pdf,($x+$width),$y+$ystep+round($i*$ystep));
		pdf_stroke($pdf);
	}

	// 2. the box outline 	
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);
	
	// divide the first box
	pdf_moveto($pdf,($x+$xstep),($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));
	
	// little box that juts out
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x,$y);
	pdf_lineto($pdf,$x,$y+round($ystep/2));
	pdf_lineto($pdf,$x+$xstep,$y+round($ystep/2));
	
	
	// 3. Vertical divisions
	for($i=$x+round(2*$xstep);$i<$x+$width;$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,$y+$ystep);
	}
	pdf_stroke($pdf);
	
	// 4. Vertical Scale
	pdf_setfont($pdf,$font,10.0);
  $hYs=array();
	$vs=array("30","35","40","45","50","55","60","65");
	$j=0;
	for($i=($y+$ystep);($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$hYs[]= $i;
		if(4==$j){
			pdf_setfont($pdf,$font,10.0);
			// Average
			pdf_show_xy($pdf,$mltxt[31],$x-15,$i);
			pdf_setfont($pdf,$font,10.0);
		}
		$j++;
	}
  //die(print_r($hYs));
	pdf_show_xy($pdf,"70",($x+round(2*$xstep/3)),$y+$height-2);
	pdf_setfont($pdf,$font,10.0);
	
	//"Very","Low","","","High","Very"
	//"Low","","","","","High"
	$vs=array("",$mltxt[32],$mltxt[33],"","","",$mltxt[34],$mltxt[32],"");
	$vs1=array("",$mltxt[33],"","","","","",$mltxt[34],"");
	$j=0;
	//for($i=($y+round(1.5*$ystep));($i<$y+$height);$i+=$ystep){
	 foreach($hYs as $hY){
	  switch($j){
	   case 1:
	   case 7:	   
	     $adjY = $hY+10;
	     break;
	   case 2: 
	     $adjY = $hY+25;
	     break;	 
	   case 6: 
	     $adjY = $hY-25;
	     break;
     default:
       $adjY = $hY; 
       break;       
	  }
	  
		pdf_show_xy($pdf,$vs[$j],$x-3,$adjY);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}

	// which scale do we have? we can determine from the SID
	switch($data[0][3]){
		case 1:
			//"Perspective Taking","Creating Solutions","Expressing Emotions","Reaching Out","Reflective Thinking","Delay Responding","Adapting"
			$vs=array($mltxt[1],$mltxt[2],$mltxt[3],$mltxt[4],$mltxt[5],$mltxt[6],$mltxt[7]);
			break;
		case 8:
			//"Winning","Displaying Anger","Demeaning Others","Retaliating","Avoiding","Yielding","Hiding Emotions","Self- Criticizing"
			$vs=array($mltxt[11],$mltxt[12],$mltxt[13],$mltxt[14],$mltxt[15],$mltxt[16],$mltxt[17],$mltxt[18]);
			break;
		case 22:
			//"Unrely","Over Analyze","Unapp","Aloof","Micro Manage","Self Centered","Abrasive","Untrust","Hostile"
			//$vs=array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]); //???
			$vs=($lid==1)? array("Unreliable",
                "Overly- Analytical",
                "Unapp- reciative",
                "Aloof",
                "Micro- Managing",
                "Self- Centered",
                "Abrasive",
                "Untrust- worthy",
                "Hostile") : array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]);
			break;
	}
	
	// 5. Draw the scale
	if($data[0][3]==22) 	pdf_setfont($pdf,$font,9.0);
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$offset=round(0.30*$ystep)+2;	
    if(($vs[$i] != "Reaching Out")&&(count(explode(" ",$vs[$i])) > 1)){
      $offset=round(0.40*$ystep)+2;
    }
		pdf_setfont($pdf,$font,9.0);
		pdf_show_boxed($pdf,$vs[$i],$j,($y+round($ystep/2)),$xstep,$offset,"center","");
		$j+=$xstep;
	}
	pdf_setfont($pdf,$font,10.0);
	
	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,$y,$xstep,round(0.35*$ystep),"center","");
		$j+=$xstep;
	}

	// Value
	pdf_show_xy($pdf,$mltxt[35],$x+5,$y+5);
	
	// 7. Draw the graph
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val<0){
		$clip2=true;
		$val=0;
		// 01-25-2005: Fixing a border condition
		$n1=0;
	}
	elseif($val>40){
		$clip2=true;
		$val=40;
		// 01-25-2005: Fixing a border condition
		$n1=40;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=30;
		if($val<0){
			$clip2=true;
			$val=0;
			$n1=0;
		}
		elseif($val>40){
			$clip2=true;
			$val=40.5;
			$n1=40.5;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+$ystep+($n1*($ystep/5)));
		}
		
		$yc=$y+$ystep+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_moveto($pdf,$x+5,$y+round($ystep/3));
	pdf_lineto($pdf,$x+$xstep-5,$y+round($ystep/3));
	pdf_stroke($pdf);	

	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);

	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val>=0&&$val<=40){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=30;
		if($val>=0&&$val<=40){
			$yc=$y+$ystep+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	/*	*/
	pdf_circle($pdf,$x+round($xstep/2),$y+round($ystep/3),4);
	pdf_fill_stroke($pdf);
}

function renderPage5($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Constructive Responses
	$title=$mltxt[51];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	Higher numbers are more desirable
	$title=$mltxt[52];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getSelfOnlyResponses($cid,"1","7");
	if($rows){
		drawSelfOnlyGraph($pdf,50,150,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage6($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//  Destructive Responses
	$title=$mltxt[61];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=$mltxt[62];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getSelfOnlyResponses($cid,"8","15");
	if($rows){
		drawSelfOnlyGraph($pdf,50,150,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage7($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind7");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title=$mltxt[1];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,$mltxt[11],70,660);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	pdf_continue_text($pdf,$mltxt[18]);
	pdf_continue_text($pdf,$mltxt[19]);
	pdf_continue_text($pdf,$mltxt[20]);

  $size=10.0;
  $arYs=array();
  pdf_setfont($pdf,$bfont,$size);
  pdfUnderline($pdf,$mltxt[39],70,530);   $arYs[]=530; 
  $dY=500;
	pdf_show_xy($pdf,$mltxt[2],70,$dY);     $arYs[]=$dY;

	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[3]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf,$mltxt[63]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[4]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");            
	pdf_continue_text($pdf,$mltxt[5]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[6]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[7]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf,$mltxt[67]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[8]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[9]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[10]);     $arYs[]=pdf_get_value($pdf,"texty", 0); 
  
	pdf_setfont($pdf,$bfont,$size);
  pdfUnderline($pdf,$mltxt[40],180,$arYs[0]); //???
  	
  pdf_setfont($pdf,$font,$size);
	$fmtA="fontname=Helvetica fontsize=10 encoding=host alignment=left";  
	//Those who are unreliable, miss deadlines and cannot be counted on.  
	//pdf_show_xy($pdf,$mltxt[21],180,$arYs[1]);
 
	$textflow = pdf_create_textflow($pdf, $mltxt[21], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[0]-20, "");

  //Those who are perfectionists, over-analyze things and focus too much on minor issues.
	$textflow = pdf_create_textflow($pdf, $mltxt[23], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[1]-30, "");

  //Those who fail to give credit to other or seldom praise good performance.
	$textflow = pdf_create_textflow($pdf, $mltxt[25], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[2]-30, "");
	
	//Those who isolate themselves, do not seek input from other or are hard to approach.
	$textflow = pdf_create_textflow($pdf, $mltxt[27], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[3]-30, "");
  
  //Those who constantly monitor and check up on the work of others.
	$textflow = pdf_create_textflow($pdf, $mltxt[29], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[4]-30, "");  
  
  //Those who are self-centered or believe they are always correct.
	$textflow = pdf_create_textflow($pdf, $mltxt[31], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[5]-30, "");  
  
  //Those who are arrogant, sarcastic and abrasive.
	$textflow = pdf_create_textflow($pdf, $mltxt[33], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[6]-30, "");    
   
  //Those who exploit others, take undeserved credit or cannot be trusted. 
	$textflow = pdf_create_textflow($pdf, $mltxt[35], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[7]-30, "");  
  
  //Those who lose their tempers, become angry or yell at others.
	$textflow = pdf_create_textflow($pdf, $mltxt[38], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[8]-30, "");      
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage8($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Hot Buttons
	$title=$mltxt[81];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=($lid==1)? "(Higher numbers indicate greater frustration or irritation in response to this kind of behavior.)" : $mltxt[82];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);

	$rows=getSelfOnlyResponses($cid,"22","30");
	if($rows){			
		drawSelfOnlyGraph(&$pdf,50,150,500,450,$rows,$lid);	
	}
	else{			
		pdf_show_xy($pdf,"Cannot graph hot buttons",200,500);	
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

?>

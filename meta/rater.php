<?php
require_once "dbfns.php";
require_once "mailfns.php";

// get raters for a consultant
function getConsRaters($cid,$catid=NULL){
    $conn=dbConnect();
    $query="select RID,CID,CATID,FNAM,LNAM,EMAIL,STARTDT,ENDDT,EXPIRED from RATER where CID=$cid";
    if(!is_null($catid)){
		$query=$query." and CATID=$catid";
    }
	//echo $query."<br>";
    $rs=mysql_query($query);
    if(!$rs){
		return false;
    }
    return dbRes2Arr($rs);
}

// get whether or not we can add based number of raters for a consultant
function getCanAdd($cid,$max=12){
    $conn=dbConnect();
    $query="select count(*) from RATER where CID=$cid";
	//echo $query."<br>";
    $rs=mysql_query($query);
    if(!$rs){
		return false;
    }
    $row=mysql_fetch_row($rs);
	return $row[0]<$max;
}

function getCatCount($cid,$catid){
    $conn=dbConnect();
    $query="select count(*) from RATER where CID=$cid and CATID=$catid";
	//echo $query."<br>";
    $rs=mysql_query($query);
    if(!$rs){
		return 0;
    }
    $row=mysql_fetch_row($rs);
	return $row[0];
}

function getRaterTest($cid){
  $qry="select c.TID from RATER a, CANDIDATE b, PROGINSTR c 
         where a.CID = b.CID and b.PID = c.PID and a.RID = $cid limit 0,1";
  
    $rs=mysql_query($qry);
    if(!$rs){
		return 0;
    }
    $row=mysql_fetch_row($rs);
	return $row[0];  
}

function listRaterForms($cid,$frm){
	$weCanAdd=getCanAdd($cid);
	$catCount=getCatCount($cid,"2");
    echo "<p><table border=1 cellpadding=5>";
    echo "<tr><td colspan=7><small>Boss (1 recommended, 1 minimum required, $catCount assigned)</small></td></tr>";
    echo "<tr>";
    echo "<td colspan=1><small>First Name</small></td>";
    echo "<td colspan=1><small>Last Name</small></td>";
    echo "<td colspan=1><small>Email</small></td>";
    echo "<td colspan=1><small>Survey Sent</small></td>";
	echo "<td colspan=2>&nbsp</td></tr>";

    $rows=getConsRaters($cid,"2");
	$idx=0;
	$cnt=0;
    if($rows){
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid=$row[0];
			$exp=("N"!=$row[8])?"Yes":"No";
			echo "<td colspan=1><input type='text' name='fname$idx' value='".stripslashes($row[3])."'></td>";
			echo "<td colspan=1><input type='text' name='lname$idx' value='".stripslashes($row[4])."'></td>";
			echo "<td colspan=1><input type='text' name='email$idx' value='".stripslashes($row[5])."'></td>";
			echo "<td colspan=1><small>$exp</small></td>";
			echo "<td colspan=1><input type='button' value='Edit' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='save';$frm.submit();\"></td>";
			echo "<td colspan=1><input type='button' value='Delete' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='del';$frm.submit();\"></td>";
			echo "</tr>";
		}
    }

	// Can we still Add?
	if($weCanAdd){
		// If so, display at least one boss addition box
		do{
			$idx++;
			echo "<tr>";
			echo "<td colspan=1><input type='text' value='' name='fname$idx'></td>";
			echo "<td colspan=1><input type='text' value='' name='lname$idx'></td>";
			echo "<td colspan=1><input type='text' value=''  name='email$idx'></td>";
			echo "<td colspan=1><small>&nbsp;</small></td>";
			echo "<td colspan=2><input type='button' value='Add' onClick=\"javascript:$frm.idx.value='$idx';$frm.what.value='add';$frm.catid.value='2';$frm.submit();\"></td>";
			echo "</tr>";
			$cnt++;
		}while($cnt<1);
	}
	echo "</table></p>";

	$catCount=getCatCount($cid,"3");
    echo "<p><table border=1 cellpadding=5>";
    echo "<tr><td colspan=7><small>Peer (5 recommended, 3 minimum required, $catCount assigned)</small></td></tr>";
    echo "<tr>";
    echo "<td colspan=1><small>First Name</small></td>";
    echo "<td colspan=1><small>Last Name</small></td>";
    echo "<td colspan=1><small>Email</small></td>";
    echo "<td colspan=1><small>Survey Sent</small></td>";
	echo "<td colspan=2>&nbsp</td></tr>";
    $rows=getConsRaters($cid,"3");
	$cnt=0;
    if($rows){
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid=$row[0];
			$exp=("N"!=$row[8])?"Yes":"No";
			echo "<tr>";
			echo "<td colspan=1><input type='text' name='fname$idx' value='".stripslashes($row[3])."'></td>";
			echo "<td colspan=1><input type='text' name='lname$idx' value='".stripslashes($row[4])."'></td>";
			echo "<td colspan=1><input type='text' name='email$idx' value='".stripslashes($row[5])."'></td>";
			echo "<td colspan=1><small>$exp</small></td>";
			echo "<td colspan=1><input type='button' value='Edit' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='save';$frm.submit();\"></td>";
			echo "<td colspan=1><input type='button' value='Delete' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='del';$frm.submit();\"></td>";
			echo "</tr>";
		}
    }

	// Can we still Add?
	if($weCanAdd){
		// If so, display at least five peer addition boxes
		do{
			$idx++;
			echo "<tr>";
			echo "<td colspan=1><input type='text' value='' name='fname$idx'></td>";
			echo "<td colspan=1><input type='text' value='' name='lname$idx'></td>";
			echo "<td colspan=1><input type='text' value=''  name='email$idx'></td>";
			echo "<td colspan=1><small>&nbsp;</small></td>";
			echo "<td colspan=2><input type='button' value='Add' onClick=\"javascript:$frm.idx.value='$idx';$frm.what.value='add';$frm.catid.value='3';$frm.submit();\"></td>";
			echo "</tr>";
			$cnt++;
		}while($cnt<5);
	}

    echo "</table></p><p>";

	$catCount=getCatCount($cid,"4");
    echo "<table border=1 cellpadding=5>";
    echo "<tr><td colspan=7><small>Direct Report (5 recommended, 3 minimum required, $catCount assigned)</small></td></tr>";
    echo "<tr>";
    echo "<td colspan=1><small>First Name</small></td>";
    echo "<td colspan=1><small>Last Name</small></td>";
    echo "<td colspan=1><small>Email</small></td>";
    echo "<td colspan=1><small>Survey Sent</small></td>";
	echo "<td colspan=2>&nbsp</td></tr>";
    $rows=getConsRaters($cid,"4");
	$cnt=0;
    if($rows){
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid=$row[0];
			$exp=("N"!=$row[8])?"Yes":"No";
			echo "<tr>";
			echo "<td colspan=1><input type='text' name='fname$idx' value='".stripslashes($row[3])."'></td>";
			echo "<td colspan=1><input type='text' name='lname$idx' value='".stripslashes($row[4])."'></td>";
			echo "<td colspan=1><input type='text' name='email$idx' value='".stripslashes($row[5])."'></td>";
			echo "<td colspan=1><small>$exp</small></td>";
			echo "<td colspan=1><input type='button' value='Edit' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='save';$frm.submit();\"></td>";
			echo "<td colspan=1><input type='button' value='Delete' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='del';$frm.submit();\"></td>";
			echo "</tr>";
		}
    }

	// Can we still Add?
	if($weCanAdd){
		// If so, display at least five DR addition boxes
		do{
			$idx++;
			echo "<tr>";
			echo "<td colspan=1><input type='text' value='' name='fname$idx'></td>";
			echo "<td colspan=1><input type='text' value='' name='lname$idx'></td>";
			echo "<td colspan=1><input type='text' value=''  name='email$idx'></td>";
			echo "<td colspan=1><small>&nbsp;</small></td>";
			echo "<td colspan=2><input type='button' value='Add' onClick=\"javascript:$frm.idx.value='$idx';$frm.what.value='add';$frm.catid.value='4';$frm.submit();\"></td>";
			echo "</tr>";
			$cnt++;
		}while($cnt<5);
	}

    echo "</table></p>";
}

function raterEmailExists($cid,$email,$rid=NULL){
//
// 01-15-2005: Patti Visconi says she needs this to be disabled so that she can test
//
return false;
//
//
    $conn=dbConnect();
    $query="select * from RATER where CID=$cid and EMAIL='$email'";
    if(!is_null($rid)){
		$query=$query." and RID<>$rid";
    }
    $rs=mysql_query($query);
    return 0==mysql_num_rows($rs)?false:true;
}
/*
function addRater($ratData){
    $conn=dbConnect();
    $i=getKey("RATER","RID");
    $query="insert into RATER (RID,CID,CATID,FNAM,LNAM,EMAIL,STARTDT,ENDDT,EXPIRED) values ($i,$ratData[0],$ratData[1],'$ratData[2]','$ratData[3]','$ratData[4]',null,null,'N')";
    if(!mysql_query($query))
		return false;
    $query="insert into RATERINSTR (RID,TID,BEGDT,ENDDT,SCOREDT,SCORED,EXPIRED) values ($i,$ratData[5],null,null,null,'N','N')";
    if(!mysql_query($query))
		return false;
    return $i;
}
*/
function addRater($ratData, $rid=null){
    if (is_null($rid)) {
       // No rid specified
       $conn=dbConnect();
       $rid=getKey("RATER","RID");
    }
    
    // Check if rater already exists
    $query = "select count(RID) as CNT from RATER where RID=$rid";
    $rs=mysql_query($query);
    $row=mysql_fetch_array($rs);
    if ($row["CNT"] == 0) {
       // New rater
       $query="insert into RATER (RID,CID,CATID,FNAM,LNAM,EMAIL,STARTDT,ENDDT,EXPIRED) values ($rid,$ratData[0],$ratData[1],'$ratData[2]','$ratData[3]','$ratData[4]',null,null,'N')";
       if(!mysql_query($query))
	   	return false;
       $query="insert into RATERINSTR (RID,TID,BEGDT,ENDDT,SCOREDT,SCORED,EXPIRED) values ($rid,$ratData[5],null,null,null,'N','N')";
       if(!mysql_query($query))
   		return false;
   		
    } else {
       // Existing rater
       $query="update RATER set CID=$ratData[0], CATID=$ratData[1], FNAM='$ratData[2]', LNAM='$ratData[3]', EMAIL='$ratData[4]' where RID=$rid";
       if(!mysql_query($query))
	   	return false;
       $query="update RATERINSTR set TID=$ratData[5] where RID=$rid"; 
       if(!mysql_query($query))
   		return false;
    }

    return $rid;
}

function deleteRater($rid){
    $conn=dbConnect();
    $query="delete from RATERRESP where RID=$rid";
    if(!mysql_query($query))
		return false;
    $query="delete from RATERCMT where RID=$rid";
    if(!mysql_query($query))
		return false;
    $query="delete from RATERINSTR where RID=$rid";
    if(!mysql_query($query))
		return false;
    $query="delete from RATER where RID=$rid";
    	return mysql_query($query);
}

function saveRater($ratData){
	$conn=dbConnect();
	$query="update RATER set FNAM='$ratData[2]', LNAM='$ratData[3]', EMAIL='$ratData[4]' where RID=$ratData[5] and CID=$ratData[0] ";
	//echo $query."<br>";
	return mysql_query($query);
}

function updateRater($col,$val,$rid,$cid){
	$conn=dbConnect();
	$query="update RATER set $col=$val where RID=$rid and CID=$cid";
	//echo $query."<br>";
	return mysql_query($query);
}

function raterList($cid){
	$conn=dbConnect();
	$query="select RID,FNAM,LNAM,EMAIL,EXPIRED,CID,CATID from RATER where CID=$cid and CATID<>1 order by LNAM";
	//echo"<hr>$query<hr>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

function raterMailList($cid, $selectedRids=array()){
	$rs=raterList($cid);
	$count=0;
	foreach($rs as $row){
		echo "\n\t" . '<tr>';
		echo "\n\t\t" . '<td><small>' . $row[2] . ', ' . $row[1] . '</small></td>';
		echo "\n\t\t" . '<td><small>' . $row[3] . '</small></td>';
		echo "\n\t\t" . '<td>';
		echo "\n\t\t\t" . '<center>';
		echo "\n\t\t\t" . '<small>';
		if (in_array($row[0], $selectedRids)) {
			$yesChecked = 'checked="checked"';
	  	$noChecked = '';
	  } else {
	   	$yesChecked = '';
	   	$noChecked = 'checked="checked"';
		}
		echo "\n\t\t\t" . 'Yes <input type="radio" name="rcp'.$count.'" id="rcp'.$count.'" value="'.$row[0].'" '.$yesChecked.' />';
		echo "\n\t\t\t" . 'No <input type="radio" name="rcp'.$count.'" id="rcp'.$count.'" value="N" '.$noChecked.' />';
		echo "\n\t\t\t" . '</small>';
		echo "\n\t\t\t" . '</center>';
		echo "\n\t\t" . '</td>';
		echo "\n\t" . '</tr>';
		$count++;
	}
	echo "\n" . '<input type="hidden" name="count" id="count" value="'.$count.'">';
}


function sendRaterEmail($rcp,$type,$mailbody,$cid){
	// get a comma separated list of RID
	$rcps=implode($rcp,",");
	if(!$rcps){
	    return false;
	}
	// use this email address as sender
	$conn=dbConnect();
	$query="";
    $query="select EMAIL from CANDIDATE where CID=$cid";
	$rs=mysql_query($query)
	    or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	$sender=$rw[0];

	// get all the recipients
	$rc=array("");
	$query="select a.EMAIL,FNAM,LNAM,RID,FNAME, LNAME from RATER a, CANDIDATE b where a.CID=b.CID and a.CID=$cid and RID in ($rcps)";
	$rs=mysql_query($query)
	    or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	while($rw){
	    $rc[]=$rw[0];
	    // build an email and send it
	    $to=$rw[0];
	    $subject="Conflict Dynamics Profile";
	    $name=$rw[1]." ".$rw[2];
	    $rid=$rw[3];
		$candnm=$rw[4]." ".$rw[5];
	    $body="";
	    if("I"==$type){
			$body=initialRaterEmail($cid, $rid,$candnm,$rw[0],false);
	    }
	    if("R"==$type){
			$body=reminderRaterEmail($cid, $rid,$candnm,$rw[0], false);
	    }
	    if("C"==$type){
			$body=customRaterEmail($mailbody, $cid, $rid,false);
	    }
	    sendGenericMail($to,$sender,$subject,$body,$name);
		// Indicate that the email is sent to this fellow
		updateRater("EXPIRED","'S'",$rid,$cid);
	    $rw=mysql_fetch_row($rs);
	}
	return implode($rc,",");
}

// added email info...
function initialRaterEmail($cid,$rid,$cand,$email,$previewOnly=false){
	$catid = getRaterCatid($rid);

	if ($previewOnly) {
		$displayEmail = '_______ <i style="background-color:#FFFC17;">(Email address will be inserted when email is sent.)</i>';
		$displayRid = '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>';
	} else {
		$displayEmail = $email;
		$displayRid = $rid;
	}

	$rc=$cand." is requesting you to fill out a respondent questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n".getURLRoot()."/assess/ \n\nYou will be asked to provide your email address and a PIN number.\n\n";
	$rc=$rc."The email address you should enter is $displayEmail \n\nYour PIN number is $displayRid \n\n";
	$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
	$rc=$rc."The questionnaire will take about 20 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	if (($catid == 3) || ($catid == 4)) {
		// Only for Peers and Direct Reports
		$rc=$rc."Your responses are strictly confidential and they will not be included in the report unless there are three or more raters.\n\n";
	}
	return $rc.getDisclaimer();
}

function reminderRaterEmail($cid,$rid,$cand,$email,$previewOnly=false){
	$catid = getRaterCatid($rid);

	if ($previewOnly) {
		$displayEmail = '_______ <i style="background-color:#FFFC17;">(Email address will be inserted when email is sent.)</i>';
		$displayRid = '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>';
	} else {
		$displayEmail = $email;
		$displayRid = $rid;
	}

	$rc="This is a reminder that ".$cand." is requesting you to fill out a respondent questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n".getURLRoot()."/assess/ \n\nYou will be asked to provide your email address and a PIN number.\n\n";
	$rc=$rc."The email address you should enter is $displayEmail \n\nYour PIN number is $displayRid \n\n";
	$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
	$rc=$rc."The questionnaire will take about 20 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	if (($catid == 3) || ($catid == 4)) {
		// Only for Peers and Direct Reports
		$rc=$rc."Your responses are strictly confidential and they will not be included in the report unless there are three or more raters.\n\n";
	}
	$rc=$rc."If you have already completed the questionnaire, you can disregard this message.\n\n";
	return $rc.getDisclaimer();
}

function customRaterEmail($body,$cid,$rid,$previewOnly=false){
	$catid = getRaterCatid($rid);

	if ($previewOnly) {
		$displayRid = '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>';
	} else {
		$displayRid = $rid;
	}

	$rc="$body\n\n";
	$rc=$rc."Your link is:\n\n".getURLRoot()."/assess/ \n\nYour pin is $displayRid \n\n";
	$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
	return $rc.getDisclaimer();
	
}

// allows a rater to log in, and verifies expiration status
function raterLogin($uid,$pin){
  //08-27-2008 JPC - removed stripslashes as people w/apostrohes in email couldn't log in
	/*$conn=dbConnect();
	$query="select EXPIRED from RATER where UPPER(EMAIL)='".strtoupper($uid)."' and RID=$pin";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row?$row[0]:false;*/
	$mysqli=dbiConnect();
	if (!($query = $mysqli->prepare("select EXPIRED from RATER where UPPER(EMAIL)=? and RID=?"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_param("si", strtoupper($uid), $pin)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if (!$query->execute()) {
		echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_result($out_EXPIRED)) {
		echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$row = $query->fetch();
	return $out_EXPIRED;
}

// returns a raters Candidate by RID
function raterCandidate($rid){
	$conn=dbConnect();
	$query="select CID from RATER where  RID=$rid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row?$row[0]:false;
}

// Resolves a Candidate's name by RID
function getCandName($rid){
	$conn=dbConnect();
	$query="select a.FNAME, a.LNAME from CANDIDATE a, RATER b where a.CID=b.CID and b.RID=$rid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row?stripslashes($row[0])." ".stripslashes($row[1]):false;
}

// Resolves a programs expiration status by RID
function getProgramStatus($rid){
	$conn=dbConnect();
	$query="select c.EXPIRED from RATER a, CANDIDATE b, PROGRAM c where  a.RID=$rid and a.CID=b.CID and b.PID=c.PID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row?$row[0]:false;
}

// Get all raters that have not received an email yet
function getUnmailedRaters($cid){
    $conn=dbConnect();
    $query="select RID from RATER where CID=$cid and CATID<>1 and EXPIRED='N'";
	//echo $query."<br>";
    $rs=mysql_query($query);
    if(!$rs){
		return false;
    }
	$rc=array();
	$rows=dbRes2Arr($rs);
	foreach($rows as $row){
		$rc[]=$row[0];
	}
	return $rc;
}

// send an initial email to all raters that have not yet received one
function sendInitialEmailToAllRaters($cid){
	$rcps=getUnmailedRaters($cid);
	$rs=sendRaterEmail($rcps,"I","",$cid);
	return false==$rs?"<font color=\"#aa0000\">No emails sent</font>":"<font color=\"#00aa00\">Sent emails to ".$rs."</font>";
}

function getRaterCatid( $rid ) {
	$conn=dbConnect();
	$query="select CATID from RATER where RID=$rid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row[0];
}

function getRaterOrgid($rid){
  $query="SELECT a.ORGID
            FROM CONSULTANT a, PROGCONS b, CANDIDATE c, RATER d
           WHERE a.CONID = b.CONID
             AND b.PID = c.PID
             AND c.CID = d.CID
             AND d.RID = $rid";
  return fetchOne($query,'O');       
}
?>

<?php
/*=============================================================================*
* 04-22-2005 TRM: Added support for "Self Only" candidates
*
* 11-21-2005: Mulitlingual support
*=============================================================================*/
require_once 'dbfns.php';
require_once 'mailfns.php';

// Insert a new candidate
function candInsert($candData){
	$conn=dbConnect();
	// This looks funny, but is correct since self is also a RATER
	$i=getKey("RATER","RID");
	// Insert the CANDIDATE row
	//$query="insert into CANDIDATE (CID,PID,FNAME,LNAME,EMAIL,EXPIRED,ARCHFLAG,SELF,OTH) values ( $i,$candData[0],'$candData[1]','$candData[2]','$candData[3]','N','N','$candData[4]','$candData[5]')";
	// wbs 12/7/2006
	//$query="insert into CANDIDATE (CID,PID,FNAME,LNAME,EMAIL,EXPIRED,ARCHFLAG,SELF,OTH) values ( $i,$candData[0],".quote_smart($candData[1]).",".quote_smart($candData[2]).",".quote_smart($candData[3]).",'N','N','$candData[4]','$candData[5]')";
	// jpc Replacing addslashes with mysql_real_escape_string() 04/27/2010
  $query=sprintf("insert into CANDIDATE (CID,PID,FNAME,LNAME,EMAIL,EXPIRED,ARCHFLAG,SELF,OTH) values ( $i,$candData[0],'%s','%s','%s','N','N','$candData[4]','$candData[5]')",
                mysql_real_escape_string($candData[1]),
                mysql_real_escape_string($candData[2]),
                mysql_real_escape_string($candData[3]));
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	// Insert the CANDCONS row
	$query="insert into CANDCONS (CID,CONID) values ( $i,$candData[6])";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	// Language support: Insert the RATERLANG row
	$query="insert into RATERLANG (RID,PID,LID) values ( $i,$candData[0],$candData[7])";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}

	return insertSelfRater($i);
}

// insert the self rater
function insertSelfRater($cid){
	$conn=dbConnect();
	$query="insert into RATER (RID,CID,CATID,FNAM,LNAM,EMAIL,STARTDT,ENDDT,EXPIRED) select CID,CID,1,FNAME,LNAME,EMAIL,STARTDT,ENDDT,EXPIRED from CANDIDATE where CID=$cid";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return insertSelfSurvey($cid);
}

// insert the self survey
function insertSelfSurvey($cid){
	$conn=dbConnect();
	$query="insert into RATERINSTR (RID,TID,EXPIRED) select RID,1,EXPIRED from RATER where CID=$cid and CATID=1";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return $cid;
}

// save candidate info
function saveCandidate($candData){
	$conn=dbConnect();
	// Save changes to CANDIDATE row
	//$query="update CANDIDATE  set FNAME='$candData[1]',LNAME='$candData[2]',EMAIL='$candData[3]' where CID=$candData[0]";
	// wbs 12/7/2006
	// jpc Replacing addslashes with mysql_real_escape_string() 04/27/2010
	$query=sprintf("update CANDIDATE  set FNAME='%s',LNAME='%s',EMAIL='%s' where CID=$candData[0]",
                  mysql_real_escape_string($candData[1]),
                  mysql_real_escape_string($candData[2]),
                  mysql_real_escape_string($candData[3]));
  if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	// but remember a CANDIDATE is also a RATER - best to update that row as well
	//$query="update RATER set FNAM='$candData[1]',LNAM='$candData[2]',EMAIL='$candData[3]' where CID=$candData[0] and RID=$candData[0]";
	$query=sprintf("update RATER set FNAM='%s',LNAM='%s',EMAIL='%s' where CID=$candData[0] and RID=$candData[0]",
                  mysql_real_escape_string($candData[1]),
                  mysql_real_escape_string($candData[2]),
                  mysql_real_escape_string($candData[3]));
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}

	// and finally udate the langauge
	$query="update RATERLANG set LID=$candData[4] where RID=$candData[0]";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return true;
}

// check that email is unique within a program
function checkCandEmail($email,$pid,$cid=NULL){
	if("0"==$pid){
		// A self-only candidate
		// we can re-use the email as much as we like
		return true;
	}
//
// 01-25-2005: Patti Viscomi wants this to be disabled while testing
//
return true;

	$conn=dbConnect();
	$query="select EMAIL from CANDIDATE where PID=$pid and EMAIL='$email'";
	if(!is_null($cid)){
	    $query=$query." and CID<>$cid";
	}
	//echo $query;
	$rs=mysql_query($query);
	if(!$rs){
	    return false;
	}
	return !mysql_fetch_row($rs);
}

// return data for a single candidate
function getCandInfo($cid){
	$conn=dbConnect();
	// added language support
	$query="select a.CID,a.FNAME,a.LNAME,a.EMAIL,a.EXPIRED,a.ARCHFLAG,a.SELF,a.OTH,a.STARTDT,a.ENDDT,c.LID,c.DESCR from CANDIDATE a,RATERLANG b, LANG c where a.CID=$cid and a.CID=b.RID and b.LID=c.LID";
	$rs=mysql_query($query);
	if(!$rs){
	    echo $query."<br>";
	    return false;
	}
	return mysql_fetch_array($rs);
}

// return a list of all candidates for a program
// added language support
function candidateList($pid,$arch="N"){
	$conn=dbConnect();
	$query="select a.CID,a.FNAME,a.LNAME,a.EMAIL,a.EXPIRED,a.ARCHFLAG,a.SELF,a.OTH,a.STARTDT,a.ENDDT,c.LID,c.DESCR from CANDIDATE a,RATERLANG b,LANG c where a.PID=$pid and a.ARCHFLAG='$arch' and a.CID=b.RID and b.LID=c.LID order by a.LNAME";
	//echo "<hr>$query<hr>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

// render a list of all candidates for a program
// Added language support
function listCandidates($pid,$frm,$tid="1"){
	$rs=candidateList($pid);
	foreach($rs as $row){
		$useUrl="../usr/conshome.php";
		if($tid=="3"){
			// jump straight into the self only survey in a separate window
			$useUrl="../assess/selfonly.php';$frm.target='selfonlysurvey";
//			$useUrl="../assess/conshome.php";
		}
			$participantName = $row[1] . " " . $row[2];
	    echo "<tr>";
	    echo "<td nowrap=\"nowrap\"><small>$row[2], $row[1]</small></td>";
	    echo "<td nowrap=\"nowrap\"><small>$row[3]</small></td><td><small>$row[0]</small></td>";
	    echo "<td nowrap=\"nowrap\"><small>".$row[11]."</small></td>"; //removed utf8_decode() - JPC 05/23/08
	    echo "<td nowrap=\"nowrap\">";
	    echo "<input type='button' value='Edit' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.what.value='edit';$frm.action='canddetail.php';$frm.submit();\" title=\"Edit $participantName\">";
	    echo "<input type='button' value='Delete' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.what.value='del';verifyDelete();\" title=\"Delete $participantName\">";
	    echo "<input type='button' value='Use' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.pin.value='$row[0]';$frm.uid.value='$row[3]';$frm.action='$useUrl';$frm.submit();\" title=\"Act on behalf of $participantName\">";
	    echo "</td></tr>";
	}

}

// return a list of all self-only candidates for a consultant
// Note: we drive off the RATER table
function selfOnlyCandidateList($conid,$arch="N"){
	$conn=dbConnect();
	$query="select a.RID,a.FNAM,a.LNAM,a.EMAIL,a.EXPIRED,IFNULL(a.STARTDT,'Pending'),IFNULL(a.ENDDT,'Incomplete') from RATER a,CANDCONS b, CANDIDATE c  where a.RID=b.CID and a.RID=c.CID and b.CONID=$conid and ARCHFLAG='$arch' and OTH='N' order by LNAM";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

// render a list of all self-only candidates
function listSelfOnlyCandidates($conid,$frm){
	$rs=selfOnlyCandidateList($conid);
	foreach($rs as $row){
	    echo "<tr>";
	    echo "<td><small>$row[2], $row[1]</small></td>";
	    echo "<td><small>$row[3]</small></td><td><small>$row[0]</small></td>";
		echo "<td><u>Start date:</u> $row[5]<br><u>Complete date:</u> $row[6]</td>";
	    echo "<td>";
	    echo "<input type='button' value='Edit' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.what.value='edit';$frm.action='selfcanddetail.php';$frm.submit();\">";
	    echo "<input type='button' value='Email' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.what.value='email';$frm.submit();\">";
		if($row[6]!="Incomplete"){
			//-- we let the consultant generate the report whenever it suits her
			//-- this requires that we score and consume licenses accordingly
			//-- and that we must make sure she has enough licenses to do it
			echo "<br>";
			echo "<a href='showselfonlyreport.php?cid=$row[0]' target='selfonlyreport'>View report</a>";
		}
	    echo "</td></tr>";
	}
}

// render a  mail list of all candidates for a program
function candidateMailList($pid, $selectedCids=array()){
	$rs=candidateList($pid);
	$count=0;
	foreach($rs as $row){
	    echo "\n\t" . '<tr>';
	    echo "\n\t\t" . '<td><small>' . $row[2] . ', ' . $row[1] . '</small></td>';
	    echo "\n\t\t" . '<td><small>' . $row[3] . '</small></td>';
			echo "\n\t\t" . '<td>';
			echo "\n\t\t\t" . '<center>';
			echo "\n\t\t\t" . '<small>';
	    if (in_array($row[0], $selectedCids)) {
	    	$yesChecked = 'checked="checked"';
	    	$noChecked = '';
	    } else {
	    	$yesChecked = '';
	    	$noChecked = 'checked="checked"';
	    }
	    echo "\n\t\t\t" . 'Yes <input type="radio" name="rcp'.$count.'" id="rcp'.$count.'" value="'.$row[0].'" '.$yesChecked.' />';
	    echo "\n\t\t\t" . 'No <input type="radio" name="rcp'.$count.'" id="rcp'.$count.'" value="N" '.$noChecked.' />';
	    echo "\n\t\t\t" . '</small>';
			echo "\n\t\t\t" . '</center>';
	    echo "\n\t\t" . '</td>';
	    echo "\n\t" . '</tr>';
	    $count++;
	}
	echo "\n" . '<input type="hidden" name="count" id="count" value="'.$count.'">';
}

// deletes a consumed license for a candidate
function deleteCandLicense($cid,$conid){
	$conn=dbConnect();
	$query="delete from CANDLIC where CONID=$conid and CID=$cid";
	//echo $query."<br>";
	return mysql_query($query);
}

// deletes a candidate that hasn't used his link yet
function deleteCandidate($cid,$conid){
	$conn=dbConnect();
	// we should get rid of all the raters and their answers, too.
	// since this is 3.3 we cand do the multiple table deletes, so we have to do it the hard way
	if(!deleteRaterResponseForCandidate($cid)){
	    return false;
	}
	$query="delete from RATER where CID=$cid";
	if(!mysql_query($query)){
	    return false;
	}
	$query="delete from CANDCONS where CID=$cid";
	if(!mysql_query($query)){
	    return false;
	}

	// delete the language row
	$query="delete from RATERLANG where RID=$cid";
	if(!mysql_query($query)){
	    return false;
	}

	// Send an email to the candidate before deleting
	$query="select EMAIL from CONSULTANT where CONID=$conid";

	//echo $query."<br>";
	$rs=mysql_query($query);
	if($rs){
		$row=mysql_fetch_row($rs);
		$from=$row[0];
		$query="select a.FNAME, a.LNAME, a.EMAIL, b.DESCR from CANDIDATE a, PROGRAM b where a.PID=b.PID and a.CID=$cid";
		//die($query);
		$rs=mysql_query($query);
		if($rs){
			$row=mysql_fetch_row($rs);
			$body="You have been removed from the Conflict Dynamics Profile.";
			$body.=getDisclaimer();
			sendGenericMail($row[2],$from,"Conflict Dynamics Profile Deletion",$body,$row[0]." ".$row[1]);
		}
	}

	$query="delete from CANDIDATE where CID=$cid ";
	//echo $query."<br>";
	return mysql_query($query);

	// License will most probably be consumed when the report is generated
	//return mysql_query($query)?deleteCandLicense($cid,$conid):false;
}

// a clunky 3.3 way to delete the responses and comments
function deleteRaterResponseForCandidate($cid){
	$conn=dbConnect();
	$query="select RID from RATER where CID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
	    return false;
	}
	$rows=mysql_fetch_array($rs);
	foreach ($rows as $row){
	    $query="delete from RATERRESP where RID=$row[0]";
	    if(!mysql_query($query)){
	        return false;
	    }
	    $query="delete from RATERCMT where RID=$row[0]";
	    if(!mysql_query($query)){
		return false;
	    }
	    $query="delete from RATERINSTR where RID=$row[0]";
	    if(!mysql_query($query)){
		return false;
	    }
		// delete the language row as well
	    $query="delete from RATERLANG where RID=$row[0]";
	    if(!mysql_query($query)){
		return false;
	    }
	}
	return true;
}

function sendCandidateEmail($rcp,$type,$mailbody,$conid,$ccEmailids=array()){
	// if $returnValuesOnly = true then the email is not sent, instead an array
	// is returned that contains the email recipients, subject and body
/* for debugging
echo "<li>RCP = "; var_dump($rcp);
echo "<li>TYPE = $type";
echo "<li>BODY = $mailbody";
echo "<li>CONID = $conid";
echo "<li>CCEMAILIDS = "; var_dump($ccEmailids);
*/
	// get a comma separated list of CID
	$rcps=implode($rcp,",");
	if(!$rcps){
	    return false;
	}
	// use this email address as sender
	$conn=dbConnect();
	$query="select EMAIL from CONSULTANT where CONID=$conid";
	$rs=mysql_query($query)
	    or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	$sender=$rw[0];

	// get all the recipients
	$rc=array();
	$query="select a.EMAIL,a.FNAME,a.LNAME,a.CID,b.LID from CANDIDATE a, RATERLANG b where a.CID in ($rcps) and a.CID=b.RID";
	//echo "<br>".$query."<br>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	while($rw){
	    $rc[]=$rw[0];
	    // build an email and send it
	    $to=$rw[0];
	    $subject="Conflict Dynamics Profile";
	    $name=$rw[1]." ".$rw[2];
	    $cid=$rw[3];
		$lid=$rw[4];
	    $body="";
	    if("I"==$type){
			// Initial 360
			$body=initialEmail($cid,$lid);
	    }
	    elseif("R"==$type){
			// Reminder 360
			$body=reminderEmail($cid,$lid);
	    }
	    elseif("C"==$type){
			// Custom 360
			$body=customEmail($mailbody,$cid,$lid);
	    }
	    elseif("S"==$type||"SI"==$type){
			// Initial S.O.
			$body=selfOnlyEmail($cid,$lid);
	    }
	    elseif("SR"==$type){
			// Reminder S.O.
			$body=soReminderEmail($cid,$lid);
	    }
	    elseif("SC"==$type){
			// Custom S.O.
			$body=soCustomEmail($mailbody,$cid,$lid);
	    }
	    // set encodeding for Portuguese text
      $encode=(substr($type,0,1)!="S")? false : "MIME-Version: 1.0\n"."Content-type: text/html; charset=utf-8\n";
	    
    	// send the email
	    sendGenericMail($to,$sender,$subject,$body,$name,$lid,$ccEmailids,$encode);
	    $rw=mysql_fetch_row($rs);
	}
	// Ok, now they have emails
	mysql_query("update CANDIDATE set SENT='Y' where CID in ($rcps)");
	return implode($rc,",");
}

function getConEmail($conid){
	// use this email address as sender
	$conn=dbConnect();
	$query="select EMAIL from CONSULTANT where CONID=$conid";
	//echo "<hr>$query<hr>";
	$rs=mysql_query($query)
		or die($query."<br>".mysql_error());
	$rw=mysql_fetch_row($rs);
	$sender=$rw[0];
	return $sender;
}

// This is the Initial email for self-only
/* We've added multilingual support
function selfOnlyEmail($cid,$lid,$previewOnly=false){
	if ($previewOnly) $displayCid = '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>';
		else $displayCid = $cid;
	if("8"==$lid){
		$rc="Se le está pidiendo que llene un cuestionario sobre el Perfil de las Dinámicas del Conflicto.  Para acceder al cuestionario, usted debe ir a:\n\n";
		$rc=$rc.getURLRoot()."/assess/spsurvey.php \n\nSe le pedirá su correo electrónico y número de clave.\n\nSu número de Clave es $displayCid \n\n";
		$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
		$rc=$rc."El cuestionario tomará aproximadamente de 20-25 minutos para ser completado.  Usted debe guardar este correo electrónico para tener su clave en caso de que tenga que regresar al cuestionario.\n\n";
		$rc = utf8_encode($rc);
	}
	else{
		$rc="You are being asked to fill out a questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n";
		$rc=$rc.getURLRoot()."/assess/survey.php \n\nYou will be asked to provide your email address and a PIN number.\n\nYour PIN number is $displayCid \n\n";
		$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
		$rc=$rc."The questionnaire will take about 20-25 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	}
	return utf8_decode($rc.getDisclaimer($lid));
}
*/

function selfOnlyEmail($cid,$lid,$previewOnly=false){
	$displayCid =($previewOnly)?  '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>' : $cid;
  $dirs=explode("/",$_SERVER['PHP_SELF']);
  $alias=(in_array('ptl',$dirs))? "/ptl" : "";  
	switch($lid){
	   case "8" : //Spanish
  		$p1 ="<p>Se le está pidiendo que llene un cuestionario sobre el Perfil de las "; 
  		$p1.="Dinámicas del Conflicto.  Para acceder al cuestionario, usted debe ";
  		$p1.="ir a:</p>";
      $useURL=getURLRoot()."$alias/assess/survey.php?lang=es";
      
      $p3 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      
      $p3.="<p>Se le pedirá su correo electrónico y número de clave.</p>";
      
      $p4 ="<p>Su número de Clave es $displayCid </p>";
  		
      $p5 ="<p>Due Date: " . getCandidateDueDate($cid) . "</p>";
  		
      $p6 ="<p>El cuestionario tomará aproximadamente de 20-25 minutos para ser ";
      $p6.="completado. Usted debe guardar este correo electrónico para tener su ";
      $p6.="clave en caso de que tenga que regresar al cuestionario.</p>";	
      $rc = $p1.$p2.$p3.$p4.$p5.$p6.$p7.getDisclaimer($lid);
      break;
     case "9" : //Portuguese (Added 08/25/2009 JPC )
  		$p1 ="<p>Esta mensagem é um convite para ter as suas respostas ao questionário ";
  		$p1.='do "Conflict Dynamics Profile" (CDP). O CDP é uma ferramenta de ';
  		$p1.="desenvolvimento de competências que ajuda as pessoas a perceberem como ";
  		$p1.="se comportam em situações de conflito.</p>";
  		
      $p2 ="<p>Poderá ter acesso ao questionário no endereço:</p>";
      $useURL=getURLRoot()."$alias/assess/survey.php?lang=pt";
      
      $p3 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      
      $p4 ="<p>Precisará de entrar o seu endereço de correio electrónico e o seu PIN.</p>";
      
      $p5 ="<p>O seu PIN é $displayCid</p>";
      $ptdates=changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y");
      $p6 ="<p>Data limite: $ptdates</p>";
      
      $p7 ="<p>Vai necessitar de 20 a 25 minutos para responder a este questionário. ";
      $p7.="Aconselhamos a que guarde esta mensagem para ter sempre acesso ao seu ";
      $p7.="PIN, caso precise voltar ao questionário.</p>";   
      $rc = $p1.$p2.$p3.$p4.$p5.$p6.$p7."<p>".getDisclaimer($lid)."</p>"; 
      break;
     case "10" : //French
      $p1 ="<p>Il vous est demandé de bien vouloir répondre au questionnaire du Conflict Dynamics Profile®. Le Conflict Dynamics Profile (CDP) est un instrument d’aide au développement  destiné à mieux cerner les manières les plus caractéristiques dont les personnes réagissent aux situations conflictuelles dans leurs relations de travail.</p>";
      $p2 ="<p>Pour accéder au questionnaire, vous devez vous connecter grâce au lien ci-dessous :</p>";
      $useURL=getURLRoot()."$alias/assess/survey.php?lang=fr";
      $p3 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      $p4 ="<p>Il vous sera demandé de saisir votre adresse email (courriel) et un numéro d’identifiant personnel – PIN (unique et confidentiel).</p>";
      $p5 ="<p>Votre PIN est $displayCid </p>"; 
      $p6 ="<p>Date d’échéance: " . changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y") . "</p>";
      $p7 ="<p>Prévoyez environ 20 à 25 minutes pour renseigner ce questionnaire. Merci de sauvegarder cet email de manière à retrouver votre numéro d’identifiant personnel (PIN), dans l’éventualité où vous souhaiteriez vous interrompre. Vous pourrez reprendre et terminer ultérieurement  la passation du questionnaire par le même chemin d’accès en utilisant vos identifiants.</p>";     
      $p7.="<p>Pour une efficacité optimale de nos programmes, nous nous accordons un délai de 10 jours ouvrés après envoi pour rassembler et traiter les données de nos questionnaires.</p>";
      $rc=$p1.$p2.$p3.$p4.$p5.$p6.$p7.getDisclaimer($lid); 
      break;     
     case "12" : //French CA
      $p1 ="<p>Il vous est demandé de bien vouloir répondre au questionnaire du Conflict Dynamics Profile®. Le Conflict Dynamics Profile (CDP) est un instrument d’aide au développement  destiné à mieux cerner les manières les plus caractéristiques dont les personnes réagissent aux situations conflictuelles dans leurs relations de travail.</p>";
      $p2 ="<p>Pour accéder au questionnaire, vous devez vous connecter grâce au lien ci-dessous :</p>";
      $useURL=getURLRoot()."$alias/assess/survey.php?lang=fr-CA";
      $p3 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      $p4 ="<p>Il vous sera demandé de saisir votre adresse email (courriel) et un numéro d’identifiant personnel – PIN (unique et confidentiel).</p>";
      $p5 ="<p>Votre PIN est $displayCid </p>"; 
      $p6 ="<p>Date d’échéance: " . changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y") . "</p>";
      $p7 ="<p>Prévoyez environ 20 à 25 minutes pour renseigner ce questionnaire. Merci de sauvegarder cet email de manière à retrouver votre numéro d’identifiant personnel (PIN), dans l’éventualité où vous souhaiteriez vous interrompre. Vous pourrez reprendre et terminer ultérieurement  la passation du questionnaire par le même chemin d’accès en utilisant vos identifiants.</p>";     
      $p7.="<p>Pour une efficacité optimale de nos programmes, nous nous accordons un délai de 10 jours ouvrés après envoi pour rassembler et traiter les données de nos questionnaires.</p>";
      $rc=$p1.$p2.$p3.$p4.$p5.$p6.$p7.getDisclaimer($lid); 
      break;    
     case "11" : //German
      $p1 ="<p>wir freuen uns über Ihr Interesse, mehr zu Ihrem persönlichen Verhaltensprofil in Konfliktsituationen zu erfahren. Hierzu bitten wir Sie den CDP Fragebogen online auszufüllen. Das Ergebnis - Ihr „Conflict Dynamics Profile“ –  dient als wichtige Grundlage für den Feedback- und Entwicklungsprozess. Es zeigt auf, wie Sie sich in Konfliktsituationen verhalten. Ihr Feedback-Report wird Ihnen in einem persönlichen Feedbackgespräch von einem zertifizierten CDP Coach erläutert und mögliche Entwicklungsmaßnahmen werden gemeinsam definiert.</p>";
      $p2 ="<p>Konflikte gibt es in allen Lebenssituationen, also privat wie beruflich. Unser Fragebogen bezieht sich jedoch nur auf Ihre Arbeitswelt! Das Ausfüllen dauert ca. 20 bis  25 Minuten, aber nehmen Sie sich gerne mehr Zeit dafür.</p>";
      $p3 ="<p>Ihr persönlicher Zugang zum Online-System:</p>";
      $useURL=getURLRoot()."$alias/assess/survey.php?lang=de";
      $p4 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      $p5 ="<p>Sie werden dort gebeten eine e-Mail-Adresse und nachstehenden PIN anzugeben.</p>";
      $p6 ="<p>Ihr PIN ist $displayCid </p>"; 
      $p7 ="<p>Das Ausfüllen dauert ca. 20 bis  25 Minuten, aber nehmen Sie sich gerne mehr Zeit dafür.</p>";
      $p8 ="<p>Wir bitten Sie den Fragebogen bis zum... ".changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y")."</p>";
      $p9 ="<p>Auszufüllen, damit wir die Ergebnisse rechtzeitig zum Feedbackgespräch zur Verfügung haben. Wir danken Ihnen, dass Sie sich die Zeit nehmen, den CDP-Fragebogen aufmerksam zu bearbeiten und freuen uns jetzt schon auf das Gespräch mit Ihnen.</p>";
      $rc=$p1.$p2.$p3.$p4.$p5.$p6.$p7.$p8.$p9.getDisclaimer($lid); 
      break;        
     default : //English
  		$p1 ="<p>You are being asked to fill out a questionnaire on the Conflict ";
      $p1.="Dynamics Profile. The Conflict Dynamics Profile (CDP) is a developmental ";
      $p1.="tool that helps people better understand how they typically respond to ";
      $p1.="conflict.</p>";
  		
      $p2 ="<p>To access the questionnaire you should go to:</p>";
      
      $useURL=getURLRoot()."$alias/assess/survey.php";
      $p3 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      
      $p4 ="<p>You will be asked to provide your email address and a PIN number. </p>";
      
      $p5 ="<p>Your PIN number is $displayCid </p>";
      
      $p6 ="<p>Due Date: " . getCandidateDueDate($cid) . "</p>";
      
      $p7 ="<p>The questionnaire will take about 20-25 minutes to complete.  You ";
      $p7.="should save this email so you will have your PIN number in case you ";
      $p7.="need to return to the questionnaire.</p>"; 
      $rc=$p1.$p2.$p3.$p4.$p5.$p6.$p7.getDisclaimer($lid); 
      break;         
	}

	return $rc;  //removed utf8_decode() - JPC 05/23/08
}

/*
function soReminderEmail($cid,$lid,$previewOnly=false){
	if ($previewOnly) $displayCid = '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>';
		else $displayCid = $cid;
	if("8"==$lid){
		$rc="Este es un correo electrónico recordatorio para que llene un cuestionario sobre el Perfil de Las Dinámicas del Conflicto.  Para acceder a este cuestionario usted debe ir a:\n\n";
		$rc=$rc.getURLRoot()."/assess/spsurvey.php \n\nSe le pedirá su correo electrónico y número de clave.\n\nSu número de Clave es $displayCid \n\n";
		$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
		$rc=$rc."El cuestionario tomará aproximadamente de 20-25 minutos para ser completado.  Usted debe guardar este correo electrónico para tener su clave en caso de que tenga que regresar al cuestionario.\n\n";
		$rc = utf8_encode($rc);
	}
	else{
		$rc="This is a reminder email to fill out a questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n";
		$rc=$rc.getURLRoot()."/assess/survey.php \n\nYou will be asked to provide your email address and a PIN number.\n\nYour PIN number is $displayCid \n\n";
		$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
		$rc=$rc."The questionnaire will take about 20-25 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	}
	return utf8_decode($rc.getDisclaimer($lid));
}
*/

function soReminderEmail($cid,$lid,$previewOnly=false){
	$displayCid =($previewOnly)?  '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>' : $cid;
  $dirs=explode("/",$_SERVER['PHP_SELF']);
  $alias=(in_array('ptl',$dirs))? "/ptl" : "";  
  switch($lid){
    case "8" : //Spanish
  		$p1 ="<p>Este es un correo electrónico recordatorio para que llene un cuestionario ";
  		$p1.="sobre el Perfil de Las Dinámicas del Conflicto. Para acceder a este ";
  		$p1.="cuestionario usted debe ir a:</p>";
  		
  		$useURL = getURLRoot()."$alias/assess/survey.php?lang=es";
      $p2 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      
      $p3 ="<p>Se le pedirá su correo electrónico y número de clave.</p>";
      
      $p4 ="<p>Su número de Clave es $displayCid </p>";
      
  		$p5 ="<p>Due Date: " . getCandidateDueDate($cid) . "</p>";
  		
  		$p6 ="<p>El cuestionario tomará aproximadamente de 20-25 minutos para ser completado. ";
      $p6.="Usted debe guardar este correo electrónico para tener su clave en caso de ";
      $p6.="que tenga que regresar al cuestionario.</p>";
  
      $rc = $p1.$p2.$p3.$p4.$p5.$p6;    
      break;
    case "9" : //Portuguese
  		$p1 ="<p>Continuamos a aguardar as suas respostas ao questionário do ";
  		$p1.='"Conflict Dynamics Profile" (CDP). O CDP é uma ferramenta de ';
  		$p1.="desenvolvimento de competências que ajuda as pessoas a perceberem ";
  		$p1.="como se comportam em situações de conflito.</p>";
  		
  		$lineA ="--------------------------------------------------------------------\n";
  		
  		$p2 ="<p>Poderá ter acesso ao questionário no endereço:</p>";
  		
  		$useURL = getURLRoot()."$alias/assess/survey.php?lang=pt";
      $p3 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
  		
  		$p4 ="<p>Precisará de entrar o seu endereco de correio electrónico e o seu PIN.</p>";
  		
  		$p5 ="<p>O seu PIN é $displayCid </p>";
  		
      $ptdates=changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y");  
  		$p6 ="<p>Data limite: $ptdates</p>";
  		
  		$p7 ="<p>Vai necessitar de 20 a 25 minutos para responder a este questionário. ";
      $p7.="Aconselhamos a que guarde esta mensagem para ter sempre acesso ao seu PIN, ";
      $p7.="caso precise voltar ao questionário.</p>";
      
      $rc=$p1.$p2.$p3.$p4.$p5.$p6.$p7;    
      break;
     case 10: //French
      $p1 ="<p>Voici un rappel de notre courriel initial vous priant de bien vouloir répondre au questionnaire du Conflict Dynamics Profile®. Le Conflict Dynamics Profile (CDP) est un instrument d’aide au développement  destiné à mieux cerner les manières les plus caractéristiques dont les personnes réagissent aux situations conflictuelles dans leurs relations de travail.</p>";
  		$lineA ="--------------------------------------------------------------------\n";
      $p2 ="<p>Pour accéder au questionnaire, vous devez vous connecter grâce au lien ci-dessous :</p>";
  		$useURL = getURLRoot()."$alias/assess/survey.php?lang=fr";
      $p3 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      $p4 ="<p>Il vous sera demandé de saisir votre adresse email (courriel) et un numéro d’identifiant personnel – NIP (unique et confidentiel). </p>";
      $p5 ="<p>Votre NIP est $displayCid</p>";
      $frdates=changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y");  
      $p6 ="<p>Date d’échéance :  $frdates</p>";
      $p7 ="<p>Prévoyez  environ 20 à 25 minutes pour renseigner ce questionnaire. Merci de sauvegarder cet email de manière à retrouver votre numéro d’identifiant personnel (NIP), dans l’éventualité où vous souhaiteriez vous interrompre. Vous pourrez reprendre et terminer ultérieurement la passation du questionnaire par le même chemin d’accès en utilisant vos identifiants.</p>";
      $rc=$p1.$p2.$p3.$p4.$p5.$p6.$p7;
      break;
     case 12: //French CA     
      $p1 ="<p>Voici un rappel de notre courriel initial vous priant de bien vouloir répondre au questionnaire du Conflict Dynamics Profile®. Le Conflict Dynamics Profile (CDP) est un instrument d’aide au développement  destiné à mieux cerner les manières les plus caractéristiques dont les personnes réagissent aux situations conflictuelles dans leurs relations de travail.</p>";
  		$lineA ="--------------------------------------------------------------------\n";
      $p2 ="<p>Pour accéder au questionnaire, vous devez vous connecter grâce au lien ci-dessous :</p>";
  		$useURL = getURLRoot()."$alias/assess/survey.php?lang=fr-CA";
      $p3 ="<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";
      $p4 ="<p>Il vous sera demandé de saisir votre adresse email (courriel) et un numéro d’identifiant personnel – NIP (unique et confidentiel). </p>";
      $p5 ="<p>Votre NIP est $displayCid</p>";
      $frdates=changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y");  
      $p6 ="<p>Date d’échéance :  $frdates</p>";
      $p7 ="<p>Prévoyez  environ 20 à 25 minutes pour renseigner ce questionnaire. Merci de sauvegarder cet email de manière à retrouver votre numéro d’identifiant personnel (NIP), dans l’éventualité où vous souhaiteriez vous interrompre. Vous pourrez reprendre et terminer ultérieurement la passation du questionnaire par le même chemin d’accès en utilisant vos identifiants.</p>";
      $rc=$p1.$p2.$p3.$p4.$p5.$p6.$p7;
      break;
     case 11: //German
      $useURL = getURLRoot()."$alias/assess/survey.php?lang=de";
    default : //English
  		$p1 ="<p>This is a reminder email to fill out a questionnaire on the Conflict ";
  		$p1.="Dynamics Profile. The Conflict Dynamics Profile (CDP) is a developmental ";
  		$p1.="tool that helps people better understand how they typically respond to ";
  		$p1.="conflict.</p>"; 


  		
  		$p2 ="<p>To access the questionnaire you should go to:</p>";
  		
  		$p3 ="<p>".getURLRoot()."$alias/assess/survey.php </p>";
  		
  		$p4 ="<p>You will be asked to provide your email address and a PIN number.</p>";
  		
  		$p5 ="<p>Your PIN number is $displayCid </p>";
  
  		$p6 ="<p>Due Date: " . getCandidateDueDate($cid) . "</p>";
  		
  		$p7 ="<p>The questionnaire will take about 20-25 minutes to complete. You should "; 
      $p7.="save this email so you will have your PIN number in case you need to ";
      $p7.="return to the questionnaire.</p>";
      
      $rc=$p1.$p2.$p3.$p4.$p5.$p6.$p7;    
      break;
  }

	return $rc.getDisclaimer($lid); //removed utf8_decode() - JPC 05/23/08
}

function soCustomEmail($body,$cid,$lid,$previewOnly=false){
	if ($previewOnly) $displayCid = '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>';
		else $displayCid = $cid;

  $dirs=explode("/",$_SERVER['PHP_SELF']);
  $alias=(in_array('ptl',$dirs))? "/ptl" : "";  
  	
	switch($lid){
	   case "8" : //Spanish
  		 $rc = "$body\n\n";
  		 $rc .= "Su enlace es:\n\n".getURLRoot()."$alias/assess/survey.php?lang=es \n\nSu clave es $displayCid \n\n";
  		 $rc .= "Due Date: " . getCandidateDueDate($cid) . "\n\n";
  		 $rc = utf8_encode($rc);	   
	     break;
	   case "9" : //Portuguese
  		 $rc = "<p>$body</p>";
  		 $rc .= "<p>O seu &quot;link&quot; é:</p>";
  		 $useURL = getURLRoot()."$alias/assess/survey.php?lang=pt";  
       $rc .= "<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";		 
       $rc .= "<p>O seu código de acesso (PIN) é: $displayCid </p>";
  		 $ptdates=changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y");
  		 $rc .= "<p>Data limite: ".$ptdates."</p>";
  		 //$rc = utf8_encode($rc);	   
	     break;
	    case "10" : //French
  		 $rc = "<p>$body</p>";
  		 $rc .= "<p>Vous devez vous connecter grâce au lien ci-dessous.</p>";
  		 $useURL = getURLRoot()."$alias/assess/survey.php?lang=fr";  
       $rc .= "<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";		 
       $rc .= "<p>Votre NIP est $displayCid</p>";
  		 $frdates=changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y");  
       $rc .="<p>Date d’échéance :  $frdates</p>";
       break;	    
	    case "12" : //French CA
  		 $rc = "<p>$body</p>";
  		 $rc .= "<p>Vous devez vous connecter grâce au lien ci-dessous.</p>";
  		 $useURL = getURLRoot()."$alias/assess/survey.php?lang=fr-CA";  
       $rc .= "<p><a href=\"$useURL\" target=\"_blank\">$useURL</a></p>";		 
       $rc .= "<p>Votre NIP est $displayCid</p>";
  		 $frdates=changeDateFormat(getCandidateDueDate($cid),"m/d/Y","d/m/Y");  
       $rc .="<p>Date d’échéance :  $frdates</p>";
       break;	    
	    case "11" : //German
  		 $useURL = getURLRoot()."$alias/assess/survey.php?lang=de";  	    
	   default  : //English
  		 $rc = "<p>$body</p>";
  		 $rc .= "<p>Your link is:<br /><br />".getURLRoot()."$alias/assess/survey.php <br /><br />Your pin is $displayCid </p>";
  		 $rc .= "<p>Due Date: " . getCandidateDueDate($cid) . "</p>";
  		 $rc = utf8_encode($rc);	   
	     break;
	}
	return $rc.getDisclaimer($lid); //removed utf8_decode() - JPC 05/23/08
}

/* This is the emails that are sent to 360
function initialEmail($cid,$lid,$previewOnly=false){
	if ($previewOnly) $displayCid = '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>';
		else $displayCid = $cid;
	$rc="You are being asked to fill out a questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n";
	$rc=$rc.getURLRoot()."/usr/ \n\nYou will be asked to provide your email address and a PIN number.\n\nYour PIN number is $displayCid \n\n";
	$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
	$rc=$rc."The questionnaire will take about 20-25 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	$rc=$rc."You need to invite raters to CDP to get the full benefit of the survey\n\n";
	return $rc.getDisclaimer();
}
*/

function initialEmail($cid,$lid,$previewOnly=false){
	$displayCid =($previewOnly)? '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>' : $cid;

  $p1 ="You are being asked to fill out a questionnaire on the Conflict Dynamics ";
  $p1.="Profile. The Conflict Dynamics Profile (CDP) is a developmental tool that ";
  $p1.="helps people better understand how they typically respond to conflict.  ";
  $p1.="To avoid the disappointment of arriving at your feedback session and ";
  $p1.="finding that your report is not available, please read the instructions ";
  $p1.="below. To access the questionnaire you should go to:\n\n";

  $p2=getURLRoot()."/usr/ \n\n";
  
  $p3="You will be asked to provide your email address and a PIN number.\n\n";
  
  $p4="Your PIN number is $displayCid \n\n";
  
	$p5="Due Date: " . getCandidateDueDate($cid) . "\n\n";
	
	$lineA="-----------------------------------------------------------------------\n\n";
	
	$p6="Decide on the members of your respondent group:\n\n";
	
	$p7 ="If you would like feedback from all three respondent groups (boss, \n";
  $p7.="peers, and direct reports), you should distribute the online survey \n";
  $p7.="to eleven (11) respondents: \n";
	$p7.="Immediate Boss*\n";
	$p7.="      *The feedback you receive from your immediate boss will not be ";
  $p7.="anonymous. We suggest you make this clear when you give your boss the ";
  $p7.="questionnaire.\n";
  $p7.="   3 or more Peers**\n";
  $p7.="   3 or more Direct Reports**\n";
  $p7.="**The feedback you receive from these respondents will remain ";
  $p7.="anonymous.\n\n";
  
  $p8 ="If you would like all respondent feedback to be from one group only,  \n";
  $p8.="you should distribute the online respondent surveys to all peers or all \n";
  $p8.="direct reports, in addition to your immediate boss.\n\n";
  
	$lineA="-----------------------------------------------------------------------\n\n";  
	
	$p9 ="The questionnaire will take about 20-25 minutes to complete. You should ";
	$p9.="save this email so you will have your PIN number in case you need to ";
	$p9.="return to the questionnaire.\n\n";

  return $p1.$p2.$p3.$p4.$p5.$lineA.$p6.$p7.$p8.$lineA.$p9.getDisclaimer();
}

/*
function reminderEmail($cid,$lid,$previewOnly=false){
	if ($previewOnly) $displayCid = '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>';
		else $displayCid = $cid;
	$rc="This is a reminder email to fill out a questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n";
	$rc=$rc.getURLRoot()."/usr/ \n\nYou will be asked to provide your email address and a PIN number.\n\nYour PIN number is $displayCid \n\n";
	$rc=$rc."Due Date: " . getCandidateDueDate($cid) . "\n\n";
	$rc=$rc."The questionnaire will take about 20-25 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	$rc=$rc."You need to invite raters to CDP to get the full benefit of the survey\n\n";
	$rc=$rc."If you have already completed the questionnaire, you can disregard this message.\n\n";
	return $rc.getDisclaimer();
}
*/

function reminderEmail($cid,$lid,$previewOnly=false){

	$displayCid =($previewOnly)? '_______ <i style="background-color:#FFFC17;">(PIN will be inserted when email is sent.)</i>' : $cid;

  $p1 ="This is a reminder email to fill out a questionnaire on the Conflict ";
  $p1.="Dynamics Profile. The Conflict Dynamics Profile (CDP) is a developmental ";
  $p1.="tool that helps people better understand how they typically respond to ";
  $p1.="conflict. To avoid the disappointment of arriving at your feedback ";
  $p1.="session and finding that your report is not available, please read the ";
  $p1.="instructions below. To access the questionnaire you should go to:\n\n";

  $p2 =getURLRoot()."/usr/ \n\n";
  
  $p3 ="You will be asked to provide your email address and a PIN number.\n\n";
  
  $p4 ="Your PIN number is $displayCid \n\n";
  
  $p5 ="Due Date: " . getCandidateDueDate($cid) . "\n\n";
  
  $p6 ="Please make sure to assign your raters as described below.  Please also ";
  $p6.="make sure you remind any of your raters that have not completed the ";
  $p6.="survey on your behalf.\n";

	$lineA="-----------------------------------------------------------------------\n\n";
	
	$p7 ="Decide on the members of your respondent group:\n\n";
	
	$p8 ="If you would like feedback from all three respondent groups (boss, \n";
  $p8.="peers, and direct reports), you should distribute the online survey \n";
  $p8.="to around eleven (11) or more respondents (up to 30): \n";
	$p8.="Immediate Boss*\n";
	$p8.="      *The feedback you receive from your immediate boss will not be ";
  $p8.="anonymous. We suggest you make this clear when you give your boss the ";
  $p8.="questionnaire.\n";
  $p8.="   3 or more Peers**\n";
  $p8.="   3 or more Direct Reports**\n";
  $p8.="**The feedback you receive from these respondents will remain ";
  $p8.="anonymous.\n\n";
  
  $p9 ="If you would like all respondent feedback to be from one group only,  \n";
  $p9.="you should distribute the online respondent surveys to all peers or all \n";
  $p9.="direct reports, in addition to your immediate boss.\n\n";

	$lineA="-----------------------------------------------------------------------\n\n";
	
	$p10 ="The questionnaire will take about 20-25 minutes to complete. You should ";
	$p10.="save this email so you will have your PIN number in case you need to ";
	$p10.="return to the questionnaire.\n\n";      

  $p11 ="If you have already completed the questionnaire, you can disregard this ";
  $p11.="message.\n\n";
  
  return $p1.$p2.$p3.$p4.$p5.$p6.$lineA.$p7.$p8.$p9.$lineA.$p10.$p11.getDisclaimer();
}

function customEmail($body,$cid,$lid,$previewOnly=false){
	$rc = "$body\n\n";
	$rc .= "Your link is:\n\n".getURLRoot()."/usr/ \n\nYour pin is $cid \n\n";
	$rc .= "Due Date: " . getCandidateDueDate($cid) . "\n\n";
	return $rc.getDisclaimer();
}

function candidateSummary($cid){
    $conn=dbConnect();
    // first see if we've completed the survey
    $rs=mysql_query("select * from RATER where RID=$cid and CID=$cid and CATID=1 and EXPIRED='N' and ENDDT is NULL");
    if(mysql_num_rows($rs)){
		echo "<tr><td><small>You have not completed the self assessment.</small></td></tr>";
		echo '<tr><td onClick="javascript:frm1.action=\'../assess/self.php\';frm1.submit();"><small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp;Click here to Complete Self Assessment</small><td></tr>';
    }
    else{
		echo "<tr><td><small><img src='../images/g.gif'>&nbsp;You have completed the self assessment.</small></td></tr>";
    }

    // See how many raters we have
    $rs1=mysql_query("select CATID from RATER where CID=$cid and CATID<>1");
    $cnt=mysql_num_rows($rs1);
    echo "<tr><td><small>You have  assigned $cnt Raters. You may assign up to 30 Raters.</small></td></tr>";

	// We can still add raters
	if(11>$cnt){
		echo '<tr><td onClick="javascript:frm1.action=\'raterhome.php\';frm1.submit();">';
		echo '<small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp; Click here to Add Raters</small><td></tr>';
    }

	// We can manage/email raters
    if(0<$cnt){
		echo '<tr><td onClick="javascript:frm1.action=\'raterhome.php\';frm1.submit();">';
		echo '<small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp; Click here to Manage Raters</small> <td></tr>';

		echo '<tr><td onClick="javascript:frm1.action=\'emailrater.php\';frm1.submit();">';
		echo '<small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp; Click here to send Email to Raters</small> <td></tr>';
    }

    // Check the completion status
    //??? Quick Fix, need to determine what 'S' Flag is used for and if it should be set after Rater expired
    $rs2=mysql_query("select CATID from RATER where CID=$cid and CATID<>1 and EXPIRED IN ('S','Y') and ENDDT is not NULL");
    $cmp=mysql_num_rows($rs2);

	// Only show if any raters have completed survey
    if(0<$cmp){
		// some are still pending
		if($cnt!=$cmp){
			echo "<tr><td><small> $cmp of $cnt Raters have completed the assessment.</small></td></tr>";
			echo "<tr><td onClick=\"javascript:frm1.action='checkcomp.php';frm1.submit();\">";
			echo '<small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
			echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp; Click Here to Check Completion Status</small> <td></tr>';
		}
		// all done
		else{
			echo "<tr><td><small><img src='../images/g.gif'>&nbsp;$cmp of $cnt Raters have completed the assessment.</small></td></tr>";
		}
    }
}

// Shows completion date for a Candidate's Raters (including self)
function checkCompletionStatus($cid){
	$conn=dbConnect();
	$query="select CID,CATID,LNAM, FNAM, IFNULL(ENDDT,'Incomplete') from RATER where CID=$cid group by CID,CATID,LNAM, FNAM order by CID,CATID,LNAM";
	$rs=mysql_query($query)
		or die(mysql_error());
	return dbRes2Arr($rs);
}

// Shows completion sttaus for all raters in a program
function checkProgramCompletionStatus($pid){
	$conn=dbConnect();
	
	// Get test id
	$query="select TID from PROGINSTR where PID = $pid";
	$rs=mysql_query($query);
	if($rs) {
		$row=mysql_fetch_row($rs);
		$tid = $row[0];
	} else {
		$tid = 0;
	}	
	if ($tid == 3) {
		// Individual (self-only) survey
		$query="select a.CID, a.CATID,a.LNAM,a.FNAM, IFNULL(a.ENDDT,'Incomplete'), CONCAT(b.LNAME, ', ', b.FNAME) AS CANDNAME from RATER a, CANDIDATE b,PROGRAM c where c.PID=$pid and a.CID=b.CID and b.PID=c.PID group by a.CID,a.CATID,a.LNAM,a.FNAM order by a.LNAM, a.FNAM";
	} else {
		// 360 survey
		$query="select a.CID, a.CATID,a.LNAM,a.FNAM, IFNULL(a.ENDDT,'Incomplete'), CONCAT(b.LNAME, ', ', b.FNAME) AS CANDNAME from RATER a, CANDIDATE b,PROGRAM c where c.PID=$pid and a.CID=b.CID and b.PID=c.PID group by a.CID,a.CATID,a.LNAM,a.FNAM order by a.CID,a.CATID,a.LNAM";
	}
	$rs=mysql_query($query)
		or die(mysql_error());
	return dbRes2Arr($rs);
}

function showCompletion($pid, $tid=1){
	$numComplete = 0;
	$data=checkProgramCompletionStatus($pid);
	$cat=array("","Self","Boss","Peer","Direct Report");
	if (count($data) > 0) {
		if (strcasecmp($row[4], "Incomplete") != 0) $numComplete++; 
		foreach($data as $row){
			echo "\n\t" . '<tr>';
			echo "\n\t\t" . '<td nowrap="nowrap">' . $row[0] . " - " . $row[2] . ', ' . $row[3] . '</td>';
			echo "\n\t\t" . '<td><span style="display:none;">' . $row[1] . '</span>' . $cat[$row[1]] . '</td>';
			echo "\n\t\t" . '<td>' . $row[4] . '</td>';
			if ($tid == 3) {
				if (strcasecmp($row[4], "incomplete") == 0) {
					echo "\n\t\t" . '<td style="text-align:center;">n/a</td>';
				} else {
					if (selfOnlyLicenseAlreadyConsumed($row[0], $_SESSION["conid"], $tid)) {
						echo "\n\t\t" . '<td style="text-align:center;color:green;font-weight:bold;">Yes</td>';
					} else {
						echo "\n\t\t" . '<td style="text-align:center;color:crimson;font-weight:bold;">No</td>';
					}
				}
			}
			echo "\n\t" . '</tr>';
		}
	} else {
		echo "\n\t" . '<tr>';
		echo "\n\t\t" . '<td colspan="100%" style="text-align:center;">No participants in program</td>';
		echo "\n\t" . '</tr>';
	}

	return $numComplete;
	
}

function showCompletionWithCandidate($pid){
	$data=checkProgramCompletionStatus($pid);
	$cat=array("","Self","Boss","Peer","Direct Report");
	if (count($data) > 0) {
		foreach($data as $row){
			echo "<tr>";
			echo "<td nowrap=\"nowrap\">$row[2], $row[3]</td>";
			echo "<td nowrap=\"nowrap\">$row[5]</td>";
			echo "<td nowrap=\"nowrap\"><span style=\"display:none;\">".$row[1]."</span>".$cat[$row[1]]."</td>";
			echo "<td nowrap=\"nowrap\">$row[4]</td>";
			echo "</tr>";
		}
	} else {
		echo "<tr>";
		echo "<td colspan=\"3\" style=\"text-align:center;\">No participants in program</td>";
		echo "</tr>";
	}

}

// Will return all active candidates in a program
function getAllCandidatesInProgram($pid){
	$conn=dbConnect();
	$query="select CID from CANDIDATE where PID=$pid and EXPIRED<>'Y'";
	//echo $query;
	$rs=mysql_query($query);
	$rc=array();
	$rows=dbRes2Arr($rs);
	foreach($rows as $row){
		$rc[]=$row[0];
	}
	return $rc;
}

// Send emails to all xcandidates that haven't already gotten one
function getAllUnmailedCandidatesInProgram($pid){
	$conn=dbConnect();
	$query="select CID from CANDIDATE where PID=$pid and EXPIRED<>'Y' and SENT<>'Y'";
	//$query="select CID from CANDIDATE where PID=$pid and EXPIRED<>'Y'";
	//echo $query;
	$rs=mysql_query($query);
	$rc=array();
	$rows=dbRes2Arr($rs);
	foreach($rows as $row){
		$rc[]=$row[0];
	}
	return $rc;
}

// Send a scoring request email to Admin
function sendScoreRequestEmail($pid){
	$conn=dbConnect();
	// Get admin's email
//	$admid=$_SESSION['admid'];
//	$query="select EMAIL from CONSULTANT where CONID=$admid";
	$query="select EMAIL from CONSULTANT, ADMINS where CONID=ADMID";
	$rs=mysql_query($query);
	if($rs){
		$row=mysql_fetch_row($rs);
		$to=$row[0];
		// Get consultant and program data
		$query="select a.EMAIL, a.FNAME, a.LNAME, c.DESCR from CONSULTANT a, PROGCONS b, PROGRAM c where a.CONID=b.CONID and b.PID=c.PID and b.PID=$pid";
		$rs=mysql_query($query);
		if($rs){
			$row=mysql_fetch_row($rs);
			$body="$row[1] $row[2] has indicated that the Conflict Dynamics Program '".$row[3]."' is ready to be scored.\n\n".getDisclaimer();
			sendGenericMail($to,$row[0],"Conflict Dynamics Profile Scoring Request",$body,"Administrator");
			return "Sent the following message:<br>".$body;
		}

	}
	return "Requested action taken, but unable to send email";
}

// Sends an initial email to all candidates that are part of the program
// that haven't received an email before
function sendInitialEmailToCandidates($pid,$conid,$type,$ccEmailids=array()){
	$cids=getAllUnmailedCandidatesInProgram($pid);
	//$cids=getAllCandidatesInProgram($pid);
//var_dump($cids);
	if ($cids == false) {
		$retVal = "<font color=\"#aa0000\">Emails already sent to all candidates</font>";
	} else {
		$retVal = "<font color=\"#00aa00\"> Sent initial emails to ";
		$retVal .= sendCandidateEmail($cids,"$type","",$conid,$ccEmailids);
		$retVale .= "</font>";
	}
	return $retVal;
}

function getCandidateDueDate($cid, $standardFormat=true) {
	// Returns the program due date when given a candidate id
	// If standard format then date is returned in mm/dd/yyy format, otherwise it 
	// uses MySQL format yyyy-m-dd
	$conn=dbConnect();
	$query="select b.ENDDT from CANDIDATE AS a LEFT JOIN PROGRAM AS b ON b.PID = a.PID WHERE a.CID=$cid";
	//echo "<hr>$query</hr>";
	$rs=mysql_query($query);
	if($rs){
		$row=mysql_fetch_row($rs);
		$duedate = $row[0];
	} else {
		$duedate = null;
	}
		
	if ($standardFormat) $duedate = date('m/d/Y', strtotime($duedate));
	return $duedate;
	
}

function selfOnlyLicenseAlreadyConsumed($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from SELFLIC where CONID=$conid and CID=$cid and TID=$tid");
	$row=mysql_fetch_row($rs);
	return $row[0]>0?true:false;
}

function changeDateFormat($date,$instr,$outstr){
  /**
   * Function takes a date string and format strings to change formatting.
   * Originally written for Portuguese translations, it is scalable because of 
   * the switch statements. Add formats as needed - JPC 08/27/2009
   *    
   * */           
    switch($instr){
      case "m/d/Y" : //example "06/30/2008" 
        $dt=explode("/",$date);
        $ts=mktime(0, 0, 0, $dt[0], $dt[1], $dt[2]);
        break;
      default:
        break;
    }
    
  if(!isset($ts))
    return $date;
    
    switch($outstr){
      case "d/m/Y" : // example "30/06/2008"
        $date=date("d/m/Y", $ts);
        break;
      default :
        break;
    } 
    
    return $date;   
}

?>

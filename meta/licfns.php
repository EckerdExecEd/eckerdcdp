<?php
/*=============================================================================*
* 04-22-2005 TRM: Added support for multiple instruments. Default is 1, i.e. 
* "360" Conflict Dynamics Profile. This is to support instrument 3, i.e.
* "Self Only" version.
*
*=============================================================================*/
require_once 'dbfns.php';
require_once 'mailfns.php';

// Insert a new License into the database
function insertLicense($conid,$qty,$pmt,$instr,$transdata=""){ 
	$conn=dbConnect();
	// Insert the LICENSE row
	$query="insert into LICENSE (CONID,TRANSID,TYPEID,INSTR,CNT,TS,TRANSDATA) values ($conid,NULL,$pmt,$instr,$qty,NOW(),'$transdata')";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return true;
}

// Returns an array of arrays with license records
function consListLicense($narrow,$active,$instr="1"){
	$crit=$narrow."%";
	$conn=dbConnect();
	$licTable="CANDLIC";
	if($instr=="3"){
		$licTable="SELFLIC";
	}
/*
	$query="select a.CONID,FNAME,LNAME,sum(CNT)-count(CID) as AVAILABLE from CONSULTANT a LEFT JOIN LICENSE b on a.CONID=b.CONID LEFT JOIN $licTable c on a.CONID=c.CONID where a.CONID<>1 and a.ACTIVE='$active' and LNAME like '$crit' and INSTR=$instr group by a.CONID,FNAME,LNAME order by LNAME";
*/

// 5/21/08 CDZ - Fixed the above query because it calculated the available licenses wrong
$query ="select a.CONID, a.FNAME, a.LNAME, sum(b.CNT) - c.USED as AVAILABLE 
from CONSULTANT a 
LEFT JOIN LICENSE b on a.CONID=b.CONID 
LEFT JOIN (select a.CONID, count(distinct b.CID) as USED
           from CONSULTANT a 
           LEFT JOIN $licTable b on b.CONID=a.CONID 
           where a.CONID<>1 
             and a.ACTIVE='$active' 
             and a.LNAME like '$crit' 
           group by a.CONID, a.FNAME, a.LNAME 
           order by a.LNAME) c on a.CONID=c.CONID 
where a.CONID<>1 
	and a.ACTIVE='$active' 
	and a.LNAME like '$crit' 
	and b.INSTR=$instr 
group by a.CONID, a.FNAME, a.LNAME 
order by a.LNAME
";


//echo $query;

	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

// gets All Consultants that doesn't have a particular type of license
// Note: this is necessary, in order to list the licenses by type (nulls etc. make things stupid)
function getConsultantsWithoutLicenses($narrow,$active,$conids){
	$crit=$narrow."%";
	$conn=dbConnect();
	$query="select CONID,FNAME,LNAME from CONSULTANT where CONID<>1 and ACTIVE='$active' and LNAME like '$crit' and CONID not in ($conids) order by LNAME";

//echo $query;

	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

// displays license info for all consultants
function listAllLicenses($narrow,$active,$frm,$instr="1"){
	$rows=consListLicense($narrow,$active,$instr);
	$conids=array();
	$i=0;
	echo "<tr><td>Name</td><td>Available licenses</td><td>Add Licenses</td></tr>";
	echo "<tr><td colspan=3>Consultants with licenses</td></tr>";
	foreach($rows as $row){
		echo "<tr><td>".stripslashes($row[2]).",".stripslashes($row[1])."</td><td>";
		echo $row[3];
		echo "</td><td>";
	    echo "<input type='hidden' name='conid".$i."' value='".$row[0]."'>";
	    echo "Qty:&nbsp;<input type='text' name='qty".$i."' value='0' size=5>&nbsp;";
	    echo "<select name='pmt".$i."'>";
	    echo "<option value='1' selected>Check</option>";
	    echo "<option value='3' >Purchase Order</option>";
	    echo "<option value='2' >Credit Card</option>";
	    echo "</select>&nbsp;";
	    echo "<input type='button' value='Add' onClick='javascript:subForm($frm,$i);'>";
	    echo "</td></tr>";
		$conids[]=$row[0];
	    $i++;
	}
	if($i==0){
		$conids="0";
	}
	else{
		$conids=implode($conids,",");
	}
	$rows=getConsultantsWithoutLicenses($narrow,$active,$conids);
	echo "<tr><td colspan=3>Consultants without licenses</td></tr>";
	foreach($rows as $row){
		echo "<tr><td>".stripslashes($row[2]).",".stripslashes($row[1])."</td><td>";
		echo "0";
		echo "</td><td>";
	    echo "<input type='hidden' name='conid".$i."' value='".$row[0]."'>";
	    echo "Qty:&nbsp;<input type='text' name='qty".$i."' value='0' size=5>&nbsp;";
	    echo "<select name='pmt".$i."'>";
	    echo "<option value='1' selected>Check</option>";
	    echo "<option value='3' >Purchase Order</option>";
	    echo "<option value='2' >Credit Card</option>";
	    echo "</select>&nbsp;";
	    echo "<input type='button' value='Add' onClick='javascript:subForm($frm,$i);'>";
	    echo "</td></tr>";
	    $i++;
	}
	
	echo "<input type='hidden' name='items' value='".$i."'>";
	return true;
}
    
// send an email receipt to consultant
function sendReceipt($conid,$qty){
	// use this email address as sender
	$conn=dbConnect();
	$query="select EMAIL from CONSULTANT where CONID=".$_SESSION['admid'];
	$rs=mysql_query($query)
	    or die(mysql_error());
	$row=mysql_fetch_row($rs);
	$sender=$row[0];
	
	$query="select NOW(),FNAME,LNAME,EMAIL from CONSULTANT where CONID=".$conid;
	$rs=mysql_query($query)
	    or die(mysql_error());
	$row=mysql_fetch_row($rs);
	$msgBody="This is a receipt for $qty license(s) purchased on ".date("m/d/Y").".".getDisclaimer();
	if(!sendGenericMail($row[3],$sender,"Receipt: Conflict Dynamics Profile Licenses",$msgBody,$row[1]." ".$row[2])){
	    return false;
	}
	return true;
}

function totPurchased($conid,$instr="1"){
	$conn=dbConnect();
	$rs=mysql_query("select SUM(CNT) from LICENSE where CONID=$conid and INSTR=$instr");
	$rw=mysql_fetch_row($rs);
	return $rw[0];
}

function totConsumed($conid,$instr="1"){
	// Default to 360
	$licTbl="CANDLIC";
	if("3"==$instr){
		$licTbl="SELFLIC";
	}
	$conn=dbConnect();
	$rs=mysql_query("select COUNT(*) from $licTbl where CONID=$conid");
	$rw=mysql_fetch_row($rs);
	return $rw[0];
}

function purchased($conid,$instr="1",$table=true){
	$conn=dbConnect();
	$rs=mysql_query("select CNT, TS from LICENSE where CONID=$conid and INSTR=$instr");
	$rw=mysql_fetch_row($rs);
	if($table){
  	echo "<table border=0>";
  	while($rw){
  	    $dt=date("Y-m-d",strtotime($rw[1]));
  	    echo "<tr><td nowrap>$rw[0] on $dt</td></tr>";
  	    $rw=mysql_fetch_row($rs);
  	}
  	echo "</table>";
  }else{
    $rows="";
  	while($rw){
  	    $dt=date("Y-m-d",strtotime($rw[1]));
  	    $rows.="$rw[0] on $dt<br />";
  	    $rw=mysql_fetch_row($rs);
  	}
    return $rows;  
  }
}

function consumed($conid,$instr="1",$table=true){
	// Default to 360
	$licTbl="CANDLIC";
	if("3"==$instr){
		$licTbl="SELFLIC";
	}
	$conn=dbConnect();
	$rs=mysql_query("select FNAME,LNAME,TS from $licTbl a, CANDIDATE b where a.CID=b.CID and CONID=$conid");
	$rw=mysql_fetch_row($rs);
	if($table){
  	echo "<table border=0>";
  	while($rw){
  	    $dt=date("Y-m-d",strtotime($rw[2]));
  	    echo "<tr><td nowrap>$rw[0] $rw[1] on $dt</td></tr>";
  	    $rw=mysql_fetch_row($rs);
  	}
  	echo "</table>";
  }else{
    $rows="";
    while($rw){
  	    $dt=date("Y-m-d",strtotime($rw[2]));
  	    $rows.= "$rw[0] $rw[1] on $dt<br />";
  	    $rw=mysql_fetch_row($rs);
  	}
    return $rows;  
  }
}

?>

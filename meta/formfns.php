<?php
/**=============================================================================
 * Generic functions for gernerating form elements
 * =============================================================================
 **/   
  
function textInput($name, $value="",$size="25",$maxlength="50",$params=""){
  /**
   *  Generates HTML for a standard text input
   *  - $name => What you want to name the text field   
   *  - $value => Value for input, if exists
   *  - $length => Display length
   *  - $maxlength => maxlength of string allowed      
   *  - $params => any string of text to be used with the input   
   **/
   
   //Begin with Input name
   $tag = '<input type="text" name="'.$name.'" ';
   //Add Value
   $tag.='value="'.$value.'" ';
   //Add Size and Maxlength
   $tag.=(($size!="")&&(is_numeric($size)))? 'size="'.$size.'" ' : ''; 
   $tag.=(($maxlength!="")&&(is_numeric($maxlength)))? 'maxlength="'.$maxlength.'" ' : '';
   //Add any additional field parameters
   if(strlen($params)) $tag.= ' '.$params .' '; 
   //Close the tag
   $tag.=' />';
   
   return $tag;  
}

function selectList( $name='', $valuePairs, $default='',$descOption='',$params='' ) {
		/**
		 *	Generates HTML for a select list element
		 *	  - $name => What you want the select list to be named
		 *	  - $valuePairs => [0] Value, [1] Description
		 *	  - $default => value to make SELECTED if needed
		 *	  - $descOption => Empty description option 
		 *	  - $params => any string of text to be used in the select tag                    		 
		 **/
		
		// Begin Select with name
		$tag = '<select name="' . $name . '"';
		// Add any additional parameters needed within the select tag
		if (strlen($params)) $tag .= ' ' . $params . ' ';
		// Close the select tag
		$tag .= '>';
		// If a descritor is needed, it is added i.e. select .....
		$tag .=(strlen($descOption))? "<option value=''>".$descOption."</option>"." \n" : "";
		// Loop through the value pairs. Checks against the default value to mark selected
    for($x = 0; $x < count($valuePairs); $x++){
      $y=1;
      // Check to see if there is actually a pair of values
      if(count($valuePairs[$x]) != 2){ return 'Incorrect Name/Value Set'; }
      // initialize 
      foreach($valuePairs[$x] as $value){
        $y=($y > 0 )? 0 : 1;
        if($y == 0){ $sValue = $value; }
        if($y == 1){ $sDesc = utf8_decode($value); }
      }   
      $tag .= '<option value="'. str_replace('"', '&#34;',$sValue) . '"';
      if ($sValue == $default){
          $tag .= ' selected="selected"';
      }
      $tag .= '>' .$sDesc. '</option>'." \n";
   }
		$tag .= '</select>'." \n";
		return $tag;
}

function radioButtons($name='', $disp='&nbsp;', $valuePairs, $default='', $params=''){
  /**
   *
   *
   **/
  //Begin radio buttons
  $tag='';
  for($x = 0; $x < count($valuePairs); $x++){
   $y=1;
   // Check to see if there is actually a pair of values
   if(count($valuePairs[$x]) != 2){ return 'Incorrect Name/Value Set'; }
      // initialize 
      foreach($valuePairs[$x] as $value){
        $y=($y > 0 )? 0 : 1;
        if($y == 0){ $sValue = $value; }
        if($y == 1){ $sDesc = utf8_decode($value); }
      }     

    $tag.="<input type=\"radio\" name=\"$name\" value=\"$sValue\"";
    $tag.=(strlen($params))? ' ' . $params . ' ' : '';
    $tag.=($default==$sValue)? 'checked' : '';
    $tag.=" />$sDesc $disp \n";
  }
  return $tag;        
}  

?>

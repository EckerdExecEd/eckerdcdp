<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

define("DEFAULT_COLLAPSE_ON_CLICK", false);

$linkStyle = "font:normal 10pt arial;color:#15317e;text-decoration:none;";
$onMouseOver = "this.style.color='#488ac7';this.style.fontWeight='normal';this.style.backgroundColor='#faf8cc';this.style.textDecoration='underline';";
$onMouseOut = "this.style.color='#15317e';this.style.fontWeight='normal';this.style.backgroundColor='';this.style.textDecoration='none';";
$selectedStyle = "color:#C35617;";
$hideFirstImg = true;

/*
// Original expandable menu
$img_expand   = "tree_expand.png";
$img_collapse = "tree_collapse.png";
$img_line     = "tree_vertline.png";  
$img_split    = "tree_split.png";
$img_end      = "tree_end.png";
$img_leaf     = "tree_leaf.png";
$img_spc      = "tree_space.png";
*/

// New Folders
$img_expand   = "folderclose.gif";
$img_collapse = "folderopen.gif";
$img_line     = "linemiddle.gif";
$img_split    = "joinmiddle.gif";
$img_end      = "joinbottom.gif";
$img_leaf     = "page.gif";
$img_spc      = "blank_8x7.png";

/*
$img_expand   = "blank_8x7.png";
$img_collapse = "blank_8x7.png";
$img_line     = "blank_8x7.png";
$img_split    = "blank_8x7.png";
$img_end      = "blank_8x7.png";
$img_leaf     = "blank_8x7.png";
$img_spc      = "blank_8x7.png";
*/

if (!isset($_SESSION["tmv"])) session_start();

/*

  CREATING A MENU
 
  There are only two steps needed to create a menu:

    STEP 1 - CREATING THE MENU OBJECT:
 
      $menu = New MenuTree($menu_identifier , $definition_file)
	
      Parameters: 
        $menu_identifier - id to uniquely identify the menu (Only needed if mulitple menus are
                           being used)  The default value is `default`.
        $definition_file - file that contains the menu definitions.
				$request_vars    - array of name/value pairs for the POST and GET variables
				$exclude_items   - array of items to be excluded from the menu.  These can either
				                   be item indices or item labels.
		
    STEP 2 - DISPLAY THE MENU
    
      echo $menu->show();
	
	-----------------------------------------------------------------------------------------
	
	Menu Definition File Format
	
	  Each line contains a single menu item.  If a # character in the first column then the
	  entire line is treated as a comment.  Blank lines are ignored.
	  
	  All fields on a line except for the first two are separated by the vertical bar 
	  character.  The fields are:
	    Field 1 : a series of dots indicating the depth of the menu item. (Root menu
	              items will have one dot.  Second level items will have two dots, etc.)
	    Field 2 : the text that will be displayed for the item.  This can include HTML tags.
	    Field 3 : the url link, included all POST and GET variables. (See details below)
	    Field 4 : the target frame (example _TOP).  If none is specified no target frame 
	              is used.
	    Field 5 : a string of characters used to specify special settings. Valid 
	              characters are:
	                 `+` : non-collapsible item, i.e. once the item is opened, it cannot be closed.
	                 `-` : collapsible item, i.e. each time the item is clicked it alternates
      	                 between opening and closing
      	           `x` : the menu item is expanded by default
      Field 6 : flyover text message
	
	-----------------------------------------------------------------------------------------
	
	GET and POST variables
	
    In the url link define variables as you normally would, except for one exception.  If
    a variable name begins with the character ^ then the variable will be treated as a 
    POST variable. Otherwise all variables are treated as GET variables.

    If you do not specify a value for the variable, then MenuTree will search for a
    variable with that name.  If one is found then it's current value will be used.
    Otherwise, the variable will be ignored.  (So if you do not specify a value for 
    a variable, make sure it has a value before you create the MenuTree object.
    
	-----------------------------------------------------------------------------------------
	
	TECHNICAL NOTES
	
	  Javascript must be enabled for MenuTree to work.

*/

class MenuTree {
	var $selected_id	= 0;
	var $tree					= array();
	var $expand				= array();
	var $visible			= array();
	var $levels				= array();
	var $explevels		= array();
	var $maxlevel			= 0;
	var $i						= 0;
	var $_menuid      = "default";
	var $_menuClicked = false;
	var $request_vars = array();
	var $script;
	var $relativepath;

	// New Folders
	var $img_expand		= "folderclose.gif";
	var $img_collapse	= "folderopen.gif";
	var $img_line			= "linemiddle.gif";
	var $img_split		= "joinmiddle.gif";
	var $img_end			= "joinbottom.gif";
	var $img_leaf			= "page.gif";
	var $img_spc			= "blank_8x7.png";

	function MenuTree( $menuid="default", $items=array(), $request_vars=array(), $exclude_items=array() ) {

		global $img_expand, $img_collapse, $img_line, $img_split, $img_end, $img_leaf, $img_spc;

		// Set menu id
		$this->_menuid = $menuid;
		if (!isset($_SESSION["tmv"])) $_SESSION["tmv"] = array();
		if (!isset($_SESSION["tmv"][$this->_menuid])) $_SESSION["tmv"][$this->_menuid] = array();

		// Determine if an item was selected
		// Note: The selected item does not have to be generated by the menu, it can be
		// passed by any link in a GET or POST variable named `tmv`.  (GET variable takes precedence)
		// The `tmv` POST variable generated by the menu is an array, but if the selected item
		// is passed from a link using a POST variable, it does not need to be an array.
		$selectedItemId = -1;
		if (isset($_GET["tmv"])) {
			$selectedItemId = $_GET["tmv"];
		} else {
			if (isset($_POST["tmv"])) {
				$selectedItemId = $_POST["tmv"];
				if ((isset($_POST["tmv_clicked"])) && ($_POST["tmv_clicked"] == 1)) $this->_menuClicked = true;
			}
		}
		if (!is_numeric($selectedItemId)) {
			// Label passed in
				// ???? Need to add code to determine the selected item when a label is passed in 
			$selectedItemId = $this->matchSelectedItemIdFromFile($items, $exclude_items, $selectedItemId);
		} else {
			if ($selectedItemId < 0) {
				if (is_array($items)) {		
					// Menu array used
					// Need to add code for when menu items are coming from an array instead of a file
					$selectedItemId = $this->findSelectedItemIdFromFile($items, $exclude_items);
				} else {
					// Menu file used
					$selectedItemId = $this->findSelectedItemIdFromFile($items, $exclude_items);
				}
			}
		}

		if (!is_array($exclude_items)) $exclude_items = array($exclude_items);

		$this->script = isset($_SERVER["PATH_INFO"]) ? $_SERVER["PATH_INFO"] : $_SERVER["SCRIPT_NAME"];
		$this->relativepath = $this->_getRelativePath();

		$this->img_expand		= $this->relativepath . "MenuTree/" . $img_expand;
		$this->img_collapse	= $this->relativepath . "MenuTree/" . $img_collapse;
		$this->img_line			= $this->relativepath . "MenuTree/" . $img_line;
		$this->img_split		= $this->relativepath . "MenuTree/" . $img_split;
		$this->img_end			= $this->relativepath . "MenuTree/" . $img_end;
		$this->img_leaf			= $this->relativepath . "MenuTree/" . $img_leaf;
		$this->img_spc			= $this->relativepath . "MenuTree/" . $img_spc;

		// Set request variables (POST and GET), if specified
		$this->request_vars = $request_vars;

		// Get hidden and expand arrays from session variables (if set)
		$this->fetchExpandIds();
		//echo "<hr><b>EXPAND:</b> "; var_dump($this->expand); echo "<hr>";
		
		// Create node tree
		if (is_array($items)) {		
			$this->loadTreeFromArray($items, $exclude_items);
		} else {
			$this->loadTreeFromFile($items, $exclude_items);
		}
		//foreach ($this->tree as $id => $node) { echo "<li>id=$id "; if ($node["expand"]) echo "true"; else echo "false"; }

		// ----------------------------------------------------
		// Process Selected Item
		// ----------------------------------------------------
		if (!$selectedItemId < 0) {
			if (isset($_SESSION["tmv"][$this->_menuid]["selected_id"])) $this->selected_id = $_SESSION["tmv"][$this->_menuid]["selected_id"];
				else $this->selected_id = 0;
		} else {
			// Expand or collapse selected item as needed
			$this->selected_id = $selectedItemId;
		}
		
			
		if (isset($this->tree[$this->selected_id])) {
			// -----------------------------------------
			// Set selected item		
			// -----------------------------------------
			$this->tree[$this->selected_id]["selected"] = true;
			$this->tree[$this->selected_id]["hidden"] = false;

			// -----------------------------------------
			// Expand/Collapse as needed
			// -----------------------------------------
			if ($this->_menuClicked) {
				if (($this->tree[$this->selected_id]["expand"]) && ($this->tree[$this->selected_id]["collapseonclick"])) {
					if ((isset($_POST["tmv_id"])) && ($_POST["tmv_ts"] !== $_SESSION["tmv"][$this->_menuid]["ts"])) {
						$this->tree[$this->selected_id]["expand"] = false;
						$this->expand[$this->selected_id] = false;
					} else {
						$this->tree[$this->selected_id]["expand"] = true;
						$this->expand[$this->selected_id] = true;
					}
				
				} else {
					if ((isset($_POST["tmv_id"])) && ($_POST["tmv_ts"] !== $_SESSION["tmv"][$this->_menuid]["ts"])) {
						$this->tree[$this->selected_id]["expand"] = true;
						$this->expand[$this->selected_id] = true;
					} else {
						$this->tree[$this->selected_id]["expand"] = false;
						$this->expand[$this->selected_id] = false;
					}
				}
			}
			
		}
		
		
		// ---------------------------------------------------------
		// Verify All Preceding Levels (Parents) Are Expanded
		// ---------------------------------------------------------
		$parentid = $this->getParentid($this->selected_id);
		while ($parentid >= 0) {
			$this->tree[$parentid]["expand"] = true;
			$parentid = $this->getParentid($parentid);
		}

		if (!isset($_SESSION["tmv"][$this->_menuid])) $_SESSION["tmv"][$this->_menuid] = array();
		$_SESSION["tmv"][$this->_menuid]["selected_id"] = $this->selected_id;
		$_SESSION["tmv"][$this->_menuid]["ts"] = $_POST["tmv_ts"];

		//echo "<hr><b>tree</b>\n\n"; var_dump($this->tree); echo "\n\n<hr>";

	}

	function loadTreeFromArray( $items, $exclude_items=array() ) {

		if (!is_array($exclude_items)) $exclude_items = array($exclude_items);

		$this->maxlevel = 0;

		for ($i=0; $i<count($items); $i++) {
			$item = $items[$i][0];
			$level = strspn($item, ".");
			$node = explode("|", rtrim(substr($item, $level)));
			if (isset($node[0])) $label = $node[0]; else $label = "";
			if (isset($node[1])) $link = $node[1]; else $link = "";
			if (isset($node[2])) $target = $node[2]; else $target = "";
			if (isset($node[3])) $settings = $node[3]; else $settings = "";
			if (isset($node[4])) $title = $node[4]; else $title = "";
			$this->addNode($level, $label, $link, $target, $settings, $title);
		}
		
		$this->setNodeRelationships();
		//var_dump($this->tree);		
		
	}

	function dump() {
		$hStyle = 'background-color: #cccccc;padding: 2px;margin-right: 1px;margin-bottom: 1px;';
		$cStyle = 'background-color: #f0f0f0;vertical-align:top;padding: 2px;margin-right: 1px;margin-bottom: 1px;';

		echo "\n" . '<p style="font:bold 10pt arial;margin:2px;">menuid: ' . $this->_menuid . '</p>';
		echo "\n" . '<p style="font:bold 10pt arial;margin:2px;">selected_id: ' . $this->selected_id . '</p>';

		$expand = array();
		if (isset($_SESSION["tmv"][$this->_menuid]["expand"])) {
			$t1 = explode("|", $_SESSION["tmv"][$this->_menuid]["expand"]);
			for ($i=0; $i<count($t1); $i++) {
				$t2 = explode("=", $t1[$i]);
				$id = $t2[0];
				if (isset($t2[1])) $value = $t2[1];
					else $value = ".";
				$expand[$id] = $value;
			}
		}

		echo "\n" . '<table style="background-color: #ffffff;font: normal 9pt arial;">';
		echo "\n\t" . '<tr>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">id</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">level</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">parentid</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">haschildren</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">siblingorder</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">label</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">link</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">target</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '" title="Expand node - left column is current node value, right column is session variable value" colspan="2">exp.</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '" title="Collapse menu on click">coc</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">title</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">postvars</td>';
		echo "\n\t\t" . '<th style="' . $hStyle . '">getvars</td>';
		echo "\n\t" . '</tr>';
		foreach ($this->tree as $id => $node) {
			if ((isset($node["selected"])) && ($node["selected"] == true)) echo "\n\t" . '<tr style="background-color:#ff00ff;">';
				else echo "\n\t" . '<tr>';
			echo "\n\t\t" . '<td style="' . $cStyle . 'font-weight:bold;text-align:center;">' . $id . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . 'text-align:center;">' . ((isset($node["level"])) ? $node["level"] : "?") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . 'text-align:center;">' . ((isset($node["parentid"])) ? $node["parentid"] : "?") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . 'font-weight:bold;text-align:center;" nowrap="nowrap">' . (($node["haschildren"]) ? "Y" : "&nbsp;") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . '" nowrap="nowrap">' . ((strlen($node["siblingorder"])) ? $node["siblingorder"] : "&nbsp;") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . '">' . ((strlen($node["label"])) ? $node["label"] : "&nbsp;") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . '" nowrap="nowrap">' . ((strlen($node["link"])) ? $node["link"] : "&nbsp;") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . '" nowrap="nowrap">' . ((strlen($node["target"])) ? $node["target"] : "&nbsp;") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . 'font-weight:bold;">' . (($node["expand"]) ? "Y" : "&nbsp;") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . 'background-color:#fff0e0;font-weight:bold;">' . ((isset($expand[$id])) ? $expand[$id] : "?") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . 'font-weight:bold;">' . (($node["collapseonclick"]) ? "Y" : "&nbsp;") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . '" nowrap="nowrap">' . ((strlen($node["title"])) ? $node["title"] : "&nbsp;") . '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . '">';
			if (count($node["postvars"]) > 0) {
				//var_dump($node["postvars"]);
				foreach ($node["postvars"] as $name => $value) {
					echo "$name=$value<br />";
				}
			} else {
				echo "&nbsp;";
			}
			echo '</td>';
			echo "\n\t\t" . '<td style="' . $cStyle . '">';
			if (count($node["getvars"]) > 0) {
				//var_dump($node["getvars"]);
				foreach ($node["getvars"] as $name => $value) {
					echo "$name=$value<br />";
				}
			} else {
				echo "&nbsp;";
			}
			echo '</td>';
			echo "\n\t" . '</tr>';
		}
		echo "\n" . '</table>';

	}

	function loadTreeFromFile( $itemsFile, $exclude_items=array() ) {

		if (!is_array($exclude_items)) $exclude_items = array($exclude_items);

		$this->maxlevel = 0;

		$fd = fopen($itemsFile, "r");
		if ($fd == 0) die($this->script . " : Unable to open file $itemsFile");

		$itemIndex = 0;

		while ($buffer = fgets($fd, 4096)){
			$buffer = trim($buffer);
			if (strlen($buffer) > 0) {
				// ignore blank lines
				if (substr($buffer, 0, 1) != "#") {
					// Note that comments begin with # and are ignored
					$level = strspn($buffer, ".");
					$node = explode("|", rtrim(substr($buffer, $level)));
					if (isset($node[0])) $label = $node[0]; else $label = "";
					if (isset($node[1])) $link = $node[1]; else $link = "";
					if (isset($node[2])) $target = $node[2]; else $target = "";
					if (isset($node[3])) $settings = $node[3]; else $settings = "";
					if (isset($node[4])) $title = $node[4]; else $title = "";
					
					$ignoreItem = false;
					for($n=0; $n<count($exclude_items); $n++) {
						if (is_numeric($exclude_items[$n])) {
							if ($itemIndex == $exclude_items[$n]) {
								$ignoreItem = true;
								break;
							}
						} else {
							if (strcasecmp(strip_tags($label), strip_tags($exclude_items[$n])) == 0) {
								$ignoreItem = true;
								break;
							}
						}
					}
					if (!$ignoreItem) $this->addNode($level, $label, $link, $target, $settings, $title);
					
					$itemIndex++;
					
				}
			}
		}
		fclose($fd);
		
		$this->setNodeRelationships();
		//var_dump($this->tree);		
		
	}

	function setNodeRelationships() {

		// ------------------------
		// Set parent node ids
		// ------------------------
		$numNodes = count($this->tree);
		for ($id=0; $id<$numNodes; $id++) {
			$level = $this->tree[$id]["level"];
			for ($j=($id - 1); $j>-1; $j--) {
				if ($this->tree[$j]["level"] < $this->tree[$id]["level"]) {
					$this->tree[$id]["parentid"] = $this->tree[$j]["id"];
					break;
				}
			}
		}

		// ------------------------
		// Set has children
		// ------------------------
		$numNodes = count($this->tree);
		for ($id=0; $id<$numNodes; $id++) {
			for ($j=($id + 1); $j<$numNodes; $j++) {
				if ($this->tree[$j]["parentid"] == $id) {
					$this->tree[$id]["haschildren"] = true;
					break;
				}
			}
		}
		
		// ------------------------
		// Set child order
		// ------------------------
		$numNodes = count($this->tree);
		
		// Set root nodes
		$this->tree[0]["siblingorder"] = "first";
		$numRoot = 1;
		for ($id=1; $id<$numNodes; $id++) {
			if ($this->tree[$id]["level"] == 1) {
				$this->tree[$id]["siblingorder"] = "middle";
				$numRoot++;
			}
		}
		for ($id=($numNodes - 1); $id>-1; $id--) {
			if ($this->tree[$id]["level"] == 1) {
				if ($numRoot == 1) $this->tree[$id]["siblingorder"] = "only";
					else $this->tree[$id]["siblingorder"] = "last";
				break;
			}
		}
		
		// Set all child nodes
		for ($id=0; $id<$numNodes; $id++) {
			if ($this->tree[$id]["haschildren"]) {
				$previousChild = -1;
				$numChildren = 0;
				for ($j=($id + 1); $j<$numNodes; $j++) {
					if ($this->tree[$j]["parentid"] == $id) {
						if ($numChildren == 0) {
							$this->tree[$j]["siblingorder"] = "first";
							$previousChild = $j;
							$numChildren++;
						} else {
							$this->tree[$j]["siblingorder"] = "middle";
							$previousChild = $j;
							$numChildren++;
						}
					}
				}
				if ($numChildren == 0) {
					$this->tree[$previousChild]["siblingorder"] = "only";
				} else {
					$this->tree[$previousChild]["siblingorder"] = "last";
				}
			}
		}
		
	}

	function addNode( $level, $label, $link="", $target="", $settings="", $title="" ) {
	
		$id = count($this->tree);
	
		$this->tree[$id]["id"] = $id;
		$this->tree[$id]["level"] = $level;
		$this->tree[$id]["parentid"] = -1;
		$this->tree[$id]["haschildren"] = "";
		$this->tree[$id]["siblingorder"] = "";
		$this->tree[$id]["label"] = "<nobr>$label</nobr>";
		$this->tree[$id]["target"] = $target;
		$this->tree[$id]["title"] = $title;
		$this->tree[$id]["selected"] = false;
		$this->tree[$id]["collapseonclick"] = DEFAULT_COLLAPSE_ON_CLICK;
		$this->tree[$id]["getvars"] = array();
		$this->tree[$id]["postvars"] = array();

		// ------------------------------------------------------------
		// Set Link URL
		// ------------------------------------------------------------
		if (strpos($link, "?") === false) {
			$this->tree[$id]["link"] = $link;
			$varPairs = array();
		} else {
			$this->tree[$id]["link"] = substr($link, 0, strpos($link, "?"));
			$varPairs = explode("&", substr($link, strpos($link, "?") + 1));
		}

		// ------------------------------------------------------------
		// Set POST variables
		// Note: POST variables begin with the character ^ and are
		// placed in ["postvars"] variable.  All other variables are
		// appended to the link.
		// ------------------------------------------------------------

		$request_vars = array();
		
		// Get array of parameters
		for ($i=0; $i<count($varPairs); $i++) {
			$validParam = true;
			
			if (strpos($varPairs[$i], "=") !== false) {
				$t = explode("=", $varPairs[$i]);
				$name = trim($t[0]);
				$value = $t[1];
			} else {
				$name = $varPairs[$i];
				$value = "";
			}
					
			// -------------------------------------------------------------------------------
			// FIND VARIABLE VALUE
			//
			// If the variable has no value then search the request variables that were
			// passed in by the user.  Otherwise, search global variables for that variable.
			// 
			// If the specified value is a variable name, i.e. it begins with a $ character
			// then	search the request variables that were passed in by the user for that
			// variable.  Otherwise, search global variables for that variable.
			// -------------------------------------------------------------------------------
			if ((strlen($value) == 0) || (substr($value, 0, 1) == '$')) {

				if (count($this->request_vars) > 0) {
					// ------------------------------------
					// Search user defined variable values
					// ------------------------------------
					if ((strlen($value) > 0) && ((substr($value, 0, 1) == '$'))) $searchName = substr($value, 1);
						else $searchName = $name;

					if (array_key_exists($searchName, $this->request_vars)) {
						$value = $this->request_vars[$searchName];
					} else {
						$validParam = false;
					}

				} else {
					// ------------------------------------
					// Search for matching global variable
					// ------------------------------------
					if ((strlen($value) > 0) && ((substr($value, 0, 1) == '$'))) $searchName = substr($value, 1);
						else $searchName = $name;

					$varname = $searchName;
					global $$varname;
					//echo "<li>PID($value) = " . $$varname;
					if (isset($$varname)) {
						$value = $$varname;
					} else {
						$validParam = false;
					}
					
				}
				
			}
				
			if ($validParam ) $request_vars[$name] = $value;
				
		}
			
		foreach ($request_vars as $name => $value) {
			if ((substr($name, 0, 1) === "^") || ($name == "tmv")) {
				$this->tree[$id]["postvars"][substr($name, 1)] = $value;
			} else {
				$this->tree[$id]["getvars"][$name] = $value;
			}
		}

		// ------------------
		// Expand
		// ------------------
		if (count($this->expand) > 0) {
			if (array_key_exists($id, $this->expand)) {
				$this->tree[$id]["expand"] = $this->expand[$id];
			} else {
				$this->tree[$id]["expand"] = false;
			}

		} else {
			if (strpos(strtolower($settings), "x") === false) $this->tree[$id]["expand"] = false;
				else $this->tree[$id]["expand"] = true;
		}

		// -------------------
		// Collapse on Click
		// -------------------
		if (strpos(strtolower($settings), "+") !== false) $this->tree[$id]["collapseonclick"] = false;
		if (strpos(strtolower($settings), "-") !== false) $this->tree[$id]["collapseonclick"] = true;

//?????		$this->tree[$id]["selected"] = false;

		if ($level > $this->maxlevel) $this->maxlevel = $level;

//echo "<hr>"; var_dump($this->tree[$id]);

	}

	function get(){
	
		// Save TreeMenu SESSION variables
		if (!isset($_SESSION["tmv"][$this->_menuid])) $_SESSION["tmv"][$this->_menuid] = array();
		$expand = "";
		foreach ($this->tree as $id => $node) {
			if ($node["expand"]) $expand .= "|$id=1"; else $expand .= "|$id=0";
		}
		if (strlen($expand) > 0) $expand = substr($expand, 1);
		//echo "<li>expand = $expand";
		$_SESSION["tmv"][$this->_menuid]["expand"] = $expand;
		
		// Output HTML
		return $this->getHTMLTable();

		//echo "<div style=\"background-color:#f0e8ff;\"><b>SESSION VARIABLES:</b> "; var_dump($_SESSION); echo "</div>";
		
	}

	function show() {
		echo $this->get();
	}
	
 	function getHTMLTable(){
 	
 		global $linkStyle, $onMouseOver, $onMouseOut, $selectedStyle, $hideFirstImg;

		$mouseEvents = '';
		if (strlen($onMouseOver) > 0) $mouseEvents .= 'onMouseOver="' . $onMouseOver . '"';
		if (strlen($onMouseOut) > 0) $mouseEvents .= 'onMouseOut="' . $onMouseOut . '"';

		// Output nicely formatted tree
		$this->levels = array();
		for ($i=0; $i<$this->maxlevel; $i++) {
			$this->levels[$i] = 1;
		}
		
		$html .= "\n" . '<table cellspacing="0" cellpadding="0" border="0" cols="' . (2*$this->maxlevel) . '" width="100%">';
		$html .= "\n\t" . '<tr>';

		for ($i=0; $i<(2*$this->maxlevel) - 1; $i++) $html .= "\n\t\t" . '<td></td>';
		$html .= "\n\t\t" . '<td width="100%"></td>';
		$html .= "\n\t" . '</tr>';

		// -----------------------------------------------------------
		// Create an array of all post variables and their values
		// -----------------------------------------------------------
		
		$postvar_values = array();
		foreach ($this->tree as $id => $node) {

			if (count($node["postvars"]) > 0) {

				foreach ($node["postvars"] as $name => $value) {
					$found = false;
					for ($n=0; $n<count($postvar_values); $n++) {
						if (($postvar_values[$n]["name"] == $name) && ($postvar_values[$n]["value"] == $value)) {
							$found = true;
							break;
						}
					}
					if (!$found) {
						$index = count($postvar_values);
						$postvar_values[$index]["name"] = $name;
						$postvar_values[$index]["value"] = $value;
					}
				}
			}
		}
		
		$expandedLevels = array();
		for ($i=0; $i<$this->maxlevel; $i++) $expandedLevels[$i+1] = false;

		foreach ($this->tree as $id => $node) {

			$parentid = $node["parentid"];
			$colCount = 0;

			// Check to see if the level of the node should be expanded
			if ($node["expand"]) $expandedLevels[$node["level"]] = true;
				else $expandedLevels[$node["level"]] = false;
			if ($node["siblingorder"] == "last") $expandedLevels[$node["level"]] = false;


			// -------------------------------------------------------------
			// Create an array of all post variable ids for this node
			// -------------------------------------------------------------
			$postvar_ids = array();
			if (count($node["postvars"]) > 0) {
				foreach ($node["postvars"] as $name => $value) {
					for ($j=0; $j<count($postvar_values); $j++) {
						if (($name == $postvar_values[$j]["name"]) && ($value == $postvar_values[$j]["value"])) {
							$postvar_ids[] = $j;
							break;
						} 
					}
				}
			}

			// --------------------------------------------------------
			// Create link by appending GET variables to node link
			// --------------------------------------------------------
			$target = str_replace('"', '&quot;', $node["target"]);
			$link = str_replace('"', '&quot;', $node["link"]);

			if (count($node["getvars"]) > 0) {
				$link .= "?";
				$addAmpersand = false;
				foreach ($node["getvars"] as $name => $value) {
					if ($addAmpersand) $link .= "&";
					$link .= "$name=$value";
					$addAmpersand = true;
				}
			}
			

			if ((!isset($this->tree[$parentid])) || ($this->tree[$parentid]["expand"])) {  

				// start new row
				$html .= "\n\t" . '<tr>';
      
      	// ------------------------------------
				// vertical lines from higher levels
      	// ------------------------------------
				$i = 0;
				while ($i < $node["level"] - 1) {
					if (($i == 0) && ($hideFirstImg)) {
						// don't show vertical lines for first level
						$html .= "\n\t\t" . '<td></td>';
					} else {
						$level = $i + 1;
						if ($expandedLevels[$level]) $html .= "\n\t\t" . '<td style="width:1px;"><a name="' . $id . '"></a><img src="' . $this->img_line . '"></td>';
							else $html .= "\n\t\t" . '<td style="width:1px;"></td>';
					}
					$colCount++;
					$i++;
				}
      
      	// -------------------------------------
				// corner at end of subtree or t-split
      	// -------------------------------------
				if (($node["siblingorder"] == "last") && ($node["level"] != 1)) {
					$html .= "\n\t\t" . '<td><img src="' . $this->img_end . '"></td>';
					$this->levels[$this->tree[$id]["level"]-1] = 0;
					$colCount++;
				} else {
					if (($node["level"] == 1) && ($hideFirstImg)) {
						// don't show vertical lines for first level
						$html .= "\n\t\t" . '<td></td>';
					} else {
						$html .= "\n\t\t" . '<td style="width:1px;"><img src="' . $this->img_split . '"></td>';
					}
					$colCount++;
					$this->levels[$node["level"]-1] = 1;    
				}
      
				// Node (with subtree) or Leaf (no subtree)
				if ((isset($this->tree[$id+1]["level"])) && ($this->tree[$id+1]["level"] > $node["level"])) {
        
					if ($this->expand[$id] == 0){
						$html .= "\n\t\t" . '<td style="width:1px;"><a href="#" onclick="javascript:submitMenuTree(\'' . $link . '\', ' . $id . ', \'' . implode('|', $postvar_ids) . '\', \'' . $target . '\');"><img src="' . $this->img_expand . '" border="no"></a></td>';
					} else {
						$html .= "\n\t\t" . '<td style="width:1px;"><a href="#" onclick="javascript:submitMenuTree(\'' . $link . '\', ' . $id . ', \'' . implode('|', $postvar_ids) . '\', \'' . $target . '\');"><img src="' . $this->img_collapse . '" border="no"></a></td>';
					}
					$colCount++;

				} else {

					// Tree Leaf 
					$html .= "\n\t\t" . '<td style="width:1px;"><a href="#" onclick="javascript:submitMenuTree(\'' . $link . '\', ' . $id . ', \'' . implode('|', $postvar_ids) . '\', \'' . $target . '\');"><img src="' . $this->img_leaf . '"></a></td>';    
					$colCount++;
					     
				}
      
				// output item text
				if ($node["selected"]) $style = $selectedStyle; else $style = '';
				if ($node["link"] == ""){
//					$html .= "\n\t\t" . '<td colspan="' . ($this->maxlevel - $node["level"] + 1) . '"><a href="javascript:submitMenuTree(\'' . $link . '\', ' . $id . ', \'' . implode('|', $postvar_ids) . '\', \'' . $target . '\');" style="' . $linkStyle . '" ' . $mouseEvents . '><span style="' . $style . '">' . $node["label"] . '</span></a></td>';
					$html .= "\n\t\t" . '<td colspan="' . ((2*$this->maxlevel) - $colCount) . '"><a href="#" onclick="javascript:submitMenuTree(\'' . $link . '\', ' . $id . ', \'' . implode('|', $postvar_ids) . '\', \'' . $target . '\');" style="' . $linkStyle . '" ' . $mouseEvents . '><span style="' . $style . '">' . $node["label"] . '</span></a></td>';
					
				} else {
//					$html .= "\n\t\t" . '<td colspan="' . ($this->maxlevel - $node["level"] + 1) . '"><a href="javascript:submitMenuTree(\'' . $link . '\', ' . $id . ', \'' . implode('|', $postvar_ids) . '\', \'' . $target . '\');" style="' . $linkStyle . '" title="' . $node["title"] . '" ' . $mouseEvents . '><span style="' . $style .'">' . $node["label"] . '</span></a></td>';
					$html .= "\n\t\t" . '<td colspan="' . ((2*$this->maxlevel) - $colCount) . '"><a href="#" onclick="javascript:submitMenuTree(\'' . $link . '\', ' . $id . ', \'' . implode('|', $postvar_ids) . '\', \'' . $target . '\');" style="' . $linkStyle . '" title="' . $node["title"] . '" ' . $mouseEvents . '><span style="' . $style .'">' . $node["label"] . '</span></a></td>';

				}

				// end row 
				$html .= "\n\t" . '</tr>';

			}
			
		}
		
		$html .= "\n" . '</table>';
	
//echo "<hr><b>_POST:</b> "; var_dump($_POST); echo "<hr>";
//echo "<b>postvars:</b> "; var_dump($postvars); echo "<hr>";
//echo "<b>postvar_values:</b> "; var_dump($postvar_values); echo "<hr>";

		// -------------------------------------------------
		// Create form and javascript for POST variables
		// -------------------------------------------------
		$html .= "\n" . '<form name="frmSubmitMenuTree" id="frmSubmitMenuTree" action="" method="POST" style="margin:0px;">';
		$html .= "\n\t" . '<input type="hidden" name="tmv"         id="tmv"         value="" />';
		$html .= "\n\t" . '<input type="hidden" name="tmv_id"      id="tmv_id"      value="' . $this->_menuid . '" />';
		$html .= "\n\t" . '<input type="hidden" name="tmv_clicked" id="tmv_clicked" value="1" />';
		$html .= "\n\t" . '<input type="hidden" name="tmv_ts"      id="tmv_ts"      value="" />';
		$html .= "\n" . '</form>';
		$html .= "\n";
		$html .= "\n" . '<script language="Javascript">';
		$html .= "\n";
		$html .= "\n" . 'var postvars = [';
		for ($i=0; $i<count($postvar_values); $i++) {
			$html .= "\n\t\t" . '["' . str_replace('"', '&quot;', $postvar_values[$i]["name"]) . '", "' . str_replace('"', '&quot;', $postvar_values[$i]["value"]) . '"],';
		}
		$html .= "\n\t\t" . '];';
		$html .= "\n";
		$html .= "\n" . 'function submitMenuTree( action, id, valstr, target ) {';
		$html .= "\n";
		$html .= "\n\t" . 'document.getElementById("tmv").value = id;';
  	$html .= "\n\t" . 'document.getElementById("frmSubmitMenuTree").target = target;';
		$html .= "\n";
		$html .= "\n\t" . 'if (valstr.length > 0) {';
		$html .= "\n\t\t" . 'var vals = valstr.split("|");';
		$html .= "\n\t\t" . 'var formElem = document.getElementById("frmSubmitMenuTree");';
		$html .= "\n\t\t" . 'for (var i=0; i<vals.length; i++) {';
  	$html .= "\n\t\t\t" . 'var elem = document.createElement("INPUT");';
  	$html .= "\n\t\t\t" . 'var id = vals[i];';
  	$html .= "\n\t\t\t" . 'elem.setAttribute("type", "hidden");';
  	$html .= "\n\t\t\t" . 'elem.setAttribute("id", postvars[id][0]);';
  	$html .= "\n\t\t\t" . 'elem.setAttribute("name", postvars[id][0]);';
  	$html .= "\n\t\t\t" . 'elem.setAttribute("value", postvars[id][1]);';
  	$html .= "\n\t\t\t" . 'formElem.appendChild(elem);';
		$html .= "\n\t\t" . '}';
		$html .= "\n\t" . '}';
		$html .= "\n\t" . '// set timestamp';
		$html .= "\n\t" . 'var currentTime = new Date();';
		$html .= "\n\t" . 'document.getElementById("tmv_ts").value = currentTime.getTime();';
		$html .= "\n";
		$html .= "\n\t" . 'document.frmSubmitMenuTree.action = action;';
		$html .= "\n\t" . 'document.frmSubmitMenuTree.submit();';
		$html .= "\n" . '}';
		$html .= "\n";
		$html .= "\n" . '</script>';

		return $html;

	}

	function setVisibleNodes() {
		// all root nodes are always visible
		for ($i=0; $i < count($this->tree); $i++) {
			if ($this->tree[$i]["level"] == 1) {
				$this->tree[$i]["visible"] = true;
			}
		}

		for ($i=0; $i < count($this->explevels); $i++) {
			$n = $this->explevels[$i];
			if (($this->tree[$n]["visible"]) && ($this->tree[$n]["expand"])) {
				$j=$n+1;
				while ($this->tree[$j]["level"] > $this->tree[$n]["level"]) {
					if ($this->tree[$j]["level"] == $this->tree[$n]["level"]+1) $this->tree[$j]["visible"] = true;
					$j++;
				}
			}
		}
	}

	/**
	 *
	 * This method populates the $this->expand array from $_SESSION["tmv"][$this->_menuid]["expand"]
	 *
	 **/
	function fetchExpandIds() {

		$this->expand = array();
		if (isset($_SESSION["tmv"][$this->_menuid]["expand"])) {
			$t1 = explode("|", $_SESSION["tmv"][$this->_menuid]["expand"]);
			for ($i=0; $i<count($t1); $i++) {
				$t2 = explode("=", $t1[$i]);
				$id = $t2[0]; 
				if (isset($t2[1])) $value = $t2[1]; else $value = 0;
				if ($value == 1) $value = true; 
					else $value = false;
				$this->expand[$id] = $value;
			}
		}

	}

	function _getRelativePath() {
		/**
		*
		*	This method returns the relative path of this file with reference to the main PHP file.
		*
		**/

		// File Include Path		
		if (strpos(dirname(__FILE__), '/') !== false) {
			// Linux, etc.
			$includePath = explode('/', dirname(__FILE__));
		} else {
			// Windows
			$includePath = explode("\\", dirname(__FILE__));
		}
		if ((count($includePath) > 0) && (strlen($includePath[0]) == 0)) array_shift($includePath);
		
		// File Base Path
		$basePath = explode("/", $_SERVER['SCRIPT_FILENAME']);
		if (count($basePath) > 0) array_pop($basePath);

		if ((count($basePath) > 0) && (strlen($basePath[0]) == 0)) array_shift($basePath);

		while (((count($includePath) > 0) && (count($basePath) > 0)) && ($includePath[0] == $basePath[0])) {
			array_shift($includePath);
			array_shift($basePath);
		}

		$done = false;
		$i = 0;		// To prevent endless loop
		while ((count($includePath) > 0) && (count($basePath) > 0) && !$done && ($i <40)) {
			if ($includePath[0] == $basePath[0]) {
				array_shift($includePath);
				array_shift($basePath);
			} else { 
				$done = true;
			}
			$i++;
		}

		$relativePath = array();
		for ($i=0; $i<count($basePath); $i++) $relativePath[] = "..";
		for ($i=0; $i<count($includePath); $i++) $relativePath[] = $includePath[$i];

		if (count($relativePath) > 0) {
			return implode("/", $relativePath) . "/";
		} else {
			return "";
		}

		return true;
		
	}
	
	function getParentid( $nodeid ) {
		$parentid = -1;
		foreach ($this->tree as $i => $node) {
			$id = $node["id"];
			if ($id == $nodeid) {
				$parentid = $node["parentid"];
				break;
			}
		}
		return $parentid;
	}

	function matchSelectedItemIdFromFile( $itemsFile, $excludeItems=array(), $selectedLabel ) {
		$fd = fopen($itemsFile, "r");
		if ($fd == 0) die($this->script . " : Unable to open file $itemsFile");
		
		$itemIndex = -1;

		$found = false;
		$i = -1;
		while ((!$found) && ($buffer = fgets($fd, 4096))) {
			$buffer = trim($buffer);
			if ((strlen($buffer) > 0) && (substr($buffer, 0, 1) != "#")) {
				// Ignore blank lines and comments
				$i++;

				$temp = explode("|", $buffer);

				// --------------------------------------------------
				// See if selected Label matches the url
				// --------------------------------------------------
				$url = $temp[1];

				if ((strlen($url) == strlen($selectedLabel)) && (strcasecmp($url, $selectedLabel) == 0)) {
					$found = true;
					$itemIndex = $i;
				} else if ((strlen($url) > strlen($selectedLabel)) && (strcasecmp($selectedLabel, substr($url, - strlen($selectedLabel))) == 0)) {
					$found = true;
					$itemIndex = $i;
				} else if((strlen($selectedLabel) > strlen($url)) && (strcasecmp($url, substr($selectedLabel, - strlen($url))) == 0)) {
					$found = true;
					$itemIndex = $i;
				}

				if (!$found) {
					// --------------------------------------------------
					// See if selected Label matches the url
					// --------------------------------------------------
					$label = trim($temp[0], ".");

					if ((strlen($label) == strlen($selectedLabel)) && (strcasecmp($label, $selectedLabel) == 0)) {
						$found = true;
						$itemIndex = $i;
					} else if ((strlen($label) > strlen($selectedLabel)) && (strcasecmp($selectedLabel, substr($label, - strlen($selectedLabel))) == 0)) {
						$found = true;
						$itemIndex = $i;
					} else if((strlen($selectedLabel) > strlen($label)) && (strcasecmp($label, substr($selectedLabel, - strlen($label))) == 0)) {
						$found = true;
						$itemIndex = $i;
					}
				
				}
				
			}
		}
		
		return $itemIndex;

	}

	function findSelectedItemIdFromFile( $itemsFile, $excludeItems=array() ) {

		$selectedItemId = -1;
		
		$currentUrl = $_SERVER["REQUEST_URI"];
		$getvars = array();
		if (strpos($currentUrl, "?") !== false) $currentUrl = substr($currentUrl, 0, strpos($currentUrl, "?"));
	
		$fd = fopen($itemsFile, "r");
		if ($fd == 0) die($this->script . " : Unable to open file $itemsFile");

		$itemIndex = 0;
		$found = false;
		while ((!$found) && ($buffer = fgets($fd, 4096))) {
			$buffer = trim($buffer);
			if ((strlen($buffer) > 0) && (substr($buffer, 0, 1) != "#")) {
				// Ignore blank lines and comments
			
				// ------------------------------
				// Get url
				// ------------------------------
				$temp = explode("|", $buffer);
				$url = $temp[1];
				
				// ------------------------------
				// Get GET and POST variables
				// ------------------------------
				if (strpos($url, "?") !== false) {
					//
					// Parse GET and POST variables
					$varStr = substr($url, strpos($url, "?") + 1);
					$varPairs = explode("&", $varStr);
					for ($i=0; $i<count($varPairs); $i++) {

						$getvars = array();
						$postvars = array();

						if (strpos($url, "=") !== false) {
							$temp = explode("=", $varPairs[$i]);
							$varname = trim($temp[0]);
							$varvalue = trim($temp[1]);
						} else {
							$varname = $varPairs[$i];
							$varValue = "";
						}

						if ((strlen($varvalue) != 0) && (substr($varvalue, 0, 1) != "$")) {
							if (substr($varname, 0, 1) == "^") {
								// POST variable
								$postvars[substr($varname, 1)] = $varvalue;
							} else {
								// GET variable
								$getvars[$varname] = $varvalue;
							}
						}						
					}
					
					// ------------------------------
					// Remove GET parameters from url
					// ------------------------------
					$url = substr($url, 0, strpos($url, "?"));

				}

				// ----------------------------------------
				// Check if the urls match
				// ----------------------------------------
				if ((strlen($url) > strlen($currentUrl)) && (strcasecmp($currentUrl, substr($url, - strlen($currentUrl))) == 0)) {
					$urlMatched = true;
				} else if((strlen($currentUrl) > strlen($url)) && (strcasecmp($url, substr($currentUrl, - strlen($url))) == 0)) {
					$urlMatched = true;
				} else {
					$urlMatched = false;
				}
				
				// -----------------------------------------
				// Urls matched so see if REQUEST vars match
				// -----------------------------------------
				if ($urlMatched) {
					// Need to verify the parameters match
					$allRequestVarsMatched = true;
					foreach ($getvars as $varname => $varvalue) {
						if ((!array_key_exists($varname, $_GET)) || (strcasecmp($_GET[$varname], $varvalue) != 0)) {
							$allRequestVarsMatched = false;
							break;							
						}
					}
					if ($allRequestVarsMatched) {
						foreach ($postvars as $varname => $varvalue) {
							if ((!array_key_exists($varname, $_POST)) || (strcasecmp($_POST[$varname], $varvalue) != 0)) {
								$allRequestVarsMatched = false;
								break;							
							}
						}
					}

					if ($allRequestVarsMatched) {
						$found = true;
						$selectedItemId = $itemIndex;
					}
				}
				
				$itemIndex++;

			}
		}

		return $selectedItemId;

	}

}

?>
<?php
class table
{
  var $width;
  var $class;
  var $id;
  var $style;
  var $summary;
  var $rows = array();
  var $attributes = array();
  var $setAttributes;
  
  function table($width = "", $class = "", $id = "", $style = "", $summary = "")
  {
    $this->width = $width;
    $this->class = $class;
    $this->id = $id;
    $this->style = $style;
    $this->summary = $summary;
  }
  
  function addRow($class = "", $id = "", $placement = "body", $valign = "")
  {
    $row = new row;
    $row->class = $class;
    $row->id = $id;
    $row->placement = $placement;
    $row->valign = $valign;
    $this->rows[] = $row;
  }
  
  function getSetAttributes($attributes)
  {
    foreach($attributes as $attribute)
    {
      if(!empty($this->$attribute))
      {
        $this->attributes[] = $attribute;
      }
    }
    foreach($this->attributes as $attribute)
    {
      $this->setAttributes.=" ".strtolower($attribute)." = \"".$this->$attribute."\"";
    }
  }
  
  function draw($returnHTML=false)
  {
    $this->getSetAttributes(array("width", "class", "id", "style", "summary"));
    $output.= "<table ".$this->setAttributes.">\n";
    $placements = array("head", "body", "foot");
    foreach($placements as $placement)
    {
        $output2 = "";      
        foreach($this->rows as $row)
        {
          if($row->placement == $placement)
          {
            $output2.=$row->draw();            
          }
        }
        if(!empty($output2))
        {
          $output .="\t<t$placement>\n";
          $output .=$output2;
          $output .="\t</t$placement>\n";
        }        
    }
    $output.="</table>\n";
    if(!$returnHTML)
      echo $output;
    else
      return $output;
  }
 
}

class row extends table
{
  var $valign;
  var $placement;
  var $cells = array();
  
  function addCell($content, $type = "d",  $class = "", $id = "", $rowspan = "", $colspan = "", $valign ="",  $scope = "")
  {
    $cell = new cell;
    $cell->content = $content;
    $cell->type = $type;
    $cell->class = $class;
    $cell->id = $id;
    $cell->rowspan = $rowspan;
    $cell->colspan = $colspan;
    $cell->valign = $valign;
    $cell->scope = $scope;
    $this->cells[]= $cell;
  }
  
  function draw()
  {
    table::getSetAttributes(array("class", "id", "valign"));
    $output .= "\t\t<tr ".$this->setAttributes.">\n";
    foreach($this->cells as $cell)
    {
      $output.=$cell->draw();
    }
    $output.="\t\t</tr>\n";
    return $output;
  }
}

class cell extends row
{
  var $type;
  var $rowspan;
  var $colspan;
  var $scope;
  var $content;

  function draw()
  {
    table::getSetAttributes(array("class", "id", "rowspan", "colspan", "valign", "scope"));
    $output = "\t\t\t<t".$this->type." ".$this->setAttributes.">".$this->content."</t".$this->type.">\n";        
      return $output;
  }
}
?>
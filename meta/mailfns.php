<?php
// Multilingual support added
function sendGenericMail($to,$from,$subject,$body,$name,$lid="1",$ccEmailids=array(),$encode=false){
  /**    
   * returnpath added 01/27/2009, thanks Jake Carr @ rackspace for this suggestion.
   * JPC
   * */
   //$returnpath = "<apache@onlinecdp.org>";
   $returnpath = "<cdp@eckerd.edu>";
  
  // Added Encoding so that charcters are displayed properly in email
  $hdrs=($encode)? $encode : ""; 
	$hdrs .= "From: Conflict Dynamics <administrator@onlinecdp.org>\n";
	$hdrs .= "Reply-To: $from\n";

	if (count($ccEmailids) > 0) {
		$hdrs .="Cc: " . implode(", ", $ccEmailids) . "\n";
	}
	
  //$hdrs .= "Bcc: Pat Cheely <pcheely@thediscoverytec.com>\n"; 
  $hdrs .="Bcc: alburymg@eckerd.edu,pcheely.presensit@gmail.com\n";
	$bod='';
    
  if(!is_null($name)){
		$bod.= getEmailGreeting($name, $lid) . "\n\n".$body;
	} else {
		$bod.=$body;
	}
	// But we always include the sender in the email address
	switch($lid){
	 case "8" :
	     mail($to,$subject,$bod."\n\n( Emisor : ".$from." )\n\n",$hdrs);
	   break;
	 case "9" :
	     mail($to,$subject,$bod."\n\n( Sender : ".$from." )\n\n",$hdrs);
	   break;
	 default :
       mail($to,$subject,$bod."\n\n( Sender : ".$from." )\n\n",$hdrs);	 
	   break;
	}
    // next line for debugging
    error_log("[MAIL][TO: $to]");
    return true;
}

/**/
function getURLRoot(){
	// production
	// temporary Rackspace URL
	$rc= "https://www.onlinecdp.org";
	return $rc;
}

/**
 * 03/04/2008 - JPC
 * Replaced the above function so it could be used for the main and 
 * aliased directories.
 * 
function getURLRoot($alias=0){
	// production
	$dirs=explode("/",$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
	$URLroot ="https://";
	for($x=0; $x<=$alias; $x++){
	$URLroot.=($x==0)? $dirs[$x] : "/".$dirs[$x];	
  }
	return $URLroot;
}*/ 

function getReportURLRoot($alias=false){
	// production
	// temporary Rackspace URL
	$rc= "https://www.onlinecdp.org/";
  $rc.=($alias)? $alias."/cons/" : "cons/";
	return $rc;
}

/* Same as above, but for port 80, not port 443
// This has to do with an IE bug for reporting
function getReportURLRoot(){
	// production
	// temporary Rackspace URL
	$rc= "http://www.onlinecdp.org/cons/";
	return $rc;
}
*/

// multilingual support added
function getDisclaimer($lid="1"){
  switch($lid){
    case "8" : //Spanish
      $disclaimer="  POR FAVOR NO RESPONDA A ESTE MENSAJE.\n\n  LAS RESPUESTAS A ESTE MENSAJE POR VIA ELECTRONICA, SON ENVIADAS A UNA DIRECCION DE CORREOS NO FUNCIONAL, Y NO RECIBIRAN RESPUESTA.";
      break;
    case "9" : //Portuguese
      $disclaimer="  POR FAVOR NÃO RESPONDA A ESTA MENSAGEM QUE FOI ENVIADA POR UM PROGRAMA AUTOMÁTICO.";
      break;
    case "10" : //French
    case "12" : //French CA    
      $disclaimer="  MERCI DE NE PAS REPONDRE A CE MESSAGE.\n\n  LES REPONSES A CE MESSAGE SONT ADRESSEES A UNE BOITE DE MESSAGERIE INOPERANTE QUI NE TRAITE PAS LES REPONSES.";
      break;      
    default : //English
      $disclaimer="  PLEASE DO NOT REPLY TO THIS MESSAGE.\n\n  REPLIES TO THIS MESSAGE ARE SENT TO A NON-OPERATIONAL MAIL LOCATION AND WILL NOT RECEIVE A RESPONSE.";
      break; 
  }
  return $disclaimer;
/*  
	if($lid=="8"){
		return "  POR FAVOR NO RESPONDA A ESTE MENSAJE.\n\n  LAS RESPUESTAS A ESTE MENSAJE POR VIA ELECTRONICA, SON ENVIADAS A UNA DIRECCION DE CORREOS NO FUNCIONAL, Y NO RECIBIRAN RESPUESTA.";
	}
	return "  PLEASE DO NOT REPLY TO THIS MESSAGE.\n\n  REPLIES TO THIS MESSAGE ARE SENT TO A NON-OPERATIONAL MAIL LOCATION AND WILL NOT RECEIVE A RESPONSE.";
*/
}

function getEmailGreeting( $name=null, $lid=1, $previewOnly=false ) {
	// Returns the greeting for the email body
	
	if ($previewOnly) $name = '________, <i style="background-color:#FFFC17;">(Participant\'s name will be inserted when email is sent.)</i>';
		else $name .= ",";

	if (!is_null($name)) {
    switch($lid){
	   case "8" : //Spanish
	     $str="Estimado(a) $name";
	     break;
	   case "9" : //Portuguese
	     $str="Caro/Cara $name";	   
	     break;
	   case "10" :  //French
     case "12" : //French CA	   
	     $str="Cher(e) $name";
	     break;
	   case "11" : //German
	     $str="Sehr geehrte Dame/Herr $name";
	     break;
	   default : //English
	     $str="Dear $name";
	     break;
	  }
  }

  return $str;

}

function sendLogEmail($to,$from,$subject,$body,$name,$lid="1",$ccEmailids=array(),$encode=false){
   
  //$returnpath = "<apache@onlinecdp.org>";
  $returnpath = "<cdp@eckerd.edu>";
  
  // Added Encoding so that charcters are displayed properly in email
  $hdrs=($encode)? $encode : ""; 
	
  // Note: We always send from this account
	$hdrs .= "From: Conflict Dynamics <administrator@onlinecdp.org>\n";
	$hdrs .= "Reply-To: $from\n";

	if (count($ccEmailids) > 0) {
		$hdrs .="Cc: " . implode(", ", $ccEmailids) . "\n";
	}
	
	mail($to,$subject,$body."\n\n( Sender : ".$from." )\n\n",$hdrs); 
}
?>

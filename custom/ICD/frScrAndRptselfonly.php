<?php
// Pulls in dbfns.php as well
require_once "../meta/multilingual.php";
require_once "../meta/pdffns.php";
require_once "selfOnlyRptFns.php";  
// A standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);
define("MARGIN_LEFT",50);
define("MARGIN_RIGHT",565);
define("MARGIN_TOP",765);
define("MARGIN_BOTTOM",27);

// RGB values for the report
define("R_",(205/255));
define("G_",(230/255));
define("B_",(255/255));
 
function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");
	
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}  
	writeReportFooter($pdf,$name,$page,"1");
}

function renderPage1($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);
 	$dY=580;
  $txt=array();
  $txt[]="Profil de Conflit";
  $txt[]="Auto Evaluation";
  $txt[]="Feedback";
  $txt[]="Sal Capobianco, Ph.D.";
  $txt[]="Mark Davis, Ph.D.";               
  $txt[]="Linda Kraus, Ph.D.";
  $txt[]="Participant:";
  $fontg=pdf_load_font($pdf,"georgia","winansi","");
  $fontgb=pdf_load_font($pdf,"georgiab","winansi","");
  $fontgi=pdf_load_font($pdf,"georgiai","winansi",""); 
   
  // ( 1 ) Show title
  pdf_setfont($pdf,$fontg,32.0);
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[0], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[0]);
  // ( 2 ) Show subtitle
  $dY-=26;
  pdf_setfont($pdf,$fontg,22.0);  
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[1], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[1]);
  // ( 3 ) Show Title 
  $dY-=60; 
  pdf_setcolor($pdf,'both','rgb',(128/255),(128/255),(128/255),0);  
  pdf_setfont($pdf,$fontgi,36.0);
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[2], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[2]); 
  // ( 4 ) Show Authors
  $dY-=60;
  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0); 
  pdf_setfont($pdf,$fontg,13.0); 
  pdf_set_text_pos($pdf,MARGIN_LEFT+40,$dY); 
  pdf_show($pdf,$txt[3]); 
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[4], PAGE_WIDTH/2),$dY);
  pdf_show($pdf,$txt[4]);
  pdf_set_text_pos($pdf,rightXcord($pdf,$txt[5],MARGIN_RIGHT-40),$dY);
  pdf_show($pdf,$txt[5]);
  // ( 5 ) Show PIN
  $dY-=80;
  pdf_setfont($pdf,$fontg,15.0);     
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[6]." $name", PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[6]." $name");   
  // ( 6 ) Show Date
  $dY-=30;
  $dt=date("d/m/Y");
  pdf_setfont($pdf,$fontg,14.0);  
  pdf_set_text_pos($pdf,centerXcord($pdf, $dt, PAGE_WIDTH/2),$dY);
  pdf_show($pdf,$dt);  
  // ( 7 ) Show CDP Logo  
  $dY-=200;      
 	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,$dY,0.2);
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage2($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	
	$flid=getFLID("meta","ind0"); //3000
	$mltxt=getMLText($flid,"3",$lid);
  $fontg=pdf_load_font($pdf,"georgia","winansi","");
  $fontgb=pdf_load_font($pdf,"georgiab","winansi","");
  
	$size=16.0;
	pdf_setfont($pdf,$fontgb,$size);
	
	// Table of contents
	$title=$mltxt[21];
	$dY=560;	
	pdf_show_xy($pdf,$title,centerXcord($pdf, $title, PAGE_WIDTH/2),$dY);
	
	$size=12.0;
	pdf_setfont($pdf,$fontg,$size);
	$y=$dY;
	$x=55;
	$x2=475;
	
//	Introduction
	pdf_show_xy($pdf,$mltxt[22],$x,$y-=30);	
	pdf_show_xy($pdf,"3",$x2,$y);

//	Guide to Your Feedback Report
	pdf_show_xy($pdf,$mltxt[23],$x,$y-=30);	
	pdf_show_xy($pdf,"4",$x2,$y);

//	Constructive Response Profile
	pdf_show_xy($pdf,$mltxt[24],$x,$y-=30);	
	pdf_show_xy($pdf,"5",$x2,$y);

//	Destructive Response Profile
	pdf_show_xy($pdf,$mltxt[25],$x,$y-=30);	
	pdf_show_xy($pdf,"6",$x2,$y);

//	Hot Buttons Profile
	pdf_show_xy($pdf,$mltxt[26],$x,$y-=30);	
	pdf_show_xy($pdf,"7",$x2,$y);
/**/
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage3($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind3"); //3003
	//die("FLID = $flid");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $fontb=pdf_load_font($pdf,"georgiab","winansi","");
		
	$size=16.0;
	pdf_setfont($pdf,$fontb,$size);
	// Introduction
	$title=$mltxt[1];
	$dY=620;
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$dY);
	

  $txt = "La notion de conflit évoque toute situation impliquant des personnes rencontrant une incompatibilité d’intérêts ou d’objectifs, des différences marquées de sensibilité ou de principes personnels. Il s’agit là d’une définition assez large qui englobe de nombreuses situations. Par exemple, un conflit peut résulter d’une série de désaccords passés, faire suite à une divergence d’opinion sur une stratégie ou les méthodes à employer pour atteindre un but professionnel, à des convictions incompatibles, ou encore à une lutte pour des ressources ou des moyens. Un conflit peut également surgir lorsqu’une personne est perçue, de par ses actes, comme indélicate, irréfléchie ou grossière. En résumé, un conflit peut résulter de toute relation susceptible de vous placer en opposition à d’autres personnes.";
	$textflow = pdf_create_textflow($pdf, $txt, "fontname=georgia fontsize=11 encoding=winansi alignment=justify");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=20, "");
  $dY-=110;

  $txt = "Ainsi, le conflit est-il une composante inévitable de la vie en société. Malgré tous nos efforts pour y échapper ou y faire obstacle, nous nous trouvons invariablement, à un moment ou à un autre, en désaccord avec autrui. Pourtant, cela n’est pas nécessairement une mauvaise chose. Certains conflits peuvent se révéler fructueux – des points de vue divergents étant susceptibles d’amener des solutions inventives. Ce qui distingue particulièrement le différend utile du conflit destructeur tient dans la façon d’y répondre lorsqu'il se produit. De sorte que, s’il est inévitable, certains modes de réponse inefficaces et néfastes pourraient être écartés, tandis que des réponses efficaces et productives peuvent être acquises et développées. Cette proposition est au cœur du modèle servant de fondement au rapport de feedback à votre disposition : le Conflict Dynamics Profile® (CDP).";
	$textflow = pdf_create_textflow($pdf, $txt, "fontname=georgia fontsize=11 encoding=winansi alignment=justify");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY, "");
  $dY-=120;
  
  $txt = "Certaines réponses à un conflit peuvent être considérées comme des éléments constructifs, qu’elles interviennent aux premiers stades du conflit ou lorsque celui-ci se développe. En d’autres termes, ces réponses ont pour effet de ne pas aggraver le conflit davantage. De telles conduites réduisent même les tensions, en permettant de centrer les ressorts du conflit sur les faits et les idées plutôt que sur les questions de personnalité. Les réponses considérées comme destructives et donc néfastes tendent, quant à elles, à rendre les choses plus difficiles. Ces conduites n’interviennent que très peu dans la bonne résolution du conflit parce qu’elles ne font qu’accentuer les questions relatives aux personnes. Si le conflit peut aisément être comparé à un incendie, alors les réponses constructives permettent de l’éteindre, tandis que les réponses destructives ne font que l’attiser. Il est donc incontestablement préférable de répondre à un conflit par des approches constructives au lieu de s’engager dans des conduites destructives.";
	$textflow = pdf_create_textflow($pdf, $txt, "fontname=georgia fontsize=11 encoding=winansi alignment=justify");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY, "");
  $dY-=130;
  
  $txt = "Au-delà des aspects constructif et destructif, il est aussi possible de considérer d’autres variantes aux réponses à apporter au conflit sous la forme de conduites actives et passives. Les réponses actives sont celles par lesquelles une personne entreprend des actions tangibles en réponse à une opposition ou une provocation. De tels comportements peuvent être constructifs autant que destructifs. Le fait qu’ils demandent un effort personnel sensible caractérise leur nature active. Par contraste, les réponses passives ne nécessitent pas tant d’effort ou d’initiative personnelle. Leur caractère passif tient dans le fait que la personne n’engage que peu ou pas d’action manifeste, alors que ces comportements s’expriment néanmoins. De même, les réponses passives peuvent être autant constructives que destructives et ainsi contribuer à rendre les choses meilleures ou bien à les faire empirer.";
	$textflow = pdf_create_textflow($pdf, $txt, "fontname=georgia fontsize=11 encoding=winansi alignment=justify");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY, "");

  pdf_delete_textflow($pdf, $textflow);
/**/
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage4($cid,&$pdf,&$page,$pid,$lid){
  $name=writeHeader($cid,$pdf,$lid);
  $font=pdf_load_font($pdf,"georgia","winansi","");
  $fontb=pdf_load_font($pdf,"georgiab","winansi","");
  $fmtA="fontname=georgia fontsize=11 encoding=winansi alignment=left";
  $fmtB="fontname=georgiab fontsize=11 encoding=winansi alignment=left";
	
	$size=14.0;
	pdf_setfont($pdf,$fontb,$size);
  $title = "Guide d’interprétation du Rapport de Feedback";
  $dY=620;
  pdf_show_xy($pdf,$title,centerXcord($pdf, $title, PAGE_WIDTH/2),$dY);
  $size=10.5;
  pdf_setfont($pdf,$fontb,$size);
  $title="Profil de Réponses Constructives";
	pdf_show_xy($pdf,$title,MARGIN_LEFT,$dY-=32);
	pdf_setfont($pdf,$font,$size);
  $txt="Les sept comportements ayant pour effet de réduire le conflit sont :";
	pdf_show_xy($pdf,$txt,MARGIN_LEFT,$dY-=12);  
  
  $txt1="Prendre du Recul (";
  $txt2="PR";
  $txt3=") – se mettre à la place de l’autre et chercher à comprendre son point de vue";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=10;
	
	$txt1="Créer des Solutions (";
  $txt2="CS";
  $txt3=") – réfléchir avec l’autre personne et poser des questions ; tenter d’imaginer et d’élaborer des solutions au problème";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, MARGIN_LEFT, $dY-=10, "");   
	$dY-=20;  
   
	$txt1="Exprimer ses Emotions (";
  $txt2="EE";
  $txt3=") – parler ouvertement avec l’autre personne et exprimer honnêtement ses pensées et sentiments";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;

	$txt1="Aller vers Autrui (";
  $txt2="AA";
  $txt3=") – faire le premier pas, tendre la main ; tenter de réparer la situation pour se racheter";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;
	
	//Analyser la Situation (AS) – prendre la mesure en pesant le pour et le contre ; faire preuve de sens critique et penser à la meilleure réponse à apporter
	$txt1="Analyser la Situation (";
  $txt2="AS";
  $txt3=") – prendre la mesure en pesant le pour et le contre ; faire preuve de sens critique et penser à la meilleure réponse à apporter";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;	
	
	//TEmporiser (TE) – laisser les choses se calmer ou se donner du temps quand la situation s’envenime ; attendre une possibilité de conclusion
	$txt1="TEmporiser (";
  $txt2="TE";
  $txt3=") – laisser les choses se calmer ou se donner du temps quand la situation s’envenime ; attendre une possibilité de conclusion";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA);  
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;	
	
	//Rester Flexible (RF) – rester souple et positif ; voir le bon côté de la situation pour en tirer le meilleur parti
	$txt1="Rester Flexible (";
  $txt2="RF";
  $txt3=") – rester souple et positif ; voir le bon côté de la situation pour en tirer le meilleur parti";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;

  //$size=11;
  pdf_setfont($pdf,$fontb,$size);
  $title="Profil de Réponses Destructives";
	pdf_show_xy($pdf,$title,MARGIN_LEFT,$dY-=32);  
  
  pdf_setfont($pdf,$font,$size);
  $txt="Les huit comportements ayant pour effet d’envenimer le conflit sont :";
	pdf_show_xy($pdf,$txt,MARGIN_LEFT,$dY-=11);  

  $txt1="GAgner à tout prix (";
  $txt2="GA";
  $txt3=") – défendre vigoureusement son point de vue et tenter de l’emporter à tout prix";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=10;
	
  $txt1="Montrer sa Colère (";
  $txt2="MC";
  $txt3=") – laisser éclater sa colère, élever la voix et utiliser des mots crus, durs et agressifs";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20;
	
  $txt1="Dévaloriser Autrui (";
  $txt2="DA";
  $txt3=") – rire de l’autre et se montrer sarcastique ; ridiculiser ou discréditer ses idées ou ses positions";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20;

  $txt1="Vouloir se Venger (";
  $txt2="VV";
  $txt3=") – tenter de prendre sa revanche ; faire obstruction à l’autre ou user de représailles";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20;  

  $txt1="Éviter Autrui (";
  $txt2="EA";
  $txt3=") – se dérober ou ignorer l’autre personne ; agir de manière distante et lointaine sans s’occuper de la situation";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20; 
	
  $txt1="Céder à Autrui (";
  $txt2="CA";
  $txt3=") – renoncer à ses positions et céder du terrain en espérant échapper à la potentialité et au risque du conflit";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20; 
	
  //Cacher son Ressenti (CR) – retenir ses véritables émotions malgré la contrariété et l’importance des sentiments
  $txt1="Cacher son Ressenti (";
  $txt2="CR";
  $txt3=") – retenir ses véritables émotions malgré la contrariété et l’importance des sentiments";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20; 
	
  //Se Critiquer (SC) – rejouer l’incident dans sa tête et se critiquer pour n’avoir pas mieux géré la situation
  $txt1="Se Critiquer (";
  $txt2="SC";
  $txt3=") – rejouer l’incident dans sa tête et se critiquer pour n’avoir pas mieux géré la situation";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20;   
                      
  writeReportFooter($pdf,$name,$page,$lid);   
}

function getSelfOnlyResponses($cid,$loSid,$hiSid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from SELFONLYREPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID between $loSid and $hiSid order by a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function drawSelfOnlyGraph(&$pdf,$x,$y,$width,$height,$data,$lid){
	$flid=getFLID("meta","indgraph"); //3009
	$mltxt=getMLText($flid,"3",$lid);

	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=count($data);
	$ystep=round($height/9);
	$xstep=round($width/($scales+1));
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
  
  $shade=array(0,3,6);	
	// 1. Horizontal divisions
	$i;
	for($i=0;$i<8;$i++){
	  if(in_array($i,$shade)){
		 pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
     pdf_rect($pdf,($x+$xstep),$y+$ystep+round($i*$ystep),($width-$xstep),(2*$ystep));
     pdf_fill($pdf);
     pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);	  
	  }
		pdf_moveto($pdf,($x+$xstep),$y+$ystep+round($i*$ystep));
		pdf_lineto($pdf,($x+$width),$y+$ystep+round($i*$ystep));
		pdf_stroke($pdf);
	}

	// 2. the box outline 	
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);
	
	// divide the first box
	pdf_moveto($pdf,($x+$xstep),($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));
	
	// little box that juts out
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x,$y);
	pdf_lineto($pdf,$x,$y+round($ystep/2));
	pdf_lineto($pdf,$x+$xstep,$y+round($ystep/2));
	
	
	// 3. Vertical divisions
	for($i=$x+round(2*$xstep);$i<$x+$width;$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,$y+$ystep);
	}
	pdf_stroke($pdf);
	
	// 4. Vertical Scale
	pdf_setfont($pdf,$font,10.0);
  $hYs=array();
	$vs=array("30","35","40","45","50","55","60","65");
	$j=0;
	for($i=($y+$ystep);($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$hYs[]= $i;
		if(4==$j){
			pdf_setfont($pdf,$font,10.0);
			// Average
			//die(print_r($mltxt));
			pdf_show_xy($pdf,$mltxt[31],$x-15,$i);
			pdf_setfont($pdf,$font,10.0);
		}
		$j++;
	}
  //die(print_r($hYs));
	pdf_show_xy($pdf,"70",($x+round(2*$xstep/3)),$y+$height-2);
	pdf_setfont($pdf,$font,10.0);
	
	//"Very","Low","","","High","Very"
	//"Low","","","","","High"
	$vs=array("",$mltxt[32],$mltxt[33],"","","",$mltxt[34],$mltxt[32],"");
	$vs1=array("",$mltxt[33],"","","","","",$mltxt[34],"");
	$j=0;
	//for($i=($y+round(1.5*$ystep));($i<$y+$height);$i+=$ystep){
	 foreach($hYs as $hY){
	  switch($j){
	   case 1:
	   case 7:	   
	     $adjY = $hY+10;
	     break;
	   case 2: 
	     $adjY = $hY+25;
	     break;	 
	   case 6: 
	     $adjY = $hY-25;
	     break;
     default:
       $adjY = $hY; 
       break;       
	  }
	  
		pdf_show_xy($pdf,$vs[$j],$x-3,$adjY);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}

	// which scale do we have? we can determine from the SID
	switch($data[0][3]){
		case 1:
			//"Perspective Taking","Creating Solutions","Expressing Emotions","Reaching Out","Reflective Thinking","Delay Responding","Adapting"
			$vs=array($mltxt[1],$mltxt[2],$mltxt[3],$mltxt[4],$mltxt[5],$mltxt[6],$mltxt[7]);
			break;
		case 8:
			//"Winning","Displaying Anger","Demeaning Others","Retaliating","Avoiding","Yielding","Hiding Emotions","Self- Criticizing"
			$vs=array($mltxt[11],$mltxt[12],$mltxt[13],$mltxt[14],$mltxt[15],$mltxt[16],$mltxt[17],$mltxt[18]);
			break;
		case 22:
			//"Unrely","Over Analyze","Unapp","Aloof","Micro Manage","Self Centered","Abrasive","Untrust","Hostile"
			//$vs=array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]); //???
			$vs=($lid==1)? array("Unreliable",
                "Overly- Analytical",
                "Unapp- reciative",
                "Aloof",
                "Micro- Managing",
                "Self- Centered",
                "Abrasive",
                "Untrust- worthy",
                "Hostile") : array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]);
			break;
	}
	
	// 5. Draw the scale
	$sTxt=array("Reaching Out","Non Fiable");
	if($data[0][3]==22) 	pdf_setfont($pdf,$font,9.0);
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$offset=round(0.30*$ystep)+2;	
		
    if((!in_array($vs[$i], $sTxt))&&(count(explode(" ",$vs[$i])) > 1)){
      $offset=round(0.40*$ystep)+2;
    }
		pdf_setfont($pdf,$font,9.0);
		pdf_show_boxed($pdf,$vs[$i],$j,($y+round($ystep/2)),$xstep,$offset,"center","");
		$j+=$xstep;
	}
	pdf_setfont($pdf,$font,10.0);
	
	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,$y,$xstep,round(0.35*$ystep),"center","");
		$j+=$xstep;
	}

	// Value
	pdf_show_xy($pdf,$mltxt[35],$x+5,$y+5);
	
	// 7. Draw the graph
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val<0){
		$clip2=true;
		$val=0;
		// 01-25-2005: Fixing a border condition
		$n1=0;
	}
	elseif($val>40){
		$clip2=true;
		$val=40;
		// 01-25-2005: Fixing a border condition
		$n1=40;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=30;
		if($val<0){
			$clip2=true;
			$val=0;
			$n1=0;
		}
		elseif($val>40){
			$clip2=true;
			$val=40.5;
			$n1=40.5;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+$ystep+($n1*($ystep/5)));
		}
		
		$yc=$y+$ystep+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_moveto($pdf,$x+5,$y+round($ystep/3));
	pdf_lineto($pdf,$x+$xstep-5,$y+round($ystep/3));
	pdf_stroke($pdf);	

	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);

	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val>=0&&$val<=40){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=30;
		if($val>=0&&$val<=40){
			$yc=$y+$ystep+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	/*	*/
	pdf_circle($pdf,$x+round($xstep/2),$y+round($ystep/3),4);
	pdf_fill_stroke($pdf);
}

function renderPage5($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Constructive Responses
	$title="Réponses Constructives";  //$mltxt[51];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	Higher numbers are more desirable
	$title="- des valeurs plus élevées sont préférables -";  //$mltxt[52];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),600);


	$rows=getSelfOnlyResponses($cid,"1","7");
	if($rows){
		drawSelfOnlyGraph($pdf,50,130,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage6($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0"); //3000
	//die("flid = $flid");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");
  
	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//  Destructive Responses
	$title=$mltxt[61];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title="- des valeurs moins élevées sont préférables -";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),600);


	$rows=getSelfOnlyResponses($cid,"8","15");
	if($rows){
		drawSelfOnlyGraph($pdf,50,130,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage7($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind7");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");
  $fmtA="fontname=georgia fontsize=11 encoding=winansi alignment=justify";
  $fmtB="fontname=georgiab fontsize=11 encoding=winansi alignment=justify";
  		
	$size=16.0;
	pdf_setfont($pdf,$bfont,$size);
	$title="Profil des Déclencheurs – Hot Buttons";
	pdf_show_xy($pdf,$title,centerXcord($pdf, $title, PAGE_WIDTH/2),620); 


  $dY=610;	
  $txtA = "Cette partie du Rapport du ";
  $txtB = "Conflict Dynamics Profile";
  $txtC = "® est sensiblement différente des deux précédentes. Au lieu d’indiquer votre manière particulière de réagir aux situations de conflit, cette section apporte un éclairage spécifique quant aux types de personnes ou de situations susceptibles de vous contrarier et de causer un conflit potentiel – autrement dit, vos points sensibles (ou Hot Buttons) sont identifiés ici comme facteurs déclencheurs.";
  $textflow = pdf_create_textflow($pdf, $txtA, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txtB, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txtC, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, MARGIN_LEFT, $dY-=10, "");   
	$dY-=55;
	
  $txt = "Vous trouverez ci-dessous une première description des déclencheurs de conflit mesurés par le CDP-I et un graphe illustrant votre degré d’irritabilité selon les personnes ou les situations. Évidemment, ces facteurs ne recouvrent pas l’ensemble de tout ce qui est susceptible d’affecter les personnes ; par contre, ils recouvrent les plus courants. Dans chaque cas, un score particulièrement élevé indique votre degré d’irritabilité ou de contrariété le plus sensible, avec un potentiel de conflit accru pour cette situation, comparativement à la moyenne des réponses au questionnaire CDP-I.";
  $textflow = pdf_create_textflow($pdf, $txt, $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, MARGIN_LEFT, $dY-=10, "");

	$size=11.0;
	$dY-=(8*$size);
	pdf_setfont($pdf,$font,$size);  
  $txt="Les neuf facteurs déclencheurs de conflit (comportement d’autrui ou situation critique) sont :";
	pdf_show_xy($pdf,$txt,MARGIN_LEFT,$dY);    
    
  

  $sY=$dY;
  $sY-=($size*2)-10;
  $sY2=$sY;
	$size=10.0;


	pdf_setfont($pdf,$bfont,$size);
//  pdfUnderline($pdf,$mltxt[40],180,$sY); //???
  $sY-=($size);  	
  pdf_setfont($pdf,$font,$size);
	//pdf_show_xy($pdf,$mltxt[21],180,500); ???
	pdf_show_xy($pdf,"qui ne respecte pas ses engagements (réalisations, délais) et sur",180,$sY);
	pdf_continue_text($pdf,"qui on ne peut compter faute d’implication et de régularité");
	//pdf_continue_text($pdf,$mltxt[22]); ???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	//pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui sur-analyse les situations et montre un souci exagéré du");
	pdf_continue_text($pdf,"détail ou qui se concentre sur des détails mineurs");
pdf_continue_text($pdf," ");  	
//	pdf_continue_text($pdf,$mltxt[23]); //???
//	pdf_continue_text($pdf,$mltxt[24]); //???
//	pdf_continue_text($pdf,$mltxt[64]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui ne reconnaît pas le mérite des autres ou qui en apprécie rarement les résultats");	
	pdf_continue_text($pdf,"à leur juste valeur");	
//	pdf_continue_text($pdf,$mltxt[25]); //???
//	pdf_continue_text($pdf,$mltxt[26]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui s'isole ou qui est difficile d'accès, qui ne recherche pas ou n'intègre pas");	
	pdf_continue_text($pdf,"l'avis des autres");
//	pdf_continue_text($pdf,$mltxt[27]); //???
//	pdf_continue_text($pdf,$mltxt[28]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui surveille et vérifie constamment le travail des autres (usage excessif du");	
	pdf_continue_text($pdf,"contrôle, des procédures ou du reporting)");
//	pdf_continue_text($pdf,$mltxt[29]); //???
//	pdf_continue_text($pdf,$mltxt[30]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui est centré/e sur lui/elle-même, qui perçoit le monde à travers son propre point");	
	pdf_continue_text($pdf,"de vue ou qui croit avoir toujours raison");
//	pdf_continue_text($pdf,$mltxt[31]); //???
//	pdf_continue_text($pdf,$mltxt[32]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui est arrogant/e, mordant/e, sarcastique et irritant/e, autoritaire et inflexible");	
//	pdf_continue_text($pdf,$mltxt[33]); //???
//	pdf_continue_text($pdf,$mltxt[34]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,"qui exploite les autres et qui s'attribue leur mérite, qui manque d'honnêteté et");	
	pdf_continue_text($pdf,"qui peut mentir voire trahir");
	//pdf_continue_text($pdf,$mltxt[35]); //???
	//pdf_continue_text($pdf,$mltxt[36]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui perd son sang-froid, se met en colère et crie après les autres ou qui se");	
	pdf_continue_text($pdf,"montre inamical/e ou brutal/e");	
	//pdf_continue_text($pdf,$mltxt[37]); //???
	//pdf_continue_text($pdf,$mltxt[38]); //???

	pdf_setfont($pdf,$bfont,$size);
	

	//Unreliable
	//Overly-Analytical
	//Unappreciative
	//Aloof
	//Micro-Managing
	//Self-Centered
	//Abrasive
	//Untrustworthy
	//Hostile
//  pdfUnderline($pdf,$mltxt[39],70,530); //???
  
	pdf_show_xy($pdf,"Non Fiable",70,$sY2-$size);
	
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Perfectionniste");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Insensible");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Solitaire");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Micro Manager");
	pdf_continue_text($pdf,"(Sur Contrôle)");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Egocentrique");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Cassant");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Indigne de");
	pdf_continue_text($pdf,"Confiance");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Violent");
	pdf_continue_text($pdf," ");
/**/
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage8($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");       //3000

	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Hot Buttons
	$title=$mltxt[81];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title = "- pour les comportements présentés, des valeurs plus élevées sont indicatrices de plus grande frustration ou d’irritation accrue, suscitant les réponses décrites par les graphes pages 5 et 6 -"; 
	$fmtA="fontname=georgia fontsize=11 encoding=winansi alignment=center";
	
  $textflow = pdf_create_textflow($pdf, $title, $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, MARGIN_LEFT, 610, "");


	$rows=getSelfOnlyResponses($cid,"22","30");
	if($rows){			
		drawSelfOnlyGraph(&$pdf,50,120,500,450,$rows,$lid);	
	}
	else{			
		pdf_show_xy($pdf,"Cannot graph hot buttons",200,500);	
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function getFRdate($format){
  setlocale(LC_ALL, 'fr_FR'); 
  $retVal=strftime($format);
  return $retVal;                        
}
?>
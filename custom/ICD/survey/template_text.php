<?php
$tmp=array();
$tmp['title']="Institute for Conflict Dynamics - Welcome to the Center for Conflict Dynamics at Eckerd College";
$tmp['css']="../custom/ICD/survey/styles_backend.css";
$tmp['topright_img']="../custom/ICD/survey/topright.png";
$tmp['support']="&nbsp;";
$tmp['foot_logo']="<a href=\"http://www.conflict-institute.com/\"><img alt=\"Institute for Conflict Dynamics\" class=\"left\" height=\"66\" src=\"../custom/ICD/survey/footer_logo.png\" title=\"ICD Home\" width=\"207\"/></a>";
$tmp['foot_contact']="Kurfürstendamm 184, 10707 Berlin, Germany<br />Contact: <a href=\"mailto:service@conflict-institute.com?Subject=Assessment%20Center\">service@conflict-institute.com</a><br /><a href=\"http://www.conflict-institute.com\">www.conflict-institute.com</a>";
$tmp['foot_copyright']="Authorized exploitation of the English edition © 1999 Center <br />for Conflict Dynamics / USA for this edition, published and<br />sold by permission of Center for Conflict Dynamics, the <br />owner of all rights to publish and sell the name.";


?>
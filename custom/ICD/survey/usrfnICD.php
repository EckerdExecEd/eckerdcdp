<?php


/*   */
function writeHead($title,$script=false){
  	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"; 
	  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">"; 
    echo "<head>"; 
    echo "<title>$title</title>"; 
    echo "<link href=\"../custom/ICD/survey/styles_backend.css\" rel=\"stylesheet\" type=\"text/css\" />"; 
	  if($script)
	    echo "$script"; 
	  echo "<script type=\"text/javascript\" src=\"../meta/sortabletable/sortabletable.js\"></script>\n";
	  echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../meta/sortabletable/sortabletable.css\" />\n";
    echo "</head>";	  
}

function writeBody(){
  echo"<body class=\"internal\"> 
	       <div id=\"wrapper\"> 
		        <div id=\"main\"> 
 			        <div id=\"banner\"> 
                <h5><a href=\"../admin/home.php\">CCD Home</a></h5> 
                <h6>Home of the Conflict Dynamics Profile</h6> 
			        </div><!-- end banner --> 
			        <div id=\"content\">";
}

function writeFooter(){
$cust_dir="../custom/ICD/survey/";
$dir=getcwd(); 
  echo"</div><!-- end content --> 
		  </div><!-- end main -->
		  <div id=\"footer\"> 
      <div id=\"extras\"> 
    <div id=\"copyright\"> 
    <p>Authorized exploitation of the English edition © 1999 Center <br />for Conflict Dynamics / USA for this edition, published and<br />sold by permission of Center for Conflict Dynamics, the <br />owner of all rights to publish and sell the name.</p> 
    </div> 
    <!-- end copyright --> 
    <a href=\"http://www.eckerd.edu\"><img alt=\"Institute for Conflict Dynamics Home\" class=\"left\" height=\"66\" src=\"".$cust_dir."footer_logo.png\" title=\"ICD Home\" width=\"207\"/></a> 
    <p>Kurfürstendamm 184, 10707 Berlin, Germany<br />Contact: <a href=\"mailto:service@conflict-institute.com?Subject=Assessment%20Center\">service@conflict-institute.com</a><br /><a href=\"http://www.conflict-institute.com\">www.conflict-institute.com</a></p> 
    </div><!-- end extras --> 
 		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>"; 
}
			        
?>
<?php
// Functions for the self survey
require_once "template_text.php";

function writeCustomHeader($lid="1"){
    global $tmp;
    $ttl =& $tmp['title'];
  	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"; 
	  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"; 
    echo "<head>\n"; 
    echo "<title>$ttl</title>\n"; 
    echo "<link href=\"../custom/ICD/survey/styles_backend.css\" rel=\"stylesheet\" type=\"text/css\" />\n"; 
    echo "</head>\n";	 
  echo"<body class=\"internal\" onload=\"positionFixeddiv()\" onscroll=\"repositionFixeddiv()\"  onkeydown=\"repositionFixeddiv()\" > 
	       <div id=\"wrapper\"> 
		        <div id=\"main\"> 
 			        <div id=\"banner\"> 
                <h5><a href=\"#\">ICD Home</a></h5> 
                <h6>&nbsp;</h6> 
			        </div><!-- end banner --> 
			        <div id=\"content\">";    
}


// Write the page footer for the survey
function writeCustomFooter($lid="1"){
  global $tmp;
  $cust_dir="../custom/ICD/survey/";
  $ftCopy = $tmp['foot_copyright'];
  $ftContact = $tmp['foot_contact'];
  echo"</div><!-- end content --> 
		  </div><!-- end main -->
		  <div id=\"footer\"> 
      <div id=\"extras\"> 
    <div id=\"copyright\"> 
    <p>$ftCopy</p> 
    </div> 
    <!-- end copyright --> 
    <a href=\"http://www.eckerd.edu\"><img alt=\"Institute for Conflict Dynamics Home\" class=\"left\" height=\"66\" src=\"".$cust_dir."footer_logo.png\" title=\"ICD Home\" width=\"207\"/></a> 
    <p>$ftContact</p> 
    </div><!-- end extras --> 
 		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>"; 
}

?>
<?php
//----------------------------------------------------
//------ Reporting
//----------------------------------------------------
function getCandidateName($cid){
	$conn=dbConnect();
	$query="select FNAME, LNAME from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0]." ".$row[1];	
}

function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function writeHeader($cid,$pdf,$lid){
  $sY=740;
	$name=getCandidateName($cid);
	$lidVs=array("1"=>"English","8"=>"Spanish","10"=>"French","11"=>"German");
  $txt=array();
  $txt[]="Conflict Dynamics Profile";
  $txt[]="Contact : cdp@eckerd.edu";
  $txt[]="CDP";
  $txt[]="Individual";
  $txt[]="Conflict Dynamics Profile ®";
  $txt[]=$lidVs[$lid]." Version";
 
  // ( 1 ) Place Image
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);

	$img=pdf_open_image_file($pdf,"PNG","../images/custom/IFCD_Logog_empty_185x174.png","",0);
//	pdf_place_image($pdf,$img,48,$sY-66,0.6);
	
	
  // ( 2 ) Display Organization Title
	pdf_setcolor($pdf,'both','rgb',(33/255),(53/255),(109/255),0);
	$font=pdf_load_font($pdf,"georgiab","winansi","");
	pdf_setfont($pdf,$font,18.0);
  pdf_show_xy($pdf,$txt[0],rightXcord($pdf,$txt[0],MARGIN_RIGHT),$sY);	
//$sX = 50;
//pdf_show_xy($pdf,$txt[0],$sX,$sY);

  // ( 3 ) Display Contact
  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_load_font($pdf,"georgia","winansi","");
	pdf_setfont($pdf,$font,10.0);
  pdf_show_xy($pdf,$txt[1],rightXcord($pdf,$txt[1],MARGIN_RIGHT),$sY-12);	
 
  // ( 4 ) Display Assessment Title 2nd Word
  pdf_setcolor($pdf,'both','rgb',(51/255),(121/255),(123/255),0);
  pdf_setfont($pdf,$font,36.0);
  $sX=rightXcord($pdf,$txt[3],MARGIN_RIGHT);
//  $sX = 50;
  pdf_show_xy($pdf,$txt[3],$sX,$sY-56);

  // ( 5 ) Display Assessment Title 1st Word  
  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
  pdf_setfont($pdf,$font,30.0);
  pdf_show_xy($pdf,$txt[2],rightXcord($pdf,$txt[2],$sX-2),$sY-56);
//  pdf_show_xy($pdf,$txt[2],$sX + 170,$sY-56);

  // ( 6a ) Render bar
  pdf_setcolor($pdf,'both','rgb',(153/255),(204/255),(255/255),0);
  pdf_rect($pdf,MARGIN_LEFT,$sY-80,515,12);
	pdf_fill_stroke($pdf);

  // ( 6b ) Render bar outline
  pdf_setcolor($pdf,'both','rgb',(79/255),(129/255),(189/255),0);
  pdf_rect($pdf,MARGIN_LEFT,$sY-80,515,12);
  pdf_stroke($pdf);
  
  // ( 6c ) Render title copyright
  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
  pdf_setfont($pdf,$font,8.0);
  pdf_set_text_pos($pdf,MARGIN_LEFT+5,$sY-77);
  pdf_show($pdf,$txt[4]);
  
  // ( 6d ) Render language version 
  pdf_set_text_pos($pdf,rightXcord($pdf,$txt[5],MARGIN_RIGHT-5),$sY-77);
  pdf_show($pdf,$txt[5]);       
	return $name;	

}

function writeReportFooter(&$pdf,$name,&$page,$lid){
  $sY=70;
  $fontg=pdf_load_font($pdf,"georgia","winansi","");
  $hfont=pdf_load_font($pdf,"Helvetica","host","");
  
  $txt=array();
  $txt[]=""; //"© Institute for Conflict Dynamics Europe GmbH";
  $txt[]=""; //"Kurfürstendamm 184, 10707 Berlin, Germany; Managing Partners: Pierre Naquet, Stefan Schönholz, Frank Strathus;";
  $txt[]=""; //"Handelsregistergericht: Berlin, Handelsregisternummer: HRB 137780 B; Steuernummer: 27/049/00059 ; VAT DE 280186509";
  $txt[]="Authorized exploitation of the English edition © 1999 Center for Conflict Dynamics / USA for this edition, published and sold by";
  $txt[]="permission of Center for Conflict Dynamics, the owner of all rights to publish and sell the name.";
  if($page >1){
    // ( 1a ) Render bar
    pdf_setcolor($pdf,'both','rgb',(153/255),(204/255),(255/255),0);
    pdf_rect($pdf,MARGIN_LEFT,$sY,515,12);
  	pdf_fill_stroke($pdf);
  
    // ( 1b ) Render bar outline
    pdf_setcolor($pdf,'both','rgb',(79/255),(129/255),(189/255),0);
    pdf_rect($pdf,MARGIN_LEFT,$sY,515,12);
    pdf_stroke($pdf);
    
    // ( 1c ) Display Date
    pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
    pdf_setfont($pdf,$fontg,8.0);
    pdf_set_text_pos($pdf,MARGIN_LEFT+5,$sY+3);
    pdf_show($pdf,date("d/m/Y"));
      
    // ( 1d ) Display Participant PIN (i.e. Fname/Lname)
    $pin="Participant PIN: $name";
    pdf_set_text_pos($pdf,centerXcord($pdf, $pin, PAGE_WIDTH/2),$sY+3); 
    pdf_show($pdf,$pin);
  
    // ( 1e ) Display Page #
    pdf_setfont($pdf,$fontg,10.0);  
    pdf_set_text_pos($pdf,rightXcord($pdf,$page,MARGIN_RIGHT-5),$sY+3);
    pdf_show($pdf,$page);   

  }
/*
  pdf_setcolor($pdf,'both','rgb',(62/255),(93/255),(120/255),0);   //#3E5D78 62,93,120
  pdf_setfont($pdf,$hfont,9.0);
  $dY = $sY - 10;
  foreach($txt as $line){
     pdf_set_text_pos($pdf,centerXcord($pdf, $line, PAGE_WIDTH/2),$dY);
     pdf_show($pdf,$line);
     pdf_setfont($pdf,$hfont,8.0);
     $dY-=($dY==($sY-10))? 10 : 9;
  }
*/      	
	pdf_end_page($pdf);
	// Increment the page number
	$page+=1;
}	

//----------------------------------------------------
//--- Scoring
//----------------------------------------------------
// Has the candidate responded to enough questions?
function isItComplete($cid,$pid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from RATERRESP where RID=$cid and TID=3 and VAL is not NULL");
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	// We allow 3 missing answers currently i.e. must have 96 of 99 to score
	return $row[0]>=96;
}

// get rid of all pre-existing scores for this candidate
function getRidOfOldScores($cid,$pid){
	$conn=dbConnect();
	$query="delete from SELFONLYREPORTSCORE where CID=$cid";
	mysql_query($query);
	$query="delete from SCALESCORE where RID=$cid and CID=$cid and PID=$pid";
	mysql_query($query);
}

// Compute the raw and standardized scale scores for a single self only rater
function computeSelfOnlyScaleScores($cid,$pid){
	$conn=dbConnect();
	
	// Raw score
	$query="insert into SCALESCORE (SID,RID,PID,CID,CATID,RAWSCORE,STDSCORE) ";
	$query=$query."select b.SID, a.RID, $pid, $cid, 1, AVG(VAL), 0 from RATERRESP a , SCALEITEM b, RATER c ";
	$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and a.RID=$cid and VAL is not NULL group by b.SID,a.RID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;	
	}
	
	// Standardized score
	$rows=getScaleMeanAndStd($cid,$pid);
	if(!$rows){
		return false;	
	}
	foreach($rows as $row){
		if(0!=$row[2]){
			$std=50+(10*(($row[0]-$row[1])/$row[2]));
			$query="update SCALESCORE set STDSCORE=$std where SID=$row[3] and RID=$cid and CID=$cid and PID=$pid";
			mysql_query($query);
		}
	}
	return "Calculated scale scores - OK";	
}
// function to return the appropriate Mean and Standard deviation etc for a Scale
// for self only
function getScaleMeanAndStd($cid,$pid){
	$conn=dbConnect();
	$query="select b.RAWSCORE,a.MEANVAL1,a.DEVVAL1,a.SID from SCALE a,SCALESCORE b where a.SID=b.SID and b.RID=$cid and b.CID=$cid and b.RID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// calculates the appropriate report scores for self only
function calculateSelfOnlyReportScores($cid,$pid){
	$conn=dbConnect();
	// always calculate for Self
	$query="insert into SELFONLYREPORTSCORE (SID,CID,AVGSCORE) select SID,CID,STDSCORE from SCALESCORE where RID=$cid and CID=$cid and PID=$pid and CATID=1 and STDSCORE is not NULL";
	if(false===mysql_query($query)){
		return false;
	}
	return "Calculated individual report scores - OK";
}

// checks to see if a license has been used for this candidate
function alreadyConsumedLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from SELFLIC where CONID=$conid and CID=$cid and TID=$tid");
	$row=mysql_fetch_row($rs);
	return $row[0]>0?true:false;
}

// Consumes a self-only license
function consumeSelfOnlyLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("insert into SELFLIC (CONID,CID,TID,TS) values ($conid,$cid,$tid,NOW())");
	return $rs;
}

// Score all the candidates in a program
function scoreProgram($pid){
	$conn=dbConnect();
	$msg="Scoring...";
	
	$rs=mysql_query("select CONID from PROGCONS where PID=$pid");
	$row=mysql_fetch_row($rs);
	$conid=$row[0];
	
	$rs=mysql_query("select CID,FNAME,LNAME from CANDIDATE where PID=$pid");
	$rows=dbRes2Arr($rs);
	if(false==$rows){
		return "<font color=\"#ff0000\">Unable to score program: unable to retrive participants</font>";
	}
	
	$ok=false;
	foreach($rows as $row){
		// we really don't need to score here since there's no group report
		// so let's just iterate over each candidate
		
		// Only consume a license if they are complete
		$query="select IFNULL(a.ENDDT,'Incomplete') from RATER a, CANDIDATE b,PROGRAM c where c.PID=$pid and a.CID=" . $row[0] . " and a.CID=b.CID and b.PID=c.PID";
		$rs=mysql_query($query);
		$rw=mysql_fetch_row($rs);
		if ($rw[0] != "Incomplete") {
		
			$query="select count(CID) from SELFLIC where CID=" . $row[0] . " and TID=3 and CONID=" . $_SESSION["conid"];
			$rs=mysql_query($query);
			$rw=mysql_fetch_row($rs);
			$ok=true;
		
			if ($rw[0] == 0) {
				// Only consume a license if one has not already been consumed
				$msg=$msg."<br> $row[1] $row[2]";
				$cid=$row[0];
				consumeSelfOnlyLicense($cid,$conid,3);
			}
		}
	}

	if(false==$ok){
		return "<font color=\"#ff0000\">Unable to score program: no participants</font>";
	} else {
		$msg .= '<p style="color:green;font-weight:bold;margin-bottom:0px;">The program has now been scored.<br />If you need to re-score the program you will need to re-open it first.</p>';
	}
	
	// Set status to "Scored"
	$query="update PROGRAM set EXPIRED='S' where PID=$pid";
	//echo $query."<br>";
	mysql_query($query);
	
	// Send email to consultant (added 7/6/08 by CDZ)
	sendScoreEmail($pid);
	
	return $msg."<br>";
}

/*
function writeICDHeader($pdf,$cid,$lid){
	$name=getCandidateName($cid);
 
	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,750,515,15);
	pdf_fill_stroke($pdf);
	// Note: (R) is hex AE, so well use sprintf to format properly
//	$what="Conflict Dynamics Profile ".sprintf("%c",0xae);
	$what=$mltxt[1];
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,centerXcord($pdf, $what, PAGE_WIDTH/2),754);

	return $name;	
}

function writeICDFooter($pdf,$name,$page,$lid){
  $sY=70;
  $fontg=pdf_load_font($pdf,"georgia","winansi","");
  $hfont=pdf_load_font($pdf,"Helvetica","host","");
  
  $txt=array();
  $txt[]="© Institute for Conflict Dynamics Europe GmbH";
  $txt[]="Kurfürstendamm 184, 10707 Berlin, Germany; Managing Partners: Pierre Naquet, Stefan Schönholz, Frank Strathus;";
  $txt[]="Handelsregistergericht: Berlin, Handelsregisternummer: HRB 137780 B; Steuernummer: 27/049/00059 ; VAT DE 280186509";
  $txt[]="Authorized exploitation of the English edition © 1999 Center for Conflict Dynamics / USA for this edition, published and sold by";
  $txt[]="permission of Center for Conflict Dynamics, the owner of all rights to publish and sell the name.";
  if($page >1){
    // ( 1a ) Render bar
    pdf_setcolor($pdf,'both','rgb',(153/255),(204/255),(255/255),0);
    pdf_rect($pdf,MARGIN_LEFT,$sY,515,12);
  	pdf_fill_stroke($pdf);
  
    // ( 1b ) Render bar outline
    pdf_setcolor($pdf,'both','rgb',(79/255),(129/255),(189/255),0);
    pdf_rect($pdf,MARGIN_LEFT,$sY,515,12);
    pdf_stroke($pdf);
    
    // ( 1c ) Display Date
    pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
    pdf_setfont($pdf,$fontg,8.0);
    pdf_set_text_pos($pdf,MARGIN_LEFT+5,$sY+3);
    pdf_show($pdf,date("d/m/Y"));
      
    // ( 1d ) Display Participant PIN (i.e. Fname/Lname)
    $pin="Participant PIN: $name";
    pdf_set_text_pos($pdf,centerXcord($pdf, $pin, PAGE_WIDTH/2),$sY+3); 
    pdf_show($pdf,$pin);
  
    // ( 1e ) Display Page #
    pdf_setfont($pdf,$fontg,10.0);  
    pdf_set_text_pos($pdf,rightXcord($pdf,$page,MARGIN_RIGHT-5),$sY+3);
    pdf_show($pdf,$page);   

  }
  pdf_setcolor($pdf,'both','rgb',(62/255),(93/255),(120/255),0);   //#3E5D78 62,93,120
  pdf_setfont($pdf,$hfont,9.0);
  $dY = $sY - 10;
  foreach($txt as $line){
     pdf_set_text_pos($pdf,centerXcord($pdf, $line, PAGE_WIDTH/2),$dY);
     pdf_show($pdf,$line);
     pdf_setfont($pdf,$hfont,8.0);
     $dY-=($dY==($sY-10))? 10 : 9;
  }
      	
	pdf_end_page($pdf);
	// Increment the page number
	$page+=1;
}	
*/

?>

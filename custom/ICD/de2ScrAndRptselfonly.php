<?php
// Pulls in dbfns.php as well  
require_once "../meta/multilingual.php";
require_once "../meta/pdffns.php";
require_once "selfOnlyRptFns2.php";
// A standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);
define("MARGIN_LEFT",50);
define("MARGIN_RIGHT",565);
define("MARGIN_TOP",765);
define("MARGIN_BOTTOM",27);

// RGB values for the report
define("R_",(218/255));
define("G_",(218/255));
define("B_",(255/255));

function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");
	
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}
	writeReportFooter($pdf,$name,$page,"1");
}

function renderPage1($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);
	$name=ck4decode($name);
	$dY=580;
  $txt=array();
  $txt[]="Ihr Dynamisches Konfliktprofil";
  $txt[]="- Selbsteinschätzung -";
  $txt[]="Feedback Bericht";
  $txt[]="Sal Capobianco, Ph.D.";
  $txt[]="Mark Davis, Ph.D.";               
  $txt[]="Linda Kraus, Ph.D.";
  $txt[]="Participant:";
  $fontg=pdf_load_font($pdf,"georgia","winansi","");
  $fontgb=pdf_load_font($pdf,"georgiab","winansi","");
  $fontgi=pdf_load_font($pdf,"georgiai","winansi",""); 
   
  // ( 1 ) Show title
  pdf_setfont($pdf,$fontg,32.0);
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[0], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[0]);
  // ( 2 ) Show subtitle
  $dY-=26;
  pdf_setfont($pdf,$fontg,22.0);  
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[1], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[1]);
  // ( 3 ) Show Title 
  $dY-=60; 
  pdf_setcolor($pdf,'both','rgb',(128/255),(128/255),(128/255),0);  
  pdf_setfont($pdf,$fontgi,36.0);
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[2], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[2]); 
  // ( 4 ) Show Authors
  $dY-=60;
  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0); 
  pdf_setfont($pdf,$fontg,13.0); 
  pdf_set_text_pos($pdf,MARGIN_LEFT+40,$dY); 
  pdf_show($pdf,$txt[3]); 
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[4], PAGE_WIDTH/2),$dY);
  pdf_show($pdf,$txt[4]);
  pdf_set_text_pos($pdf,rightXcord($pdf,$txt[5],MARGIN_RIGHT-40),$dY);
  pdf_show($pdf,$txt[5]);
  // ( 5 ) Show PIN
  $dY-=80;
  pdf_setfont($pdf,$fontg,15.0);     
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[6]." $name", PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[6]." $name");   
  // ( 6 ) Show Date
  $dY-=30;
  $dt=date("d/m/Y");
  pdf_setfont($pdf,$fontg,14.0);  
  pdf_set_text_pos($pdf,centerXcord($pdf, $dt, PAGE_WIDTH/2),$dY);
  pdf_show($pdf,$dt);  
  // ( 7 ) Show CDP Logo  
  $dY-=200;      
 	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,$dY,0.2);
	writeReportFooter($pdf,$name,$page,$lid);

}

function renderPage2($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

  $txt=array();
  $txt[]="Inhaltsverzeichnis";
  $txt[]="Einleitung ............................................................................................................      Seite	3";
  $txt[]="Leitfaden für Ihren Feedback Bericht .................................................................      Seite	4";
  $txt[]="Ihr konstruktives Antwortprofil ..........................................................................      Seite	5";
  $txt[]="Ihr destruktives Antwortprofil ............................................................................      Seite	6";
  $txt[]='Ihr „Hot Buttons“ Profil ......................................................................................      Seite	7';  
	
	$flid=getFLID("meta","ind0"); //3000
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

  $fontg=pdf_load_font($pdf,"georgia","winansi","");
  $fontgb=pdf_load_font($pdf,"georgiab","winansi","");
	$size=14.0;
	pdf_setfont($pdf,$fontgb,$size);
	
	// Table of contents
	$title=$mltxt[21];
	$dY=560;	
	pdf_show_xy($pdf,$txt[0],centerXcord($pdf, $txt[0], PAGE_WIDTH/2),$dY);
	
	$size=12.0;
	pdf_setfont($pdf,$fontg,$size);

	$x=55;
	$x2=475;
	
//	Introduction
	pdf_show_xy($pdf,$txt[1],$x,$dY-=30);	
	
//	Guide to Your Feedback Report
	pdf_show_xy($pdf,$txt[2],$x,$dY-=30);	
	
//	Constructive Response Profile
	pdf_show_xy($pdf,$txt[3],$x,$dY-=30);	
	
//	Destructive Response Profile
	pdf_show_xy($pdf,$txt[4],$x,$dY-=30);	
	
//	Hot Buttons Profile
	pdf_show_xy($pdf,$txt[5],$x,$dY-=30);	
/**/	

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage3($cid,&$pdf,&$page,$pid,$lid){

	$name=writeHeader($cid,$pdf,$lid);
	$txt=array();
	$txt[]="Einleitung";
	$txt[]="Konflikte entstehen immer dann, wenn Menschen unterschiedliche Interessen, Ziele, Prinzipien oder Gefühle haben, die sich nicht miteinander vereinbaren lassen. Natürlich ist dies nur eine übergeordnete Beschreibung, die viele unterschiedliche Situationen beinhaltet. Ein Konflikt kann durch verschiedene Faktoren ausgelöst werden, zum Beispiel:";
  $txt[]="  -   durch seit langem bestehende Streitpunkte";
  $txt[]="  -   durch Meinungsverschiedenheiten";
  $txt[]="  -   durch unvereinbare Wertvorstellungen oder Einstellungen";
  $txt[]="  -   durch den Konkurrenzkampf um Ressourcen.";
  $txt[]="Dies sind nur einige der möglichen Faktoren. Andererseits können Konflikte auch durch das Verhalten Einzelner ausgelöst werden, das von anderen als unsensibel, gedankenlos oder unhöflich wahrgenommen wird. Folglich sind Konflikte in unserem Leben unvermeidbar. Selbst wenn wir unser Bestes geben, lassen sie sich manchmal einfach nicht umgehen. Konflikte jedoch müssen nicht unbedingt als etwas Negatives betrachtet werden. Manche können auch als produktiv angesehen werden, denn durch unterschiedliche Betrachtungsweisen entstehen neue, kreative Lösungsansätze.";
  $txt[]="Der größte Unterschied zwischen einem konstruktiven und einem destruktiven Konflikt besteht darin, wie der Einzelne auf einen auftretenden Konflikt reagiert. Auch wenn Konflikte als solche unvermeidlich bleiben, kann man es erlernen, ineffiziente und schädliche Reaktionen darauf zu vermeiden und stattdessen effektiv und vorteilhaft zu reagieren. Dieser gedankliche Ansatz ist die Basis des Dynamischen Konflikt-Profils (englisch: CDP).";
  $txt[]="Einige Verhaltensmuster, unabhängig davon ob zu Anfang eines Konflikts oder auch nachdem er bereits entstanden ist, können als konstruktiv angesehen werden. Durch konstruktives Verhalten kann folglich eine weitere Eskalation im Konflikt vermieden werden. Entsprechend wird die situationsbedingte Spannung verringert. Im Mittelpunkt der Konfliktsituation stehen nunmehr Sachthemen und Ideen, während persönliche Differenzen in den Hintergrund rücken.";            
  $txt[]="Andererseits verschlechtern destruktive Verhaltensweisen die Situation. Sie tragen nur wenig zur Lösung des Konflikts bei und führen zu persönlichen Auseinandersetzungen. Würde man Konflikte als ein Feuer betrachten, so würde das konstruktive Verhalten helfen, die Flammen zu löschen, während das destruktive Verhalten das Feuer weiter anschürt. Offensichtlich ist es also klüger, in Konfliktsituationen konstruktiv und nicht destruktiv zu reagieren.";
  $txt[]="Weiterhin ist es möglich, die Verhaltensweisen nicht nur einfach als konstruktiv oder destruktiv darzustellen, sondern sie zusätzlich darin zu unterscheiden, ob sie aktiv oder passiv sind.";
  $txt[]="Aktive Verhaltensweisen zeichnen sich dadurch aus, dass Personen, die Konflikten oder Provokationen ausgesetzt sind, offensiv agieren, bzw. reagieren. Das aktive Verhalten kann dabei sowohl konstruktiv als auch destruktiv sein. Die Aktivität drückt sich vielmehr dadurch aus, dass eine Person offensiv mit einem Konflikt umgeht.";
  $txt[]="Die Passivität bedingt dagegen, dass diejenigen, die in einen Konflikt verwickelt sind, sich bewusst  oder unbewusst gegen aktive Handlungen entscheiden. Auch passive Verhaltensweisen können konstruktiv oder destruktiv sein. Sie tragen entweder dazu bei, die Situation zu verbessern, oder diese zu verschlechtern.";
        
  $font=pdf_load_font($pdf,"georgia","winansi","");
  $fontb=pdf_load_font($pdf,"georgiab","winansi","");
	$size=14.0;	
	pdf_setfont($pdf,$fontb,$size);
	// Introduction
	$title=$mltxt[1];
	$dY=620;
	pdf_show_xy($pdf,$txt[0],centerXcord($pdf, $txt[0], PAGE_WIDTH/2),$dY);
	$size=10;	

	$textflow = pdf_create_textflow($pdf, $txt[1], "fontname=georgia fontsize=10 encoding=winansi alignment=left");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=60;	

	pdf_setfont($pdf,$font,10);
  pdf_show_xy($pdf,$txt[2],MARGIN_LEFT+10,$dY);  	
  $dY-=$size+2;
	pdf_show_xy($pdf,$txt[3],MARGIN_LEFT+10,$dY);
  $dY-=$size+2;
	pdf_show_xy($pdf,$txt[4],MARGIN_LEFT+10,$dY);
  $dY-=$size+2;
	pdf_show_xy($pdf,$txt[5],MARGIN_LEFT+10,$dY);
  $dY-=0;

	$textflow = pdf_create_textflow($pdf, $txt[6], "fontname=georgia fontsize=10 encoding=winansi alignment=left");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=60;	  

	$textflow = pdf_create_textflow($pdf, $txt[7], "fontname=georgia fontsize=10 encoding=winansi alignment=left");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=50;

	$textflow = pdf_create_textflow($pdf, $txt[8], "fontname=georgia fontsize=10 encoding=winansi alignment=left");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=50;

	$textflow = pdf_create_textflow($pdf, $txt[9], "fontname=georgia fontsize=10 encoding=winansi alignment=left");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=50;
  
	$textflow = pdf_create_textflow($pdf, $txt[10], "fontname=georgia fontsize=10 encoding=winansi alignment=left");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=20;  

	$textflow = pdf_create_textflow($pdf, $txt[11], "fontname=georgia fontsize=10 encoding=winansi alignment=left");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=40; 
     
	$textflow = pdf_create_textflow($pdf, $txt[12], "fontname=georgia fontsize=10 encoding=winansi alignment=left");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=60;      
  
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage4($cid,&$pdf,&$page,$pid,$lid){
  $name=writeHeader($cid,$pdf,$lid);
  $font=pdf_load_font($pdf,"georgia","winansi","");
  $fontb=pdf_load_font($pdf,"georgiab","winansi","");
	
	$size=14.0;
	pdf_setfont($pdf,$fontb,$size);
  $title = "Leitfaden für Ihren Feedback Bericht";
	$dY=620;
	pdf_show_xy($pdf,$title,centerXcord($pdf, $title, PAGE_WIDTH/2),$dY);

  $txt=array();
  $txt[] ="Konstruktive Verhaltensmuster";
  $txt[] ="Die folgenden sieben Verhaltensmuster tragen dazu bei, eine Konfliktsituation zu verbessern:";                  //  1
  $txt[] ="Perspektive einnehmen (PE)";                                                                                    //  2
  $txt[] ="Sie versetzen sich in die Situation Ihres Konfliktgegners und versuchen, dessen Ansichten zu verstehen.";       //  3
  $txt[] ="Lösungen entwickeln (LE)";                                                                                      //  4
  $txt[] ="Sie betreiben Brainstorming mit Ihrem Konfliktgegner, stellen Fragen und versuchen, Lösungen zu finden.";       //  5
  $txt[] ="Gefühle verbalisieren (GV)";                                                                                    //  6
  $txt[] ="Sie sprechen offen mit Ihrem Konfliktgegner und drücken ihre Gedanken und Gefühle aus.";                        //  7
  $txt[] ="Den ersten Schritt machen (ES)";                                                                                //  8
  $txt[] ="Sie gehen auf Ihren Konfliktgegner zu und machen den ersten Schritt, um die Situation positiv zu gestalten.";   //  9
  $txt[] ="Reflektierendes Denken (RD)";                                                                                   //  10
  $txt[] ="Sie analysieren die Situation, wägen Vor- u. Nachteile ab. Ihr Ziel ist, sich lösungsorientiert zu verhalten."; //  11
  $txt[] ="Verzögert antworten (VA)";                                                                                      //  12
  $txt[] ="Abwarten bis sich die Situation beruhigt; eine Auszeit nehmen, wenn der Konflikt emotional eskaliert.";         //  13
  $txt[] ="Flexibles Anpassen (FA)";                                                                                       //  14
  $txt[] ="Sie bleiben flexibel und versuchen, das Beste aus der Situation zu machen.";                                    //  15
  $sz=10.5;
	pdf_setfont($pdf,$fontb,$sz);	
	pdf_show_xy($pdf,$txt[0],centerXcord($pdf, $txt[0], PAGE_WIDTH/2),$dY-=(2*$sz));
  $fmtA="fontname=georgia fontsize=10 encoding=winansi alignment=left";
  $fmtB="fontname=georgiab fontsize=10 encoding=winansi alignment=left";

	$tf = pdf_create_textflow($pdf, $txt[1], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
	$tf = pdf_create_textflow($pdf, $txt[2], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=16, "");  
	$tf = pdf_create_textflow($pdf, $txt[3], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
	$tf = pdf_create_textflow($pdf, $txt[4], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[5], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
  $tf = pdf_create_textflow($pdf, $txt[6], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[7], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
	$tf = pdf_create_textflow($pdf, $txt[8], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[9], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
  $tf = pdf_create_textflow($pdf, $txt[10], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[11], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;  
	$tf = pdf_create_textflow($pdf, $txt[12], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[13], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
  $tf = pdf_create_textflow($pdf, $txt[14], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[15], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;           
	

  $txt[] ="Destruktive Verhaltensmuster";                                                                                   //  16
  $txt[] ="Die folgenden acht Verhaltensweisen führen dazu, dass ein Konflikt weiter eskaliert:";                           //  17
  $txt[] ="Gewinnen um jeden Preis (GP)";                                                                                   //  18
  $txt[] ="Sie beharren auf Ihrem Standpunkt und setzen sich mit Vehemenz durch.";                                          //  19
  $txt[] ="Wut zeigen (WZ)";                                                                                                //  20
  $txt[] ="Sie drücken offen Ihren Ärger aus, werden laut und benutzen harte und böse Worte.";                              //  21
  $txt[] ="Jemanden lächerlich machen (LM)";                                                                                //  22
  $txt[] ="Sie lachen Ihren Konfliktgegner aus, machen sich über dessen Ideen lustig und reagieren sarkastisch.";           //  23
  $txt[] ="Vergeltung üben (VÜ)";                                                                                           //  24
  $txt[] ="Sie stellen Ihrem Konfliktgegner Hindernisse in den Weg, üben Vergeltung und versuchen, sich zu rächen.";        //  25
  $txt[] ="Kontakt vermeiden (KV)";                                                                                         //  26
  $txt[] ="Sie gehen Ihrem Konfliktgegner aus dem Weg, ignorieren ihn und verhalten sich zurückhaltend.";                   //  27
  $txt[] ="Nachgeben (NG)";                                                                                                 //  28
  $txt[] ="Sie geben klein bei und ordnen sich unter, um weitere Konflikte zu vermeiden.";                                  //  29
  $txt[] ="Gefühle zurückhalten (GZ)";                                                                                      //  30
  $txt[] ="Sie unterdrücken Ihre wahren Gefühle, obwohl sie emotionalisiert sind.";                                         //  31
  $txt[] ="Selbstkritik üben (SÜ)";                                                                                         //  32
  $txt[] ="Sie grübeln im Nachhinein, weil Sie der Meinung sind, Sie hätten es besser machen können.";                      //  33


  $sz=10.5;
	pdf_setfont($pdf,$fontb,$sz);	
	pdf_show_xy($pdf,$txt[16],centerXcord($pdf, $txt[16], PAGE_WIDTH/2),$dY-=($sz*2));
   
	$tf = pdf_create_textflow($pdf, $txt[17], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
	$tf = pdf_create_textflow($pdf, $txt[18], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=16, "");  
	$tf = pdf_create_textflow($pdf, $txt[19], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
	$tf = pdf_create_textflow($pdf, $txt[20], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[21], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
  $tf = pdf_create_textflow($pdf, $txt[22], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[23], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
	$tf = pdf_create_textflow($pdf, $txt[24], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[25], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
  $tf = pdf_create_textflow($pdf, $txt[26], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[27], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;  
	$tf = pdf_create_textflow($pdf, $txt[28], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[29], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8;
  $tf = pdf_create_textflow($pdf, $txt[30], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[31], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");
  $dY-=8; 
  $tf = pdf_create_textflow($pdf, $txt[32], $fmtB);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");  
	$tf = pdf_create_textflow($pdf, $txt[33], $fmtA);
	$rs = pdf_fit_textflow($pdf, $tf, 555, 20, MARGIN_LEFT+5, $dY-=10, "");            

  writeReportFooter($pdf,$name,$page,$lid);   
}

function getSelfOnlyResponses($cid,$loSid,$hiSid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from SELFONLYREPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID between $loSid and $hiSid order by a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function drawSelfOnlyGraph(&$pdf,$x,$y,$width,$height,$data,$lid){
	$flid=getFLID("meta","indgraph"); //3009
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=count($data);
	$ystep=round($height/9);
	$xstep=round($width/($scales+1));
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
  
  $shade=array(0,3,6);	
	// 1. Horizontal divisions
	$i;
	for($i=0;$i<8;$i++){
	  if(in_array($i,$shade)){
		 pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
     pdf_rect($pdf,($x+$xstep),$y+$ystep+round($i*$ystep),($width-$xstep),(2*$ystep));
     pdf_fill($pdf);
     pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);	  
	  }
		pdf_moveto($pdf,($x+$xstep),$y+$ystep+round($i*$ystep));
		pdf_lineto($pdf,($x+$width),$y+$ystep+round($i*$ystep));
		pdf_stroke($pdf);
	}

	// 2. the box outline 	
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);
	
	// divide the first box
	pdf_moveto($pdf,($x+$xstep),($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));
	
	// little box that juts out
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x,$y);
	pdf_lineto($pdf,$x,$y+round($ystep/2));
	pdf_lineto($pdf,$x+$xstep,$y+round($ystep/2));
	
	
	// 3. Vertical divisions
	for($i=$x+round(2*$xstep);$i<$x+$width;$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,$y+$ystep);
	}
	pdf_stroke($pdf);
	
	// 4. Vertical Scale
	pdf_setfont($pdf,$font,10.0);
  $hYs=array();
	$vs=array("30","35","40","45","50","55","60","65");
	$j=0;
	for($i=($y+$ystep);($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$hYs[]= $i;
		if(4==$j){
			pdf_setfont($pdf,$font,10.0);
			// Average
			//die(print_r($mltxt));
			pdf_show_xy($pdf,"Mittel",$x-15,$i);
			pdf_setfont($pdf,$font,10.0);
		}
		$j++;
	}
  //die(print_r($hYs));
	pdf_show_xy($pdf,"70",($x+round(2*$xstep/3)),$y+$height-2);
	pdf_setfont($pdf,$font,10.0);
	
	//"Very","Low","","","High","Very"
	//"Low","","","","","High"
	$vs=array("","Sehr","Niedrig","","","","Hoch","Sehr","");
	$vs1=array("","niedrig","","","","","","hoch","");
	$j=0;
	//for($i=($y+round(1.5*$ystep));($i<$y+$height);$i+=$ystep){
	 foreach($hYs as $hY){
	  switch($j){
	   case 1:
	   case 7:	   
	     $adjY = $hY+10;
	     break;
	   case 2: 
	     $adjY = $hY+25;
	     break;	 
	   case 6: 
	     $adjY = $hY-25;
	     break;
     default:
       $adjY = $hY; 
       break;       
	  }
	  
		pdf_show_xy($pdf,$vs[$j],$x-3,$adjY);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}

	// which scale do we have? we can determine from the SID
	$scl=$data[0][3];
	switch($scl){
		case 1:
			//"Perspective Taking","Creating Solutions","Expressing Emotions","Reaching Out","Reflective Thinking","Delay Responding","Adapting"
			//$vs=array($mltxt[1],$mltxt[2],$mltxt[3],$mltxt[4],$mltxt[5],$mltxt[6],$mltxt[7]);
			$vs=array("Perspektive einnehmen (PE)",
                "Lösungen entwickeln (LE)",
                "Gefühle verbalisieren (GV)",
                "Den ersten Schritt machen (ES)",
                "Reflektierendes Denken (RD)",
                "Verzögert antworten (VA)",
                " Flexibles  Anpassen (FA)");
      $sz=7;                   
			break;
		case 8:
			//"Winning","Displaying Anger","Demeaning Others","Retaliating","Avoiding","Yielding","Hiding Emotions","Self- Criticizing"
			//$vs=array($mltxt[11],$mltxt[12],$mltxt[13],$mltxt[14],$mltxt[15],$mltxt[16],$mltxt[17],$mltxt[18]);
			$vs=array("Gewinnen um jeden Preis (GP)", 
                "Wut zeigen    (WZ)", 
                "Jemanden lächerlich machen (LM)", 
                "Vergeltung    üben (VÜ)", 
                "Kontakt vermeiden (KV)", 
                "Nachgeben   (NG)", 
                "Gefühle zurückhalten (GZ)", 
                "Selbstkritik    üben (SÜ)" );
      $sz=7;                
			break;
		case 22:
			//"Unrely","Over Analyze","Unapp","Aloof","Micro Manage","Self Centered","Abrasive","Untrust","Hostile"
			//$vs=array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]); //???
			$vs=($lid==1)? array("Unreliable",
                "Overly- Analytical",
                "Unapp- reciative",
                "Aloof",
                "Micro- Managing",
                "Self- Centered",
                "Abrasive",
                "Untrust- worthy",
                "Hostile") : 
                array("Unzuver- lässiges Verhalten","Überanaly- tisches Verhalten","Mangelnde Anerkennung","Distanziertes Verhalten","Mikro- Management",
                      "Egozen- trisches Auftreten","Grobes Verhalten","Vertrauensun- würdiges Verhalten","Feindseliges Verhalten");
      $sz=7;
			break;
	}
	
	// 5. Draw the scale
	$sTxt=array("Reaching Out","Non Fiable");
  pdf_setfont($pdf,$font,8.0);
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$offset=round(0.30*$ystep)+2;	
		
    if((!in_array($vs[$i], $sTxt))&&(count(explode(" ",$vs[$i])) > 1)){
      $offset=(($scl==8)||($scl==22))? round(0.40*$ystep)+4 : round(0.40*$ystep)+2;
    }
		pdf_setfont($pdf,$font,$sz);
		pdf_show_boxed($pdf,$vs[$i],$j,($y+round($ystep/2)),$xstep,$offset,"center","");
		$j+=$xstep;
	}
	pdf_setfont($pdf,$font,10.0);
	
	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,$y,$xstep,round(0.35*$ystep),"center","");
		$j+=$xstep;
	}

	// Value
	pdf_show_xy($pdf,"Werte",$x+5,$y+5);
	
	// 7. Draw the graph
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val<0){
		$clip2=true;
		$val=0;
		// 01-25-2005: Fixing a border condition
		$n1=0;
	}
	elseif($val>40){
		$clip2=true;
		$val=40;
		// 01-25-2005: Fixing a border condition
		$n1=40;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=30;
		if($val<0){
			$clip2=true;
			$val=0;
			$n1=0;
		}
		elseif($val>40){
			$clip2=true;
			$val=40.5;
			$n1=40.5;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+$ystep+($n1*($ystep/5)));
		}
		
		$yc=$y+$ystep+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_moveto($pdf,$x+5,$y+round($ystep/3));
	pdf_lineto($pdf,$x+$xstep-5,$y+round($ystep/3));
	pdf_stroke($pdf);	

	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);

	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val>=0&&$val<=40){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=30;
		if($val>=0&&$val<=40){
			$yc=$y+$ystep+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	/*	*/
	pdf_circle($pdf,$x+round($xstep/2),$y+round($ystep/3),4);
	pdf_fill_stroke($pdf);
}

function renderPage5($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");
	

	// draw title
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
//	Constructive Responses
	$title="Konstruktive Verhaltensmuster";  //$mltxt[51];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	Higher numbers are more desirable
	$title="(Höhere Werte sind erstrebenswert)";  //$mltxt[52];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),600);


	$rows=getSelfOnlyResponses($cid,"1","7");
	if($rows){
		drawSelfOnlyGraph($pdf,50,130,500,430,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage6($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0"); //3000
	//die("flid = $flid");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");
  
	// draw title
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
//  Destructive Responses
	$title="Destruktive Verhaltensmuster";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title="(Niedrigere Werte sind erstrebenswert)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),600);


	$rows=getSelfOnlyResponses($cid,"8","15");
	if($rows){
		drawSelfOnlyGraph($pdf,50,130,500,430,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage7($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);
  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");

  $txt=array();
  $txt[]='Ihr „Hot Buttons“ Profil';
  $txt[]='Dieser Abschnitt Ihres dynamischen Konfliktprofils CDP unterscheidet sich von den anderen. Anstatt herauszustellen, wie Sie normalerweise in Konfliktsituationen reagieren, bekommen Sie in diesem Teil Einblick darüber, welches Verhalten von Menschen oder welche Situationen Sie am meisten aufregen. Hier erfahren Sie mehr über die Faktoren, die Sie zur „Weißglut“ bringen können, also Ihre „Hot Buttons“.';
  $txt[]='Zunächst finden Sie eine kurze Beschreibung der „Hot Buttons“, die mittels des CDP gemessen werden. Anschließend enthält die Folgeseite eine Grafik, die Ihnen zeigt, wie stark Sie, im Vergleich zu anderen Menschen, auf das jeweilige Verhalten Ihres Gegenübers reagieren. Selbstverständlich existieren noch weit mehr „Hot Buttons“ als hier angegeben. Dieser CDP-Test stellt lediglich die „Hot Buttons“ dar, die am häufigsten im beruflichen Umfeld vorkommen. Auf jeden Fall können Sie anhand hoher Werte im Diagramm schnell erkennen, welche speziellen Verhaltensweisen anderer Personen Sie besonders stark verärgern oder aufregen und dadurch Ihre destruktive Konfliktbereitschaft erhöhen.';    
  
  $txt[]="Unzuverlässiges Verhalten";
  $txt[]="Jemand hält Fristen und Termine nicht ein. Man kann nicht auf ihn bauen.";
  $txt[]="Überanalytisches Verhalten";
  $txt[]="Jemand ist perfektionistisch, detailorientiert und konzentriert sich zu sehr auf Unwichtigkeiten.";
  $txt[]="Mangelnde Anerkennung";
  $txt[]="Jemand würdigt nie die Arbeit anderer und lobt selten.";
  $txt[]="Distanziertes Verhalten";
  $txt[]="Jemand sondert sich von den anderen ab, interessiert sich nicht für deren Meinung und es ist schwer,";
  $txt[]="an sie/ihn heranzukommen.";
  $txt[]="Mikro-Management";
  $txt[]="Jemand kontrolliert ständig die Arbeiten der anderen bis ins kleinste Detail.";
  $txt[]="Egozentrisches Auftreten";
  $txt[]="Jemand sieht sich selbst ständig im Mittelpunkt und glaubt sich immer im Recht.";
  $txt[]="Grobes Verhalten";
  $txt[]="Jemand ist arrogant, sarkastisch und/oder aggressiv.";
  $txt[]="Vertrauensunwürdiges Verhalten";
  $txt[]="Jemand nutzt andere aus und schmückt sich mit fremden Federn. Man kann ihm nicht trauen.";
  $txt[]="Feindseliges Verhalten";
  $txt[]="Jemand verliert schnell seine Beherrschung, wird wütend und laut.";
                                  
	$size=16.0;
  $sY=620;	
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,$txt[0],calcCenterStartPos($pdf,$txt[0],$font,$size),$sY);
	$size=11;
	pdf_setfont($pdf,$font,$size);	
	$sY-=20;
  $sY=text_block($pdf,$txt[1],105,12,MARGIN_LEFT,$sY); 
	$sY-=10;
  $sY=text_block($pdf,$txt[2],102,12,MARGIN_LEFT,$sY); 
	$sY-=10;
  pdf_show_xy($pdf," ",MARGIN_LEFT,$sY);
  pdf_continue_text($pdf," ");	    	
	$c=3;
	

	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;                      //3
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;                      //4
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;                      //5
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;                      //6
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;                     //7
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;                     //8
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;                     //9
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;                     //10
	pdf_continue_text($pdf,$txt[$c]); $c++;                     //11	
	pdf_continue_text($pdf," ");
	
	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_continue_text($pdf," ");      	

	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_setfont($pdf,$font,$size);	
	pdf_continue_text($pdf,$txt[$c]); $c++;
	pdf_continue_text($pdf," ");       	

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage8($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");       //3000

	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");

	// draw title
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
//	Hot Buttons
	$title="Hot Buttons";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=($lid==1)? "(Higher numbers indicate greater frustration or irritation in response to this kind of behavior.)" : 
   "(Höhere Werte zeigen an, welche Verhaltensweise Sie besonders stark irritiert)"; 
	
	if($lid==10)
	 $sY=text_block($pdf,$title,98,11,MARGIN_LEFT,600);
	else
    pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),600);


	$rows=getSelfOnlyResponses($cid,"22","30");
	if($rows){			
		drawSelfOnlyGraph(&$pdf,50,130,500,430,$rows,$lid);	
	}
	else{			
		pdf_show_xy($pdf,"Cannot graph hot buttons",200,500);	
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function getFRdate($format){
  setlocale(LC_ALL, 'fr_FR'); 
  $retVal=strftime($format);
  return $retVal;                        
}
?>

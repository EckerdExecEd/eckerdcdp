<?php
// Pulls in dbfns.php as well
require_once "../meta/multilingual.php";
require_once "../meta/pdffns.php";
require_once "selfOnlyRptFns.php";
// A standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);
define("MARGIN_LEFT",50);
define("MARGIN_RIGHT",565);
define("MARGIN_TOP",765);
define("MARGIN_BOTTOM",27);

// RGB values for the report
define("R_",0.85);
define("G_",0.85);
define("B_",1.0);


//----------------------------------------------------
//--- Scoring
//----------------------------------------------------

function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");
	
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}
	writeReportFooter($pdf,$name,$page,"1");
}

function renderPage1($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);
	$dY=580;
  $txt=array();
  $txt[]="Conflict Profile";
  $txt[]="Self-Assessment";
  $txt[]="Feedback";
  $txt[]="Sal Capobianco, Ph.D.";
  $txt[]="Mark Davis, Ph.D.";               
  $txt[]="Linda Kraus, Ph.D.";
  $txt[]="Participant:";
  $fontg=pdf_load_font($pdf,"georgia","winansi","");
  $fontgb=pdf_load_font($pdf,"georgiab","winansi","");
  $fontgi=pdf_load_font($pdf,"georgiai","winansi",""); 
   
  // ( 1 ) Show title
  pdf_setfont($pdf,$fontg,32.0);
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[0], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[0]);
  // ( 2 ) Show subtitle
  $dY-=26;
  pdf_setfont($pdf,$fontg,22.0);  
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[1], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[1]);
  // ( 3 ) Show Title 
  $dY-=60; 
  pdf_setcolor($pdf,'both','rgb',(128/255),(128/255),(128/255),0);  
  pdf_setfont($pdf,$fontgi,36.0);
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[2], PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[2]); 
  // ( 4 ) Show Authors
  $dY-=60;
  pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0); 
  pdf_setfont($pdf,$fontg,13.0); 
  pdf_set_text_pos($pdf,MARGIN_LEFT+40,$dY); 
  pdf_show($pdf,$txt[3]); 
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[4], PAGE_WIDTH/2),$dY);
  pdf_show($pdf,$txt[4]);
  pdf_set_text_pos($pdf,rightXcord($pdf,$txt[5],MARGIN_RIGHT-40),$dY);
  pdf_show($pdf,$txt[5]);
  // ( 5 ) Show PIN
  $dY-=80;
  pdf_setfont($pdf,$fontg,15.0);     
  pdf_set_text_pos($pdf,centerXcord($pdf, $txt[6]." $name", PAGE_WIDTH/2),$dY); 
  pdf_show($pdf,$txt[6]." $name");   
  // ( 6 ) Show Date
  $dY-=30;
  $dt=date("m/d/Y");
  pdf_setfont($pdf,$fontg,14.0);  
  pdf_set_text_pos($pdf,centerXcord($pdf, $dt, PAGE_WIDTH/2),$dY);
  pdf_show($pdf,$dt);  
  // ( 7 ) Show CDP Logo  
  $dY-=200;      
 	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,$dY,0.2);
  
  
    
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage2($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	
	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);
/* */
  $fontg=pdf_load_font($pdf,"georgia","winansi","");
  $fontgb=pdf_load_font($pdf,"georgiab","winansi","");
	$size=14.0;
	pdf_setfont($pdf,$fontgb,$size);
	
	// Table of contents
	$title=$mltxt[21];
	$dY=560;		
	pdf_show_xy($pdf,$title,centerXcord($pdf, $title, PAGE_WIDTH/2),$dY);
	
	$size=12.0;
	pdf_setfont($pdf,$fontg,$size);
	$y=$dY;
	$x=55;
	$x2=475;
	
//	Introduction
	pdf_show_xy($pdf,$mltxt[22],$x,$y-=30);	
	pdf_show_xy($pdf,"3",$x2,$y);

//	Guide to Your Feedback Report
	pdf_show_xy($pdf,$mltxt[23],$x,$y-=30);	
	pdf_show_xy($pdf,"4",$x2,$y);

//	Constructive Response Profile
	pdf_show_xy($pdf,$mltxt[24],$x,$y-=30);	
	pdf_show_xy($pdf,"5",$x2,$y);

//	Destructive Response Profile
	pdf_show_xy($pdf,$mltxt[25],$x,$y-=30);	
	pdf_show_xy($pdf,"6",$x2,$y);

//	Hot Buttons Profile
	pdf_show_xy($pdf,$mltxt[26],$x,$y-=30);	
	pdf_show_xy($pdf,"7",$x2,$y);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage3($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind3");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $fontb=pdf_load_font($pdf,"georgiab","winansi","");
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	// Introduction
	$title=$mltxt[1];
	$dY=620;
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$dY);
	
	$size=11.0;
	pdf_setfont($pdf,$font,$size);
//	Conflict refers to any situation in which people have incompatible interests, goals, principles, or
//	feelings. This is, of course, a broad definition and encompasses many different situations. A conflict
//	could arise, for instance, over a long-standing set of issues, a difference of opinion about strategy or
//	tactics in the accomplishment of some business goal, incompatible beliefs, competition for resources,
//	and so on. Conflicts can also result when one person acts in a way that another individual sees as
//	insensitive, thoughtless, or rude. A conflict, in short, can result from anything that places you and
//	another person in opposition to one another.
	pdf_show_xy($pdf,$mltxt[11],50,$dY-=40);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	pdf_continue_text($pdf,$mltxt[18]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	
//	Thus, conflict in life is inevitable. Despite our best efforts to prevent it, we inevitably find ourselves
//	in disagreements with other people at times. This is not, however, necessarily bad. Some kinds of
//	conflict can be productive--differing points of view can lead to creative solutions to problems. What
//	largely separates useful conflict from destructive conflict is how the individuals respond when the
//	conflict occurs. Thus, while conflict itself is inevitable, ineffective and harmful responses to conflict can
//	be avoided, and effective and beneficial responses to conflict can be learned. That proposition is at
//	the heart of the Conflict Dynamics Profile (CDP) Feedback Report you have received.
	pdf_continue_text($pdf,$mltxt[21]);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,$mltxt[26]);
	pdf_continue_text($pdf,$mltxt[27]);
	pdf_continue_text($pdf,$mltxt[28]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	
//	Some responses to conflict, whether occurring at its earliest stages or after it develops, can be
//	thought of as constructive responses. That is, these responses have the effect of not escalating the
//	conflict further. They tend to reduce the tension and keep the conflict focused on ideas, rather than
//	personalities. Destructive responses, on the other hand, tend to make things worse--they do little to
//	reduce the conflict, and allow it to remain focused on personalities. If conflict can be thought of as a
//	fire, then constructive responses help to put the fire out, while destructive responses make the fire
//	worse. Obviously, it is better to respond to conflict with constructive rather than destructive responses.
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf,$mltxt[32]);
	pdf_continue_text($pdf,$mltxt[33]);
	pdf_continue_text($pdf,$mltxt[34]);
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf,$mltxt[37]);
	pdf_continue_text($pdf,$mltxt[38]);
	pdf_continue_text($pdf,$mltxt[39]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

//	It is also possible to think of responses to conflict not simply as constructive or destructive, but as
//	differing in terms of how active or passive they are. Active responses are those in which the individual
//	takes some overt action in response to the conflict or provocation. Such responses can be either
//	constructive or destructive--what makes them active is that they require some overt effort on the part
//	of the individual. Passive responses, in contrast, do not require much in the way of effort from the
//	person. Because they are passive, they primarily involve the person deciding to not take some kind of
//	action. Again, passive responses can be either constructive or destructive--that is, they can make
//	things better or they can make things worse.
	pdf_continue_text($pdf,$mltxt[41]);
	pdf_continue_text($pdf,$mltxt[42]);
	pdf_continue_text($pdf,$mltxt[43]);
	pdf_continue_text($pdf,$mltxt[44]);
	pdf_continue_text($pdf,$mltxt[45]);
	pdf_continue_text($pdf,$mltxt[46]);
	pdf_continue_text($pdf,$mltxt[47]);
	pdf_continue_text($pdf,$mltxt[48]);
	pdf_continue_text($pdf,$mltxt[49]);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderNewPage4($cid,&$pdf,&$page,$pid,$lid){
  $name=writeHeader($cid,$pdf,$lid);
	$flid=getFLID("meta","ind5");   //3005
	$mltxt=getMLText($flid,"3",$lid);  
	//die(print_r($mltxt));	
  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");
	//$ifont=pdf_findfont($pdf,"Helvetica-Italic","host",0);	
	$size=14.0;
	pdf_setfont($pdf,$font,$size);
  $dY=620;
  pdf_show_xy($pdf,$mltxt[1],calcCenterStartPos($pdf,$mltxt[1],$font,$size),$dY);

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$mltxt[2],50,$dY-=24);
  pdf_setfont($pdf,$font,$size);
  $dY-=20;   
  for($i=5; $i<=40; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],105,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }
    
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,$mltxt[45],50,$dY);
  pdf_setfont($pdf,$font,$size);  
  $dY-=24;   
  for($i=50; $i<=90; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],105,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }   
                             
  writeReportFooter($pdf,$name,$page,$lid);    
}

function renderPage4($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind5");   //3005
	$mltxt=getMLText($flid,"3",$lid); 

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");
		
	$size=14.0;
	pdf_setfont($pdf,$font,$size);
  $dY=620;
  pdf_show_xy($pdf,$mltxt[1],calcCenterStartPos($pdf,$mltxt[1],$font,$size),$dY);

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$mltxt[2],50,$dY-=24);
  pdf_setfont($pdf,$font,$size);
  $dY-=20;   
  for($i=5; $i<=40; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],100,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }
    
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,$mltxt[45],50,$dY);
  pdf_setfont($pdf,$font,$size);  
  $dY-=24;   
  for($i=50; $i<=90; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],100,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }   
        
	writeReportFooter($pdf,$name,$page,$lid);
}

function getSelfOnlyResponses($cid,$loSid,$hiSid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from SELFONLYREPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID between $loSid and $hiSid order by a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function drawSelfOnlyGraph(&$pdf,$x,$y,$width,$height,$data,$lid){
	$flid=getFLID("meta","indgraph");
	$mltxt=getMLText($flid,"3",$lid);

	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=count($data);
	$ystep=round($height/9);
	$xstep=round($width/($scales+1));
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
  
  $shade=array(0,3,6);	
	// 1. Horizontal divisions
	$i;
	for($i=0;$i<8;$i++){
	  if(in_array($i,$shade)){
		 pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
     pdf_rect($pdf,($x+$xstep),$y+$ystep+round($i*$ystep),($width-$xstep),(2*$ystep));
     pdf_fill($pdf);
     pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);	  
	  }
		pdf_moveto($pdf,($x+$xstep),$y+$ystep+round($i*$ystep));
		pdf_lineto($pdf,($x+$width),$y+$ystep+round($i*$ystep));
		pdf_stroke($pdf);
	}

	// 2. the box outline 	
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);
	
	// divide the first box
	pdf_moveto($pdf,($x+$xstep),($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));
	
	// little box that juts out
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x,$y);
	pdf_lineto($pdf,$x,$y+round($ystep/2));
	pdf_lineto($pdf,$x+$xstep,$y+round($ystep/2));
	
	
	// 3. Vertical divisions
	for($i=$x+round(2*$xstep);$i<$x+$width;$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,$y+$ystep);
	}
	pdf_stroke($pdf);
	
	// 4. Vertical Scale
	pdf_setfont($pdf,$font,10.0);
  $hYs=array();
	$vs=array("30","35","40","45","50","55","60","65");
	$j=0;
	for($i=($y+$ystep);($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$hYs[]= $i;
		if(4==$j){
			pdf_setfont($pdf,$font,10.0);
			// Average
			pdf_show_xy($pdf,$mltxt[31],$x-15,$i);
			pdf_setfont($pdf,$font,10.0);
		}
		$j++;
	}
  //die(print_r($hYs));
	pdf_show_xy($pdf,"70",($x+round(2*$xstep/3)),$y+$height-2);
	pdf_setfont($pdf,$font,10.0);
	
	//"Very","Low","","","High","Very"
	//"Low","","","","","High"
	$vs=array("",$mltxt[32],$mltxt[33],"","","",$mltxt[34],$mltxt[32],"");
	$vs1=array("",$mltxt[33],"","","","","",$mltxt[34],"");
	$j=0;
	//for($i=($y+round(1.5*$ystep));($i<$y+$height);$i+=$ystep){
	 foreach($hYs as $hY){
	  switch($j){
	   case 1:
	   case 7:	   
	     $adjY = $hY+10;
	     break;
	   case 2: 
	     $adjY = $hY+25;
	     break;	 
	   case 6: 
	     $adjY = $hY-25;
	     break;
     default:
       $adjY = $hY; 
       break;       
	  }
	  
		pdf_show_xy($pdf,$vs[$j],$x-3,$adjY);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}

	// which scale do we have? we can determine from the SID
	switch($data[0][3]){
		case 1:
			//"Perspective Taking","Creating Solutions","Expressing Emotions","Reaching Out","Reflective Thinking","Delay Responding","Adapting"
			$vs=array($mltxt[1],$mltxt[2],$mltxt[3],$mltxt[4],$mltxt[5],$mltxt[6],$mltxt[7]);
			break;
		case 8:
			//"Winning","Displaying Anger","Demeaning Others","Retaliating","Avoiding","Yielding","Hiding Emotions","Self- Criticizing"
			$vs=array($mltxt[11],$mltxt[12],$mltxt[13],$mltxt[14],$mltxt[15],$mltxt[16],$mltxt[17],$mltxt[18]);
			break;
		case 22:
			//"Unrely","Over Analyze","Unapp","Aloof","Micro Manage","Self Centered","Abrasive","Untrust","Hostile"
			//$vs=array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]); //???
			$vs=($lid==1)? array("Unreliable",
                "Overly- Analytical",
                "Unapp- reciative",
                "Aloof",
                "Micro- Managing",
                "Self- Centered",
                "Abrasive",
                "Untrust- worthy",
                "Hostile") : array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]);
			break;
	}
	
	// 5. Draw the scale
	if($data[0][3]==22) 	pdf_setfont($pdf,$font,9.0);
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$offset=round(0.30*$ystep)+2;	
    if(($vs[$i] != "Reaching Out")&&(count(explode(" ",$vs[$i])) > 1)){
      $offset=round(0.40*$ystep)+2;
    }
		pdf_setfont($pdf,$font,9.0);
		pdf_show_boxed($pdf,$vs[$i],$j,($y+round($ystep/2)),$xstep,$offset,"center","");
		$j+=$xstep;
	}
	pdf_setfont($pdf,$font,10.0);
	
	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,$y,$xstep,round(0.35*$ystep),"center","");
		$j+=$xstep;
	}

	// Value
	pdf_show_xy($pdf,$mltxt[35],$x+5,$y+5);
	
	// 7. Draw the graph
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val<0){
		$clip2=true;
		$val=0;
		// 01-25-2005: Fixing a border condition
		$n1=0;
	}
	elseif($val>40){
		$clip2=true;
		$val=40;
		// 01-25-2005: Fixing a border condition
		$n1=40;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=30;
		if($val<0){
			$clip2=true;
			$val=0;
			$n1=0;
		}
		elseif($val>40){
			$clip2=true;
			$val=40.5;
			$n1=40.5;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+$ystep+($n1*($ystep/5)));
		}
		
		$yc=$y+$ystep+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_moveto($pdf,$x+5,$y+round($ystep/3));
	pdf_lineto($pdf,$x+$xstep-5,$y+round($ystep/3));
	pdf_stroke($pdf);	

	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);

	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val>=0&&$val<=40){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=30;
		if($val>=0&&$val<=40){
			$yc=$y+$ystep+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	/*	*/
	pdf_circle($pdf,$x+round($xstep/2),$y+round($ystep/3),4);
	pdf_fill_stroke($pdf);
}

function renderPage5($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Constructive Responses
	$title=$mltxt[51];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	Higher numbers are more desirable
	$title=$mltxt[52];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),600);


	$rows=getSelfOnlyResponses($cid,"1","7");
	if($rows){
		drawSelfOnlyGraph($pdf,50,130,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage6($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//  Destructive Responses
	$title=$mltxt[61];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=$mltxt[62];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),600);


	$rows=getSelfOnlyResponses($cid,"8","15");
	if($rows){
		drawSelfOnlyGraph($pdf,50,130,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage7($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind7");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");
	$ifont=pdf_load_font($pdf,"georgiai","winansi","");
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title=$mltxt[1];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);
	

	//This portion of the Conflict Dynamics Profile Feedback Report is a bit different from the others.
	//Instead of indicating how you typically respond to conflict situations, this section provides insight into
	//the kinds of people and situations which are likely to upset you and potentially cause conflict to occur:
	//in short, your hot buttons.
	//Below you will find a brief description of each of the hot buttons measured by the CDP, and on the
	//following page a graph which illustrates how upsetting--compared to people in general--you find each
	//situation. Obviously, these do not represent every possible hot button that people may have; they are
	//simply some of the most common ones. In each case, a higher score on the scale indicates that you
	//get especially irritated and upset by that particular situation.

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,$mltxt[11],70,600);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	pdf_continue_text($pdf,$mltxt[18]);
	pdf_continue_text($pdf,$mltxt[19]);
	pdf_continue_text($pdf,$mltxt[20]);
	
  $size=10.0;
  $arYs=array();
  pdf_setfont($pdf,$bfont,$size);
  pdfUnderline($pdf,$mltxt[39],70,470);   $arYs[]=470; 
  $dY=450;
	pdf_show_xy($pdf,$mltxt[2],70,$dY);     $arYs[]=$dY;

	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[3]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf,$mltxt[63]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[4]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");            
	pdf_continue_text($pdf,$mltxt[5]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[6]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[7]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf,$mltxt[67]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[8]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[9]);      $arYs[]=pdf_get_value($pdf,"texty", 0); 
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[10]);     $arYs[]=pdf_get_value($pdf,"texty", 0); 
  
	pdf_setfont($pdf,$bfont,$size);
  pdfUnderline($pdf,$mltxt[40],180,$arYs[0]); //???
  	
  pdf_setfont($pdf,$font,$size);
	$fmtA="fontname=georgia fontsize=10 encoding=winansi alignment=left";  
	//Those who are unreliable, miss deadlines and cannot be counted on.  
	//pdf_show_xy($pdf,$mltxt[21],180,$arYs[1]);
	$textflow = pdf_create_textflow($pdf, $mltxt[21], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[0]-10, "");

  //Those who are perfectionists, over-analyze things and focus too much on minor issues.
	$textflow = pdf_create_textflow($pdf, $mltxt[23], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[1]-30, "");

  //Those who fail to give credit to other or seldom praise good performance.
	$textflow = pdf_create_textflow($pdf, $mltxt[25], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[2]-30, "");
	
	//Those who isolate themselves, do not seek input from other or are hard to approach.
	$textflow = pdf_create_textflow($pdf, $mltxt[27], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[3]-30, "");
  
  //Those who constantly monitor and check up on the work of others.
	$textflow = pdf_create_textflow($pdf, $mltxt[29], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[4]-30, "");  
  
  //Those who are self-centered or believe they are always correct.
	$textflow = pdf_create_textflow($pdf, $mltxt[31], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[5]-30, "");  
  
  //Those who are arrogant, sarcastic and abrasive.
	$textflow = pdf_create_textflow($pdf, $mltxt[33], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[6]-30, "");    
  
  
  //Those who exploit others, take undeserved credit or cannot be trusted. 
	$textflow = pdf_create_textflow($pdf, $mltxt[35], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[7]-30, "");  
  
  //Those who lose their tempers, become angry or yell at others.
	$textflow = pdf_create_textflow($pdf, $mltxt[38], $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, 180, $arYs[8]-30, ""); 	
/*
	$size=10.0;
	pdf_setfont($pdf,$bfont,$size);
  pdfUnderline($pdf,$mltxt[40],180,470); //???
  	
  pdf_setfont($pdf,$font,$size);
	//pdf_show_xy($pdf,$mltxt[21],180,500); ???
	pdf_show_xy($pdf,"Those who are unreliable, miss deadlines and cannot be counted on.",180,440);
	//pdf_continue_text($pdf,$mltxt[22]); ???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Those who are perfectionists, over-analyze things and focus too much ");
	pdf_continue_text($pdf,"on minor issues.");
	pdf_continue_text($pdf," ");  	
//	pdf_continue_text($pdf,$mltxt[23]); //???
//	pdf_continue_text($pdf,$mltxt[24]); //???
//	pdf_continue_text($pdf,$mltxt[64]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Those who fail to give credit to other or seldom praise good");	
	pdf_continue_text($pdf,"performance.");	
//	pdf_continue_text($pdf,$mltxt[25]); //???
//	pdf_continue_text($pdf,$mltxt[26]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Those who isolate themselves, do not seek input from other or are hard");	
	pdf_continue_text($pdf,"to approach.");
//	pdf_continue_text($pdf,$mltxt[27]); //???
//	pdf_continue_text($pdf,$mltxt[28]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Those who constantly monitor and check up on the work of others.");	
//	pdf_continue_text($pdf,$mltxt[29]); //???
//	pdf_continue_text($pdf,$mltxt[30]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,"Those who are self-centered or believe they are always correct.");	
//	pdf_continue_text($pdf,$mltxt[31]); //???
//	pdf_continue_text($pdf,$mltxt[32]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,"Those who are arrogant, sarcastic and abrasive.");	
//	pdf_continue_text($pdf,$mltxt[33]); //???
//	pdf_continue_text($pdf,$mltxt[34]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,"Those who exploit others, take undeserved credit or cannot be trusted.");
	//pdf_continue_text($pdf,$mltxt[35]); //???
	//pdf_continue_text($pdf,$mltxt[36]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	//pdf_continue_text($pdf,$mltxt[37]); //???
	pdf_continue_text($pdf,$mltxt[38]); //???

	pdf_setfont($pdf,$bfont,$size);
	

	//Unreliable
	//Overly-Analytical
	//Unappreciative
	//Aloof
	//Micro-Managing
	//Self-Centered
	//Abrasive
	//Untrustworthy
	//Hostile
  pdfUnderline($pdf,$mltxt[39],70,470); //???
  
	pdf_show_xy($pdf,$mltxt[2],70,440);
	
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[3]);
	pdf_continue_text($pdf,$mltxt[63]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[4]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[5]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[6]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[7]);
	pdf_continue_text($pdf,$mltxt[67]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[8]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[9]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[10]);
	
*/	
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage8($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

  $font=pdf_load_font($pdf,"georgia","winansi","");
  $bfont=pdf_load_font($pdf,"georgiab","winansi","");

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Hot Buttons
	$title=$mltxt[81];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),620);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=($lid==1)? "(Higher numbers indicate greater frustration or irritation in response to this kind of behavior.)" : $mltxt[82];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),600);

	$rows=getSelfOnlyResponses($cid,"22","30");
	if($rows){			
		drawSelfOnlyGraph(&$pdf,50,130,500,450,$rows,$lid);	
	}
	else{			
		pdf_show_xy($pdf,"Cannot graph hot buttons",200,500);	
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

?>

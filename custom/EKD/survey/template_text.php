<?php
$tmp=array();
$tmp['title']="Center for Conflict Dynamics - Welcome to the Center for Conflict Dynamics at Eckerd College";
$tmp['css']="../custom/EKD/survey/styles_backend.css";
$tmp['topright_img']="../custom/EKD/survey/CCD-topright.png";
$tmp['support']="<h3>".$lang['login_support_header']."</h3><p>".$lang['login_support_days']."<br />".$lang['login_support_hours']."</p><p>".$lang['login_support_phone']."<br /><a href=\"".$lang['login_support_email']."\">".$lang['login_support_email']."</a></p>";             
$tmp['foot_logo']="<a href=\"https://www.eckerd.edu\"><img alt=\"Eckerd College Home\" class=\"left\" height=\"66\" src=\"../custom/EKD/survey/foot_logo.gif\" title=\"Eckerd College Home\" width=\"207\"/></a>";
$tmp['foot_contact']="4200 54th Avenue South<br />St. Petersburg, Florida 33711<br />888-359-9906 | <a href=\"mailto:cdp@eckerd.edu\">cdp@eckerd.edu</a>";
$tmp['foot_copyright']="Conflict Dynamics Profile is a registered trademark of Eckerd College";
?>
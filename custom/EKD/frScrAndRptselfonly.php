<?php
// Pulls in dbfns.php as well
require_once "../meta/multilingual.php";
require_once "../meta/pdffns.php";
//require_once "selfOnlyRptFns.php";  
// A standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);
define("MARGIN_LEFT",50);
define("MARGIN_RIGHT",565);
define("MARGIN_TOP",765);
define("MARGIN_BOTTOM",27);

// RGB values for the report
define("R_",0.85);
define("G_",0.85);
define("B_",1.0);


//----------------------------------------------------
//--- Scoring
//----------------------------------------------------

// Has the candidate responded to enough questions?
function isItComplete($cid,$pid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from RATERRESP where RID=$cid and TID=3 and VAL is not NULL");
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	// We allow 3 missing answers currently i.e. must have 96 of 99 to score
	return $row[0]>=96;
}

// get rid of all pre-existing scores for this candidate
function getRidOfOldScores($cid,$pid){
	$conn=dbConnect();
	$query="delete from SELFONLYREPORTSCORE where CID=$cid";
	mysql_query($query);
	$query="delete from SCALESCORE where RID=$cid and CID=$cid and PID=$pid";
	mysql_query($query);
}

// Compute the raw and standardized scale scores for a single self only rater
function computeSelfOnlyScaleScores($cid,$pid){
	$conn=dbConnect();
	
	// Raw score
	$query="insert into SCALESCORE (SID,RID,PID,CID,CATID,RAWSCORE,STDSCORE) ";
	$query=$query."select b.SID, a.RID, $pid, $cid, 1, AVG(VAL), 0 from RATERRESP a , SCALEITEM b, RATER c ";
	$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and a.RID=$cid and VAL is not NULL group by b.SID,a.RID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;	
	}
	
	// Standardized score
	$rows=getScaleMeanAndStd($cid,$pid);
	if(!$rows){
		return false;	
	}
	foreach($rows as $row){
		if(0!=$row[2]){
			$std=50+(10*(($row[0]-$row[1])/$row[2]));
			$query="update SCALESCORE set STDSCORE=$std where SID=$row[3] and RID=$cid and CID=$cid and PID=$pid";
			mysql_query($query);
		}
	}
	return "Calculated scale scores - OK";	
}

// function to return the appropriate Mean and Standard deviation etc for a Scale
// for self only
function getScaleMeanAndStd($cid,$pid){
	$conn=dbConnect();
	$query="select b.RAWSCORE,a.MEANVAL1,a.DEVVAL1,a.SID from SCALE a,SCALESCORE b where a.SID=b.SID and b.RID=$cid and b.CID=$cid and b.RID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// calculates the appropriate report scores for self only
function calculateSelfOnlyReportScores($cid,$pid){
	$conn=dbConnect();
	// always calculate for Self
	$query="insert into SELFONLYREPORTSCORE (SID,CID,AVGSCORE) select SID,CID,STDSCORE from SCALESCORE where RID=$cid and CID=$cid and PID=$pid and CATID=1 and STDSCORE is not NULL";
	if(false===mysql_query($query)){
		return false;
	}
	return "Calculated individual report scores - OK";
}

// checks to see if a license has been used for this candidate
function alreadyConsumedLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from SELFLIC where CONID=$conid and CID=$cid and TID=$tid");
	$row=mysql_fetch_row($rs);
	return $row[0]>0?true:false;
}

// Consumes a self-only license
function consumeSelfOnlyLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("insert into SELFLIC (CONID,CID,TID,TS) values ($conid,$cid,$tid,NOW())");
	return $rs;
}

// Score all the candidates in a program
function scoreProgram($pid){
	$conn=dbConnect();
	$msg="Scoring...";
	
	$rs=mysql_query("select CONID from PROGCONS where PID=$pid");
	$row=mysql_fetch_row($rs);
	$conid=$row[0];
	
	$rs=mysql_query("select CID,FNAME,LNAME from CANDIDATE where PID=$pid");
	$rows=dbRes2Arr($rs);
	if(false==$rows){
		return "<font color=\"#ff0000\">Unable to score program: unable to retrive participants</font>";
	}
	
	$ok=false;
	foreach($rows as $row){
		// we really don't need to score here since there's no group report
		// so let's just iterate over each candidate
		
		// Only consume a license if they are complete
		$query="select IFNULL(a.ENDDT,'Incomplete') from RATER a, CANDIDATE b,PROGRAM c where c.PID=$pid and a.CID=" . $row[0] . " and a.CID=b.CID and b.PID=c.PID";
		$rs=mysql_query($query);
		$rw=mysql_fetch_row($rs);
		if ($rw[0] != "Incomplete") {
		
			$query="select count(CID) from SELFLIC where CID=" . $row[0] . " and TID=3 and CONID=" . $_SESSION["conid"];
			$rs=mysql_query($query);
			$rw=mysql_fetch_row($rs);
			$ok=true;
		
			if ($rw[0] == 0) {
				// Only consume a license if one has not already been consumed
				$msg=$msg."<br> $row[1] $row[2]";
				$cid=$row[0];
				consumeSelfOnlyLicense($cid,$conid,3);
			}
		}
	}

	if(false==$ok){
		return "<font color=\"#ff0000\">Unable to score program: no participants</font>";
	} else {
		$msg .= '<p style="color:green;font-weight:bold;margin-bottom:0px;">The program has now been scored.<br />If you need to re-score the program you will need to re-open it first.</p>';
	}
	
	// Set status to "Scored"
	$query="update PROGRAM set EXPIRED='S' where PID=$pid";
	//echo $query."<br>";
	mysql_query($query);
	
	// Send email to consultant (added 7/6/08 by CDZ)
	sendScoreEmail($pid);
	
	return $msg."<br>";
}

//----------------------------------------------------
//------ Reporting
//----------------------------------------------------
function getCandidateName($cid){
	$conn=dbConnect();
	$query="select FNAME, LNAME from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0]." ".$row[1];	
}

function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function writeHeader($cid,&$pdf,$lid){
	$name=getCandidateName($cid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));
	
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,750,515,15);
	pdf_fill_stroke($pdf);
	// Note: (R) is hex AE, so well use sprintf to format properly
//	$what="Conflict Dynamics Profile ".sprintf("%c",0xae);
	$what=$mltxt[1];
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),754);

	return $name;	
}

function writeReportFooter(&$pdf,$name,&$page,$lid){
	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));
	
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,35,515,15);
	pdf_fill_stroke($pdf);
	$rptdt=date('D M d, Y');
	//$what=$rptdt."             This report was prepared for: ".$name."               Page ".$page;
	$what=$rptdt."             $mltxt[2] ".$name."               $mltxt[3] ".$page;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),39);	
	pdf_end_page($pdf);
	// Increment the page number
	$page+=1;
}	

function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");
	
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}
	writeReportFooter($pdf,$name,$page,"1");
}

function renderPage1($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);
	$name=ck4decode($name);
	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,600,0.2);
		
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	$title="Individual Version";  
	$title=$mltxt[11];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),525);
	$title="Feedback Report";
	$title=$mltxt[12];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),500);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="Sal Capobianco, Ph.D.               Mark Davis, Ph.D.               Linda Kraus, Ph.D.";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),450);

	// 6 inches form top of 11 inch page
	$size=14.0;
	$ypos=round((5.25*PAGE_HEIGHT)/11);
	$linehgth=1.5*$size;
	
	pdf_setfont($pdf,$font,$size);
//	$title="Prepared for:";
	$title=$mltxt[13];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos);

	pdf_show_xy($pdf,strtoupper($name),calcCenterStartPos($pdf,strtoupper($name),$font,$size),$ypos-$linehgth);

	$title=date("F j, Y");
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos-$linehgth-$linehgth);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage2($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	
	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=16.0;
	pdf_setfont($pdf,$bfont,$size);
	
	// Table of contents
	$title=$mltxt[21];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),650);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$y=650;
	$x=55;
	$x2=475;
	
//	Introduction
	pdf_show_xy($pdf,$mltxt[22],$x,$y-=30);	
	pdf_show_xy($pdf,"3",$x2,$y);

//	Guide to Your Feedback Report
	pdf_show_xy($pdf,$mltxt[23],$x,$y-=30);	
	pdf_show_xy($pdf,"4",$x2,$y);

//	Constructive Response Profile
	pdf_show_xy($pdf,$mltxt[24],$x,$y-=30);	
	pdf_show_xy($pdf,"5",$x2,$y);

//	Destructive Response Profile
	pdf_show_xy($pdf,$mltxt[25],$x,$y-=30);	
	pdf_show_xy($pdf,"6",$x2,$y);

//	Hot Buttons Profile
	pdf_show_xy($pdf,$mltxt[26],$x,$y-=30);	
	pdf_show_xy($pdf,"7",$x2,$y);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage3($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind3");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	// Introduction
	$title=$mltxt[1];
	$dY=680;
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$dY);

 $txt = "La notion de conflit �voque toute situation impliquant des personnes rencontrant une incompatibilit� d�int�r�ts ou d�objectifs, des diff�rences marqu�es de sensibilit� ou de principes personnels. Il s�agit l� d�une d�finition assez large qui englobe de nombreuses situations. Par exemple, un conflit peut r�sulter d�une s�rie de d�saccords pass�s, faire suite � une divergence d�opinion sur une strat�gie ou les m�thodes � employer pour atteindre un but professionnel, � des convictions incompatibles, ou encore � une lutte pour des ressources ou des moyens. Un conflit peut �galement surgir lorsqu�une personne est per�ue, de par ses actes, comme ind�licate, irr�fl�chie ou grossi�re. En r�sum�, un conflit peut r�sulter de toute relation susceptible de vous placer en opposition � d�autres personnes.";
	$textflow = pdf_create_textflow($pdf, $txt, "fontname=Helvetica fontsize=11 encoding=winansi alignment=justify");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY-=20, "");
  $dY-=110;

  $txt = "Ainsi, le conflit est-il une composante in�vitable de la vie en soci�t�. Malgr� tous nos efforts pour y �chapper ou y faire obstacle, nous nous trouvons invariablement, � un moment ou � un autre, en d�saccord avec autrui. Pourtant, cela n�est pas n�cessairement une mauvaise chose. Certains conflits peuvent se r�v�ler fructueux � des points de vue divergents �tant susceptibles d�amener des solutions inventives. Ce qui distingue particuli�rement le diff�rend utile du conflit destructeur tient dans la fa�on d�y r�pondre lorsqu'il se produit. De sorte que, s�il est in�vitable, certains modes de r�ponse inefficaces et n�fastes pourraient �tre �cart�s, tandis que des r�ponses efficaces et productives peuvent �tre acquises et d�velopp�es. Cette proposition est au c�ur du mod�le servant de fondement au rapport de feedback � votre disposition : le Conflict Dynamics Profile� (CDP).";
	$textflow = pdf_create_textflow($pdf, $txt, "fontname=Helvetica fontsize=11 encoding=winansi alignment=justify");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY, "");
  $dY-=120;
  
  $txt = "Certaines r�ponses � un conflit peuvent �tre consid�r�es comme des �l�ments constructifs, qu�elles interviennent aux premiers stades du conflit ou lorsque celui-ci se d�veloppe. En d�autres termes, ces r�ponses ont pour effet de ne pas aggraver le conflit davantage. De telles conduites r�duisent m�me les tensions, en permettant de centrer les ressorts du conflit sur les faits et les id�es plut�t que sur les questions de personnalit�. Les r�ponses consid�r�es comme destructives et donc n�fastes tendent, quant � elles, � rendre les choses plus difficiles. Ces conduites n�interviennent que tr�s peu dans la bonne r�solution du conflit parce qu�elles ne font qu�accentuer les questions relatives aux personnes. Si le conflit peut ais�ment �tre compar� � un incendie, alors les r�ponses constructives permettent de l��teindre, tandis que les r�ponses destructives ne font que l�attiser. Il est donc incontestablement pr�f�rable de r�pondre � un conflit par des approches constructives au lieu de s�engager dans des conduites destructives.";
	$textflow = pdf_create_textflow($pdf, $txt, "fontname=Helvetica fontsize=11 encoding=winansi alignment=justify");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY, "");
  $dY-=140;
 
  $txt = "Au-del� des aspects constructif et destructif, il est aussi possible de consid�rer d�autres variantes aux r�ponses � apporter au conflit sous la forme de conduites actives et passives. Les r�ponses actives sont celles par lesquelles une personne entreprend des actions tangibles en r�ponse � une opposition ou une provocation. De tels comportements peuvent �tre constructifs autant que destructifs. Le fait qu�ils demandent un effort personnel sensible caract�rise leur nature active. Par contraste, les r�ponses passives ne n�cessitent pas tant d�effort ou d�initiative personnelle. Leur caract�re passif tient dans le fait que la personne n�engage que peu ou pas d�action manifeste, alors que ces comportements s�expriment n�anmoins. De m�me, les r�ponses passives peuvent �tre autant constructives que destructives et ainsi contribuer � rendre les choses meilleures ou bien � les faire empirer.";
	$textflow = pdf_create_textflow($pdf, $txt, "fontname=Helvetica fontsize=11 encoding=winansi alignment=justify");
	$rs = pdf_fit_textflow($pdf, $textflow, 555, 100, MARGIN_LEFT+5, $dY, "");

  pdf_delete_textflow($pdf, $textflow);
  
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderNewPage4($cid,&$pdf,&$page,$pid,$lid){
  $name=writeHeader($cid,$pdf,$lid); 
	$flid=getFLID("meta","ind5");   //3005
	$mltxt=ck4decode(getMLText($flid,"3",$lid));    
	$font=pdf_findfont($pdf,"Helvetica","winansi",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","winansi",0);
	//$ifont=pdf_findfont($pdf,"Helvetica-Italic","host",0);	
	$size=16.0;
  $dY=680;     
	pdf_setfont($pdf,$font,$size);
  $title = "Guide to your Feedback Report";
  pdf_show_xy($pdf,$mltxt[1],calcCenterStartPos($pdf,$mltxt[1],$font,$size),$dY);

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$mltxt[2],50,$dY-=24);
  pdf_setfont($pdf,$font,$size);
  $dY-=20;   
  for($i=5; $i<=40; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],102,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }
    
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,$mltxt[45],50,$dY);
  pdf_setfont($pdf,$font,$size);  
  $dY-=24;   
  for($i=50; $i<=90; $i+=5){
    $dY=text_block($pdf,$mltxt[$i],102,11,MARGIN_LEFT,$dY);
    $dY-=11; 
  }     
/*  
  $ptext=array();
  $ptext[]="Constructive Response Profile";
  $ptext[]="Seven ways of responding to conflict that have the effect of reducing conflict which are:";
  $ptext[]="Perspective Taking - putting yourself in the other person's position and trying to understand that";
  $ptext[]="person's point of view.";
  $ptext[]="Creating Solutions - brainstorming with the other person, asking questions, and trying to create";
  $ptext[]="solutions to the problem.";
  $ptext[]="Expressing Emotions - talking honestly with the other person and expressing your thoughts and";
  $ptext[]="feelings.";
  $ptext[]="Reaching Out - reaching out to the other person, making the first move, and trying to make amends.";
  $ptext[]="Reflective Thinking - analyzing the situation, weighing the pros and cons, and thinking about the";
  $ptext[]="best response.";
  $ptext[]='Delay Responding - waiting things out, letting matters settle down, or taking a "time out" when';
  $ptext[]="emotions are running high.";
  $ptext[]="Adapting - staying flexible, and trying to make the best of the situation.";

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$ptext[0],50,640);
	pdf_continue_text($pdf," ");  
  
  pdf_setfont($pdf,$font,$size);
  pdf_continue_text($pdf,$ptext[1]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[2]);
  pdf_continue_text($pdf,$ptext[3]);
	pdf_continue_text($pdf," ");
  pdf_continue_text($pdf,$ptext[4]);
  pdf_continue_text($pdf,$ptext[5]);
	pdf_continue_text($pdf," ");
  pdf_continue_text($pdf,$ptext[6]);
  pdf_continue_text($pdf,$ptext[7]);
	pdf_continue_text($pdf," ");	
  pdf_continue_text($pdf,$ptext[8]);
	pdf_continue_text($pdf," ");
  pdf_continue_text($pdf,$ptext[9]);
  pdf_continue_text($pdf,$ptext[10]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[11]);
  pdf_continue_text($pdf,$ptext[12]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[13]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
    
	pdf_setfont($pdf,$bfont,$size); 
  $ptext=array();
  $ptext[]="Destructive Response Profile";
  $ptext[]="Eight ways of responding to conflict that have the effect of escalating conflict which are:"; 
  $ptext[]="Winning at All Costs - arguing vigorously for your own position and trying to win at all costs."; 
  $ptext[]="Displaying Anger - expressing anger, raising your voice, and using harsh, angry words.";
  $ptext[]="Demeaning Others - laughing at the other person, ridiculing the other's ideas, and using sarcasm."; 
  $ptext[]="Retaliating - obstructing the other person, retaliating against the other, and trying to get revenge."; 
  $ptext[]="Avoiding - avoiding or ignoring the other person, and acting distant and aloof."; 
  $ptext[]="Yielding - giving in to the other person in order to avoid further conflict."; 
  $ptext[]="Hiding Emotions - concealing your true emotions even though feeling upset."; 
  $ptext[]="Self-Criticizing - replaying the incident over in your mind, and criticizing yourself for not handling it"; 
  $ptext[]="better.";
    
  pdf_continue_text($pdf,$ptext[0]);
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$font,$size);
  pdf_continue_text($pdf,$ptext[1]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[2]);	
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[3]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[4]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[5]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[6]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[7]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[8]);
	pdf_continue_text($pdf," "); 
  pdf_continue_text($pdf,$ptext[9]);
  pdf_continue_text($pdf,$ptext[10]);  	  	  	  	  	  	  	  
*/                              
  writeReportFooter($pdf,$name,$page,$lid);    
}

function renderPage4($cid,&$pdf,&$page,$pid,$lid){
  $name=writeHeader($cid,$pdf,$lid);
  $font=pdf_findfont($pdf,"Helvetica","winansi",0);
	$fontb=pdf_findfont($pdf,"Helvetica-Bold","winansi",0);  
  $fmtA="fontname=Helvetica fontsize=11 encoding=winansi alignment=left";
  $fmtB="fontname=Helvetica-Bold fontsize=11 encoding=winansi alignment=left";
	
	$size=14.0;
	pdf_setfont($pdf,$fontb,$size);
  $title = "Guide d�interpr�tation du Rapport de Feedback";
  $dY=680;
  pdf_show_xy($pdf,$title,centerXcord($pdf, $title, PAGE_WIDTH/2),$dY);
  $size=10.5;
  pdf_setfont($pdf,$fontb,$size);
  $title="Profil de R�ponses Constructives";
	pdf_show_xy($pdf,$title,MARGIN_LEFT,$dY-=32);
	pdf_setfont($pdf,$font,$size);
  $txt="Les sept comportements ayant pour effet de r�duire le conflit sont :";
	pdf_show_xy($pdf,$txt,MARGIN_LEFT,$dY-=12);  
  
  $txt1="Prendre du Recul (";
  $txt2="PR";
  $txt3=") � se mettre � la place de l�autre et chercher � comprendre son point de vue";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=10;
	
	$txt1="Cr�er des Solutions (";
  $txt2="CS";
  $txt3=") � r�fl�chir avec l�autre personne et poser des questions ; tenter d�imaginer et d��laborer des solutions au probl�me";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, MARGIN_LEFT, $dY-=10, "");   
	$dY-=20;  
   
	$txt1="Exprimer ses Emotions (";
  $txt2="EE";
  $txt3=") � parler ouvertement avec l�autre personne et exprimer honn�tement ses pens�es et sentiments";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;

	$txt1="Aller vers Autrui (";
  $txt2="AA";
  $txt3=") � faire le premier pas, tendre la main ; tenter de r�parer la situation pour se racheter";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;
	
	//Analyser la Situation (AS) � prendre la mesure en pesant le pour et le contre ; faire preuve de sens critique et penser � la meilleure r�ponse � apporter
	$txt1="Analyser la Situation (";
  $txt2="AS";
  $txt3=") � prendre la mesure en pesant le pour et le contre ; faire preuve de sens critique et penser � la meilleure r�ponse � apporter";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;	
	
	//TEmporiser (TE) � laisser les choses se calmer ou se donner du temps quand la situation s�envenime ; attendre une possibilit� de conclusion
	$txt1="TEmporiser (";
  $txt2="TE";
  $txt3=") � laisser les choses se calmer ou se donner du temps quand la situation s�envenime ; attendre une possibilit� de conclusion";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA);  
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;	
	
	//Rester Flexible (RF) � rester souple et positif ; voir le bon c�t� de la situation pour en tirer le meilleur parti
	$txt1="Rester Flexible (";
  $txt2="RF";
  $txt3=") � rester souple et positif ; voir le bon c�t� de la situation pour en tirer le meilleur parti";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");      
	$dY-=20;

  //$size=11;
  pdf_setfont($pdf,$fontb,$size);
  $title="Profil de R�ponses Destructives";
	pdf_show_xy($pdf,$title,MARGIN_LEFT,$dY-=32);  
  
  pdf_setfont($pdf,$font,$size);
  $txt="Les huit comportements ayant pour effet d�envenimer le conflit sont :";
	pdf_show_xy($pdf,$txt,MARGIN_LEFT,$dY-=11);  

  $txt1="GAgner � tout prix (";
  $txt2="GA";
  $txt3=") � d�fendre vigoureusement son point de vue et tenter de l�emporter � tout prix";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=10;
	
  $txt1="Montrer sa Col�re (";
  $txt2="MC";
  $txt3=") � laisser �clater sa col�re, �lever la voix et utiliser des mots crus, durs et agressifs";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20;
	
  $txt1="D�valoriser Autrui (";
  $txt2="DA";
  $txt3=") � rire de l�autre et se montrer sarcastique ; ridiculiser ou discr�diter ses id�es ou ses positions";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20;

  $txt1="Vouloir se Venger (";
  $txt2="VV";
  $txt3=") � tenter de prendre sa revanche ; faire obstruction � l�autre ou user de repr�sailles";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20;  

  $txt1="�viter Autrui (";
  $txt2="EA";
  $txt3=") � se d�rober ou ignorer l�autre personne ; agir de mani�re distante et lointaine sans s�occuper de la situation";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20; 
	
  $txt1="C�der � Autrui (";
  $txt2="CA";
  $txt3=") � renoncer � ses positions et c�der du terrain en esp�rant �chapper � la potentialit� et au risque du conflit";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20; 
	
  //Cacher son Ressenti (CR) � retenir ses v�ritables �motions malgr� la contrari�t� et l�importance des sentiments
  $txt1="Cacher son Ressenti (";
  $txt2="CR";
  $txt3=") � retenir ses v�ritables �motions malgr� la contrari�t� et l�importance des sentiments";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20; 
	
  //Se Critiquer (SC) � rejouer l�incident dans sa t�te et se critiquer pour n�avoir pas mieux g�r� la situation
  $txt1="Se Critiquer (";
  $txt2="SC";
  $txt3=") � rejouer l�incident dans sa t�te et se critiquer pour n�avoir pas mieux g�r� la situation";
  $textflow = pdf_create_textflow($pdf, $txt1, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txt2, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txt3, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 560, 100, MARGIN_LEFT, $dY-=10, "");
	$dY-=20;   
                      
  writeReportFooter($pdf,$name,$page,$lid);   
}

function getSelfOnlyResponses($cid,$loSid,$hiSid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from SELFONLYREPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID between $loSid and $hiSid order by a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function drawSelfOnlyGraph(&$pdf,$x,$y,$width,$height,$data,$lid){
	$flid=getFLID("meta","indgraph");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=count($data);
	$ystep=round($height/9);
	$xstep=round($width/($scales+1));
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
  
  $shade=array(0,3,6);	
	// 1. Horizontal divisions
	$i;
	for($i=0;$i<8;$i++){
	  if(in_array($i,$shade)){
		 pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
     pdf_rect($pdf,($x+$xstep),$y+$ystep+round($i*$ystep),($width-$xstep),(2*$ystep));
     pdf_fill($pdf);
     pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);	  
	  }
		pdf_moveto($pdf,($x+$xstep),$y+$ystep+round($i*$ystep));
		pdf_lineto($pdf,($x+$width),$y+$ystep+round($i*$ystep));
		pdf_stroke($pdf);
	}

	// 2. the box outline 	
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);
	
	// divide the first box
	pdf_moveto($pdf,($x+$xstep),($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));
	
	// little box that juts out
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x,$y);
	pdf_lineto($pdf,$x,$y+round($ystep/2));
	pdf_lineto($pdf,$x+$xstep,$y+round($ystep/2));
	
	
	// 3. Vertical divisions
	for($i=$x+round(2*$xstep);$i<$x+$width;$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,$y+$ystep);
	}
	pdf_stroke($pdf);
	
	// 4. Vertical Scale
	pdf_setfont($pdf,$font,10.0);
  $hYs=array();
	$vs=array("30","35","40","45","50","55","60","65");
	$j=0;
	for($i=($y+$ystep);($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$hYs[]= $i;
		if(4==$j){
			pdf_setfont($pdf,$font,10.0);
			// Average
			pdf_show_xy($pdf,$mltxt[31],$x-15,$i);
			pdf_setfont($pdf,$font,10.0);
		}
		$j++;
	}
  //die(print_r($hYs));
	pdf_show_xy($pdf,"70",($x+round(2*$xstep/3)),$y+$height-2);
	pdf_setfont($pdf,$font,10.0);
	
	//"Very","Low","","","High","Very"
	//"Low","","","","","High"
	$vs=array("",$mltxt[32],$mltxt[33],"","","",$mltxt[34],$mltxt[32],"");
	$vs1=array("",$mltxt[33],"","","","","",$mltxt[34],"");
	$j=0;
	//for($i=($y+round(1.5*$ystep));($i<$y+$height);$i+=$ystep){
	 foreach($hYs as $hY){
	  switch($j){
	   case 1:
	   case 7:	   
	     $adjY = $hY+10;
	     break;
	   case 2: 
	     $adjY = $hY+25;
	     break;	 
	   case 6: 
	     $adjY = $hY-25;
	     break;
     default:
       $adjY = $hY; 
       break;       
	  }
	  
		pdf_show_xy($pdf,$vs[$j],$x-3,$adjY);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}

	// which scale do we have? we can determine from the SID
	switch($data[0][3]){
		case 1:
			//"Perspective Taking","Creating Solutions","Expressing Emotions","Reaching Out","Reflective Thinking","Delay Responding","Adapting"
			$vs=array($mltxt[1],$mltxt[2],$mltxt[3],$mltxt[4],$mltxt[5],$mltxt[6],$mltxt[7]);
			break;
		case 8:
			//"Winning","Displaying Anger","Demeaning Others","Retaliating","Avoiding","Yielding","Hiding Emotions","Self- Criticizing"
			$vs=array($mltxt[11],$mltxt[12],$mltxt[13],$mltxt[14],$mltxt[15],$mltxt[16],$mltxt[17],$mltxt[18]);
			break;
		case 22:
			//"Unrely","Over Analyze","Unapp","Aloof","Micro Manage","Self Centered","Abrasive","Untrust","Hostile"
			//$vs=array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]); //???
			$vs=($lid==1)? array("Unreliable",
                "Overly- Analytical",
                "Unapp- reciative",
                "Aloof",
                "Micro- Managing",
                "Self- Centered",
                "Abrasive",
                "Untrust- worthy",
                "Hostile") : array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]);
			break;
	}
	
	// 5. Draw the scale
	if($data[0][3]==22) 	pdf_setfont($pdf,$font,9.0);
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$offset=round(0.30*$ystep)+2;	
    if(($vs[$i] != "Reaching Out")&&(count(explode(" ",$vs[$i])) > 1)){
      $offset=round(0.40*$ystep)+2;
    }
		pdf_setfont($pdf,$font,9.0);
		pdf_show_boxed($pdf,$vs[$i],$j,($y+round($ystep/2)),$xstep,$offset,"center","");
		$j+=$xstep;
	}
	pdf_setfont($pdf,$font,10.0);
	
	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,$y,$xstep,round(0.35*$ystep),"center","");
		$j+=$xstep;
	}

	// Value
	pdf_show_xy($pdf,$mltxt[35],$x+5,$y+5);
	
	// 7. Draw the graph
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val<0){
		$clip2=true;
		$val=0;
		// 01-25-2005: Fixing a border condition
		$n1=0;
	}
	elseif($val>40){
		$clip2=true;
		$val=40;
		// 01-25-2005: Fixing a border condition
		$n1=40;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=30;
		if($val<0){
			$clip2=true;
			$val=0;
			$n1=0;
		}
		elseif($val>40){
			$clip2=true;
			$val=40.5;
			$n1=40.5;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+$ystep+($n1*($ystep/5)));
		}
		
		$yc=$y+$ystep+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_moveto($pdf,$x+5,$y+round($ystep/3));
	pdf_lineto($pdf,$x+$xstep-5,$y+round($ystep/3));
	pdf_stroke($pdf);	

	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);

	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=30;
	if($val>=0&&$val<=40){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=30;
		if($val>=0&&$val<=40){
			$yc=$y+$ystep+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	/*	*/
	pdf_circle($pdf,$x+round($xstep/2),$y+round($ystep/3),4);
	pdf_fill_stroke($pdf);
}

function renderPage5($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Constructive Responses
	$title="R�ponses Constructives";  //$mltxt[51];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	Higher numbers are more desirable
	$title="- des valeurs plus �lev�es sont pr�f�rables -"; 
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getSelfOnlyResponses($cid,"1","7");
	if($rows){
		drawSelfOnlyGraph($pdf,50,150,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage6($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//  Destructive Responses
	$title=$mltxt[61];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title="- des valeurs moins �lev�es sont pr�f�rables -";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getSelfOnlyResponses($cid,"8","15");
	if($rows){
		drawSelfOnlyGraph($pdf,50,150,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage7($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind7");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
  $fmtA="fontname=Helvetica fontsize=11 encoding=winansi alignment=justify";
  $fmtB="fontname=Helvetica-Bold fontsize=11 encoding=winansi alignment=justify";
  		
	$size=16.0;
	pdf_setfont($pdf,$bfont,$size);
	$title="Profil des D�clencheurs � Hot Buttons";
	pdf_show_xy($pdf,$title,centerXcord($pdf, $title, PAGE_WIDTH/2),680);
	
	  $dY=670;	
  $txtA = "Cette partie du Rapport du ";
  $txtB = "Conflict Dynamics Profile";
  $txtC = "� est sensiblement diff�rente des deux pr�c�dentes. Au lieu d�indiquer votre mani�re particuli�re de r�agir aux situations de conflit, cette section apporte un �clairage sp�cifique quant aux types de personnes ou de situations susceptibles de vous contrarier et de causer un conflit potentiel � autrement dit, vos points sensibles (ou Hot Buttons) sont identifi�s ici comme facteurs d�clencheurs.";
  $textflow = pdf_create_textflow($pdf, $txtA, $fmtA); 
  pdf_add_textflow($pdf,$textflow,$txtB, $fmtB);
  pdf_add_textflow($pdf,$textflow,$txtC, $fmtA); 
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, MARGIN_LEFT, $dY-=10, "");   
	$dY-=55;
	
  $txt = "Vous trouverez ci-dessous une premi�re description des d�clencheurs de conflit mesur�s par le CDP-I et un graphe illustrant votre degr� d�irritabilit� selon les personnes ou les situations. �videmment, ces facteurs ne recouvrent pas l�ensemble de tout ce qui est susceptible d�affecter les personnes ; par contre, ils recouvrent les plus courants. Dans chaque cas, un score particuli�rement �lev� indique votre degr� d�irritabilit� ou de contrari�t� le plus sensible, avec un potentiel de conflit accru pour cette situation, comparativement � la moyenne des r�ponses au questionnaire CDP-I.";
  $textflow = pdf_create_textflow($pdf, $txt, $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, MARGIN_LEFT, $dY-=10, "");

	$size=11.0;
	$dY-=(8*$size);
	pdf_setfont($pdf,$font,$size);  
  $txt="Les neuf facteurs d�clencheurs de conflit (comportement d�autrui ou situation critique) sont :";
	pdf_show_xy($pdf,$txt,MARGIN_LEFT,$dY);    
    
  

  $sY=$dY;
  $sY-=($size*2)-10;
  $sY2=$sY;
	$size=10.0;


	pdf_setfont($pdf,$bfont,$size);
//  pdfUnderline($pdf,$mltxt[40],180,$sY); //???
  $sY-=($size);  	
  pdf_setfont($pdf,$font,$size);
	//pdf_show_xy($pdf,$mltxt[21],180,500); ???
	pdf_show_xy($pdf,"qui ne respecte pas ses engagements (r�alisations, d�lais) et sur",180,$sY);
	pdf_continue_text($pdf,"qui on ne peut compter faute d�implication et de r�gularit�");
	//pdf_continue_text($pdf,$mltxt[22]); ???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	//pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui sur-analyse les situations et montre un souci exag�r� du");
	pdf_continue_text($pdf,"d�tail ou qui se concentre sur des d�tails mineurs");
pdf_continue_text($pdf," ");  	
//	pdf_continue_text($pdf,$mltxt[23]); //???
//	pdf_continue_text($pdf,$mltxt[24]); //???
//	pdf_continue_text($pdf,$mltxt[64]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui ne reconna�t pas le m�rite des autres ou qui en appr�cie rarement les r�sultats");	
	pdf_continue_text($pdf,"� leur juste valeur");	
//	pdf_continue_text($pdf,$mltxt[25]); //???
//	pdf_continue_text($pdf,$mltxt[26]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui s'isole ou qui est difficile d'acc�s, qui ne recherche pas ou n'int�gre pas");	
	pdf_continue_text($pdf,"l'avis des autres");
//	pdf_continue_text($pdf,$mltxt[27]); //???
//	pdf_continue_text($pdf,$mltxt[28]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui surveille et v�rifie constamment le travail des autres (usage excessif du");	
	pdf_continue_text($pdf,"contr�le, des proc�dures ou du reporting)");
//	pdf_continue_text($pdf,$mltxt[29]); //???
//	pdf_continue_text($pdf,$mltxt[30]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui est centr�/e sur lui/elle-m�me, qui per�oit le monde � travers son propre point");	
	pdf_continue_text($pdf,"de vue ou qui croit avoir toujours raison");
//	pdf_continue_text($pdf,$mltxt[31]); //???
//	pdf_continue_text($pdf,$mltxt[32]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui est arrogant/e, mordant/e, sarcastique et irritant/e, autoritaire et inflexible");	
//	pdf_continue_text($pdf,$mltxt[33]); //???
//	pdf_continue_text($pdf,$mltxt[34]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");	
	pdf_continue_text($pdf,"qui exploite les autres et qui s'attribue leur m�rite, qui manque d'honn�tet� et");	
	pdf_continue_text($pdf,"qui peut mentir voire trahir");
	//pdf_continue_text($pdf,$mltxt[35]); //???
	//pdf_continue_text($pdf,$mltxt[36]); //???
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"qui perd son sang-froid, se met en col�re et crie apr�s les autres ou qui se");	
	pdf_continue_text($pdf,"montre inamical/e ou brutal/e");	
	//pdf_continue_text($pdf,$mltxt[37]); //???
	//pdf_continue_text($pdf,$mltxt[38]); //???

	pdf_setfont($pdf,$bfont,$size);
	

	//Unreliable
	//Overly-Analytical
	//Unappreciative
	//Aloof
	//Micro-Managing
	//Self-Centered
	//Abrasive
	//Untrustworthy
	//Hostile
//  pdfUnderline($pdf,$mltxt[39],70,530); //???
  
	pdf_show_xy($pdf,"Non Fiable",70,$sY2-$size);
	
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Perfectionniste");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Insensible");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Solitaire");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Micro Manager");
	pdf_continue_text($pdf,"(Sur Contr�le)");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Egocentrique");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Cassant");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Indigne de");
	pdf_continue_text($pdf,"Confiance");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Violent");
	pdf_continue_text($pdf," ");      
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage8($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=ck4decode(getMLText($flid,"3",$lid));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Hot Buttons
	$title=$mltxt[81];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title = "- pour les comportements pr�sent�s, des valeurs plus �lev�es sont indicatrices de plus grande frustration ou d�irritation accrue, suscitant les r�ponses d�crites par les graphes pages 5 et 6 -"; 
	$fmtA="fontname=Helvetica fontsize=11 encoding=winansi alignment=center";
	
  $textflow = pdf_create_textflow($pdf, $title, $fmtA);
	$rs = pdf_fit_textflow($pdf, $textflow, 550, 100, MARGIN_LEFT, 670, "");
	
	$rows=getSelfOnlyResponses($cid,"22","30");
	if($rows){			
		drawSelfOnlyGraph(&$pdf,50,150,500,450,$rows,$lid);	
	}
	else{			
		pdf_show_xy($pdf,"Cannot graph hot buttons",200,500);	
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

?>
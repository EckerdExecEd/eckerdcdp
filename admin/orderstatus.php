<?php
require_once "../meta/consultant.php";
require_once "admfn.php";
require_once "../meta/table.class.php";
require_once "../meta/orders.class.php";
require_once "../meta/orderfns.php";
require_once "../meta/formfns.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
$msg=false;
$headertext="<script type=\"text/javascript\" src=\"../meta/jquery/jquery.js\"></script>\n
<script type=\"text/javascript\" src=\"../script/js/orderstatus.js\"></script>\n
<link rel=\"stylesheet\" type=\"text/css\" href=\"../script/js/orderrpts.css\" />\n";

if((isset($_GET['status']))&&($_GET['status']!="")){
     $ordid=substr($_REQUEST['status'],1);
}
//foreach($_POST as $k=>$v){ echo "$k => $v<br>"; }  
if((isset($_POST['what']))&&($_POST['what']=='ADD')){

  $addOrdid=$_POST['ordid'];
  $status=$_POST['ordStatus'];
  $statusTS=$_POST['statusdt'];
  $sqlTS=isValidDate($statusTS);
  if($sqlTS){
  echo $sqlTS."<br>";
   insertOrderStatus($ordid,$status,$sqlTS);
   //Mail Consultant that their order has been confirmed
   $statusCk=isOrderConfirmed($ordid,$conid);
   if($statusCk){
      //SEND Confirmation email
      $body="\n\nOrder #$ordid has been confirmed and you can now download 
             the development guides from links on the order detail.\n\nThank you for your order.\n\n";
     sendGenericMail($statusCk[0]['EMAIL'],
                      "cdp@eckerd.edu",
                      //"No-Reply@onlinecdp.org",
                      "Order Confirmed",
                      nl2br($body),
                      "Valued Client",
                      "1",
                      array(),
                      "MIME-Version: 1.0\n"."Content-type: text/html; charset=utf-8\n"); 
    /* */
   }    
  }else{
    $msgX="<span style=\"color:red;\">Please make sure you enter the correct Date/Time format<br><b>mm/dd/yyyy hh:mmAM</b></span><br>";
  }
}

$msg="Select from the following options";
writeHead("Conflict Dynamics Profile - Admin",false,$headertext);
writeBody("Order Status Form",$msg);
$path=explode("/",$_SERVER['HTTP_REFERER']);
$ordDetail="orderssearch.php?detail=D".$ordid;


$urls=array('orderssearch.php',
            "$ordDetail",
            'home.php');
$txts=array('Search Orders',
            'Order Detail',
            'Back');
menu($urls,$txts,"");
$statusTable=renderOrderDetails($ordid,false,"E");
$statSelect=getAvailableOrderStatus($ordid,'ordStatus');

echo "<center><div style=\"position:relative;width:600px;top:-100px;text-align:center;\"><h4>Status Management for Order # $ordid</h4>
      $msgX<form method=\"post\" action=\"orderstatus.php?status=O".$ordid."\" id=\"statusfrm\" name=\"statusfrm\"> 
      $statusTable<br>";
  $now=date("m/d/Y h:iA");
  //isValidDate($now);
  $tbl = new table("auto","","orders");
  $tbl->addRow("","", "head");
  $tbl->rows[0]->addCell("ADD NEW STATUS","h","","","","2");
  $tbl->addRow("","");
  if(!$statSelect)
    $tbl->rows[1]->addCell("No Status Types Available","d","ct","","","2");
  else{
    $tbl->rows[1]->addCell("STATUS:","d","rt");
    $tbl->rows[1]->addCell($statSelect);
    $tbl->addRow("","");
    $tbl->rows[2]->addCell("STATUS DATE/TIME","d","rt");
    $tbl->rows[2]->addCell("<input type=\"text\" id=\"statusdt\" name=\"statusdt\" value=\"$now\" size=\"18\" maxlength=\"19\" /> (Server is on Central Time)");
    $tbl->addRow("", "");
    $tbl->rows[3]->addCell("<input type=\"button\" id=\"addStat\" name=\"addStat\" value=\"Submit\" />","d","ct","","","2");
  }
  $table=$tbl->draw(true);   

echo "$table<input type=\"hidden\" id=\"ordid\" name=\"ordid\" value=\"$ordid\" />
            <input type=\"hidden\" id=\"what\" name=\"what\" value=\"\" /></form></div></center>";
  
writeFooter(false);

function isValidDate($datevar){
    
    $dateOK=true;
    $datestr=substr($datevar,0,10);
    $dts=explode("/",$datestr);
    $dcss="darkgreen";
    if((count($dts)!=3)||(!validateDate($datestr,'MM/DD/YYYY'))){
      $dateOK=false;
      $dcss="red";
    }
    
    $time=substr($datevar,11);
    $tcss="darkgreen";
    preg_match('/^(0[1-9]|1[0-2]):(0[1-9]|[1-5][0-9])(AM|PM|pm|am)$/', $time, $matches);
    if(!$matches){
      $dateOK=false;
      $tcss="red";
    }  
    //echo "<span style=\"color:$dcss;\">date = $datestr</span><br><span style=\"color:$tcss;\">time = $time</span><br>";
    if($dateOK){
      $pm=stristr($time,'p');
      $times=preg_split('/:/',substr($time,0,-2));
      $h = (int)$times[0];
      if($h==12){
        $h=($pm===false)? 0 : 12;
      }else{
        $h=($pm===false)? $h : $h + 12;
      }
      $m= (int)$times[1];
      $dateOK=date("Y-m-d H:i", mktime($h, $m, 0, $dts[0], $dts[1], $dts[2]));
      //$dateOK= $dts[2].'-'.$dts[0].'-'.$dts[1].' '.$time;
    } 
    return $dateOK;
}
?>

<?php
/**
 * Variables are used to manage access to ECKERD's CDP site. The boolean values 
 * below switch login inputs on(true) or off(false) and replaces them with a 
 * system message.  JPC - 11/18/2008 
 **/ 

/**
 * Message to be shown instead of login inputs
 **/    
$systemMSG="<div style=\"color:green;\">The Online CDP System is currently 
            unavailable due to maintenance<br>
            please check back later.</div>";
//ECKERD Logins ================================================================ 
/**
 * Variable used to determine if Login's should be available accross all logins
 * https://www.onlinecdp.org/index.php                //Consultant login
 * https://www.onlinecdp.org/cons/index.php           //Consultant login
 * https://www.onlinecdp.org/usr/index.php            //Candidate 360 login
 * https://www.onlinecdp.org/assess/index.php         //Rater 360 login
 * https://www.onlinecdp.org/assess/survey.php        //Individual login 
 **/ 
$systemON=true;
//$systemON=false;
/**
 * Variable used to determine if Login should be available for EKD Admin
 * https://www.onlinecdp.org/admin/index.php 
 **/ 
$sysAdminON=true;
//$sysAdminON=false;

//OLIVER WYMAN Logins ==========================================================
/**
 * Variable used to determine if Login's should be available accross all logins
 * https://www.onlinecdp.org/ow/admin/index.php       //EKD Admin login
 * https://www.onlinecdp.org/ow/md/mdlogin.php        //OWD Admin login 
 * https://www.onlinecdp.org/ow/md/index.php          //OWD Rater 360 login
 **/
$owdSystemON=true; 
//$owdSystemON=false; 

?>

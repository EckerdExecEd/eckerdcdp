<?php
require_once "../meta/consultant.php";
require_once "admfn.php";
require_once "../meta/orders.class.php";
require_once "../meta/table.class.php";
session_start();
setlocale(LC_MONETARY, 'en_US');

$headertext="<link rel=\"stylesheet\" type=\"text/css\" href=\"../script/js/orderrpts.css\" />\n";

if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="Select from the following options";
writeHead("Conflict Dynamics Profile - Admin",false,$headertext);
writeBody("Order Management",$msg);
$urls=array('orderssearch.php',
            'ordersmain.php',
            'bannerreporting.php',
            'home.php');
$txts=array('Search Orders',
            'Order Reporting',
            'Banner Reporting',
            'Back');

$top=count($urls) * 33;
menu($urls,$txts,"");
$today=date("m/d/Y");
$params=array();
$params[]='bdate^'.$today;
$HTML=($srchResult=renderSearchResult($params))?  "<h4>Search Results</h4>".$srchResult : "<h4>No results found</h4>";
echo "<center><div style=\"position:relative;width:600px;top:-".$top."px;text-align:center;\"><h3>Orders for $today</h3>
         $HTML
      </div></center>";

writeFooter(false);
?>
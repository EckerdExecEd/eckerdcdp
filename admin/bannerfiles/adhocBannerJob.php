<?php

require_once "../../meta/dbfns.php";

$ordIDs=false;
die('Testing');
/* STEP 1 - Query all qualified order IDs for reporting */
$orders=getOrderMasterRecs($ordIDs);
if(count($orders)>0){
  $log.=count($orders)." orders found for Banner Reporting\n";
  $rpt=array();
  /* STEP 2 - Retrieve the order details and loop through each*/
  foreach($orders as $order){
    $details=getOrderDetailRecs($order['ORDID']);
    if(count($details)>0) $rptOrders[]=$order['ORDID'];
    foreach($details as $d){
      $rpt[$x]=array();
      $rpt[$x]['TransDate']=$order['COMPLETETS'];
      
      $detailCode=getDetailCode($d['PRDKEY'],$order['TAXSTATE'],$d['TAXED']);
      $rpt[$x]['DetailCode']=$detailCode;
      
      $amount=($d['TAXED']=='Y')? ((($order['TAXRATE']/100)+1) * ($d['QTY'] * $d['PRICE'])) : ($d['QTY'] * $d['PRICE']); 
      $amount=number_format($amount, 2, '.', '');
      $rpt[$x]['Amount']="$amount";

      $rpt[$x]['InvoiceNumber']="CDP".str_pad($order['ORDID'],5,'0',STR_PAD_LEFT);
      
      $rto=array(" ","-");
      $rfrom=array(","," - ");      
      $rpt[$x]['Description']=str_replace($rfrom,$rto,$d['PRDNAME']);
      $x++;      
    }
    //Add shipping & handling
    $SandH=number_format(($order['SHIPPING']+$order['HANDLING']), 2, '.', '');
    if($SandH>0){
      $rpt[$x]['TransDate']=$order['COMPLETETS'];
      $rpt[$x]['DetailCode']='CDPS';
      $rpt[$x]['Amount']=$SandH;
      $rpt[$x]['InvoiceNumber']="CDP".str_pad($order['ORDID'],5,'0',STR_PAD_LEFT);
      $rpt[$x]['Description']='Shipping & Handling';
      $x++;
    }      
  }
  $data="TransDate,DetailCode,Amount,InvoiceNumber,Description\n";
  $total=0;
  foreach($rpt as $line){
    $total+=$line['Amount'];
    $data.=implode(",",$line)."\n";
  } 
  $log.= rtrim($data);
  echo nl2br($data);
}
//==============================================================================
// Functions
//==============================================================================

function getOrderMasterRecs($ordids){
  $sql="SELECT a.ORDID, b.LASTSTATUS, a.SUBTOTAL , a.TAXES , a.TAXRATE , a.TAXSTATE, a.TAXEIN, a.SHIPPING , a.HANDLING , a.TOTAL , 
               DATE_FORMAT(a.ORDERTS,'%m/%d/%Y') AS ORDERTS , PAYID , APPROV , 
               IFNULL(DATE_FORMAT(a.ORDERTS,'%m/%d/%Y'),DATE_FORMAT(a.COMPLETETS,'%m/%d/%Y')) AS COMPLETETS,
               DATE_FORMAT(a.BANNERUPLOAD,'%m/%d/%Y') AS BANNERUPLOAD,
               DATE_FORMAT(b.STATUSTS,'%m/%d/%Y') AS STATUSTS  
          FROM ORDERMASTER a, ORDERINFO b
         WHERE a.ORDID = b.ORDID ";
  if($ordids){
    $sql.="AND a.ORDID IN ( $ordids ) 
           AND a.BANNERUPLOAD IS NULL ";   
  }else{
    $sql.="AND b.LASTSTATUS NOT IN ('FAILED','CANCELLED')
           AND a.BANNERUPLOAD IS NULL "; 
  } 
  $sql.="ORDER BY ORDID";
  //die($sql);
  $res=fetchArray($sql);
  return $res;
}

function getOrderDetailRecs($ordid){
  $sql="SELECT a.DETAILID, a.ORDID, a.PRDKEY, b.PRDID, b.PRDNAME, b.WEIGHT, b.TAXABLE, a.TAXED, a.QTY, a.PRICE, b.LICREQ, b.INSTR
          FROM ORDERDETAIL a, CDPPRODUCTS b
         WHERE a.PRDKEY = b.PRDKEY
           AND a.ORDID = $ordid
         ORDER BY a.SEQUENCE ASC";
  $res=fetchArray($sql);
  return $res;         
}

function getDetailCode($prdkey,$state,$taxed){
  $taxableState=array('FL','OH');
  if(in_array($state,$taxableState)){
    $stateCK =($taxed=='Y')? "= '$state' " : "IS NULL ";  
  }else{
    $taxed = 'N';
    $stateCK = "IS NULL ";
  } 

  $sql="SELECT DETAIL 
          FROM ORDERDETAILCODES
         WHERE PRDKEY = $prdkey
           AND STATE $stateCK
           AND TAXED = '$taxed'";
  
  $dCode=fetchOne($sql);
  if($dCode=="") die($sql);
  //die('dCode = '.$dCode);
  return $dCode;
}

?>
<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

require_once "../../meta/dbfns.php";

$ok=true;

$filename = "CDP_Banner_Rpt_".date('d-M-Y').".csv";
$log="Creating the $filename\n";
   
// STEP 1 - Query all qualified order IDs for reporting 
$orders=getOrderMasterRecs(false);
$rptOrders=array();

if(count($orders)>0){
  $log.=count($orders)." orders found for Banner Reporting\n";
  $rpt=array();
  // STEP 2 - Retrieve the order details and loop through each
  $x=0;
  foreach($orders as $order){
    $details=getOrderDetailRecs($order['ORDID']);
    if(count($details)>0) $rptOrders[]=$order['ORDID'];
    foreach($details as $d){
      $rpt[$x]=array();
      $rpt[$x]['TransDate']=$order['COMPLETETS'];
      
      $detailCode=getDetailCode($d['PRDKEY'],$order['TAXSTATE'],$d['TAXED']);
      $rpt[$x]['DetailCode']=$detailCode;
      
      $amount=($d['TAXED']=='Y')? ((($order['TAXRATE']/100)+1) * ($d['QTY'] * $d['PRICE'])) : ($d['QTY'] * $d['PRICE']); 
      $amount=number_format($amount, 2, '.', '');
      $rpt[$x]['Amount']="$amount";
      
      $rpt[$x]['InvoiceNumber']="CDP".str_pad($order['ORDID'],5,'0',STR_PAD_LEFT);

      $rto=array(" ","-");
      $rfrom=array(","," - ");     
      $rpt[$x]['Description']=str_replace($rfrom,$rto,$d['PRDNAME']);
      $x++;      
    }
    //Add shipping & handling
    $SandH=number_format(($order['SHIPPING']+$order['HANDLING']), 2, '.', '');
    if($SandH>0){
      $rpt[$x]['TransDate']=$order['COMPLETETS'];
      $rpt[$x]['DetailCode']='CDPS';
      $rpt[$x]['Amount']=$SandH;
      $rpt[$x]['InvoiceNumber']="CDP".str_pad($order['ORDID'],5,'0',STR_PAD_LEFT);
      $rpt[$x]['Description']='Shipping & Handling';
      $x++;
    }      
  }
  $data="TransDate,DetailCode,Amount,InvoiceNumber,Description\n";
  $total=0;
  foreach($rpt as $line){
    $total+=$line['Amount'];
    $data.=implode(",",$line)."\n";
  } 
  $log.= rtrim($data);
   
  // Write Data to file 
  if($fp = fopen($filename, "w+")){
    fwrite($fp, rtrim($data));
    fclose($fp);
    $log.="File created \n";
    $file=true;
  }else{
    $log.="Unable to create $filename \n";
    $file=false;
    $ok=false;
  }
  
  // Email file to appropriate personnel 
  if($file){
    $mailed=email_attachment('michaldm@eckerd.edu,pheilrl@eckerd.edu,timtoohill@invictaanalytics.com', 
                   'Banner reporting of CDP online orders for '.date('d-M-Y'), 
                   'Banner reporting '.date('d-M-Y'),
                   'Online CDP Cron', 
                   'doNotReply@eckerd.edu', 
                   $filename, 
                   'application/csv');
    
    if(!$mailed){
      $log.="Failed to email ".$filename."\n";
      $ok=false;
    }else{
      $log.="Mailed csv for upload to Banner\n";
    }               
  
      if($ok){
        // Update ORDERMASTER-BANNERUPLOAD 
        $orderIDs=implode(",",$rptOrders);
        if(updateBannerUpload($orderIDs))
          $log.="ORDERMASTER column BANNERUPLOAD updated for orders $orderIDs\n";
        else
          $log.="Failed to update ORDERMASTER column BANNERUPLOAD for orders $orderIDs\n";
      }
  
  }                   
  
}else{
  $log.="No outstanding orders found as of ".date('d-M-Y h:i:s a')."\n";  
  email_attachment('timtoohill@invictaanalytics.com',
                   'TEST for '.date('d-M-Y'),
                   'Banner reporting '.date('d-M-Y'),
                   'Online CDP Cron',
                   'doNotReply@onlinecdp.org',
                   $filename,
                   'application/csv');

}

echo $log;

//die($log);
// Email log for support  
$headers = 'From: doNotReply@onlinecdp.org' . "\r\n" .
           'Reply-To: doNotReply@onlinecdp.org' . "\r\n" .
           'X-Mailer: PHP/' . phpversion();

//mail('pcheely@thediscoverytec.com','Banner Reporting '.date('d-M-Y'),$log,$headers);

//==============================================================================
// Functions
//==============================================================================

function getOrderMasterRecs($ordids){
  $sql="SELECT a.ORDID, b.LASTSTATUS, a.SUBTOTAL , a.TAXES , a.TAXRATE , a.TAXSTATE, a.TAXEIN, a.SHIPPING , a.HANDLING , a.TOTAL , 
               DATE_FORMAT(a.ORDERTS,'%m/%d/%Y') AS ORDERTS , PAYID , APPROV , 
               DATE_FORMAT(a.COMPLETETS,'%m/%d/%Y') AS COMPLETETS,
               DATE_FORMAT(a.BANNERUPLOAD,'%m/%d/%Y') AS BANNERUPLOAD,
               DATE_FORMAT(b.STATUSTS,'%m/%d/%Y') AS STATUSTS  
          FROM ORDERMASTER a, ORDERINFO b
         WHERE a.ORDID = b.ORDID ";
  if($ordids){
    $sql.="AND a.ORDID IN ( $ordids ) 
           AND a.BANNERUPLOAD IS NULL ";   
  }else{
    $sql.="AND b.LASTSTATUS NOT IN ('FAILED','CANCELLED')
           AND a.PAYID IS NOT NULL
           AND a.BANNERUPLOAD IS NULL "; 
  } 
  $sql.="ORDER BY ORDID";
  //die($sql);
  $res=fetchArray($sql);
  return $res;
}

function getOrderDetailRecs($ordid){
  $sql="SELECT a.DETAILID, a.ORDID, a.PRDKEY, b.PRDID, b.PRDNAME, b.WEIGHT, b.TAXABLE, a.TAXED, a.QTY, a.PRICE, b.LICREQ, b.INSTR
          FROM ORDERDETAIL a, CDPPRODUCTS b
         WHERE a.PRDKEY = b.PRDKEY
           AND a.ORDID = $ordid
         ORDER BY a.SEQUENCE ASC";
  $res=fetchArray($sql);
  return $res;         
}

function updateBannerUpload($ordIDs=fales){
   if($ordIDs){
      $sql="UPDATE ORDERMASTER SET BANNERUPLOAD = now()
             WHERE ORDID IN ($ordIDs) AND BANNERUPLOAD IS NULL";
      //die($sql);
      if($affected=executeQurey($sql,true))
        return true;
      else
        return false;       
   }
  return true;
}

function getDetailCode($prdkey,$state,$taxed){
  $taxableState=array('FL');
  if(in_array($state,$taxableState)){
    $stateCK =($taxed=='Y')? "= '$state' " : "IS NULL ";  
  }else{
    $taxed = 'N';
    $stateCK = "IS NULL ";
  } 

  $sql="SELECT DETAIL 
          FROM ORDERDETAILCODES
         WHERE PRDKEY = $prdkey
           AND STATE $stateCK
           AND TAXED = '$taxed'";
  
  $dCode=fetchOne($sql);
  if($dCode=="") die($sql);
  //die('dCode = '.$dCode);
  return $dCode;
}

function email_attachment($to_email, $email, $subject,$our_email_name, $our_email, $file_location, $default_filetype='application/zip'){

    $email = '<font face="arial">' . $email . '</font>';
    $fileatt = $file_location;
    if(function_exists(mime_content_type)){
        $fileatttype = mime_content_type($file_location);
    }else{
        $fileatttype = $default_filetype;;
    }
    $fileattname = basename($file_location);
    //prepare attachment
    $file = fopen( $fileatt, 'rb' );
    $data = fread( $file, filesize( $fileatt ) );
    fclose( $file );
    $data = chunk_split( base64_encode( $data ) );
    //create mime boundary
    $semi_rand = md5( time() );
    $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
    //create email  section
    $message = "This is a multi-part message in MIME format.\n\n" .
    "--{$mime_boundary}\n" .
    "Content-type: text/html; charset=us-ascii\n" .
    "Content-Transfer-Encoding: 7bit\n\n" .
    $email . "\n\n";
     //create attachment section
    $message .= "--{$mime_boundary}\n" .
     "Content-Type: {$fileatttype};\n" .
     " name=\"{$fileattname}\"\n" .
     "Content-Disposition: attachment;\n" .
     " filename=\"{$fileattname}\"\n" .
     "Content-Transfer-Encoding: base64\n\n" .
     $data . "\n\n" .
     "--{$mime_boundary}--\n";
     //headers
    $exp=explode('@', $our_email);
    $domain = $exp[1];
    $headers = "From: $our_email_name<$our_email>" . "\n";
    $headers .= "Reply-To: $our_email"."\n";
    $headers .= "Return-Path: $our_email" . "\n";    // these two to set reply address
    $headers .= "Message-ID: <".time()."@" . $domain . ">"."\n";
    $headers .= "X-Mailer: Edmonds Commerce Email Attachment Function"."\n";          // These two to help avoid spam-filters
    $headers .= "Date: ".date("r")."\n";
    $headers .= "MIME-Version: 1.0\n" .
                    "Content-Type: multipart/mixed;\n" .
                    " boundary=\"{$mime_boundary}\"";
    //echo "We are trying to mail now<br>";
    
    if(!mail($to_email,$subject,$message, $headers, '-f ' . $our_email))
      return false;
    else
      return true;
}
/**/
?>

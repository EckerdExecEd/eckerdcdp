<?php
require_once "../../meta/dbfns.php";
$to = "pat.cheely@gmail.com";
$subj = "Testing the Cron";
$msg = "This is a test to see what it takes to run a php file from the crontab on ".date("d-M-Y g:i A");
$hdr = 'From: system@onlinecdp.org' . "\r\n" .
           'Reply-To: From: system@onlinecdp.org' . "\r\n" .
           'X-Mailer: PHP/' . phpversion();
mail($to, $subj, $msg, $hdr);           
?>
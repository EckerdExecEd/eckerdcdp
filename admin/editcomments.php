<?php 
require_once "../meta/licfns.php";
require_once "../meta/program.php";
require_once "admfn.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="";
$what=$_POST['what'];
$pid=$_POST['pid'];

if($what=="save"){
	// If we're here we are about to save comments
	$rid=array();
	$itemid=array();
	$val=array();
	$count=$_POST['count'];
	for($i=0;$i<$count;$i++){
		$r=$_POST["rid$i"];
		$it=$_POST["itemid$i"];
		$v=addslashes($_POST["val$i"]);
		$rid[$i]=$r;
		$itemid[$i]=$it;
		$val[$i]=$v;
	}
	saveAllComments($pid,$rid,$itemid,$val);	
}

writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Edit Rater Comments",$msg);
$narrow=$_POST['narrow'];
$urls=array('home.php');
$txts=array('Back');
//menu($urls,$txts,"");
?>
<form name="listfrm" action="editcomments.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="exp" value="Y">
<input type="hidden" name="pid" value="<?=$pid?>">
<table border=1 cellpadding=5>

<?php
    if(!listAllProgramsWithComments("listfrm")){
		echo "<font color='#aa0000'>No pending programs</font></br>";
    }
    if($what=="find"){
		listAllComments($pid,"listfrm");	
    }
?>
</table>
</form>
<?php
menu($urls,$txts,"");
writeFooter(false);
?>


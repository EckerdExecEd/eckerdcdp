<?php
require_once "../meta/consultant.php";
require_once "admfn.php";
require_once "../meta/orders.class.php";
require_once "../meta/table.class.php";
session_start();
setlocale(LC_MONETARY, 'en_US');

$headertext="<script type=\"text/javascript\" src=\"../meta/jquery/jquery.js\"></script>\n
<script type=\"text/javascript\" src=\"../script/js/bannercsv.js\"></script>\n
<link rel=\"stylesheet\" type=\"text/css\" href=\"../script/js/orderrpts.css\" />\n";

if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
$msgX="";
if(isset($_POST['what'])&&($_POST['what']=='s')){
  $ckLines=false;// 'HTML'; 
  $msgX.=createCSV($_POST['sOrderIds'],$ckLines);
  if(!$ckLines){
    $filename = "CDP_order_reporting".date('d-M-Y_His').".csv";  
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"$filename\""); 
    header("refresh:2;url=bannerreporting.php"); 
    echo rtrim($msgX);
    die();
  }
}

$msg="Select from the following options";
writeHead("Conflict Dynamics Profile - Admin",false,$headertext);
writeBody("Banner Reporting",$msg);
$urls=array('orderssearch.php',
            'ordersmain.php',
            'home.php');
$txts=array('Search Orders',
            'Order Reporting',
            'Back');
$top=count($urls) * 33;
menu($urls,$txts,"");
$today=date("m/d/Y");
$params=array();
$params[]='bdate^'.$today;
$HTML=renderBannerSelectForm();
echo "<center><div style=\"position:relative;width:600px;top:-".$top."px;text-align:center;\">
         $msgX
         $HTML
      </div></center>";

writeFooter(false);

//==============================================================================
// FUNCTIONS
//==============================================================================

function renderBannerSelectForm(){
  $orders=getOrderMasterRecs(false);
  $tbl = new table("600px","","orders");
  $tbl->addRow("","", "head"); $r=0;
  $tbl->rows[$r]->addCell("ORDERS TO INCLUDED IN NEXT BANNER CSV","h","","","","5");
  $tbl->addRow("",""); $r++;
  //$tbl->rows[$r]->addCell("&nbsp;","d","ct");  
  $tbl->rows[$r]->addCell("<b>Order ID</b>","d","ct");
  $tbl->rows[$r]->addCell("<b>Status</b>","d","ct");
  $tbl->rows[$r]->addCell("<b>Amount</b>","d","ct");
  $tbl->rows[$r]->addCell("<b>Tax Rate</b>","d","ct");
  $tbl->rows[$r]->addCell("<b>Status Date</b>","d","ct");
  $i=0;
foreach($orders as $o){
  $ordid=$o['ORDID'];
  $tbl->addRow("",""); $r++;
  //$tbl->rows[$r]->addCell("<input type=\"checkbox\" name=\"order\" value=\"$ordid\" />","d","ct");  
  $tbl->rows[$r]->addCell($ordid,"d","rt");
  $tbl->rows[$r]->addCell($o['LASTSTATUS'],"d");
  $tbl->rows[$r]->addCell($o['TOTAL'],"d","rt");
  $tbl->rows[$r]->addCell($o['TAXRATE'],"d","rt");
  $tbl->rows[$r]->addCell($o['STATUSTS'],"d","ct");
  $i++;
} 
  $tbl->addRow("",""); $r++;
/*
  $buttons ="<span style=\"position:relative;float:left;\"><input type=\"button\" id=\"selectAll\" name=\"selectAll\" value=\"Check All Orders\" /></span>";
  $buttons.="<input type=\"button\" id=\"makeCSV\" name=\"makeCSV\" value=\"Create CSV File\" />";
  $buttons.="<span style=\"position:relative;float:right;\"><input type=\"button\" id=\"selectNone\" name=\"selectNone\" value=\"Uncheck All Orders\" /></span>";
  $buttons.="<input type=\"hidden\" id=\"sOrderIds\" name=\"sOrderIds\" value=\"\" />
  
             <input type=\"hidden\" id=\"what\" name=\"what\" value=\"\" />"; 
*/
  $buttons="There are $i order(s) to be reported to Banner.";           
  $tbl->rows[$r]->addCell($buttons,"d","ct","","","5");         
  $table=$tbl->draw(true);   
$form=<<<EOD
<form method="post" action="bannerreporting.php" id="bannerForm" name="bannerForm">
 $table
</form>
EOD;
return $form;  
}

function createCSV($sOrdids,$dispType='HTML'){
  $rpt=array();
  $x=0;

  /* STEP 1 - GET DATA FROM ORDERMASTER FOR SELECTED ORDERS */
  $orders = getOrderMasterRecs($sOrdids);
  /* STEP 2 - LOOP THROUGH ORDERS */
  foreach($orders as $order){
    $details=getOrderDetailRecs($order['ORDID']);
    foreach($details as $d){
      //die(print_r($d));
      $rpt[$x]=array();
      $rpt[$x]['TransDate']=$order['COMPLETETS'];
      
      $detailCode=getDetailCode($d['PRDKEY'],$order['TAXSTATE'],$d['TAXED']);
      $rpt[$x]['DetailCode']=$detailCode;
      
      $amount=($d['TAXED']=='Y')? ((($order['TAXRATE']/100)+1) * ($d['QTY'] * $d['PRICE'])) : ($d['QTY'] * $d['PRICE']); 
      $amount=number_format($amount, 2, '.', '');
      $rpt[$x]['Amount']="$amount";
      
      $rpt[$x]['InvoiceNumber']=$order['ORDID'];
      
      $rpt[$x]['Description']=str_replace(","," ",$d['PRDNAME']);
      $x++;  
    }
    //Add shipping & handling
    $SandH=number_format(($order['SHIPPING']+$order['HANDLING']), 2, '.', '');
    if($SandH>0){
      $rpt[$x]['TransDate']=$order['COMPLETETS'];
      $rpt[$x]['DetailCode']='CDPS';
      $rpt[$x]['Amount']=$SandH;
      $rpt[$x]['InvoiceNumber']=$order['ORDID'];
      $rpt[$x]['Description']='Shipping & Handling';
      $x++;
    }  
  }

  $data="";
  $total=0;
  foreach($rpt as $line){
    $total+=$line['Amount'];
    $data.=implode(",",$line)."\n";
  } 
  
  if($dispType=='HTML'){
    return nl2br($data);
  }else{
    return rtrim($data);  
  }    
}

function getOrderMasterRecs($ordids){
  $sql="SELECT a.ORDID, b.LASTSTATUS, a.SUBTOTAL , a.TAXES , a.TAXRATE , a.TAXSTATE, a.TAXEIN, a.SHIPPING , a.HANDLING , a.TOTAL , 
               DATE_FORMAT(a.ORDERTS,'%m/%d/%Y') AS ORDERTS , PAYID , APPROV , 
               DATE_FORMAT(a.COMPLETETS,'%m/%d/%Y') AS COMPLETETS,
               DATE_FORMAT(a.BANNERUPLOAD,'%m/%d/%Y') AS BANNERUPLOAD,
               DATE_FORMAT(b.STATUSTS,'%m/%d/%Y') AS STATUSTS  
          FROM ORDERMASTER a, ORDERINFO b
         WHERE a.ORDID = b.ORDID ";
  if($ordids){
    $sql.="AND a.ORDID IN ( $ordids ) 
           AND a.BANNERUPLOAD IS NULL ";   
  }else{
    $sql.="AND b.LASTSTATUS NOT IN ('FAILED','CANCELLED')
           AND a.PAYID IS NOT NULL
           AND a.BANNERUPLOAD IS NULL "; 
  } 
  $sql.="ORDER BY ORDID";
  //die($sql);
  $res=fetchArray($sql);
  return $res;
}

function getOrderDetailRecs($ordid){
  $sql="SELECT a.DETAILID, a.ORDID, a.PRDKEY, b.PRDID, b.PRDNAME, b.WEIGHT, b.TAXABLE, a.TAXED, a.QTY, a.PRICE, b.LICREQ, b.INSTR
          FROM ORDERDETAIL a, CDPPRODUCTS b
         WHERE a.PRDKEY = b.PRDKEY
           AND a.ORDID = $ordid
         ORDER BY a.SEQUENCE ASC";
  $res=fetchArray($sql);
  return $res;         
}

function getDetailCode($prdkey,$state,$taxed){
  $taxableState=array('FL','OH');
  if(in_array($state,$taxableState)){
    $stateCK =($taxed=='Y')? "= '$state' " : "IS NULL ";  
  }else{
    $taxed = 'N';
    $stateCK = "IS NULL ";
  } 

  $sql="SELECT DETAIL 
          FROM ORDERDETAILCODES
         WHERE PRDKEY = $prdkey
           AND STATE $stateCK
           AND TAXED = '$taxed'";
  
  $dCode=fetchOne($sql);
  if($dCode=="") die($sql);
  //die('dCode = '.$dCode);
  return $dCode;
}
?>
<?php
// Common functions for the admin site

function writeHead($title,$buffered,$script=false){
    if($buffered){
	ob_start();
    }
  echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"; 
	echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">"; 
  echo "<head>"; 
  echo "<title>$title</title>"; 
  echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/styles_backend.css\" type=\"text/css\" rel=\"stylesheet\">";
	  if($script)
	    echo "$script";     
    //echo "<link href=\"http://www.conflictdynamics.org/styles_backend.css\" rel=\"stylesheet\" type=\"text/css\" />";
  echo "</head>";
}

function writeBody($title,$msg){
  echo"<body class=\"internal\"> 
	       <div id=\"wrapper\"> 
		        <div id=\"main\"> 
 			        <div id=\"banner\"> 
                <h5><a href=\"../admin/home.php\">CCD Home</a></h5> 
                <h6>Home of the Conflict Dynamics Profile</h6> 
			        </div><!-- end banner --> 
			        <div id=\"content\">";
  echo "<font face='Arial, Helvetica, Sans-Serif'><h2>$title.</h2>$msg<br>&nbsp;<br>"; 
}

function writeFooter($buffered){
    if($buffered){
	ob_end_flush();
    }
  echo"</div><!-- end content --> 
		  </div><!-- end main -->
		  <div id=\"footer\"> 
      <div id=\"extras\"> 
    <div id=\"copyright\"> 
    <p>Conflict Dynamics Profile is a registered trademark of Eckerd College</p> 
    </div> 
    <!-- end copyright --> 
    <a href=\"http://www.eckerd.edu\"><img alt=\"Eckerd College Home\" class=\"left\" height=\"66\" src=\"../index_files/foot_logo.gif\" title=\"Eckerd College Home\" width=\"207\"/></a> 
    <p>4200 54th Avenue South<br/> 
    St. Petersburg, Florida 33711<br/> 
    888-359-9906 | <a href=\"mailto:cdp@eckerd.edu\">cdp@eckerd.edu</a></p> 
    </div><!-- end extras --> 
 		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>";
}

function menu($url,$txt,$frm){
  // if no form is passed in, well create one called frm1
  $len=strlen($frm);
  if(0==len){
	  echo "<form name=\"frm1\" method=\"post\">";
	  $frm="frm1";
  }

  echo "<table border=\"0\" cellpadding=\"5\">";
  $cnt=count($url);
  $cnt-=1;
  for($i=0;$i<=$cnt;$i++){
	  echo "<tr><td class=\"adminLink\">\n";

	  if($i!=$cnt){
	    echo "<a href=\"".$url[$i]."\" style=\"text-decoration:none;\" onMouseOver=\"document.getElementById($i).src='../images/b.gif';\" onMouseOut=\"document.getElementById($i).src='../images/g.gif';\">\n";
	    echo "<img src=\"../images/g.gif\" id=\"$i\" border=\"0\">&nbsp;".$txt[$i]."</a></td></tr>\n";
	  }
	  else{
	    // last option has a red bullet for log out, back etc.
	    echo "<a href=\"".$url[$i]."\" style=\"text-decoration:none;\" onMouseOver=\"document.getElementById($i).src='../images/b.gif';\" onMouseOut=\"document.getElementById($i).src='../images/r.gif';\">\n";
	    echo "<img src=\"../images/r.gif\" id=\"$i\" border=\"0\">&nbsp;".$txt[$i]."</a></td></tr>\n";
	  }
  }
  echo "</table>\n";

  if(0==len){
	  echo "</form>\n";
  }
}

?>

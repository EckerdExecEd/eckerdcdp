<?php
session_start();
if(!empty($_GET['lerr'])){
        if ($_GET['lerr'] == 1) {
    $msg="<font color='#ff0000'>Unknown User</font>";
  } elseif ($_GET['lerr'] == 2) {
    $msg="<font color='#ff0000'>Incorrect Password</font>";
        } else {
    $msg="<font color='#ff0000'>Unknown User or Incorrect Password</font>";
  }
}

//session_unset();
require_once "../meta/mailfns.php";
include_once "../admin/default.php";  //Determines if the logins will be available
include_once "../meta/dbfns.php";

$Xuid=(isset($_GET["uid"]))? trim(htmlspecialchars(strip_tags($_GET["uid"]))) : "";
$HTML=(!$sysAdminON)? $systemMSG : "User:
<br>
<input type=\"text\" name=\"uid\" value=\"admin\" disabled>
<br>
New Password:
<br>
<input type=\"password\" value=\"\" name=\"pwd\">
<br>
<input type=\"checkbox\" value=\"include_cdp\" name=\"cdp\" checked=\"true\">Include CDP</input>
<br>
<input type=\"checkbox\" value=\"include_cdpccl\" name=\"cdpccl\" checked=\"true\">Include CDP-CCL</input>
<br>
<input type=\"checkbox\" value=\"include_cdpow\" name=\"cdpow\" checked=\"true\">Include CDP-OW</input>
<br>
<input type=\"submit\" value=\"Submit\">";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Change Admin Password</title>
    <link href="../index_files/styles_backend.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div id="wrapper">
      <div id="main">
        <div id="banner">
          <h5><a href="index.php">CCD Home</a></h5>
                  <div id="features">
            <img alt="Center for Conflict Dynamics" class="right" height="178" src="../index_files/CCD-topright.png" title="Center for Conflict Dynamics" width="306"/>
          </div><!-- end features -->
        </div><!-- end banner -->
      <div id="content">
    <div id="left">
<h1>Administrator Password</h1>
<p>
<?=$msg?>
<form name="frm1" action="<?=getURLRoot(1)?>/admin/changepassword.php" method=POST>
<input type="hidden" name="s" value="1">
<?php

        if (isset($_POST["pwd"])) {
                if (strlen($_POST["pwd"]) > 0) {
                        $selected = false;

                        echo "<list>";

                        if (isset($_POST["cdp"]) && $_POST["cdp"] == "include_cdp") {
                                $selected = true;
                                $qry = "update CONSULTANT set PWD = password('" . $_POST["pwd"] . "') where UID = 'admin'";

                                executeQuery2($qry, 'localhost', 'cdpadmin', 'ucanwin1$', 'cdpprod');

                                echo "<li>CDP Password is now set to: ";
                                echo $_POST["pwd"];
                                echo "</li>";
                        }

                        if (isset($_POST["cdpccl"]) && $_POST["cdpccl"] == "include_cdpccl") {
                                $selected = true;
                                $qry = "update CONSULTANT set PWD = password('" . $_POST["pwd"] . "') where UID = 'admin'";

                                executeQuery2($qry, 'localhost', 'cdpadmin', 'ucanwin1$', 'cdpccl');

                                echo "<li>CDP CCL Password is now set to: ";
                                echo $_POST["pwd"];
                                echo "</li>";
                        }

                        if (isset($_POST["cdpow"]) && $_POST["cdpow"] == "include_cdpow") {
                                $selected = true;
                                $qry = "update CONSULTANT set PWD = password('" . $_POST["pwd"] . "') where UID = 'admin'";

                                executeQuery2($qry, 'localhost', 'cdpadmin', 'ucanwin1$', 'cdpow');

                                echo "<li>CDP OW Password is now set to: ";
                                echo $_POST["pwd"];
                                echo "</li>";
                        }

                        echo "</list>";

                        if (!$selected) {
                                echo "You must choose an application for which to change the admin password.";
                        }
                } else {
                        echo "You must enter a password to change it.";
                }
        } else {
                echo $HTML;
        }
?>
</form></p>
    </div><!-- end left -->

                                <div id="right">

<div class="feature">

<h3>CDP/EMP</h3>

<p>Monday - Friday<br />

8:00 am - 4:00 pm</p>

<p>888-359-9906<br />

<a href="mailto:cdp@eckerd.edu">cdp@eckerd.edu</a></p>

</div>

                        </div><!-- end right -->

                        </div><!-- end content -->

                </div><!-- end main -->

                <div id="footer">

<div id="extras">

<div id="copyright">

<p>Conflict Dynamics Profile is a registered trademark of Eckerd College</p>

</div>

<!-- end copyright -->

<a href="http://www.eckerd.edu"><img alt="Eckerd College Home" class="left" height="66" src="../index_files/foot_logo.gif" title="Eckerd College Home" width="207"/></a>

<p>4200 54th Avenue South<br/>

 St. Petersburg, Florida 33711<br/>

888-359-9906 | <a href="mailto:cdp@eckerd.edu">cdp@eckerd.edu</a></p>

</div><!-- end extras -->

                </div><!-- end footer -->

        </div><!-- end wrapper -->

</body>

</html>
</script><script><!--
 var jv=1.0;
//--></script>
<script language="Javascript1.1"><!--
 jv=1.1;
//--></script>
<script language="Javascript1.2"><!--
 jv=1.2;
//--></script>
<script language="Javascript1.3"><!--
 jv=1.3;
//--></script>
<script language="Javascript1.4"><!--
 jv=1.4;
//--></script>
<img src="../index_files/sitestats.gif" width="1" height="1" style="position: absolute;">
</div></body>
</html>


<?php
/*==============================================================================
* Interface to select CDP participant w/Rater data file for upload
* 
*=============================================================================*/
require_once("../admin/admfn.php");
require_once "../meta/dbfns.php";
$log="";
if(($_POST['ckFile']=="true")&&(is_uploaded_file($_FILES['dataFile']['tmp_name']))){
  
   require_once "../upload/agencyupload.class.php";
 
   $dr = new DataReader($_FILES['dataFile']['tmp_name']);

   $i=1;
   foreach($dr->_data as $rater){
      $lname = trim($dr->getValue('lname', ($i-1)));
      $fname = trim($dr->getValue('fname', ($i-1)));
      $ckID = trim(substr($rater, 40, 8));
      $cat = trim($dr->getValue('relationship', ($i-1)));
      $know = trim($dr->getValue('familiarity', ($i-1)));
      $cid = trim($dr->getValue('candidateid', ($i-1)));
      $log.="Name record $i is $fname-$lname, ID $ckID, cat $cat, know $know, CID $cid<br>";
     // break;
    $i++;
   }
   
   $dr->processCandidate();
   $dr->processRaters();
   $log.=$dr->_dataErrorMsg;    
}

$pgms=listAgencyUploadablePrograms($tid);
   
//Step 2A - Create form to select parameters for file upload
writeHead("Conflict Dynamics Profile - Admin",false);
//$msg=" ";
writeBody("Upload Participant Responses",$msg);

$fHTML=<<<EOD
<center>
<div>
<form name="frmA1" action="uploadA1.php" method=POST enctype="multipart/form-data">
<input type="hidden" name="ckFile" value="false">
<table border=0>
  <tr bgcolor="#4f8d97">
    <td colspan="2" align="center"><span style="color:#FFF;font-weight:bold;margin:4px;">Select the Program you want the Participant to belong to</span><br>
    <select name="pid">$pgms</td></select></tr>
 <tr bgcolor="#4f8d97">
  <td colspan=2 align="center"><div style="color:#FFF;font-weight:bold;margin:4px;">Select your file</div>
                              <input type="file" name="dataFile"><br></td>
 </tr>  
 <tr bgcolor="#eaeaea">
  <td colspan=2  align="center"><span style="color:#253355;font-weight:bold;margin-left:5px;"><br>Then &nbsp;
  <input type="submit" value="Upload" onClick="document.forms['frmA1']['ckFile'].value='true';">
  &nbsp; OR &nbsp;<input type="button" value="Cancel" onClick="javascript:frmA1.action='home.php';frmA1.submit();"></span></td>
 </tr>
</table>
</form>
$log
</div>
</center>
EOD;

echo $fHTML;

writeFooter(false);

function getAgencyUploadablePrograms($tid=1){
  $CONIDS="b.CONID IN (8471668) ";
	$conn=dbConnect();	
	$query="select a.PID,DESCR 
            from PROGRAM a,PROGCONS b,PROGINSTR c 
           where a.PID=b.PID 
             and a.PID=c.PID 
             and c.TID=$tid ";
  $query.=$CONIDS;              
  $query.="and ARCHFLAG='N' 
        order by DESCR";
	//echo $query."<br>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

function listAgencyUploadablePrograms($tid){
	$pgmList=getAgencyUploadablePrograms($tid);
	$rc="<option value=''>-- Select Program --</option>";
	foreach($pgmList as $row){
		$sel="";
		if($row[0]==$pid){
			$sel="selected";
		}
		$rc=$rc."<option value='$row[0]' $sel>$row[1]</option>";
	}
	return $rc;
}
/*
$msg="";
session_start();

if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
if(isset($_SESSION['conid'])){
    // make sure we don't have a consultant selected here
    unset($_SESSION['conid']);
}
$msg="Upload Data<br>";
require_once("admfn.php");
$log="";
foreach($_REQUEST as $k=>$v){
  //$log.="$k => $v <br>";
}
//Step 1A - Check for POST variables
if(($_POST['ckFile']=="true")&&(is_uploaded_file($_FILES['dataFile']['tmp_name']))){
   require_once "../upload/zupload.php";
   
   //Step 1B - Validate 
   $dr = new DataReader($_FILES['dataFile']['tmp_name']);
   
   $cid = $dr->processCandidate();
   
   if ($cid < 0) {
      // Candidate not found so we cannot continue
      $msg .= '<center><div style="width:650px;border:solid 1px #222222;text-align:center;padding:6px;margin-top:20px;"><h4 style="color:red;margin:2px;">Candidate '.$dr->getCid().' not found so data could not be processed.</h4></div></center>';
      //die(print_r($dr));
   } else {
      $msg = '<span style="color:green;">PID = '.$dr->getPid().'</span>';
      $msg .= '<br /><span style="color:green;">CID = '.$cid.'</span>';

      // Check for data errors
      $errorMsg = $dr->getDataErrorMessage();
      $errorMsg = '';  // Comment out this line if we are doing error checking
      
      if (strlen($errorMsg) > 0) {
         // Data error that keeps us from processing the file
         $msg .= '<center><div style="width:500px;border:solid 1px #222222;text-align:left;padding:2px;margin-top:20px;"><h2 style="color:red;margin:2px;">Data Problems : no data has been processed</h2>'.$errorMsg.'</div></center>';
      } else {
         //Step 1C - Process File
         $msg .= $dr->processRaters();
      }
   }
}
//Step 2A - Create form to select parameters for file upload
writeHead("Conflict Dynamics Profile - Admin",false);
//$msg=" ";
writeBody("Upload Participant Responses",$msg);

$fHTML=<<<EOD
<div style="position:relative;left:200px;top:0px;">
<form name="frmA1" action="uploadA1.php" method=POST enctype="multipart/form-data">
<input type="hidden" name="ckFile" value="false">
<table border=0>
 <tr bgcolor="#4f8d97">
  <td colspan=2><span style="color:#253355;font-weight:bold;margin-left:5px;">Select your file<br><input type="file" name="dataFile"></span></td>
 </tr>  
 <tr bgcolor="#eaeaea">
  <td colspan=2><span style="color:#253355;font-weight:bold;margin-left:5px;"><br>Then &nbsp;
  <input type="submit" value="Upload" onClick="document.forms['frmA1']['ckFile'].value='true';">
  &nbsp; OR &nbsp;<input type="button" value="Cancel" onClick="javascript:frmA1.action='home.php';frmA1.submit();"></span></td>
 </tr>
</table>
</form>
$log
</div>
EOD;

echo $fHTML;
*/
?>

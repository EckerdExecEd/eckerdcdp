<?php
require_once "../meta/consultant.php";
require_once "../admin/admfn.php";
require_once "../meta/table.class.php";
require_once "../meta/orders.class.php";
require_once "../meta/orderfns.php";
require_once "../meta/formfns.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

if((isset($_GET['notes']))&&($_GET['notes']!="")){
     $ordid=substr($_REQUEST['notes'],1);
}

if((isset($_POST['what']))&&($_POST['what']=='ADD')){

$noteordid=$_POST['ordid'];
$newnote=$_POST['ordnote'];
$creator=$_POST['notetkr'];
$public=$_POST['whoview'];

if($noteid=insertOrderNote($noteordid,$newnote,$creator,$public))
  $msX="New note ID is $noteid";
}

$headertext="<script type=\"text/javascript\" src=\"../meta/jquery/jquery.js\"></script>\n
<script type=\"text/javascript\" src=\"../script/js/ordernotes.js\"></script>\n
<link rel=\"stylesheet\" type=\"text/css\" href=\"../script/js/orderrpts.css\" />\n";

$msg="Select from the following options";
writeHead("Conflict Dynamics Profile - Admin",false,$headertext);
writeBody("Order Notes Form",$msg);
$path=explode("/",$_SERVER['HTTP_REFERER']);
$ordDetail="orderssearch.php?detail=D".$ordid;


$urls=array('orderssearch.php',
            "$ordDetail",
            'home.php');
$txts=array('Search Orders',
            'Order Detail',
            'Back');
menu($urls,$txts,"");
echo "<center><div style=\"position:relative;width:600px;top:-100px;text-align:center;\"><h4>Order Notes for Order # $ordid</h4>
      $msgX<form method=\"post\" action=\"ordernotes.php?notes=N".$ordid."\" id=\"notesfrm\" name=\"notessfrm\"><br>"; 
  $txtArea="<textarea id=\"ordnote\" name=\"ordnote\" rows=\"8\" cols=\"55\"></textarea>";
  $notetaker="<input type=\"text\" id=\"notetkr\" name=\"notetkr\" value=\"\" size=\"20\" maxlength=\"50\" /> ";
  $whoview="<input type=\"radio\" id=\"publicY\" name=\"whoview\" value=\"Y\" /> Visible to Admin and Consultants
            <br><input type=\"radio\" id=\"publicN\" name=\"whoview\" value=\"N\" checked/> Visible only to Admin";
  $tbl = new table("600px","","orders");
  $tbl->addRow("","", "head");
  $tbl->rows[0]->addCell("ADD NEW NOTE","h","","","","2");
  $tbl->addRow("","");
  $tbl->rows[1]->addCell("NOTE:","d","rt");
  $tbl->rows[1]->addCell($txtArea); 
  $tbl->addRow("","");
  $tbl->rows[2]->addCell("WRITTEN BY:","d","rt");
  $tbl->rows[2]->addCell($notetaker);
  $tbl->addRow("","");
  $tbl->rows[3]->addCell("VISIBLE TO ALL:","d","rt");
  $tbl->rows[3]->addCell($whoview);
  $tbl->addRow("", "");
  $tbl->rows[4]->addCell("<input type=\"button\" id=\"addNote\" name=\"addNote\" value=\"Add Note\" />","d","ct","","","2");
  $table=$tbl->draw(true);  

echo "$table<input type=\"hidden\" id=\"ordid\" name=\"ordid\" value=\"$ordid\" />
            <input type=\"hidden\" id=\"what\" name=\"what\" value=\"\" /></form></div></center>";  
      
writeFooter(false);
?>
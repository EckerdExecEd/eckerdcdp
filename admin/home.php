<?php
/*=============================================================================*
* 04-22-2005 TRM: Added support for "Self Only" licensing.
*
*=============================================================================*/
$msg="";
session_start();
if("1"==$_POST['s']){
    // if we're here it's a login request
    require_once "../meta/dbfns.php";
    $mysqli=dbiConnect();
    $uid=$_POST['uid'];
    $pwd=$_POST['pwd']; 
    
    if (!($query = $mysqli->prepare("select CONID,FNAME,LNAME from CONSULTANT, ADMINS where CONID=ADMID and UID=(?) and PWD=PASSWORD(?)"))) {
    	echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    if (!$query->bind_param("ss", $uid, $pwd)) {
    	echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    if (!$query->execute()) {
    	echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    if (!$query->bind_result($out_CONID, $out_FNAME, $out_LNAME)) {
    	echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    $row = $query->fetch();
    $query->close();
    if(!($row)){
    	if (!($query2 = $mysqli->prepare("select CONID,FNAME,LNAME from CONSULTANT, ADMINS where CONID=ADMID and UID=?"))) {
    		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
    	}
    	if (!$query2->bind_param("s", $uid)) {
    		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    	}
    	if (!$query2->execute()) {
    		echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
    	}
    	if (!$query2->bind_result($out_CONID, $out_FNAME, $out_LNAME)) {
    		echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    	}
    	$row2 = $query2->fetch();
    	$query2->close();
    	if(!($row2)){
    		header("Location: index.php?lerr=2&uid=$uid");
    	} else {
    		header("Location: index.php?lerr=1&uid=$uid");
    	}
    }else{
    	$_SESSION['admid']=$out_CONID;
    	$_SESSION['name']=$out_FNAME." ".$out_LNAME;
    }
    

    /*$conn=dbConnect();
	  $query=sprintf("select CONID,FNAME,LNAME from CONSULTANT, ADMINS where CONID=ADMID and UID='%s' and PWD=PASSWORD('%s')",
      mysql_real_escape_string($uid),
      mysql_real_escape_string($pwd));*/
      
    //$conn=dbConnect();
    //$rs=mysql_query($query);

    /*if(!($row=mysql_fetch_row($rs))){
			// Login error - go back to the login page
    	$query="select CONID,FNAME,LNAME from CONSULTANT, ADMINS where CONID=ADMID and UID='".$uid."'";
	    if(!($row=mysql_fetch_row($rs))){
				header("Location: index.php?lerr=2&uid=$uid");
	    } else {
				header("Location: index.php?lerr=1&uid=$uid");
			}
    }
    else{
	$_SESSION['admid']=$row[0];
	$_SESSION['name']=$row[1]." ".$row[2];
    }*/
}
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
if(isset($_SESSION['conid'])){
    // make sure we don't have a consultant selected here
    unset($_SESSION['conid']);
}
$msg="Welcome ".$_SESSION['name']."<br>" . $debug;
require_once("admfn.php");
writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Administrator Home",$msg);
$urls=array('addcons.php',
            'listcons.php',
            'candSearch.php',
            'license.php',
            'selflicense.php',
            'mailcons.php',
            'archcons.php',
            'editcomments.php',
            'scoreprogram.php',
            'reopenprogram.php',
            'grpreport.php',
            'ordersmain.php',
            'uploadA1.php',
	    'changepassword.php',
            'index.php');
$txts=array('Add Consultant',
            'Manage Active Consultants',
            'Search For Candidates',
            '360 License Management',
            'Self-Only License Management',
            'Email Active Consultants',
            'Manage Archived Consultants',
            'Edit Rater Comments',
            'Score Program',
            'Re-open Program',
            'Create Group Report',
            'Order Management',
            'Upload Participant Responses <span  style="color:red;font-style:italic;" title="New Agency upload enhancement">In Beta</span>',
	    'Change Admin Password',
            'Log out');
menu($urls,$txts,"");
writeFooter(false);
?>


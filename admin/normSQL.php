<?php

require_once '../meta/dbfns.php';
die("Testing");
$limit =(!isset($_REQUEST['L']))? 0 : $_REQUEST['L'];
$enddt =(!isset($_REQUEST['E']))? '2010-12-31 23:59:59' : $_REQUEST['E'];  //'2007-3-31 23:59:59'
$test =(!isset($_REQUEST['T']))? '1' : $_REQUEST['T'];        // 1 = CDP 360 (Self), 2 = CDP 360 (Rater), 3 = CDP-I
$limit2 = 2700;
/*
  TABLES:     RATER


  FIELDS: 1 - CANDIDATE ID
          2 - FIRST NAME
          3 - LAST NAME
          4 - RATER CATEGORY
          5 - END DATE
          6 - GENDER                          (DMID 4 / 3)
          7 - AGE                             (DMID 5 / 4)
          8 - PROGRAM
          9 - ETHNICITY 
         10 - HIGHEST DEGREE EARNED  ?        (DMID 13)
         11 - ORGANIZATIONAL LEVEL   ?        (DMID 15)
         12 - TYPE OF ORGANIZATION A ?        (DMID 16)
         13 - ?                               (DMID 14)
         14 - FUNCTION               ?
         15 - ?
         16 - ?
         17 - ? 
  18+ -  132 - Items 1 - 114 
*/
$races=array(6=>'Native American or Alaskan Native',
             7=>'Asian or Pacific Islander',
             8=>'Black or African American',
             9=>'Hispanic',
             10=>'Caucasian',
             11=>'Other');

//Step 1 - Query all CANDIDATES who have completed a CDP 360
  $sql=($test==1)?"SELECT a.CID, a.FNAME, a.LNAME, b.ENDDT, d.DESCR  
          FROM CANDIDATE a, RATER b, RATERINSTR c, PROGRAM d, RATERRESP e
         WHERE a.CID = b.CID
           AND a.CID = b.RID
           AND a.CID = c.RID
           AND a.PID = d.PID
           AND a.CID = e.RID
           AND c.TID = e.TID
           AND b.CATID = 1
           AND b.ENDDT IS NOT NULL 
           AND b.ENDDT > '$enddt'
           AND c.TID = $test
           AND e.ITEMID = 1
        ORDER BY b.ENDDT, b.CID ASC
        LIMIT $limit, $limit2" :
       "SELECT a.CID, 
               a.FNAME, 
               a.LNAME, 
               b.ENDDT, 
               d.DESCR 
          FROM CANDIDATE a, 
               RATER b, 
               PROGINSTR c, 
               PROGRAM d, 
               RATERRESP e 
         WHERE a.CID = b.CID 
           AND a.CID = b.RID 
           AND a.PID = d.PID 
           AND a.CID = e.RID
           AND c.PID = d.PID 
           AND c.TID = e.TID 
           AND b.CATID = 1 
           AND b.ENDDT IS NOT NULL 
           AND b.ENDDT > '$enddt' 
           AND b.ENDDT < '2013-1-1'
           AND c.TID = $test 
           AND e.ITEMID = 1 
        ORDER BY b.ENDDT, b.CID ASC" ;
         
        //LIMIT $limit, $limit2
       //die($sql);     
  $cans=fetchArray($sql);

$file="cdpINDdata_2011-12.txt";

if(!$fp=fopen($file,'wb'))
  die("Unable to open ".$file);
else{
  $n=1; 
  foreach($cans as $can){
    $data=array();
    $x=0;  
    $CID=$can['CID'];
    $CFNAME=$can['FNAME'];
    $CLNAME=$can['LNAME'];
    $PDESCR=$can['DESCR'];
    
    $sql="SELECT a.CID, a.RID, a.CATID,
            CASE a.CATID WHEN 1 THEN 'Self'
                       WHEN 2 THEN 'Boss'
                       WHEN 3 THEN 'Peer'
                       WHEN 4 THEN 'DR'
                       ELSE 'OTHER' END AS CAT,
                  a.ENDDT,
                  b.TID
            FROM RATER a, RATERRESP b
           WHERE a.RID = b.RID
             AND a.CID = $CID
             AND a.ENDDT IS NOT NULL
             AND b.ITEMID = 1
        ORDER BY a.ENDDT, a.CID, a.CATID";    
      //die($sql);            
      $raters=fetchArray($sql);
      //die(print_r($raters));
    foreach($raters as $rater){
        $data[$x]=array();
        $RID = $rater['RID'];
        $CATID = $rater['CATID'];
        $TID = $rater['TID'];
        if(($CATID>1)&&($TID!=2)){
         //DO NOTHING BECAUSE TEST DOESN'T MATCH RATER CATEGORY!!
        }else{
        //CASE - RID
        array_push($data[$x],$CID);                                     //     1
        //FIRST NAME - SELF
        array_push($data[$x],$CFNAME);                                  //     2
        //LAST NAME - SELF
        array_push($data[$x],$CLNAME);                                  //     3
        //RATER - Category
        array_push($data[$x],$rater['CAT']);                            //     4
        //COMPLETE DATE
        array_push($data[$x],$rater['ENDDT']);                          //     5
        
        //if SELF get Demographics 
        if($CATID == 1){
          $sql="select DMID,VAL,TXT from DEMOGR where CID=$CID and TID=$test order by DMID asc";
          //die($sql);
          $dems=fetchArray($sql);
        
          //GENDER
          array_push($data[$x],getDemoData($dems,4));                   //     6
          //AGE
          array_push($data[$x],getDemoData($dems,5));                   //     7
          //PROGRAM DESCR
          array_push($data[$x],$PDESCR);                                //     8
          //ETHNICITY
          $race=""; $rTxt="";//die(print_r($dems));
          for($i=6; $i<=12; $i++){
            $race=getDemoData($dems,$i);
            if((strlen($race)>1)&&($i!=12))
              $rTxt=$races[$i];
            if((strlen($race)>1)&&($i==12))
              $rTxt.=" - ".$race;
          }     
          array_push($data[$x],$rTxt);                                   //    9
          //Degree
          array_push($data[$x],getDemoData($dems,13));                  //    10
          //Org1
          array_push($data[$x],getDemoData($dems,15));                  //    11
          //Org2
          array_push($data[$x],getDemoData($dems,16));                  //    12
          //Org Level
          array_push($data[$x],getDemoData($dems,14));                  //    13 
          //Function                   
          array_push($data[$x],getDemoData($dems,17));                  //    14

          
          $sql="SELECT ITEMID, RID, VAL FROM RATERRESP WHERE RID = $RID AND TID IN (1,3) ORDER BY ITEMID ASC";
          $ans=fetchArray($sql);
          foreach($ans as $a){
            array_push($data[$x],$a['VAL']); 
          }           
        }
        //if Not SELF get Demographics
        if(($CATID > 1)&&($CATID < 5)){
          $sql="select DMID,VAL,TXT from RATERDEMOGR where RID=$RID and TID=2 order by DMID asc";
          //die($sql);
          $dems=fetchArray($sql);
        
          //Gender
          array_push($data[$x],getDemoData($dems,3));                   //     6
          //Age
          array_push($data[$x],getDemoData($dems,4));                   //     7 
          // N/A
          array_push($data[$x],"");                                     //     8
          // N/A
          array_push($data[$x],"");                                     //     9
          // N/A          
          array_push($data[$x],"");                                     //    10
          // N/A          
          array_push($data[$x],"");                                     //    11
          // N/A          
          array_push($data[$x],"");                                     //    12
          // N/A
          array_push($data[$x],"");                                     //    13
          // N/A
          array_push($data[$x],"");                                     //    14
          
          $sql="SELECT ITEMID, RID, VAL FROM RATERRESP WHERE RID = $RID AND TID = 2 ORDER BY ITEMID ASC";
          $ans=fetchArray($sql);
          foreach($ans as $a){
            array_push($data[$x],$a['VAL']); 
          }                                                                                                   
        }      
      $x++;
      }
    } 
    
    foreach($data as $row){
      //die(print_r($row));
      $line=implode("|",$row)."\n";
      if(strlen($line)>10){
        echo $n.") ".nl2br($line);
        fwrite($fp,$line);
      }
    }
   $n++;
  }
  
  fclose($fp);
  echo "Done!";
}



function getDemoData($data,$demid){
  foreach($data as $d){
    if($d['DMID']==$demid)
      return $d['TXT']; 
  }
   return "";
}      
?>
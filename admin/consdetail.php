<?php 
require_once "../meta/consultant.php";
require_once "admfn.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
$conid=$_POST['conid'];
$msg="";

if("save"==$_POST['what']){
    $conid=$_POST['conid'];
    $fname=addslashes($_POST['fnam']);
    $lname=addslashes($_POST['lnam']);
    $email=$_POST['email'];
    $state=(!strlen($_POST['state']))? '' : $_POST['state'];
    $exempt=$_POST['taxexempt'];
    
    $consData=array($conid,$fname,$lname,$email,$state,$exempt);
    if(!consExists($email,$conid)){
    	if(updateCons($consData)){
	   $msg="<font color='#00aa00'>Successfully saved data</font>";
	}
	else{
	    $msg="<font color='#aa0000'>Error saving data</font>";
	}
    }
    else{
	$msg="<font color='#aa0000'>The email address already exists.</font>";
    }
}
writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Edit Consultant",$msg);
?>
<form name="listfrm" action="consdetail.php" method=POST>
<input type="hidden" name="what" value="save">
<input type="hidden" name="conid" value="<?=$conid?>">
<table border=1 cellpadding=5>

<?php
if(!showConsultant($conid)){
    echo "<font color='#aa0000'>Error displaying consultantcw data</font></br>";
}
?>
<tr><td colspan=2><input type="submit" value="Save Changes"></td></tr>
</table>
</form>

<?php
$urls=array('listcons.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>

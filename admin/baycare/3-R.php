<?php

require_once '../../meta/dbfns.php';
//die('testing');
echo "Running...<br />";

$file="3-R-baycare-20161116.txt";

$test = 2; //3; CDP-I     //1; CDP-360      

$races=array(6=>'Native American or Alaskan Native',
             7=>'Asian or Pacific Islander',
             8=>'Black or African American',
             9=>'Hispanic',
             10=>'Caucasian',
             11=>'Other');
             
//Step 1 - Query all CANDIDATES who have completed a CDP 360
  $sql=($test==1)?"SELECT a.CID, a.FNAME, a.LNAME, b.ENDDT, d.DESCR  
          FROM CANDIDATE a, RATER b, RATERINSTR c, PROGRAM d, RATERRESP e
         WHERE a.CID = b.CID
           AND a.CID = b.RID
           AND a.CID = c.RID
           AND a.PID = d.PID
           AND a.CID = e.RID
           AND c.TID = e.TID
           AND b.CATID = 1
           AND c.TID = $test           
           AND e.ITEMID = 1
           AND a.CID IN (SELECT CID FROM DEMOGR WHERE TXT LIKE '%baycare%')
        ORDER BY b.ENDDT, b.CID ASC" :
       "SELECT a.CID, 
               a.FNAME, 
               a.LNAME, 
               b.ENDDT, 
               d.DESCR 
          FROM CANDIDATE a, 
               RATER b, 
               PROGINSTR c, 
               PROGRAM d, 
               RATERRESP e 
         WHERE a.CID = b.CID 
           AND a.CID = b.RID 
           AND a.PID = d.PID 
           AND a.CID = e.RID
           AND c.PID = d.PID 
           AND c.TID = e.TID 
           AND b.CATID = 1 
           AND c.TID = $test 
           AND e.ITEMID = 1 
           AND a.CID IN (SELECT CID FROM DEMOGR WHERE TXT LIKE '%baycare%')
        ORDER BY b.ENDDT, b.CID ASC" ;
         
  $cans=fetchArray($sql);

if(!$fp=fopen($file,'w'))
  die("Unable to open ".$file);
else{ 
  foreach($cans as $can){
    $data=array();
    $x=0;  
    $CID=$can['CID'];
    $CFNAME=$can['FNAME'];
    $CLNAME=$can['LNAME'];
    $PDESCR=$can['DESCR'];
    echo "fetching scores for $CFNAME $CLNAME<br />";
    $sql="SELECT a.CID, a.RID, a.CATID,
            CASE a.CATID WHEN 1 THEN 'Self'
                       WHEN 2 THEN 'Boss'
                       WHEN 3 THEN 'Peer'
                       WHEN 4 THEN 'DR'
                       ELSE 'OTHER' END AS CAT,
                  a.ENDDT,
                  b.TID
            FROM RATER a, RATERRESP b
           WHERE a.RID = b.RID
             AND a.CID = $CID
             AND a.CATID = 1
             AND a.ENDDT IS NOT NULL
             AND b.ITEMID = 1
             AND b.TID = $test
        ORDER BY a.ENDDT, a.CID, a.CATID";    
      $raters=fetchArray($sql);
    foreach($raters as $rater){
        $data[$x]=array();
        $RID = $rater['RID'];
        $CATID = $rater['CATID'];
        $TID = $rater['TID'];
        if(($CATID>1)&&($TID!=2)){
         //DO NOTHING BECAUSE TEST DOESN'T MATCH RATER CATEGORY!!
        }else{
        //CASE - RID
        array_push($data[$x],$CID);                                     //     1
        //FIRST NAME - SELF
        array_push($data[$x],$CFNAME);                                  //     2
        //LAST NAME - SELF
        array_push($data[$x],$CLNAME);                                  //     3
        //RATER - Category
        array_push($data[$x],$rater['CAT']);                            //     4
        //COMPLETE DATE
        array_push($data[$x],$rater['ENDDT']);                          //     5
        
        //if SELF get Demographics 
        if($CATID == 1){
          $sql="select DMID,VAL,TXT from DEMOGR where CID=$CID and TID=$test order by DMID asc";
          //die($sql);
          $dems=fetchArray($sql);
        
          //GENDER
          array_push($data[$x],getDemoData($dems,4));                   //     6
          //AGE
          array_push($data[$x],getDemoData($dems,5));                   //     7
          //PROGRAM DESCR
          array_push($data[$x],$PDESCR);                                //     8
          //ETHNICITY
          $race=""; $rTxt="";//die(print_r($dems));
          for($i=6; $i<=12; $i++){
            $race=getDemoData($dems,$i);
            if((strlen($race)>1)&&($i!=12))
              $rTxt=$races[$i];
            if((strlen($race)>1)&&($i==12))
              $rTxt.=" - ".$race;
          }     
          array_push($data[$x],$rTxt);                                   //    9
          //Degree
          array_push($data[$x],getDemoData($dems,13));                  //    10
          //Org1
          array_push($data[$x],getDemoData($dems,15));                  //    11
          //Org2
          array_push($data[$x],getDemoData($dems,16));                  //    12
          //Org Level
          array_push($data[$x],getDemoData($dems,14));                  //    13 
          //Function                   
          array_push($data[$x],getDemoData($dems,17));                  //    14

          
          //$sql="SELECT ITEMID, RID, VAL FROM RATERRESP WHERE RID = $RID AND TID = $test ORDER BY ITEMID ASC";
          $sql="SELECT SID, RID, ROUND(STDSCORE) as STDSCRE FROM SCALESCORE WHERE RID = $RID AND CATID = 1 AND SID NOT IN (16,17,18,19,20,21) ORDER BY SID ASC";
          //die($sql);
          $ans=fetchArray($sql);
          //die(print_r($ans));
          foreach($ans as $a){
            array_push($data[$x],$a['STDSCRE']); 
          }           
        }
        //if Not SELF get Demographics
        if(($CATID > 1)&&($CATID < 5)){
          $sql="select DMID,VAL,TXT from RATERDEMOGR where RID=$RID and TID=2 order by DMID asc";
          //die($sql);
          $dems=fetchArray($sql);
        
          //Gender
          array_push($data[$x],getDemoData($dems,3));                   //     6
          //Age
          array_push($data[$x],getDemoData($dems,4));                   //     7 
          // N/A
          array_push($data[$x],"");                                     //     8
          // N/A
          array_push($data[$x],"");                                     //     9
          // N/A          
          array_push($data[$x],"");                                     //    10
          // N/A          
          array_push($data[$x],"");                                     //    11
          // N/A          
          array_push($data[$x],"");                                     //    12
          // N/A
          array_push($data[$x],"");                                     //    13
          // N/A
          array_push($data[$x],"");                                     //    14
          
          $sql="SELECT ITEMID, RID, VAL FROM RATERRESP WHERE RID = $RID AND TID = 2 ORDER BY ITEMID ASC";
          $ans=fetchArray($sql);
          foreach($ans as $a){
            array_push($data[$x],$a['VAL']); 
          }                                                                                                   
        }      
      $x++;
      }
      
    } 
    
    foreach($data as $row){
      //die(print_r($row));
      $line=implode("|",$row)."\n";
      if(strlen($line)>10){
        //echo nl2br($line);
        fwrite($fp,$line);
      }
    }

  }
  
  fclose($fp);
  echo "Done!";
}



function getDemoData($data,$demid){
  foreach($data as $d){
    if($d['DMID']==$demid)
      return $d['TXT']; 
  }
   return "";
}        
/**/  
?>

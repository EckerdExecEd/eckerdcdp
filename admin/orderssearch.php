<?php
require_once "../meta/consultant.php";
require_once "admfn.php";
require_once "../meta/table.class.php";
require_once "../meta/orders.class.php";
require_once "../meta/orderfns.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
setlocale(LC_MONETARY, 'en_US');

$showPage='F';

if(isset($_POST['where'])&&($_POST['where']!="")){
  $params=explode("##",substr($_POST['where'],2));
  $showPage='S';
}
if(isset($_REQUEST['detail'])&&($_REQUEST['detail']!="")){
  $showOrdid=substr($_REQUEST['detail'],1);
  $showPage='D';
}

$headertext="<script type=\"text/javascript\" src=\"../meta/jquery/jquery.js\"></script>\n
<script type=\"text/javascript\" src=\"../meta/jquery/jquery.numeric.js\"></script>\n 
<script type=\"text/javascript\" src=\"../script/js/orderssearch.js\"></script>\n
<link rel=\"stylesheet\" type=\"text/css\" href=\"../script/js/orderrpts.css\" />\n";
             
$msg="Select from the following options";
writeHead("Conflict Dynamics Profile - Admin",false,$headertext);
writeBody("Orders Search",$msg);
$urls=array('ordersmain.php',
            'home.php');
$txts=array('Order Management',
            'Back');

menu($urls,$txts,"");

switch($showPage){
  case 'S' :
    $result=($srchResult=renderSearchResult($params))?  "<h4>Search Results</h4>".$srchResult : "<h4>No results found</h4>";
    echo "<center>$result</center>";  
    break;
  case 'D' :
    $ordDetail=renderOrderDetails($showOrdid,'orderssearch.php');
    echo "<center><div style=\"position:relative;width:600px;top:-100px;text-align:center;\">$ordDetail</div></center>";      
    break;
  default:
    echo renderSearchForm();
    break;
}

writeFooter(false);

function renderSearchForm(){
$HTML=<<<EOD
<center>
<form id="srchform" name="srchform" action="orderssearch.php" method="post" >
<table>
<tr><th colspan="2">Search for Order(s) By</th></tr>
<tr><td><input type="checkbox" name="sby" value="orderid" />Order ID</td><td><input type="text" name="ordid" id="ordid" size="11" maxlength="11" \ /></td></tr>
<tr><td><input type="checkbox" name="sby" value="date" />Date(s)</td><td>From <input type="text" id="bdate" name="bdate" value="" size="10" maxlength="10" />
&nbsp;&nbsp;To  <input type="text" id="edate" name="edate" value="" size="10" maxlength="10" /></td></tr>
<tr><td><input type="checkbox" name="sby" value="status" disabled/>Status</td><td>
<select id="srchstatus" name="srchstatus" disabled>
<option value="APPROVED">APPROVED</option></select></td></tr>
<tr><td><input type="checkbox" name="sby" value="conid" disabled/>Consultant</td><td><input type="text" id="srchcon" name="srchcon" value="" size="50" maxlength="50" disabled/><input type="hidden" id="srchconid" name="srchconid" value="" /></td></tr>
<tr><td colspan="2" align="center">
<input type="hidden" id="where" name="where" value="" />
<input type="button" id="resetbut" name="resetbut" value="Reset"  />
<input type="button" id="srchbut" name="srchbut" value="Search" /></td></tr>
</table> 
</form>
</center>
EOD;

return $HTML;
}
?>
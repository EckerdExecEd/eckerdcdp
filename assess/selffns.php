<?php
/*=============================================================================*
* 04-25-2005 TRM: Modified to handle self only surveys.
* 11-2-2005 TRM: Modified for multilingual support 
*=============================================================================*/
// Functions for the self survey
require_once "../meta/dbfns.php";
require_once "../meta/survey.php";

// Write the page header for the survey
function writeHeader($lid="1"){
  	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"; 
	  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"; 
    echo "<head>\n"; 
    echo "<title>Internet Assessment System - Self</title>\n"; 
    echo "<link href=\"../index_files/styles_backend.css\" rel=\"stylesheet\" type=\"text/css\" />\n"; 
    echo "</head>\n";	 
  echo"<body class=\"internal\" onload=\"positionFixeddiv()\" onscroll=\"repositionFixeddiv()\"  onkeydown=\"repositionFixeddiv()\" > 
	       <div id=\"wrapper\"> 
		        <div id=\"main\"> 
 			        <div id=\"banner\"> 
                <h5><a href=\"../admin/home.php\">CCD Home</a></h5> 
                <h6>Home of the Conflict Dynamics Profile</h6> 
			        </div><!-- end banner --> 
			        <div id=\"content\">";    
}

// Write the page footer for the survey
function writeFooter($lid="1"){
  echo"</div><!-- end content --> 
		  </div><!-- end main -->
		  <div id=\"footer\"> 
      <div id=\"extras\"> 
    <div id=\"copyright\"> 
    <p>Conflict Dynamics Profile is a registered trademark of Eckerd College</p> 
    </div> 
    <!-- end copyright --> 
    <a href=\"http://www.eckerd.edu\"><img alt=\"Eckerd College Home\" class=\"left\" height=\"66\" src=\"../index_files/foot_logo.gif\" title=\"Eckerd College Home\" width=\"207\"/></a> 
    <p>4200 54th Avenue South<br/> 
    St. Petersburg, Florida 33711<br/> 
    888-359-9906 | <a href=\"mailto:cdp@eckerd.edu\">cdp@eckerd.edu</a></p> 
    </div><!-- end extras --> 
 		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>"; 
}

// Write the navigation bar
function writeNavigation($page,$frm,$tid,$lid="1",$topOrBottom="top"){
	$pic=array();
	$lnk=array();
	$whither="selfexit.php";
	if($tid=="3"){
		$whither="selfonlyexit.php";
	}
	// handle multiple languages here
	if("8"==$lid){
		// Spanish here
		$pE="../images/spPrevE.gif";
		$nE="../images/spNextE.gif";
		$pD="../images/spPrevD.gif";
		$nD="";
		$svX="../images/spSavex.gif";
	}elseif("9"==$lid){
		$pE="../images/ptPrevE.gif";
		$nE="../images/ptNextE.gif";
		$pD="../images/ptPrevD.gif";
		$nD="";
		$svX="../images/ptSavex.gif";	
	}elseif("10"==$lid){
		$pE="../images/frPrevE.png";
		$nE="../images/frNextE.png";
		$pD="../images/frPrevD.png";
		$nD="";
		$svX="../images/frSavex.png";	
	}elseif("11"==$lid){
		$pE="../images/dePrevE.png";
		$nE="../images/deNextE.png";
		$pD="../images/dePrevD.png";
		$nD="";
		$svX="../images/deSavex.png";	
	}else{
  	// Deafult is English
  	$pE="../images/prevE.gif";
  	$nE="../images/nextE.gif";
  	$pD="../images/prevD.gif";
  	$nD="";
  	$svX="../images/savex.gif";	
	}
	
	$pic[0]=$pE;
	$pic[1]="../images/1f.gif";
	$pic[2]="../images/2f.gif";
	$pic[3]="../images/3f.gif";
	$pic[4]="../images/4f.gif";
	$pic[5]="../images/5f.gif";
	$pic[6]="../images/6f.gif";
	$pic[7]="../images/7f.gif";
	if($tid=="3"){
		$pic[8]=$nE;
	}
	else{
		$pic[8]="../images/8f.gif";
		$pic[9]=$nE;
	}

	$pp=$page-1;
	$np=$page+1;
	
	$lnk[0]="javascript:gotoPage($pp);";
	$lnk[1]="javascript:gotoPage(1);";
	$lnk[2]="javascript:gotoPage(2);";
	$lnk[3]="javascript:gotoPage(3);";
	$lnk[4]="javascript:gotoPage(4);";
	$lnk[5]="javascript:gotoPage(5);";
	$lnk[6]="javascript:gotoPage(6);";
	$lnk[7]="javascript:gotoPage(7);";
	if($tid=="3"){
		// next page
		$lnk[8]="javascript:gotoPage($np);";
	}
	else{
		$lnk[8]="javascript:gotoPage(8);";
		$lnk[9]="javascript:gotoPage($np);";
	}
	
	// enable images and links based on the page we're on
	switch($page){
		case 1:
			$pic[0]=$pD;
			$pic[1]="../images/1n.gif";
			$lnk[0]="";
			break;
		case 2:
			$pic[2]="../images/2n.gif";
			break;
		case 3:
			$pic[3]="../images/3n.gif";
			break;
		case 4:
			$pic[4]="../images/4n.gif";
			break;
		case 5:
			$pic[5]="../images/5n.gif";
			break;
		case 6:
			$pic[6]="../images/6n.gif";
			break;
		case 7:
			$pic[7]="../images/7n.gif";
			if($tid=="3"){
				// turn off next
				$pic[8]="";
				$lnk[8]="";
			}
			break;
		case 8:
			if($tid=="1"){
				$pic[8]="../images/8n.gif";
				// turn off next
				$pic[9]="";
				$lnk[9]="";
			}
			break;
		default:
	}
	
	echo "<table border=0 cellspacing=0 cellpadding=0><tr>";
	$howMany=(8==$page)?9:10;
	if($tid=="3"){
		$howMany=(7==$page)?8:9;
	}
	
	for($i=0;$i<$howMany;$i++){
		echo "<td valign=top onClick=\"".$lnk[$i]."\">";
		echo "<img src='$pic[$i]'>";
		echo "</td>";
	}

	$lastPage=9;
	if($tid=="3"){
		$lastPage=8;
	}
	if($page!=$lastPage){
		echo "<td valign=top onClick=\"javascript:saveAndExit();\">&nbsp;&nbsp;<img src='$svX'></td>";
	}
	//echo $svX;
	echo "</tr></table>";
	
	if ($topOrBottom == "top") {
	  switch($lid){
	   case "9" :
	    $alert1= "As suas repostas vão ser guardadas e poderá acabar com o inquérito mais tarde.";
	    break;
	   case "10" :
	    $alert1= "Vos réponses seront sauvegardées et vous pourrez terminer la passation du questionnaire ultérieurement.";
	    break;
	   case "11" :
      $alert1= "Ihre Antworten werden gespeichert und Sie können den Fragebogen später beenden."; 
      break;		   
     default:
      $alert1= "Your answers will be saved and you can finish the survey later.";
      break;	   
	  }
		// Only create the gotPage function if we are creating the top navigation menu.
		// That way it prevents multiple gotoPage() functions on one page.
		echo <<<EOD

<script lang="javascript">

function gotoPage( page ) {
  var $frm = document.getElementById("$frm");
	$frm.page.value = page;
	if (typeof validSelection != "undefined") {
		validSelection = true;
	}
	$frm.what.value = "save"; 
	$frm.action = "";
	$frm.submit();
}
	
function saveAndExit() {
  var $frm = document.getElementById("$frm");
	if (typeof validSelection == "undefined") {
		alert("$alert1");
	} else {
		alert("$alert1");
		validSelection = true;
	}
	$frm.what.value = "save";
	$frm.action = "$whither";
	$frm.submit();
}

</script>

EOD;
	}

}

function writeSelfSurvey($page,$cid,$rid,$frm,$tid,$lid="1"){
	$lastPage=($tid=="3")?7:8;
	if(1==$page){
		showSelfDemographics($cid,$tid,$lid);	
		$legendText = "";
	}
	elseif($page>=2 and $page<$lastPage){
		$legendText = showSurvey($rid,$tid,$lid,$page);
	}
	elseif($page==$lastPage){
		showByeBye($rid,$frm,$tid,$lid);
		$legendText = "";
	}

	displayScrollingLegend($legendText);
	//if (strlen($legendText) > 0) displayScrollingLegend($legendText);

}

function showByeBye($rid,$frm,$tid,$lid="1"){
	$whither="selfexit.php";
	if($tid=="3"){
		$whither="selfonlyexit.php";
	}
	$cnt=getNoAnswerCount($rid,$tid);

	$showSubmitButton = false;

	echo '<table><tr><td>';
	if("3"==$tid){
		$flid=getFLID("assess","selffns.php");  //354
		$mlTxt=getMLText($flid,$tid,$lid);
    		//die("flid = $flid");
		if($cnt>3){
			echo "<p><u>$mlTxt[1]</u> </p><p>&nbsp;<br>$mlTxt[2] $cnt $mlTxt[3] 99</p>&nbsp;</p>";
			$unAnsw=getUnansweredItems($rid,$tid);
			echo "<p><u>$mlTxt[4]</u></p><p style=\"text-align:left;width:300px;\">".implode($unAnsw,",")."</p>";
		}
		elseif($cnt>0){
			echo "<p><u>$mlTxt[7]</u> </p><p>&nbsp;<br>$mlTxt[2] $cnt $mlTxt[3] 99</p>&nbsp;</p>";
			$unAnsw=getUnansweredItems($rid,$tid);
			echo "<p><u>$mlTxt[4]</u></p><p>".implode($unAnsw,",")."</p>";
			echo "<p>$mlTxt[8]<br>$mlTxt[9]</p><p>";
			echo "<input type='button' value='$mlTxt[6]' onClick=\"javascript:submitForm(false);\" style=\"font:bold 24pt arial;margin:20px 0px 8px 0px;\">";
			echo "</p><p>&nbsp;</p>";
			$showSubmitButton = true;
		}
		else{
			echo "<p> $mlTxt[5]</p><p>";
			echo "<input type='button' value='$mlTxt[6]' onClick=\"javascript:submitForm(true);\" style=\"font:bold 24pt arial;margin:20px 0px 8px 0px;\">";
			echo "</p><p>&nbsp;</p>";
			$showSubmitButton = true;
		}
	}
	else{ //360 Assessment
	  //Changed maximum unanswered questions allowed from 3 to 7 - JPC 04/17/2008
		if($cnt>7){
			echo "<p><u>Please address the following issues:</u> </p><p>&nbsp;<br>You have $cnt unanswered questions of 114</p>&nbsp;</p>";
			$unAnsw=getUnansweredItems($rid,$tid);
			echo "<p><u>Unanswered Questions:</u></p><p>".implode($unAnsw,",")."</p>";
		}
		elseif($cnt>0){
			echo "<p><u>Are you sure you want to complete the survey?</u> </p><p>&nbsp;<br>You have $cnt unanswered questions of 114</p>&nbsp;</p>";
			$unAnsw=getUnansweredItems($rid,$tid);
			echo "<p><u>Unanswered Questions:</u></p><p>".implode($unAnsw,",")."</p>";
			echo "<p> It is <b>strongly recommended</b> that you go back and answer all questions.<br>However, you may click on the 'Submit' button below to complete the survey.</p><p>";
			echo "<input type='button' value='Submit' onClick=\"javascript:submitForm(false)\" style=\"font:bold 24pt arial;margin:20px 0px 8px 0px;\" />";
			echo "</p><p>&nbsp;</p>";
			$showSubmitButton = true;
		}
		else{
			echo "<p> Click on the 'Submit' button below to complete the survey.</p><p>";
			echo "<input type='button' value='Submit' onClick=\"javascript:submitForm(true);\" style=\"font:bold 24pt arial;margin:20px 0px 8px 0px;\" />";
			echo "</p><p>&nbsp;</p>";
			$showSubmitButton = true;
		}
	}
	echo '</td></tr></table>';

	if ($showSubmitButton) {
  	switch($lid){
  	 case "9" :
  	   $alert2="Enviar o inquérito significa que está completo e não terá mais a possibilidade de\\nmudar as suas respostas.\\n\\nTem certeza que quer continuar?";
  	   $alert3="Ainda não respondeu a todas as questões.\\n\\nPode enviar o inquérito, mas depois não terá mais a possibilidade de\\nmudar as suas respostas.\\n\\nTem certeza que quer continuar?";
  	   break;
  	 case "10" :
  	   $alert2="En soumettant l’ensemble de vos réponses, vous confirmez avoir achevé la passation du questionnaire et vous ne pourrez plus modifier aucune de vos réponses. Souhaitez-vous poursuivre ?";
  	   $alert3="Il subsiste des questions restées sans réponse. Vous pouvez quand même choisir de soumettre le questionnaire pour le traitement de vos données mais, après cela, vous ne pourrez plus modifier aucune de vos réponses. Souhaitez-vous poursuivre ?";
  	   break;
  	 case "11" :
  	   $alert2="Mit dem Absenden des Fragebogens bestätigen Sie, dass Sie den Fragebogen vollständig ausgefüllt haben.\\n\\nWir weisen Sie darauf hin, dass Sie danach nicht mehr in der Lage sein werden, Ihre Antworten zu ändern.\\n\\nWollen Sie wirklich fortfahren?";
       //$alert3="Nicht alle Fragen wurden beantwortet.\\n\\nSie können immer noch die Umfrage vorlegen, aber nachdem er eingereicht wird\\nSie nicht in der Lage, alle Ihre Antworten ändern.\\n\\nSind Sie sicher, dass Sie weiterhin ?";
       $alert3="Nicht alle Fragen wurden beantwortet.\\nSie können immer noch zurückgehen und die unbeantworteten Fragen beantworten. Wenn Sie jetzt “ok”\\ndrücken sind Sie nicht mehr in der Lage die restlichen Fragen zu beantworten.\\nSind Sie sicher, dass Sie weiter machen möchten?";
       break;
     default :
  	   $alert2="By submitting the survey you are indicating that you are complete\\nand you will not be able to change any of your responses.\\n\\nAre you sure you want to continue?";
  	   $alert3="Not all questions have been answered.\\n\\nYou may still submit the survey, but after it is submitted\\nyou will not be able to change any of your responses.\\n\\nAre you sure you want to continue?";
  	   break;       	 
  	}
 /*
	$alert2=($lid!="9")? "By submitting the survey you are indicating that you are complete\\nand you will not be able to change any of your responses.\\n\\nAre you sure you want to continue?" :
	   "Enviar o inquérito significa que está completo e não terá mais a possibilidade de\\nmudar as suas respostas.\\n\\nTem certeza que quer continuar?";
	$alert3=($lid!="9")? "Not all questions have been answered.\\n\\nYou may still submit the survey, but after it is submitted\\nyou will not be able to change any of your responses.\\n\\nAre you sure you want to continue?" :
	   "Ainda não respondeu a todas as questões.\\n\\nPode enviar o inquérito, mas depois não terá mais a possibilidade de\\nmudar as suas respostas.\\n\\nTem certeza que quer continuar?";
 */   	   
		echo <<<EOD

<script lang="javascript">

var validSelection = false;

window.onbeforeunload = confirmSelectionMade;

function submitForm( allComplete ) {
  var $frm = document.getElementById("$frm");
	if (allComplete) {
		var reply = confirm("$alert2");
	} else {
		var reply = confirm("$alert3");
	}
	
	if (!reply) {
		return false
	} else {
		validSelection = true;
		$frm.what.value = "finish";
		$frm.action = "$whither";
		$frm.submit();
	}
	
}

function confirmSelectionMade() {

  if (!validSelection) {
		return "You must click the 'Submit' button to complete the survey.\\n\\nIf you do not wish to submit your answers at this time then click 'OK' to continue.\\nOtherwise, click on 'Cancel' to remain on this page.";
  }
}

</script>

EOD;
	
	}

}

?>

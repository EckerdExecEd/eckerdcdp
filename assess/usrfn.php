<?php
/*   */
function writeHead($title,$script=false){
  	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"; 
	  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">"; 
    echo "<head>"; 
    echo "<title>$title</title>"; 
    echo "<link href=\"../index_files/styles_backend.css\" rel=\"stylesheet\" type=\"text/css\" />"; 
	  if($script)
	    echo "$script"; 
	  echo "<script type=\"text/javascript\" src=\"../meta/sortabletable/sortabletable.js\"></script>\n";
	  echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../meta/sortabletable/sortabletable.css\" />\n";
    echo "</head>";	  
}

function writeBody(){
  echo"<body class=\"internal\"> 
	       <div id=\"wrapper\"> 
		        <div id=\"main\"> 
 			        <div id=\"banner\"> 
                <h5><a href=\"../admin/home.php\">CCD Home</a></h5> 
                <h6>Home of the Conflict Dynamics Profile</h6> 
			        </div><!-- end banner --> 
			        <div id=\"content\">";
}

function writeFooter(){
  echo"</div><!-- end content --> 
		  </div><!-- end main -->
		  <div id=\"footer\"> 
      <div id=\"extras\"> 
    <div id=\"copyright\"> 
    <p>Conflict Dynamics Profile is a registered trademark of Eckerd College</p> 
    </div> 
    <!-- end copyright --> 
    <a href=\"http://www.eckerd.edu\"><img alt=\"Eckerd College Home\" class=\"left\" height=\"66\" src=\"../index_files/foot_logo.gif\" title=\"Eckerd College Home\" width=\"207\"/></a> 
    <p>4200 54th Avenue South<br/> 
    St. Petersburg, Florida 33711<br/> 
    888-359-9906 | <a href=\"mailto:cdp@eckerd.edu\">cdp@eckerd.edu</a></p> 
    </div><!-- end extras --> 
 		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>"; 
}
			        
?>
<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1); 
// This is the login page for self only surveys
// Try and log in
$uid=addslashes(strip_tags($_POST['uid']));
$pin=addslashes($_POST['pin']);
$lid=addslashes($_POST['lid']);

$lang=((isset($_REQUEST['lang']))&&($_REQUEST['lang']!=""))? $_REQUEST['lang'] : "en"; 

if(strlen($lid)<1){
	$lid="1";	
}
/* Delete later
//set to correct login page
switch($lid){
  case "8" :
    $pfx="sp";
    break;
  case "9" :
    $pfx="pt";
    break;
  default :
    $pfx="";
    break;
}
*/
$exitURL="survey.php?lang=$lang";

require_once "../meta/selfonlyrater.php";
session_start();
$done=selfOnlyRaterLogin($uid,$pin);
if(false==$done){
	// Login error - go back to the login page
	header("Location: ".$exitURL."&lerr=1");
}
else{
	if($done=="Y"){
		// Already completed survey, no longer accessible
		header("Location: ".$exitURL."&lerr=2");
	}
	else{
		// Finally we're logged in
		$_SESSION['rid']=$pin;
		$_SESSION['cid']=$pin;
		$oid = getOrgid($pin);
		$_SESSION['orgid']=(!$oid)? 1 : $oid;
		// get than language that the trainer used to set up this rater
		$lid=getSelfOnlyRaterLanguage($pin); 
	}
}
// Hold it right there!
if(empty($_SESSION['rid'])){
	die("You are not logged in");	
}
if(empty($_SESSION['cid'])){
	die("You are not logged in");	
}

$orgid=((isset($_SESSION['orgid']))&&($_SESSION['orgid']!=""))? $_SESSION['orgid'] : 1;

switch($orgid){
  case 4 :
  case 3 :
    require_once "../custom/ICD/survey/usrfnICD.php";
    break;
  case 2 :
  case 1 :
  default:
    require_once "usrfn.php";
    break;  
}


require_once "../meta/survey.php";
require_once "../meta/multilingual.php";

$candName=getCandidateName($pin);

// get all the page data
$flid=getFLID("assess","selfonly.php");
$mlText=getMLText($flid,"3",$lid);  
writeHead("Internet Assessment System - Respondent");
writeBody();
?>



<form action="" method=POST name="frm1">
<input type="hidden" name="page" value="1">
<input type="hidden" name="lid" value="<?=$lid?>">
<p>
<?php
echo $mlText[1];
?>
</p>
<p>
<?php
$prompt=$mlText[2];
$cnt=getNoAnswerCount($pin,"3");
if(0!=$cnt&&114>$cnt){
  $cntTxt=str_replace("##N##", "$cnt", $mlText[5]);
	echo $cntTxt;
	echo implode(getUnansweredItems($_SESSION['cid'],"3"),",");
	$prompt=$mlText[3];	
}
$pfx="";
if("8"==$lid){
	$pfx="sp";	
}
?>
</p>
<p>
<input type="button" onClick="javascript:frm1.action='selfonlysurvey.php';frm1.submit();" value="<?=$prompt?>" >
&nbsp;&nbsp;
<input type="button" onClick="javascript:frm1.action='<?=$exitURL?>';frm1.submit();" value="<?=$mlText[4]?>">
</p>
</form>
<?php
if(("10"==$lid)&&($orgid>2)){
	echo $mlText[10];
}
?>
</td></tr></table>
</center>
<?php
writeFooter();
?>
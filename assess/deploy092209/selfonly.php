<?php
// This is the login page for self only surveys
// Try and log in
$uid=addslashes($_POST['uid']);
$pin=addslashes($_POST['pin']);
$lid=addslashes($_POST['lid']);
if(strlen($lid)<1){
	$lid="1";	
}

$pfx="";
if("8"==$lid){
	$pfx="sp";
}
require_once "../meta/selfonlyrater.php";
session_start();
$done=selfOnlyRaterLogin($uid,$pin);
if(false==$done){
	// Login error - go back to the login page
	header("Location: ".$pfx."survey.php?lerr=1");
}
else{
	if($done=="Y"){
		// Already completed survey, no longer accessible
		header("Location: ".$pfx."survey.php?lerr=2");
	}
	else{
		// Finally we're logged in
		$_SESSION['rid']=$pin;
		$_SESSION['cid']=$pin;
		// get than language that the trainer used to set up this rater
		$lid=getSelfOnlyRaterLanguage($pin);
	}
}
// Hold it right there!
if(empty($_SESSION['rid'])){
	die("You are not logged in");	
}
if(empty($_SESSION['cid'])){
	die("You are not logged in");	
}
require_once "../meta/survey.php";
require_once "../meta/multilingual.php";

$candName=getCandidateName($pin);
echo "\n" . '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
echo "\n" . '<html>';
echo "\n" . '<head>';
echo "\n" . '<title>Internet Assessment System - Respondent</title>';
echo "\n" . '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
echo "\n" . '<link title="standard" media="screen" href="../index_files/eckerd-1.css" type="text/css" rel="stylesheet">';
echo "\n" . '<style>.ivanC10400569321381 {';
echo "\n\t" . 'VISIBILITY: hidden;';
echo "\n\t" . 'POSITION: absolute';
echo "\n" . '}';
echo "\n" . '</style>';
echo "\n" . '</head>';
echo "\n" . '<body class=".body">';
echo "\n" . '<table cellspacing="0" cellpadding="0" width="100%" border="0">';
echo "\n\t" . '<tbody>';
echo "\n\t" . '<tr valign="top" align="left" height="1">';
echo "\n\t\t" . '<td colspan="3"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
echo "\n\t" . '</tr>';
echo "\n\t" . '<tr valign="top" align="right" bgcolor="#4f8d97" height="22">';
echo "\n\t\t" . '<td colspan="3"></td>';
echo "\n\t" . '</tr>';
echo "\n\t" . '<tr valign="top" align="left" height="1">';
echo "\n\t\t" . '<td colspan="3"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
echo "\n\t" . '</tr>';
echo "\n\t" . '<tr valign="center" height="42" bgcolor="#253355">';
echo "\n\t\t" . '<td align="left"><img height="64" src="../index_files/eclogotop.gif" width="155" border="0"></td>';
echo "\n\t\t" . '<td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
echo "\n\t\t" . '<td align="right"><img height="64" src="../index_files/ldi-title.gif" width="430" border="0"></td>';
echo "\n\t" . '</tr>';
echo "\n" . '</table>';
echo "\n" . '<br>&nbsp;</br>'; 

// get all the page data
$flid=getFLID("assess","selfonly.php");
$mlText=getMLText($flid,"3",$lid);
?>

<center>
<table width="750"><tr><td>

<form action="" method=POST name="frm1">
<input type="hidden" name="page" value="1">
<input type="hidden" name="lid" value="<?=$lid?>">
<p>
<?php
echo $mlText[1];
?>
</p>
<p>
<?php
$prompt=$mlText[2];
$cnt=getNoAnswerCount($pin,"3");
if(0!=$cnt&&114>$cnt){
	echo $mlText[5];
	echo implode(getUnansweredItems($_SESSION['cid'],"3"),",");
	$prompt=$mlText[3];	
}
$pfx="";
if("8"==$lid){
	$pfx="sp";	
}
?>
</p>
<p>
<input type="button" onClick="javascript:frm1.action='selfonlysurvey.php';frm1.submit();" value="<?=$prompt?>" >
&nbsp;&nbsp;
<input type="button" onClick="javascript:frm1.action='<?=$pfx?>survey.php';frm1.submit();" value="<?=$mlText[4]?>">
</p>
</form>

</td></tr></table>
</center>

</body>
</html>


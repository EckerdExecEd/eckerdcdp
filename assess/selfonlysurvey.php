<?php
session_start();
if(empty($_SESSION['cid'])){
	die("You are not logged in");	
}
$cid=$_SESSION['cid'];
// For Self Rater ID is same as Candidate ID
$rid=$cid;
$page=$_POST['page'];
$lid=$_POST['lid'];
if(strlen($page)<1){
	$page=1;	
}
$orgid=((isset($_SESSION['orgid']))&&($_SESSION['orgid']!=""))? $_SESSION['orgid'] : 1;

require_once "selffns.php";
switch($orgid){
  case 4 :
  case 3 :
    require_once "../custom/ICD/survey/selfHeadFootFns.php"; 
    break;
 default :
    break;   
}
$what=$_POST['what'];

// ML - get language ID
$lid=addslashes($_POST["lid"]);
if(strlen($lid)<0){
	$lid="1";
}
if("save"==$what){
	// ensure that we have survey items as needed
	insertSurvey($cid,$rid,"3");

	$frompage=$_POST['frompage'];

	if("1"==$frompage){
		// we came from the demographics page
		$data=array();
		$data[0]="";
		$data[1]=addslashes($_POST['dm1']);
		$data[2]=addslashes($_POST['dm2']);
		$data[3]=addslashes($_POST['dm3']);
		$data[4]=addslashes($_POST['dm4']);
		$data[5]=addslashes($_POST['dm5']);
		$data[6]=addslashes($_POST['dm6']);
		$data[7]=addslashes($_POST['dm7']);
		$data[8]=addslashes($_POST['dm8']);
		$data[9]=addslashes($_POST['dm9']);
		$data[10]=addslashes($_POST['dm10']);
		$data[11]=addslashes($_POST['dm11']);
		$data[12]=addslashes($_POST['dm12']);
		$data[13]=addslashes($_POST['dm13']);
		$data[14]=addslashes($_POST['dm14']);
		$data[15]=addslashes($_POST['dm15']);
		$data[16]=addslashes($_POST['dm16']);
		$data[17]=addslashes($_POST['dm17']);
		$data[18]=addslashes($_POST['dm18']);
		saveDemographics($cid,"3",$data);
	}
	else{
		// we came from any other page
		$keys=array();
		$values=array();
		$q1=getQ1($frompage,"3");
		$qN=getQn($frompage,"3");
		for($i=$q1;$i<=$qN;$i++){
			$item="resp$i";
			$val=$_POST[$item];
			if(strlen($val)>0){
				$keys[]=$i;
				$values[]=$val;
			}
		}
		saveItems($rid,"3",$keys,$values);
	}
}

switch($orgid){
  case 4 :
  case 3 :
    writeCustomHeader($lid);
    break;
  case 2 :        
  case 1 :
  default :
    writeHeader();
    break;
}

?>

<center>
<table width="750"><tr><td>

<form action="selfonlysurvey.php" name="frm1" id="frm1"	method=POST>
<input name="page" type="hidden" value="<?=$page?>">
<input name="what" type="hidden" value="save">
<input name="cid" type="hidden" value="<?=$cid?>">
<input name="rid" type="hidden" value="<?=$rid?>">
<input name="frompage" type="hidden" value="<?=$page?>">
<input type="hidden" name="lid" value="<?=$lid?>">
<?php
writeNavigation($page,"frm1","3",$lid);
writeSelfSurvey($page,$cid,$rid,"frm1","3",$lid);
writeNavigation($page,"frm1","3",$lid,"bottom");
?>
</form>
</td></tr></table>
</center>
<?php

switch($orgid){
  case 4 :
  case 3 :
    writeCustomFooter($lid);
    break;
  case 2 :        
  case 1 :
  default :
    writeFooter();
    break;
}
?>
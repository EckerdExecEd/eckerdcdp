<?php
session_start();
if(empty($_SESSION['rid'])){
	die("You are not logged in");	
}
$rid=$_SESSION['rid'];
if(empty($_SESSION['cid'])){
	die("You are not logged in");	
}
$cid=$_SESSION['cid'];
$orgid=((isset($_SESSION['orgid']))&&($_SESSION['orgid']!=""))? $_SESSION['orgid'] : 1;

$page=$_POST['page'];
if(strlen($page)<1){
	$page=1;	
}
require_once "selffns.php";
switch($orgid){
  case 4 :
  case 3 :
    require_once "../custom/ICD/survey/selfHeadFootFns.php"; 
    break;
  case 2 :
  case 1 :
  default :
    break;   
}
$what=$_POST['what'];

$lid=addslashes($_POST['lid']);
if(strlen($lid)<1){
	$lid="1";	
}
// get the corrcet language text
$flid=getFLID("assess","selfonlyexit.php");  //353
$mlTxt=getMLText($flid,"3",$lid);
//die("flid = ".$flid." and lid = ".$lid);
$msg="";
if("save"==$what){
	// This is for the Save and Exit functionality
	$frompage=$_POST['frompage'];
	insertSurvey($cid,$rid,"3");

	if("1"==$frompage){
		$data=array();
		$data[0]="";
		$data[1]=addslashes($_POST['dm1']);
		$data[2]=addslashes($_POST['dm2']);
		$data[3]=addslashes($_POST['dm3']);
		$data[4]=addslashes($_POST['dm4']);
		$data[5]=addslashes($_POST['dm5']);
		$data[6]=addslashes($_POST['dm6']);
		$data[7]=addslashes($_POST['dm7']);
		$data[8]=addslashes($_POST['dm8']);
		$data[9]=addslashes($_POST['dm9']);
		$data[10]=addslashes($_POST['dm10']);
		$data[11]=addslashes($_POST['dm11']);
		$data[12]=addslashes($_POST['dm12']);
		$data[13]=addslashes($_POST['dm13']);
		$data[14]=addslashes($_POST['dm14']);
		$data[15]=addslashes($_POST['dm15']);
		$data[16]=addslashes($_POST['dm16']);
		$data[17]=addslashes($_POST['dm17']);
		$data[18]=addslashes($_POST['dm18']);
		saveDemographics($cid,"3",$data);
	}
	else{
		// we came from any other page
		$keys=array();
		$values=array();
		$q1=getQ1($frompage,"3");
		$qN=getQn($frompage,"3");
		for($i=$q1;$i<=$qN;$i++){
			$item="resp$i";
			$val=$_POST[$item];
			if(strlen($val)>0){
				$keys[]=$i;
				$values[]=$val;
			}
		}
		saveItems($rid,"3",$keys,$values);
	}
	$msg="$mlTxt[1]<br>";
}

if("finish"==$what){
	// And this is for finishing the survey
	$msg="$mlTxt[3]<br>";
	finishSurvey($rid);
}

switch($orgid){
  case 4 :
  case 3 :
    writeCustomHeader($lid);
    break;
  case 2 :        
  case 1 :
  default :
    writeHeader();
    break;
}

switch($lid){
  case "8" : //Spanish
    $pfx="sp";
    break;
  case "9" : //Portuguese
    $pfx="pt";
    break;
  case "10" : //French
    $pfx="fr";
    break;
  case "11" : //German
    $pfx="de";
    break;
  default : //English
    $pfx="en";
    break;
}

$target ="survey.php?";
$target.=($orgid < 3)? "" : "orgid=$orgid";
$target.=($orgid < 3)? "lang=$pfx" : "&lang=$pfx";

?>

<center>
<table width="750"><tr><td>

<form action="<?=$target?>" name="frm1"	method=POST>
<?=$msg?>
<?=$mlTxt[2]?><br>
<input type="Submit" value="OK"> 
</form>
</td></tr></table>
</center>
<?php
switch($orgid){
  case 4 :
  case 3 :
    writeCustomFooter($lid);
    break;
  case 2 :        
  case 1 :
  default :
    writeFooter();
    break;
}
?>
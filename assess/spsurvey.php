<?php
// This is the home page for self only assessments
# The only place in the application where we create a session
session_start();
if(!empty($_GET['lerr'])){
    $lerr=$_GET['lerr'];
    if("1"==$lerr){
		$msg="<font color='#aa0000'>Invalid Login<small><i>(<u>Note:</u> Have you entered the correct email address and PIN?.)</i><small></font>";
    }
    if("2"==$lerr){
		$msg="<font color='#00aa00'><i><u>Note:</u> You have already completed the assessment. Thank you!</i><small></font>";
    }
}
session_unset();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><!-- saved from url=(0043)http://www.eckerd.edu/ldi/about/index.shtml --><title>#1 in Leadership</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="Wed, 26 Jan 2005 10:05:59 -0500" name="date"><link title="standard" media="screen" href="../index_files/eckerd-1.css" type="text/css" rel="stylesheet">
<meta content="MSHTML 6.00.2800.1458" name="GENERATOR"><style>.ivanC10400569321381 {
	VISIBILITY: hidden; POSITION: absolute
</style></head>
<body bottommargin="0" vlink="#990000" alink="#000000" link="#990000" bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" marginwidth="0" marginheight="0" margin="0">
<center>
<table cellspacing="0" cellpadding="0" width="750" border="0">
  <tbody>
  <tr valign="top" align="left" height="1">
    <td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td></tr>
  <tr valign="top" align="right" bgcolor="#4f8d97" height="22">
        <td>
 </td>
      </tr>
  <tr valign="top" align="left" height="1">
    <td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td></tr>
  <tr valign="top" align="left" height="42">
    <td>
      <table height="42" cellspacing="0" cellpadding="0" width="750" bgcolor="#253355" border="0">
        <tbody>
        <tr valign="center" height="42">
          <td align="left"><img height="64" src="../index_files/eclogotop.gif" width="155" border="0"></td>
          <td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>
          <td align="right"><img height="64" src="../index_files/ldi-title.gif" width="430" border="0"></td></tr></tbody></table></td></tr>
  <tr valign="top" align="left" height="1">
    <td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td></tr>
  <tr valign="top" align="left">
    <td>
      <table cellspacing="0" cellpadding="0" width="750" border="0">
        <tbody>
        <tr valign="top" align="left">
          <td class="primarynav" width="137" bgcolor="#253355">
            <p>
                  </p><p></p></td>
          <td width="1"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>
                <td width="450"> 
                  <table id="main" height="250" cellspacing="0" cellpadding="16" width="450" border="0">
              <tbody>
                      <tr valign="top" align="left">
                                                             <? // removed utf8_decode() - JPC 05/23/08 ?> 
                        <td class="content"> <p align="left"><?="Auto-Evaluación. Página de Entrada."?></p>
<?=$msg?>
<form name="frm1" action="selfonly.php" method=POST>
<input type="hidden" name="s" value="1">
<input type="hidden" name="lid" value="8">
<?// removed utf8_decode("Correo electrónico:") - JPC 05/23/08 ?> 
<?="Correo electrónico:" ?> 
<br>
<input type="text" name="uid" value="">
<br>
<?// removed utf8_decode("Número de clave:") - JPC 05/23/08 ?>
<?="Número de clave:"?>
<br>
<input type="text" name="pin" value="">
<br>
<input type="submit" value="Log In">
</form>
                          </td>
                      </tr></tbody></table></td>
          <td width="1"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>
                <td valign="top" width="161" bgcolor="#d2d0c8"> 
                  <div style="padding: 4px;"><span> 
                    </span>
                    <table cellspacing="0" cellpadding="0" width="161" border="0">
                      <tbody>
                        <tr valign="top" align="left" bgcolor="#4d4325" height="29"> 
                          <td valign="center"> <div style="padding: 4px; font-weight: bold; color: rgb(255, 255, 255);">Quick 
                              Contact</div></td>
                        </tr>
                        <tr valign="top" align="left" bgcolor="#ffffff" height="1"> 
                          <td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>
                        </tr>
                        <tr valign="top" align="left"> 
                          <td> 
                            <div style="padding: 4px; color: rgb(77, 67, 37);">
                              <p><b>CDP Customer Service</b></p>
                                Monday - Friday<br>
                                8:00 AM - 5:00 PM<br>
				&nbsp;<br>
                                888-359-9906<br>
                                <a href="mailto:cdp@eckerd.edu">cdp@eckerd.edu</a></p>
                              </div></td>
                        </tr>
                      </tbody>
                    </table>
                    <span>
                    <p>
</p>
                    </span></div>    </td>
              </tr>
        <tr>
          <td bgcolor="#253355">
          </td><td>
          </td><td>
            <table height="30" cellspacing="0" cellpadding="0" width="450" border="0">
              <tbody>
              <tr bgcolor="#ffffff" height="1">
                <td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td></tr>
              <tr bgcolor="#253355" height="30">
                <td><img height="30" src="../index_files/pixel.gif" width="1" border="0"></td></tr></tbody></table></td>
          <td>
          </td><td bgcolor="#d2d0c8"></td></tr></tbody></table></td></tr>
  <tr valign="top" align="left" height="16">
    <td><img height="16" src="../index_files/pixel.gif" width="1" border="0"></td></tr>
  <tr valign="top" align="left">
    <td>
      <table cellspacing="0" cellpadding="0" width="750" border="0">
        <tbody>
        <tr valign="top" align="left">
          <td width="100%" colspan="2"><a href="http://www.eckerd.edu/"><img height="51" alt="Eckerd College" src="../index_files/site-logo.gif" width="159" border="0"></a></td>
          <td width="9" rowspan="3"><img height="1" src="../index_files/pixel.gif" width="9" border="0"></td>
                <td align="right" width="81" rowspan="3">
</td>
              </tr>
        <tr valign="top" align="left" height="8">
          <td width="100%" colspan="2"><img height="8" src="../index_files/pixel.gif" width="1" border="0"></td></tr>
        <tr valign="top" align="left">
          <td width="4"><img height="1" src="../index_files/pixel.gif" width="4" border="0"></td>
          <td class="footertext" width="100%">4200 54th Avenue South . St. 
            Petersburg, Florida 33711<br>(800) 456-9009 or (727) 867-1166<br><a href="http://www.eckerd.edu/about/index.php?f=contact">E-mail</a> | 
            <a href="http://www.eckerd.edu/departments/index.php?f=home">Directory</a> 
            | <a href="http://www.eckerd.edu/its/index.php?f=pprivacy">Privacy 
            Policy</a><br><br>Copyright 2004 Eckerd College. All rights 
            reserved.</td></tr></tbody></table></td></tr></tbody></table></center><br><!--Begin SiteStats Code Dec 16, 2002-->
<div class="ivanC10400569321381" id="ivanI10400569321381">
<script language="JavaScript" src="../index_files/10400569321381.txt">
<!--End SiteStats Code-->
</body>
</html>
</script><script><!--
 var jv=1.0;
//--></script>
<script language="Javascript1.1"><!--
 jv=1.1;
//--></script>
<script language="Javascript1.2"><!--
 jv=1.2;
//--></script>
<script language="Javascript1.3"><!--
 jv=1.3;
//--></script>
<script language="Javascript1.4"><!--
 jv=1.4;
//--></script>
<img src="../index_files/sitestats.gif" width="1" height="1" style="position: absolute;">
</div></body></html>


<?php
session_start();
if(empty($_SESSION['rid'])){
	die("You are not logged in");
}
$rid=$_SESSION['rid'];
if(empty($_SESSION['r_cid'])){
	die("You are not logged in");
}
$cid=$_SESSION['r_cid'];
$page=$_POST['page'];
if(strlen($page)<1){
	$page=1;
}
require_once "respfns.php";
$what=$_POST['what'];

if("save"==$what){
	// ensure that we have survey items as needed
	insertSurvey($cid,$rid,"2");

	$frompage=$_POST['frompage'];

	if("1"==$frompage||"2"==$frompage){
		// we came from the demographics page
		$data=array();
		$data[0]="";
		$data[1]=addslashes($_POST['dm1']);
		$data[2]=addslashes($_POST['dm2']);
		$data[3]=addslashes($_POST['dm3']);
		$data[4]=addslashes($_POST['dm4']);
		saveDemographics($rid,"2",$data);
	}
	else{
		// we came from any other page
		$keys=array();
		$values=array();
		$q1=getQ1($frompage,"2");
		$qN=getQn($frompage,"2");
		for($i=$q1;$i<=$qN;$i++){
			$item="resp$i";
			$val=$_POST[$item];
			if(strlen($val)>0){
				$keys[]=$i;
				$values[]=addslashes($val);
			}
		}
		saveItems($rid,"2",$keys,$values);
	}
}

writeHeader();
?>

<center>
<table width="750" border="0"><tr><td>

<form action="respsurvey.php" name="frm1"	id="frm1" method=POST>
<input name="page" type="hidden" value="<?=$page?>">
<input name="what" type="hidden" value="save">
<input name="cid" type="hidden" value="<?=$cid?>">
<input name="rid" type="hidden" value="<?=$rid?>">
<input name="frompage" type="hidden" value="<?=$page?>">
<?php
writeNavigation($page,"frm1","2","top");
writeRespondentSurvey($page,$rid,"frm1");
writeNavigation($page,"frm1","2","bottom");
?>
</form>
</td></tr></table>
</center>
<?php
writeFooter();
?>
<?php
// This is the home page for self only assessments
# The only place in the application where we create a session
session_start();
/* */
$lang_dir='../assess/language/';
if((isset($_REQUEST['lang']))&&($_REQUEST['lang']!="")){
  $lang=$_REQUEST['lang'];
  switch($lang){
    case 'de' :                                           //German
      require_once($lang_dir.'de.php');
      break;
    case 'es' :                                           //Spanish
      require_once($lang_dir.'es.php');
      break;      
    case 'fr' :                                           //French
    case 'fr-CA':                                         //French CA    
      require_once($lang_dir.'fr.php');
      break;
    case 'pt' :                                           //Portuguese
      require_once($lang_dir.'pt.php');
      break; 
    case 'en' :                                           //English
    default:
      require_once($lang_dir.'en.php');
      break;                   
  }
}else{
  require_once($lang_dir.'en.php');                       //English
}

$orgid=((isset($_REQUEST['orgid']))&&($_REQUEST['orgid']!=""))? $_REQUEST['orgid'] : 1;


switch($orgid){
  case 4 :
  case 3 :
    require_once("../custom/ICD/survey/template_text.php");
    break;  
  case 2 :
  case 1 :
  default:
    require_once("../custom/EKD/survey/template_text.php");
    break;
} 



if(!empty($_GET['lerr'])){
    $lerr=$_GET['lerr'];
    if("1"==$lerr){
		  $msg="$lerr. ".$lang['login_error_1'];
    }
    if("2"==$lerr){
		  $msg="$lerr. ".$lang['login_error_2'];
    }
}

// 4/18/08 - changed to only unset the rid so we don't log off the consultant or admin (cdz)
unset($_SESSION["rid"]);
include_once "../admin/default.php";  //Determines if the logins will be available
//session_unset(); 
$loginfrm =$lang['login_label_1']."<br>
           <input type=\"text\" name=\"uid\" value=\"\"><br>";
$loginfrm.=$lang['login_label_2']."<br>
           <input type=\"text\" name=\"pin\" value=\"\"><br>";
$loginfrm.='<input type="submit" value="'.$lang['login_button_1'].'">';
                      
$HTML=(!$systemON)? $systemMSG : $loginfrm;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head> 
    <title><?php echo $tmp['title']; ?></title> 
    <link href="<?php echo $tmp['css']; ?>" rel="stylesheet" type="text/css" />
  </head> 
  <body> 
    <div id="wrapper"> 
      <div id="main"> 
        <div id="banner"> 
          <h5><a href="#">&nbsp;</a></h5> 
    		  <div id="features"> 
            <img alt="Center for Conflict Dynamics" class="right" height="178" src="<?php echo $tmp['topright_img']; ?>" title="Center for Conflict Dynamics" width="306"/> 
          </div><!-- end features --> 
        </div><!-- end banner --> 
      <div id="content"> 
        <div id="left">
          <h1><?=$lang['login_header_1']?><span style="font-size:60%;">&nbsp;</span></h1> 
          <?=$msg?>
          <form name="frm1" action="selfonly.php" method=POST>
            <input type="hidden" name="s" value="1">
            <input type="hidden" name="lid" value="<?=$lang['lid']?>">
            <input type="hidden" name="lang" value="<?=$lang['lang']?>">            
            <?=$HTML?>
          </form></p>
        </div><!-- end left --> 
				<div id="right"> 
          <div class="feature">
              <?=$tmp['support']?>
          </div> 
			   </div><!-- end right --> 
			</div><!-- end content --> 
		</div><!-- end main --> 
		<div id="footer"> 
      <div id="extras"> 
        <div id="copyright"> 
          <p><?=$tmp['foot_copyright']?></p> 
        </div> 
        <!-- end copyright --> 
        <?php echo $tmp['foot_logo']; ?>
        <p><?=$tmp['foot_contact']?></p> 
      </div><!-- end extras --> 
		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>
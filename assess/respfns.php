<?php
// Functions for the self survey
require_once "../meta/dbfns.php";
require_once "../meta/survey.php";

// Write the page header for the survey
function writeHeader($title,$script=false){
  	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"; 
	  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">"; 
    echo "<head>"; 
    echo "<title>$title</title>"; 
    echo "<link href=\"../index_files/styles_backend.css\" rel=\"stylesheet\" type=\"text/css\" />"; 
	  if($script)
	    echo "$script"; 
    echo "</head>";
    echo "<body class=\"internal\" onload=\"positionFixeddiv()\" onscroll=\"repositionFixeddiv()\"> 
	       <div id=\"wrapper\"> 
		        <div id=\"main\"> 
 			        <div id=\"banner\"> 
                <h5><a href=\"../admin/home.php\">CCD Home</a></h5> 
                <h6>Home of the Conflict Dynamics Profile</h6> 
			        </div><!-- end banner --> 
			        <div id=\"content\">";	  
}
/*
function writeHeader(){
	echo "\n" . '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
	echo "\n" . '<html>';
	echo "\n" . '<head>';
	echo "\n" . '<title>Internet Assessment System - Respondent</title>';
	echo "\n" . '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
	echo "\n" . '<link title="standard" media="screen" href="../index_files/eckerd-1.css" type="text/css" rel="stylesheet">';
	echo "\n" . '<style>.ivanC10400569321381 {';
	echo "\n" . 'VISIBILITY: hidden; POSITION: absolute';
	echo "\n" . '}</style></head>';
	echo "\n" . '</head>';
  echo "\n" . '<body class=".body" onload="positionFixeddiv()" onscroll="repositionFixeddiv()">';
  echo "\n" . '<table cellspacing="0" cellpadding="0" width="100%" border="0">';
	echo "\n\t" . '<tbody>';
	echo "\n\t" . '<tr valign="top" align="left" height="1">';
  echo "\n\t\t" . '<td colspan="3"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
  echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr valign="top" align="right" bgcolor="#4f8d97" height="22">';
	echo "\n\t\t" . '<td colspan="3"></td>';
	echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr valign="top" align="left" height="1">';
	echo "\n\t\t" . '<td colspan="3"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
	echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr valign="center" height="42" bgcolor="#253355">';
	echo "\n\t\t" . '<td align="left"><img height="64" src="../index_files/eclogotop.gif" width="155" border="0"></td>';
	echo "\n\t\t" . '<td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
	echo "\n\t\t" . '<td align="right"><img height="64" src="../index_files/ldi-title.gif" width="430" border="0"></td>';
	echo "\n\t" . '</tr>';
	echo "\n" . '</table>';
	echo "\n" . '<br>&nbsp;</br>'; 
}
*/
// Write the page footer for the survey
function writeFooter(){
  echo"</div><!-- end content --> 
		  </div><!-- end main -->
		  <div id=\"footer\"> 
      <div id=\"extras\"> 
    <div id=\"copyright\"> 
    <p>Conflict Dynamics Profile is a registered trademark of Eckerd College</p> 
    </div> 
    <!-- end copyright --> 
    <a href=\"http://www.eckerd.edu\"><img alt=\"Eckerd College Home\" class=\"left\" height=\"66\" src=\"../index_files/foot_logo.gif\" title=\"Eckerd College Home\" width=\"207\"/></a> 
    <p>4200 54th Avenue South<br/> 
    St. Petersburg, Florida 33711<br/> 
    888-359-9906 | <a href=\"mailto:cdp@eckerd.edu\">cdp@eckerd.edu</a></p> 
    </div><!-- end extras --> 
 		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>"; 
}
/*
function writeFooter(){
	echo "&#169; 2005 Eckerd College</body></html>";
}
*/
// Write the navigation bar
function writeNavigation($page,$frm,$tid,$topOrBottom="top"){
	$pic=array();
	$lnk=array();
	
	$pic[0]="../images/prevE.gif";
	$pic[1]="../images/1f.gif";
	$pic[2]="../images/2f.gif";
	$pic[3]="../images/3f.gif";
	$pic[4]="../images/4f.gif";
	$pic[5]="../images/5f.gif";
	$pic[6]="../images/6f.gif";
	$pic[7]="../images/7f.gif";
	$pic[8]="../images/8f.gif";
	$pic[9]="../images/9f.gif";
	$pic[10]="../images/nextE.gif";

	$pp=$page-1;
	$np=$page+1;
	
	$lnk[0]="javascript:gotoPage($pp);";
	$lnk[1]="javascript:gotoPage(1);";
	$lnk[2]="javascript:gotoPage(2);";
	$lnk[3]="javascript:gotoPage(3);";
	$lnk[4]="javascript:gotoPage(4);";
	$lnk[5]="javascript:gotoPage(5);";
	$lnk[6]="javascript:gotoPage(6);";
	$lnk[7]="javascript:gotoPage(7);";
	$lnk[8]="javascript:gotoPage(8);";
	$lnk[9]="javascript:gotoPage(9);";
	// next page
	$lnk[10]="javascript:gotoPage($np);";
	
	
	// enable images and links based on the page we're on
	switch($page){
		case 1:
			$pic[0]="../images/prevD.gif";
			$pic[1]="../images/1n.gif";
			$lnk[0]="";
			break;
		case 2:
			$pic[2]="../images/2n.gif";
			break;
		case 3:
			$pic[3]="../images/3n.gif";
			break;
		case 4:
			$pic[4]="../images/4n.gif";
			break;
		case 5:
			$pic[5]="../images/5n.gif";
			break;
		case 6:
			$pic[6]="../images/6n.gif";
			break;
		case 7:
			$pic[7]="../images/7n.gif";
			break;
		case 8:
			$pic[8]="../images/8n.gif";
			break;
		case 9:
			$pic[9]="../images/9n.gif";
			// turn off next
			$pic[10]="";
			$lnk[10]="";
			break;
		default:
	}
	
	echo "<table border=0 cellspacing=0 cellpadding=0><tr>";
	$howMany=(9==$page)?10:11;
	for($i=0;$i<$howMany;$i++){
		echo "<td valign=top onClick=\"".$lnk[$i]."\">";
		echo "<img src='$pic[$i]'>";
		echo "</td>";
	}
	if($page!=9){
		echo "<td valign=top onClick=\"javascript:saveAndExit();\">&nbsp;&nbsp;<img src='../images/savex.gif'></td>";
	}
	
	echo "</tr></table>";

	if (strcasecmp($topOrBottom, "top") == 0) {
		// Only create the gotPage function if we are creating the top navigation menu.
		// That way it prevents multiple gotoPage() functions on one page.
		echo <<<EOD

<script lang="javascript">

function gotoPage( page ) {
  var $frm = document.getElementById("$frm");
	$frm.page.value = page;
	if (typeof validSelection != "undefined") {
		validSelection = true;
	}
	$frm.what.value = "save"; 
	$frm.action = "";
	$frm.submit();
}
	
function saveAndExit() {
  var $frm = document.getElementById("$frm");
	if (typeof validSelection == "undefined") {
		alert("Your answers will be saved and you can finish the survey later.");
	} else {
		alert("Your answers will be saved and you can finish the survey later.");
		validSelection = true;
	}
	$frm.what.value = "save";
	$frm.action = "respexit.php";
	$frm.submit();
}

</script>

EOD;
	}

}

function writeRespondentSurvey($page,$rid,$frm){
	if(1==$page||2==$page){
		showRespondentDemographics($rid,"2",$page);	
		$legendText = "";
	}
	elseif($page>=3 and $page<9){
		$legendText = showSurvey($rid,"2","1",$page);
	}
	elseif($page==9){
		showByeBye($rid,$frm);
		$legendText = "";
	}

	if (strlen($legendText) > 0) displayScrollingLegend($legendText);
	
}

function showByeBye($rid,$frm){
	$cnt=getNoAnswerCount($rid,"2");

	$showSubmitButton = false;

	//Chaged maximum unanswered questions allowed from 3 to 7 - JPC 04/04/2008
  if($cnt>7){
		echo "<p><u>Please address the following issues:</u> </p><p>&nbsp;<br>You have $cnt unanswered questions of 78</p>&nbsp;</p>";
		$unAnsw=getUnansweredItems($rid,"2");
		echo "<p><u>Unanswered Questions:</u></p><p>".implode($unAnsw,",")."</p>";
	}
	elseif($cnt>0){
		echo "<p><u>Are you sure you want to complete the survey?</u> </p><p>&nbsp;<br>You have $cnt unanswered questions of 78</p>&nbsp;</p>";
		$unAnsw=getUnansweredItems($rid,"2");
		echo "<p><u>Unanswered Questions:</u></p><p>".implode($unAnsw,",")."</p>";
		echo "<p> It is strongly recommended that you go back and answer all questions.<br>However, you may click on the 'Submit' button below to complete the survey.</p><p>";
		echo "<input type='button' value='Submit' onClick=\"javascript:submitForm(false);\" style=\"font:bold 24pt arial;margin:20px 0px 8px 0px;\">";
		echo "</p><p>&nbsp;</p>";
		$showSubmitButton = true;
	}
	else{
		echo "<p> Click on the 'Submit' button below to complete the survey.</p><p>";
		echo "<input type='button' value='Submit' onClick=\"javascript:submitForm(true);\" style=\"font:bold 24pt arial;margin:20px 0px 8px 0px;\">";
		echo "</p><p>&nbsp;</p>";
		$showSubmitButton = true;
	}


	if ($showSubmitButton) {
		echo <<<EOD

<script lang="javascript">

var validSelection = false;

window.onbeforeunload = confirmSelectionMade;

function submitForm( allComplete ) {
  var $frm = document.getElementById("$frm");
	if (allComplete) {
		var reply = confirm("By submitting the survey you are indicating that you are complete\\nand you will not be able to change any of your responses.\\n\\nAre you sure you want to continue?");
	} else {
		var reply = confirm("Not all questions have been answered.\\n\\nYou may still submit the survey, but after it is submitted\\nyou will not be able to change any of your responses.\\n\\nAre you sure you want to continue?");
	}
	
	if (!reply) {
		return false
	} else {
		validSelection = true;
		$frm.what.value = "finish";
		$frm.action = "respexit.php";
		$frm.submit();
	}
	
}

function confirmSelectionMade() {

  if (!validSelection) {
		return "You must click the 'Submit' button to complete the survey.\\n\\nIf you do not wish to submit your answers at this time then click 'OK' to continue.\\nOtherwise, click on 'Cancel' to remain on this page.";
  }
}

</script>

EOD;
	
	}

}

?>

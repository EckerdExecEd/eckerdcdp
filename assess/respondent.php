<?php
// Try and log in
$uid=addslashes($_POST['uid']);
$pin=addslashes($_POST['pin']);
require_once "../meta/rater.php";
require_once "usrfn.php";
session_start();
$done=raterLogin($uid,$pin);
if(!$done){
	// Login error - go back to the login page
	header("Location: index.php?lerr=1");
}
else{
	if($done=="Y"){
		// Already completed survey, no longer accessible
		header("Location: index.php?lerr=3");
	}
	else{
		$rid=$pin;	
		$cid=raterCandidate($rid);
		if(!$cid){
			// Login error - go back to the login page
			header("Location: index.php?lerr=1");
		}
		else{
		  if($rid==$cid){
		    header("Location: index.php?lerr=7");
		  }		
			$exp=getProgramStatus($rid);
			if(!$exp){
				header("Location: index.php?lerr=2");
			}
			else if($exp=="Y"){
				header("Location: index.php?lerr=2");
			}
			else{
				$test=getRaterTest($rid);
			  if($test==3){
			     header("Location: index.php?lerr=5");
			  }else{
			   // Finally we're logged in
				 $_SESSION['rid']=$rid;
				 $_SESSION['r_cid']=$cid;
				}
			}
		}
	}
}
// Hold it right there!
if(empty($_SESSION['rid'])){
	die("You are not logged in");	
}
if(empty($_SESSION['r_cid'])){
	die("You are not logged in");	
}
$rid=$_SESSION['rid'];
$candName=getCandName($rid);
  /*
	echo "\n" . '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
	echo "\n" . '<html>';
	echo "\n" . '<head>';
	echo "\n" . '<title>Internet Assessment System - Respondent</title>';
	echo "\n" . '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
	echo "\n" . '<link title="standard" media="screen" href="../index_files/eckerd-1.css" type="text/css" rel="stylesheet">';
	echo "\n" . '<style>.ivanC10400569321381 {';
	echo "\n" . 'VISIBILITY: hidden; POSITION: absolute';
	echo "\n" . '}</style></head>';
	echo "\n" . '<body class=".body">';
	echo "\n" . '<table cellspacing="0" cellpadding="0" width="100%" border="0">';
	echo "\n\t" . '<tbody>';
	echo "\n\t" . '<tr valign="top" align="left" height="1">';
	echo "\n\t\t" . '<td colspan="3"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
	echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr valign="top" align="right" bgcolor="#4f8d97" height="22">';
	echo "\n\t\t" . '<td colspan="3"></td>';
	echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr valign="top" align="left" height="1">';
	echo "\n\t\t" . '<td colspan="3"><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
	echo "\n\t" . '</tr>';
	echo "\n\t" . '<tr valign="center" height="42" bgcolor="#253355">';
	echo "\n\t\t" . '<td align="left"><img height="64" src="../index_files/eclogotop.gif" width="155" border="0"></td>';
	echo "\n\t\t" . '<td><img height="1" src="../index_files/pixel.gif" width="1" border="0"></td>';
	echo "\n\t\t" . '<td align="right"><img height="64" src="../index_files/ldi-title.gif" width="430" border="0"></td>';
	echo "\n\t" . '</tr>';
	echo "\n" . '</table>'; */
	writeHead("Internet Assessment System - Respondent");
	writeBody();
	echo "\n" . '<br>&nbsp;</br>'; 
?>

<center>
<table width="750"><tr><td>

<form action="" method=POST name="frm1">
<input type="hidden" name="page" value="1">
<p>
Interpersonal conflict is extremely common, both at home and in the workplace.<br>
When such conflicts arise, there are many different ways to react, and none of<br>
them is always right or always wrong. The following items ask you to rate<br>
<big><b><u><?=$candName?></u></b></big> in terms of the way he or she usually responds before,<br>
during, and after interpersonal conflicts occur.
</p>
<?php
switch (getRaterCatid($rid)) {
	case 3 : // Peer
		echo "<p>You are responding to this survey as a Peer of $candName<br />and all of your responses are confidential.  Your responses will not be included<br />in the report unless there are three or more raters.</p>";
		break;
	case 4 : // Direct Report
		echo "<p>You are responding to this survey as a Direct Report of $candName<br />and all of your responses are confidential.  Your responses will not be included<br />in the report unless there are three or more raters.</p>";
		break;
	default :
		break;
}
?>
<p>
<?php
require_once "../meta/survey.php";
$prompt="Start survey process";
$cnt=getNoAnswerCount($rid,"2");
if(0!=$cnt&&78>$cnt){
	echo "Welcome back!<br>You have not provided answers to the following questions:<br>";
	echo implode(getUnansweredItems($rid,"2"),",");
	$prompt="Continue survey";	
}
?>
</p>
<p>
<input type="button" onClick="javascript:frm1.action='respsurvey.php';frm1.submit();" value="<?=$prompt?>" >
&nbsp;&nbsp;
<input type="button" onClick="javascript:frm1.action='index.php';frm1.submit();" value="Exit">
</p>
</form>

</td></tr></table>
</center>
<?php
writeFooter();
?>
<?php
$f="CDP-I.xml";
$l="en";
$p=0;

$xml = simplexml_load_file($f);

$lang = fetchLanguage($xml,$l,$p);

foreach($lang as $k=>$v)
  echo "[$k] => $v<br />";

function fetchLanguage($xml,$l,$p){
  $lanArr=getLanguageCodes($xml,$l);
  $msgArr=getPageMessages($xml,$l,$p);
  $txtArr=getPageContent($xml,$l,$p);
  return array_merge((array)$lanArr,(array)$msgArr,(array)$txtArr);  
}

function getLanguageCodes($xml,$l){
  $retArr=array();
  $retArr['lid']=(string) $xml->{$l}->language->lid;
  $retArr['lang']=(string) $xml->{$l}->language->lang;  
  return $retArr;
}

function getPageMessages($xml,$l,$p){
  $msgArr=array();
  //Get the error messages
  foreach($xml->{$l}->content->pages->page[$p]->page_msgs->msg as $msg){
    $k=(string) $msg['msg_id'];
    $v= (string) $msg;
    $msgArr[$k]=$v;
  }
  return $msgArr;
}

function getPageContent($xml,$l,$p){
  $txtArr=array();
  //Get the page content
  foreach($xml->{$l}->content->pages->page[$p]->page_content->pg_text as $txt){
    $k=(string) $txt['txt_id'];
    $v= (string) $txt;
    $txtArr[$k]=$v;
  }
  return $txtArr;
}

?>
<?php
// Portuguese system text
$lang=array();
$lang['lid']=9;
$lang['lang']="pt";

// Translations for survey.php
$lang['login_error_1']="<font color='#aa0000'>Login errado <small><i>(<u>NB:</u> verifique que preencheu com o seu endereço email e com o seu código de acesso.)</i><small></font>";
$lang['login_error_2']="<font color='#00aa00'><small><i><u>NB:</u> Já acabou de responder a todas as questões. Obrigado!</i></small></font>";

$lang['login_header_1']="Login do inquérito de auto-avaliação";
$lang['login_label_1']="Preencha SFF com o seu endereço email:";
$lang['login_label_2']="Preencha SFF com o seu código de acesso (PIN):";
$lang['login_button_1']="Login";

$lang['login_support_header']="CDP Customer Service";
$lang['login_support_days']="Monday - Friday";
$lang['login_support_hours']="8:00 am - 4:00 pm";
$lang['login_support_phone']="888-359-9906";
$lang['login_support_email']="cdp@eckerd.edu";
?>
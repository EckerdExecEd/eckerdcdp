<?php
// German system text
$lang=array();
$lang['lid']=11;
$lang['lang']="de";

// Translations for survey.php
$lang['login_error_1']="<font color='#aa0000'>Invalid Login<small><i> (<u>Note:</u> Have you entered the correct email address and PIN?.)</i><small></font>";
$lang['login_error_2']="<font color='#00aa00'><i><u>Note:</u> You have already completed the assessment. Thank you!</i><small></font>";

$lang['login_header_1']="Login für Ihre Selbsteinschätzung";
$lang['login_label_1']="Bitte geben Sie die erhaltene e-mail-Adresse ein:";
$lang['login_label_2']="Ihre PIN:";
$lang['login_button_1']="Log In";

$lang['login_support_header']="CDP Customer Service";
$lang['login_support_days']="Monday - Friday";
$lang['login_support_hours']="8:00 am - 4:00 pm";
$lang['login_support_phone']="888-359-9906";
$lang['login_support_email']="cdp@eckerd.edu";

?>
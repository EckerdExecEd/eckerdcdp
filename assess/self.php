<?php
session_start();
if(empty($_SESSION['cid'])){
	die("You are not logged in");	
}
$cid=$_SESSION['cid'];
require_once "usrfn.php";
require_once "../meta/survey.php";
 /*
	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title>Internet Assessment System - Self</title>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
	echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">";
	echo "<style>.ivanC10400569321381 {";
	echo "VISIBILITY: hidden; POSITION: absolute";
	echo "}</style></head>";
    echo "<body class=\".body\">";
    echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" border=\"0\">";
    echo "<tbody>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"top\" align=\"right\" bgcolor=\"#4f8d97\" height=\"22\"><td colspan=3></td></tr>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"center\" height=\"42\" bgcolor=\"#253355\">";
    echo "<td align=\"left\"><img height=\"64\" src=\"../index_files/eclogotop.gif\" width=\"155\" border=\"0\"></td>";
    echo "<td><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td>";
    echo "<td align=\"right\"><img height=\"64\" src=\"../index_files/ldi-title.gif\" width=\"430\" border=\"0\"></td></tr>";
    echo "</table>";
*/
writeHead("nternet Assessment System - Self");
writeBody();
?>    

<center>
<table width="750"><tr><td>

<form action="" method=POST name="frm1">
<input type="hidden" name="page" value="1">
<p style="text-align:left;">
Interpersonal conflict is extremely common, both at home and in the workplace.<br>
When such conflicts arise, there are many different ways to react, and none of<br>
them is always right or always wrong.  The following items ask about the way<br> 
you usually respond before, during, and after interpersonal conflicts occur<br>
in your life.  Please answer each one as honestly and as accurately as you can.
</p>
<p>
<?php
$prompt="Start my survey process";
$cnt=getNoAnswerCount($cid,"1");
if(0!=$cnt&&114>$cnt){
	echo "Welcome back!<br>You have not provided answers to the following questions:<br>";
	echo implode(getUnansweredItems($cid,"1"),",");
	$prompt="Continue survey";	
}
?>
</p>
<p>
<input type="button" onClick="javascript:frm1.action='../usr/home.php';frm1.submit();" value="Back to menu">
&nbsp;&nbsp;
<input type="button" onClick="javascript:frm1.action='selfsurvey.php';frm1.submit();" value="<?=$prompt?>" >
</p>
</form>

</td></tr></table>
</center>
<?php
writeFooter();
?>
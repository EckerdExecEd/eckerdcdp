<?php
session_start();
if(isset($_POST['uid'])&&isset($_POST['pin'])){
	$uid=addslashes($_POST['uid']);
	$pin=addslashes($_POST['pin']);
}
else{
	// Not in the post - try the get
	$uid=addslashes($_GET['uid']);
	$pin=addslashes($_GET['pin']);
}
require_once "../meta/dbfns.php";
$done=mdLogin($uid,$pin);
if(!$done){
	// Login error - go back to the login page
	header("Location: index.php?lerr=1");
}
else{
	if($done=="Y"){
		// Already completed survey, no longer accessible
		header("Location: index.php?lerr=3");
	}
	else{
require_once "../meta/rater.php";
		$rid=$pin;	
		$cid=raterCandidate($rid);
		if(!$cid){
			// Login error - go back to the login page
			header("Location: index.php?lerr=1");
		}
		else{
			$exp=getProgramStatus($rid);
			if(!$exp){
				header("Location: index.php?lerr=2");
			}
			else if($exp=="Y"){
				header("Location: index.php?lerr=2");
			}
			else{
				// Finally we're logged in
				$_SESSION['rid']=$rid;
				$_SESSION['cid']=$cid;
			}
		}
	}
}
// Hold it right there!
if(empty($_SESSION['rid'])){
	die("You are not logged in");	
}
if(empty($_SESSION['cid'])){
	die("You are not logged in");	
}
$rid=$_SESSION['rid'];
$candName=getCandName($rid);

	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title>Internet Assessment System - Respondent</title>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
	echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">";
	echo "<style>.ivanC10400569321381 {";
	echo "VISIBILITY: hidden; POSITION: absolute";
	echo "}</style></head>";
    echo "<body class=\".body\">";
    echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"750\" border=\"0\">";
    echo "<tbody>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"top\" align=\"right\" bgcolor=\"#4f8d97\" height=\"22\"><td colspan=3></td></tr>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"center\" height=\"42\" bgcolor=\"#253355\">";
    echo "<td align=\"left\"><img height=\"64\" src=\"../index_files/eclogotop.gif\" width=\"155\" border=\"0\"></td>";
    echo "<td><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td>";
    echo "<td align=\"right\"><img height=\"64\" src=\"../index_files/ldi-title.gif\" width=\"430\" border=\"0\"></td></tr>";
    echo "</table><br>&nbsp;</br>"; 
?>
<form action="" method=POST name="frm1">
<input type="hidden" name="page" value="1">
<p>
Interpersonal conflict is extremely common, both at home and in the workplace.<br> 
When such conflicts arise, there are many different ways to react, and none of<br>
them is always right or always wrong. The following items ask you to rate <br>
<big><b><u><?=$candName?></u></b></big> in terms of the way he or she usually responds before,<br>
during, and after interpersonal conflicts occur. A few of the items will ask<br>
you about the person's thoughts or feelings.  You may find these questions<br>
somewhat difficult, but please do your best to answer these questions by<br> 
considering what the person says and does. Even if you are less confident about<br>
making these judgments, please answer all questions.Please answer each one as<br>
honestly and as accurately as you can.</p>
<p>
<?php
require_once "../meta/survey.php";
$prompt="Start survey process";
$cnt=getNoAnswerCount($rid,"2");
if(0!=$cnt&&78>$cnt){
	echo "Welcome back!<br>You have not provided answers to the following questions:<br>";
	echo implode(getUnansweredItems($rid,"2"),",");
	$prompt="Continue survey";	
}
?>
</p>
<p>
<input type="button" onClick="javascript:frm1.action='respsurvey.php';frm1.submit();" value="<?=$prompt?>" >
&nbsp;&nbsp;
<input type="button" onClick="javascript:frm1.action='index.php';frm1.submit();" value="Exit">
</p>
</form>
</body>
</html>
<?php

function mdLogin($uid,$pin){
	/*$conn=dbConnect();
	$query="select EXPIRED from RATER where UPPER(EMAIL)='".strtoupper(stripslashes($uid))."' and RID=$pin";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row?$row[0]:false;*/
	$mysqli=dbiConnect();
	if (!($query = $mysqli->prepare("select EXPIRED from RATER where UPPER(EMAIL)=? and RID=?"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_param("si", strtoupper(stripslashes($uid)), $pin)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if (!$query->execute()) {
		echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_result($out_EXPIRED)) {
		echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$row = $query->fetch();
	$query->close();
	if(!$row){
		return false;
	}else{
		return $out_EXPIRED;
	}
}

?>

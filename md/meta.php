<?php
define("MERCER_DELTA_CONS","Mercer-Delta");
define("TID_360","183");
define("TID_IND","175");
/*
// for development purposes
define("RETURN_URL","test.php");
define("RETURN_URL_SESS","test.php");
define("COMMON_MAILBOX","kiwi@presensit.com");
*/
	
// for deployment in production and /dev
define("RETURN_URL","https://elc.oliverwyman.com/pcs/surveyUpdateNew.asp");
define("RETURN_URL_SESS","https://elc.oliverwyman.com/pm/confirmReceive.asp");
define("COMMON_MAILBOX","rater.survey@oliverwyman.com");


// For multilingual surveys
define("ENGLISH",1);
define("SPANISH",8);
?>

<?php
// Add a Mercer Delta Session
require_once "meta.php";
require_once "../meta/dbfns.php";

$trace=true;

$msg="";
$err=false;
$pid=false;

//1. get all the POST data
$ProgramID=addslashes($_POST["ProgramID"]);
$programTitle=addslashes($_POST["programTitle"]);
$AssessmentID=addslashes($_POST["AssessmentID"]);
$AssessmentName=addslashes($_POST["AssessmentName"]);
$ContactEmail=addslashes($_POST["ContactEmail"]);
$fromAddress=addslashes($_POST["fromAddress"]);
$fromName=addslashes($_POST["fromName"]);
$client=addslashes($_POST["client"]);
$clientCode=addslashes($_POST["clientCode"]);
$bannerUrl=addslashes($_POST["bannerUrl"]);
$clientID=addslashes($_POST["clientID"]);
$programStartDate=mySQLDate(addslashes($_POST["programStartDate"]));
$raterRegStartDate=mySQLDate(addslashes($_POST["raterRegStartDate"]));
$raterRegEndDate=mySQLDate(addslashes($_POST["raterRegEndDate"]));
$raterInvitationDate=mySQLDate(addslashes($_POST["raterInvitationDate"]));
$surveyStartDate=mySQLDate(addslashes($_POST["surveyStartDate"]));
$surveyDueDate=mySQLDate(addslashes($_POST["surveyDueDate"]));
$surveyEndDate=mySQLDate(addslashes($_POST["surveyEndDate"]));
$initialReminderDate=mySQLDate(addslashes($_POST["initialReminderDate"]));
$finalReminderDate=mySQLDate(addslashes($_POST["finalReminderDate"]));
$reportShipDate=mySQLDate(addslashes($_POST["reportShipDate"]));
$groupReport=addslashes($_POST["groupReport"]);
$ReportTitle=addslashes($_POST["ReportTitle"]);
$ShipToFacility=addslashes($_POST["ShipToFacility"]);
$ShipToContact=addslashes($_POST["ShipToContact"]);
$ShipToStreet=addslashes($_POST["ShipToStreet"]);
$ShipToCity=addslashes($_POST["ShipToCity"]);
$ShipToState=addslashes($_POST["ShipToState"]);
$ShipToZip=addslashes($_POST["ShipToZip"]);
$ShipToCountry=addslashes($_POST["ShipToCountry"]);
$ShipToPhone=addslashes($_POST["ShipToPhone"]); 

//2. See if we need to add it
$add=sessionExists($ProgramID);

//3. If necessary add the session
if($add){
	
	if(strlen($AssessmentID)<1){
		// handle misspelled POST data
		$AssessmentID=addslashes($_POST["assessmentID"]);
	}
	
	// get the consultantt ID for the email if it exists, otherwise get the Mercer-Delta User
	$conid=getMDID($ContactEmail);
	$pgmData=array($programTitle,$programStartDate,$surveyEndDate,$conid,$AssessmentID);
	$pid=insertProgram($pgmData);
	if(false==$pid){
		$msg="Error adding Session - 1";
		$err=true;
	}
	else{
		if($trace){
			$msg="Added new CDP Program - OK<br>";
		}
		// If we're here we should create the Mercer-Delta specific stuff
		$rc=insertMDData($pid,$ProgramID);
		if(false==$rc){
			$msg=$msg."Error adding Session-2";
			$err=true;
		}
		else{
			if($trace){
				$msg=$msg."Added Mercer-Delta Session - OK<br>";
			}
		}
	}
}

//4. Populate with all these wonderful values if no errors
if(false==$err){
		$mdData=array($ProgramID,$programTitle,$AssessmentID,$AssessmentName,$ContactEmail,$fromAddress,$fromName,
			$client,$clientID,$clientCode,$programStartDate,$raterRegStartDate,$raterRegEndDate,
			$raterInvitationDate,$surveyStartDate,$surveyDueDate,$initialReminderDate,
			$finalReminderDate,$reportShipDate,$groupReport,$ReportTitle,$ShipToFacility,$ShipToContact,
			$ShipToStreet,$ShipToCity,$ShipToState,$ShipToZip,$ShipToCountry,$ShipToPhone,
			$bannerUrl,$surveyEndDate);
		$rc=saveMDData($mdData);
		if(false==$rc){
			$msg="Error saving Session data - 3";
			$err=true;
		}
}
// 09-20-2005 Ryan says: redirect to the mercer-delta url
$redirUrl=RETURN_URL_SESS;
$rc=(false==$err)?"success":"failure";

header("Location: $redirUrl?programId=$ProgramID&assessmentID=$AssessmentID&status=$rc");

// This stuff is just for testing and debugging
?>
<html>
<head>
<title>

</title>
</head>
<body>

<?php
//echo $msg."<br>";
if(false==$err){
	$data=getMDData($ProgramID);
	if(false!=$data){
?>
<table border=0 cellspacing=5>
<tr><th colspan=2>Saved Data Successfully</th></tr>
<?php
		if($trace){
?>
<tr><td align=left>ProgramID</td><td align=left><?=$data[0]?></td></tr>
<tr><td align=left>programTitle</td><td align=left><?=$data[1]?></td></tr>
<tr><td align=left>AssessmentID</td><td align=left><?=$data[2]?></td></tr>
<tr><td align=left>AssessmentName</td><td align=left><?=$data[3]?></td></tr>
<tr><td align=left>ContactEmail</td><td align=left><?=$data[4]?></td></tr>
<tr><td align=left>fromAddress</td><td align=left><?=$data[5]?></td></tr>
<tr><td align=left>fromName</td><td align=left><?=$data[6]?></td></tr>
<tr><td align=left>client</td><td align=left><?=$data[7]?></td></tr>
<tr><td align=left>clientCode</td><td align=left><?=$data[9]?></td></tr>
<tr><td align=left>bannerUrl</td><td align=left><?=$data[29]?></td></tr>
<tr><td align=left>clientID</td><td align=left><?=$data[8]?></td></tr>
<tr><td align=left>programStartDate</td><td align=left><?=$data[10]?></td></tr>
<tr><td align=left>raterRegStartDate</td><td align=left><?=$data[11]?></td></tr>
<tr><td align=left>raterRegEndDate</td><td align=left><?=$data[12]?></td></tr>
<tr><td align=left>raterInvitationDate</td><td align=left><?=$data[13]?></td></tr>
<tr><td align=left>surveyStartDate</td><td align=left><?=$data[14]?></td></tr>
<tr><td align=left>surveyDueDate</td><td align=left><?=$data[15]?></td></tr>
<tr><td align=left>surveyEndDate</td><td align=left><?=$data[30]?></td></tr>
<tr><td align=left>initialReminderDate</td><td align=left><?=$data[16]?></td></tr>
<tr><td align=left>finalReminderDate</td><td align=left><?=$data[17]?></td></tr>
<tr><td align=left>reportShipDate</td><td align=left><?=$data[18]?></td></tr>
<tr><td align=left>groupReport</td><td align=left><?=$data[19]?></td></tr>
<tr><td align=left>ReportTitle</td><td align=left><?=$data[20]?></td></tr>
<tr><td align=left>ShipToFacility</td><td align=left><?=$data[21]?></td></tr>
<tr><td align=left>ShipToContact</td><td align=left><?=$data[22]?></td></tr>
<tr><td align=left>ShipToStreet</td><td align=left><?=$data[23]?></td></tr>
<tr><td align=left>ShipToCity</td><td align=left><?=$data[24]?></td></tr>
<tr><td align=left>ShipToState</td><td align=left><?=$data[25]?></td></tr>
<tr><td align=left>ShipToZip</td><td align=left><?=$data[26]?></td></tr>
<tr><td align=left>ShipToCountry</td><td align=left><?=$data[27]?></td></tr>
<tr><td align=left>ShipToPhone</td><td align=left><?=$data[28]?></td></tr>
<tr><td align=left>PID</td><td align=left><?=$data[31]?></td></tr>
<?php
		}
?>
</table>
<?php
	}
	else{
		$err=true;
		echo "Unable to save session data";	
	}
}
?>

</body>
</html>

<?php

// convert a string to yyy-mm-dd format
function mySQLDate($day){
	$dt=strtotime($day);
	return strftime("%Y-%m-%d",$dt);
}

// see if this session already exists in the database
function sessionExists($ProgramID){
	$conn=dbConnect();
	$qry="select count(*) from MDData where programID=$ProgramID";
	$rs=mysql_query($qry);
	$row=mysql_fetch_row($rs);
	return $row[0]==0;
}

// get the consultantt ID for the email if it exists, 
// otherwise get the Mercer-Delta User
function getMDID($eMail){
	$conn=dbConnect();
	$qry="select CONID from CONSULTANT where EMAIL like '$eMail'";
	//echo $qry;
	$rs=mysql_query($qry);
	$row=mysql_fetch_row($rs);
	if(false==$row){
		$qry="select CONID from CONSULTANT where UID='".MERCER_DELTA_CONS."'";
		$rs=mysql_query($qry);
		//echo $qry;
		$row=mysql_fetch_row($rs);
	}
	return $row[0];
}

// inserts a CDP program and all the associated stuff
function insertProgram($pgmData){ 
	$conn=dbConnect();
	// Generate a random key, and check if it exists already
	// Keep on until we find a good one
	$i=mt_rand(1,9999999);
	$query="select * from PROGRAM where PID=".$i;
	$rs=mysql_query($query);
	while(mysql_fetch_row($rs)){
	    $i=mt_rand(1,9999999);
	    $query="select * from PROGRAM where PID=".$i;
	    $rs=mysql_query($query);
	}
	
	// Insert the PROGRAM row
	$query="insert into PROGRAM (PID,DESCR,EXPIRED,ARCHFLAG,STARTDT,ENDDT) values ($i,'$pgmData[0]','N','N','$pgmData[1]','$pgmData[2]')";
	if(!mysql_query($query)){
	    if($trace){
			echo $query."<br>";
		}
	    return false;
	}
	
	// Insert the PROGCONS row
	$query="insert into PROGCONS (PID,CONID) values ($i,$pgmData[3])";
	if(!mysql_query($query)){
	    if($trace){
			echo $query."<br>";
		}
	    return false;
	}
	
	// Insert the PROGINSTR row(s)
	$assessmentID=$pgmData[4];
	if($assessmentID==TID_360){
		// For 360
		if(false==instrInsert($i,"1")){
			return false;
		}
		if(false==instrInsert($i,"2")){
			return false;
		}
	}
	elseif($assessmentID==TID_IND){
		// For Individual
		if(false==instrInsert($i,"3")){
			return false;	
		}
	}
	else{
		// None of the above
		//echo "Unknown Assessment ID: $assessmentID";
		return false;
	}
	
	return $i;
}

// Insert the appropriate Instrument
function instrInsert($pid,$tid){ 
	$conn=dbConnect();
	
	// Insert the PROGINSTR row
	$query="insert into PROGINSTR (PID,TID,EXPIRED,EXPDT) values ($pid,$tid,'N',NULL)";
	if(!mysql_query($query)){
	    if($trace){
			//echo $query."<br>";
		}
	    return false;
	}
	
	return true;
}

// Add a new MDData Row plus the glue to tie it to a CDP Survey
function insertMDData($pid,$sid){
	$conn=dbConnect();
	$query="insert into MDData (programID,PID) values ($sid,$pid)";
	if(!mysql_query($query)){
	    if($trace){
			//echo $query."<br>";
		}
	    return false;
	}
	return true;
}

function saveMDData($data){
	$conn=dbConnect();
	$query="update MDData set ";
	$query=$query." programTitle='$data[1]',assessmentID=$data[2],assessmentName='$data[3]',contactEmail='$data[4]',fromAddress='$data[5]',fromName='$data[6]', ";
	$query=$query." company='$data[7]',companyID=$data[8],companyCode='$data[9]',programStartDate='$data[10]',programRegStartDate='$data[11]',programRegEndDate='$data[12]', ";
	$query=$query." raterInvitationDate='$data[13]',surveyStartDate='$data[14]',surveyDueDate='$data[15]',initialReminderDate='$data[16]', ";
	$query=$query." finalReminderDate='$data[17]',reportShipDate='$data[18]',groupReport='$data[19]',reportTitle='$data[20]',shipToFacility='$data[21]',ShipToContact='$data[22]', ";
	$query=$query." shipToStreet='$data[23]',shipToCity='$data[24]',shipToState='$data[25]',shipToZip='$data[26]',shipToCountry='$data[27]',ShipToPhone='$data[28]', ";
	// these two don't have any corresponding row in MD's own database, but they come across in the POST data so let's save them
	$query=$query." bannerUrl='$data[29]',surveyEndDate='$data[30]'";
	$query=$query." where programID=$data[0]";
	if(!mysql_query($query)){
   	    if($trace){
			//echo $query."<br>";
		}
	    return false;
	}
	return true;
}

function getMDData($id){
	$conn=dbConnect();
	$query=" select programID,programTitle,assessmentID,assessmentName,contactEmail,fromAddress,fromName, ";
	$query=$query." company,companyID,companyCode,programStartDate,programRegStartDate,programRegEndDate, ";
	$query=$query." raterInvitationDate,surveyStartDate,surveyDueDate,initialReminderDate, ";
	$query=$query." finalReminderDate,reportShipDate,groupReport,reportTitle,shipToFacility,ShipToContact, ";
	$query=$query." shipToStreet,shipToCity,shipToState,shipToZip,shipToCountry,ShipToPhone, ";
	// these two don't have any corresponding row in MD's own database, but they come across in the POST data so let's save them
	$query=$query." bannerUrl,surveyEndDate,PID";
	$query=$query." from MDData where programID=$id";
	$rs=mysql_query($query);
	if(false==$rs){
	    if($trace){
			//echo $query."<br>";
		}
	    return false;
	}
	return mysql_fetch_array($rs);
}

?>


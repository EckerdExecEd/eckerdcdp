<?php
session_start();
require_once "meta.php";
require_once "../meta/dbfns.php";
require_once "candfn.php";

$trace=false;

$msg="";
$err=false;

//1. get all the POST data
$participantID=addslashes($_POST["participantID"]);
$programID=addslashes($_POST["programID"]);
$title=addslashes($_POST["title"]);
$assessmentID=addslashes($_POST["assessmentID"]);
$assessmentName=addslashes($_POST["assessmentName"]);
$helpDeskEmail=addslashes($_POST["helpDeskEmail"]);
$client_ID=addslashes($_POST["Client_ID"]);
$gender=addslashes($_POST["gender"]);
$email=addslashes($_POST["email"]);
$workPhone=addslashes($_POST["workPhone"]);
$workFax=addslashes($_POST["workFax"]);
$name=addslashes($_POST["name"]);
$firstName=addslashes($_POST["firstName"]);
$lastName=addslashes($_POST["lastName"]);
$clientCode=addslashes($_POST["clientCode"]);
$cid=false;
//$type=addslashes($_POST["type"]);

//2. See if we need to add it
if(empty($_POST["participantID"])){
    die("Insufficient credentials to display page");
}
$add=candidateExists($participantID);

//3. If necessary add the candidate
if($add){
	$conid=getMDID();
	$other=(TID_360==$assessmentID)?"Y":"N";
	$pid=getPID($programID);
	$pgmData=array($pid,$firstName,$lastName,$email,"Y",$other,$conid);
	echo "<br>";
	$cid=candInsert($pgmData);
	if(false==$cid){
		$msg="Error adding CDP Candidate";
		$err=true;
	}
	else{
		if($trace){
			if($trace)
				$msg="Added new CDP candidate - OK<br>";
		}
		// If we're here we should create the Mercer-Delta specific stuff
		$candData=array($participantID,$programID,$title,$assessmentID,$assessmentName,$helpDeskEmail,$client_ID,$gender,$email,
			$workPhone,$workFax,$name,$firstName,$lastName,$clientCode,$cid);

		$rc=insertMDParticipant($candData);
		if(false==$rc){
			$msg=$msg."Error adding Mercer-Delta Participant";
			$err=true;
		}
		else{
			if($trace){
				if($trace)
					$msg=$msg."Added Mercer-Delta Participant - OK<br>";
			}
		}
	}
}
writeHead("Conflict Dynamics Profile",false);
writeBody("Rater Registration",$msg);
?>

<?php
if(false==$err){
	// If we're here everything went well.
	// make sure we have the CID, we should have it if we added but
	// we could be returning to an existing participant
	$cid=getCID($participantID);
	$_SESSION['cid']=$cid;
	
	$backURL=RETURN_URL;
//	$backURL="test_showpost.php";

?>	
<!-- 06012006: set type=RR in query string -->
<form action="raterhome.php?type=RR" name="frm1" method=POST>
<input type="hidden" name="rid" value="">
<input type="hidden" name="catid" value="">
<input type="hidden" name="what" value="">
<input type="hidden" name="type" value="RR">
<input type="hidden" name="completed" value="false">
<input type="hidden" name="AssessmentID" value="<?=$assessmentID?>">
<input type="hidden" name="ProgramID" value="<?=$programID?>">
<input type="hidden" name="ParticipantID" value="<?=$participantID?>">
<p><big>Welcome <?=getName($participantID)?></big></p>
<p>
If you're not the person whose name appears above, please contact <a href="mailto:clientparticipantsupport@oliverwyman.com">Survey Support</a> at<br>
866-399-0996 (US) or 001-503-419-5364 (International).
</p>
<p>
Before selecting your raters, you are strongly encouraged to review the
<a href="raterhint.php" target="tips">Rater Selection Tips</a>.
</p>
<p>
To begin your Rater Selection, click on the <b>Select Raters</b> button below. When<br>
you have finished selecting your raters you must submit your final list by indicating<br> 
<b>Save as Final Selection</b>.
</p>
<p>
If you are unable to finish your Rater Selection during this session, you may exit<br>
by clicking the <b>Logout</b> button. If you log out your selections will be saved but<br>
marked as incomplete until you log in and click the <b>Save as Final Selection</b> button.
</p>
<p>
<input type=submit value="Select Raters">
</p>
<p>
<input type=button value="Logout" onClick="javascript:frm1.action='<?=$backURL?>';frm1.submit();">
</p>
</form>
<?
}
else{
	echo "<br>An error ocurred. Unable to proceed.<br>";
//
//	foreach($_POST as $key => $val){
//		echo "$key -- $val<br>";	
//	}
}
writeFooter(false);

// convert a string to yyy-mm-dd format
function mySQLDate($day){
	$dt=strtotime($day);
	return strftime("%Y-%m-%d",$dt);
}
// see if this Mercer-Delta candidate already exists in the database
function candidateExists($partID){
	$conn=dbConnect();
	$query="select count(*) from MDParticipant where participantID=$partID";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0]==0;
}

// get the Mercer-Delta automated user
function getMDID(){
	$conn=dbConnect();
	$rs=mysql_query("select CONID from CONSULTANT where UID='".MERCER_DELTA_CONS."'");
	$row=mysql_fetch_row($rs);
	return $row[0];
}

// Insert a new candidate    
function candInsert($candData){ 
	$conn=dbConnect();
	// This looks funny, but is correct since self is also a RATER
	$i=getKey("RATER","RID");
	// Insert the CANDIDATE row
	$query="insert into CANDIDATE (CID,PID,FNAME,LNAME,EMAIL,EXPIRED,ARCHFLAG,SELF,OTH) values ( $i,$candData[0],'$candData[1]','$candData[2]','$candData[3]','N','N','$candData[4]','$candData[5]')";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	// Insert the CANDCONS row
	$query="insert into CANDCONS (CID,CONID) values ( $i,$candData[6])";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return insertSelfRater($i);
}
    
// insert the self rater
function insertSelfRater($cid){
	$conn=dbConnect();
	$query="insert into RATER (RID,CID,CATID,FNAM,LNAM,EMAIL,STARTDT,ENDDT,EXPIRED) select CID,CID,1,FNAME,LNAME,EMAIL,STARTDT,ENDDT,EXPIRED from CANDIDATE where CID=$cid";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return insertSelfSurvey($cid);
}
    
// insert the self survey
function insertSelfSurvey($cid){
	$conn=dbConnect();
	$query="insert into RATERINSTR (RID,TID,EXPIRED) select RID,1,EXPIRED from RATER where CID=$cid and CATID=1";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return $cid;
}

// Insert the Mercer-Delta participant
function insertMDParticipant($data){
	$conn=dbConnect();
	$query="Insert into MDParticipant (participantID,programID,title,assessmentID,assessmentName,helpDeskEmail,Client_ID,gender,email, ";
	$query=$query."workPhone,workFax,name,firstName,lastName,clientCode,CID) ";
	$query=$query." values ($data[0],$data[1],'$data[2]',$data[3],'$data[4]','$data[5]',$data[6],'$data[7]','$data[8]', ";
	$query=$query."'$data[9]','$data[10]','$data[11]','$data[12]','$data[13]','$data[14]',$data[15]) ";
	if(false==mysql_query($query)){
		echo $query."<br>";
		return false;
	}
	return true;
}

// get the CID for a participant
function getCID($partID){
	$conn=dbConnect();
	$query="select CID from MDParticipant where participantID=$partID ";
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query."<br>";
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row[0];
}

// get the PID for a program
function getPID($programID){
	$conn=dbConnect();
	$query="select PID from MDData where programID=$programID ";
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query."<br>";
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row[0];
}

//
function getName($partID){
	$conn=dbConnect();
	$query="select name, firstName, lastName from MDParticipant where participantID=$partID ";
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query."<br>";
		return false;
	}
	$row=mysql_fetch_array($rs);
	return (0<strlen($row[0]))?$row[0]:$row[1]." ".$row[2];
}

?>


<?php

if (!$_COOKIE['sessionid'] && $_SERVER['SERVER_NAME'] != 'www.onlinecdp.org') {
// redirect to your main site
header('Location: https://www.onlinecdp.org'.$_SERVER['REQUEST_URI']);
}
/**/

error_reporting(E_ALL);
//set_time_limit(0);

$pid="111222333";
$post_action=basename(__FILE__,'');
$fileStr="";
$errMsg="";

header('Content-Type: text/html; charset=utf-8');
//Check to see if we have a file we can use.
echo  $_SERVER['REQUEST_URI']."<hr />";   


if(($_POST['submit']=="Upload")&&($_FILES['pfile']['name']!="")){
  include 'PHPExcel/IOFactory.php';
  $allowedExts = array("xlsx", "xls", "csv");
  $extension = end(explode(".", $_FILES['pfile']['name']));
  $fileStr = showFileData($extension);
  //Check to see if the extension is one we can use.
  if(in_array(strtolower($extension),$allowedExts)){
    //Process File
    $bdir = "/home/ptl/classes";
    $uploadedFile = $bdir.'/uploads/participants_' . $pid .'.'.strtolower($extension);
    //Is this an uploadable file?
    if(is_uploaded_file($_FILES['pfile']['tmp_name'])){
      //Have we moved it over to the server directory?
      if (!move_uploaded_file($_FILES['pfile']['tmp_name'], $uploadedFile)) {
  			$errMsg.='<div style="color:red;">File '.$_FILES['pfile']['tmp_name'].' could not be uploaded to '.$uploadedFile.'.</div>';
  		}else{
  		  $newParticipants=getUploadParticipants($uploadedFile, true);
  		  //die(print_r($newParticipants));
  		}
  	}else{
  	   $errMsg.='<div style="color:red;">File '.$_FILES['pfile']['tmp_name'].' is not an uploadable file.</div>';
  	}
		
  }else{
    $errMsg.="$extension files are not able to be used for upload.<br /> Please use a CSV, XLS, or XLSX file.";
  }
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="generator" content="PSPad editor, www.pspad.com">
  <title>Pat's Sandbox</title>
  </head>
  <body>
   <p>File Upload Test<br /><?php echo $errMsg;?></p>
   <form action="<?php echo $post_action;?>" method="post" enctype="multipart/form-data">
      <label for="file">Filename:</label><br />
      <input type="file" name="pfile" size="40" id="file"><br>
      <input type="submit" name="submit" value="Upload">
  </form>
  <hr />
  <?php
  
    foreach($_POST as $k=>$v)
      echo "<div>$k => $v</div>";
      echo $fileStr;
  /**/
  ?>
  </body>
</html>
<?php

function getUploadParticipants($uploadedFile,$dbug=false){
  //Now lets try reading the file.

  $inputFileType = PHPExcel_IOFactory::identify($uploadedFile);  
  $objReader = PHPExcel_IOFactory::createReader($inputFileType);
  $objPHPExcel = $objReader->load($uploadedFile);
  $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

  $lbs=false;
  $dbugTxt='<hr />'.$inputFileType.'<hr />';
  $r=0;
  $columns=array('A'=>'firstname','B'=>'lastname','C'=>'emailid','D'=>'error'); 
  $newParticipants = array();
  $newEmailids = array();
          
  foreach($sheetData as $row){
    $newDuplicateEmail = false;

    //clean data from file removing spaces, new lines, and tabs
    $fname=ereg_replace("[\r\n\t\ ?]", "", trim($row['A']));
    $lname=ereg_replace("[\r\n\t\ ?]", "", trim($row['B']));
    $emailid=ereg_replace("[\r\n\t\ ?]", "", trim($row['C']));
    //die("fname = $fname | lname = $lname | emailid = $emailid ");
    
    if((strlen(trim($fname)) > 0)||(strlen(trim($lname)) > 0)||(strlen(trim($emailid)> 0))){
      $dbugTxt.="<div>";  
      //Add to newParticipants array
      $newParticipants[$r][$columns['A']]=$fname;
      $newParticipants[$r][$columns['B']]=$lname;
      $newParticipants[$r][$columns['C']]=$emailid;
      $newParticipants[$r][$columns['D']]='';
     
      //die("<hr />".print_r($newParticipants));
                           
      //Check for duplicate email addresses in previous records
      if (in_array(strtolower($emailid), $newEmailids)) {
  				$newDuplicateEmail = true;
  		}
      
      $newEmailids[] = strtolower($emailid);
   
      // Missing first name  
  		if(!strlen($newParticipants[$r]['firstname'])) $newParticipants[$r]['error'] .= 'f';
      // Missing last name
	    if(!strlen($newParticipants[$r]['lastname'])) $newParticipants[$r]['error'] .= 'l';
      // Missing emailid
    	if(!strlen($newParticipants[$r]['emailid'])) $newParticipants[$r]['error'] .= 'e';
  		// Invalid emailid
 /*  		if(!validEmail($newParticipants[$r]['emailid'])) $newParticipants[$r]['error'] .= 'i';
 */     // Duplicate email
      if($newDuplicateEmail) $newParticipants[$r]['error'] .= 'd'; 
     
      $dbugTxt.="<span style=\"color:#0000FF;font-weight:normal;\">[".$columns['A']."] => ".$newParticipants[$r]['firstname']."</span> |";
      $dbugTxt.="<span style=\"color:#0000FF;font-weight:normal;\">[".$columns['B']."] => ".$newParticipants[$r]['lastname']."</span> |";
      $dbugTxt.="<span style=\"color:#0000FF;font-weight:normal;\">[".$columns['C']."] => ".$newParticipants[$r]['emailid']."</span> |";
      $dbugTxt.="<span style=\"color:#0000FF;font-weight:normal;\">[".$columns['D']."] => ".$newParticipants[$r]['error']."</span> |";            
      $dbugTxt.="</div>";
      $r++;
    }else{
      
    }
    
  }
  if($dbug)
    echo $dbugTxt;
    
    //die(print_r($newParticipants));
  return $newParticipants;
}

function showFileData($extension){
  $str="";
  if ($_FILES["pfile"]["error"] > 0){
    $str.= "Error: " . $_FILES["pfile"]["error"] . "<br>";
  }else{
    $str.= "Upload: " . $_FILES["pfile"]["name"] . "<br>";
    $str.= "Type: " . $_FILES["pfile"]["type"] . "<br>";
    $str.= "Size: " . ($_FILES["pfile"]["size"] / 1024) . " kB<br>";
    $str.= "Stored in: " . $_FILES["pfile"]["tmp_name"]. "<br>";
    $str.= "File extension is: ".$extension. "<br>";
  }
  return $str;  
}
?>
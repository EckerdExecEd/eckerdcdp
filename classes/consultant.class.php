<?php
require_once '../meta/dbfns.php';

class consultant{
  private $conid = false;
  private $pwd = false;
  public $uid = false;
  public $fname = false;
  public $lname = false; 
  public $email = false; 
  public $active = false;
  public $orgid = false;
  public $state = false;
  public $taxexempt = false;
  
  //define consultant
  function getConsultant($conid){
    $query="select CONID,FNAME,LNAME,EMAIL,UID,PWD,ACTIVE,ORGID,STATE,TAXEXEMPT from CONSULTANT where CONID=$conid";
    $rs=fetchArray($query);
    
    if($rs[0]['CONID']==$conid){
     $lp=$rs[0];
      foreach($lp as $k=>$v){
        $var=strtolower($k);       
        $this->$var=$v;
      }
      return true;
    }
    return false;
  }

  function changeConPwd($conid,$pwd){
	 $qry="update CONSULTANT set PWD=PASSWORD('$pwd') where CONID=$conid";
    return executeQurey($qry,true);
  }  
}
?>
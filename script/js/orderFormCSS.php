<?php
header("Content-Type: text/css");
$conid=$_GET['conid'];
$bkg=($conid==9545061)? "#fff" : "#fff";

echo "
.orderForm {
  font-family: Verdana;
  width: 100%;
  border: 0px solid #888;
  padding: 0.5em;
  margin: 0.5em 0.5em;
  background-color: #f1f1e0;  
}
.highlight {
  font-weight: bold;
  font-style: italic;
  border: 1px solid #888;
  padding: 0.5em;
  margin: 0.5em 0;
  background-color: $bkg;
}
.orderFormHeader {
  color:royalblue;
  font-weight:bold;
  font-size:11px;
}
.activeQty{
  color:#000000;
}
.inactiveQty{
  color:#CCCCCC;
}
.requiredfield{
  color:#990000;
  font-weight:bold;
  font-size:11px;  
}
.optionalfield{
  color:royalblue;
  font-weight:bold;
  font-size:11px;  
}
.inactivefield{
  color:gray;
  font-weight:lighter;
  font-size:11px;
}
";
?>

$(document).ready(function() {

  //Ensure only numeric on following inputs
  $('#ordid').numeric();
   
  $('#srchbut').click(function() {
    var ckd=false;
    var msg='';
    var wstr='';
    
    $('input:checked').each(function() {
      option=$(this).val();
      switch(option)
      {
        case 'orderid':
            if($('#ordid').val().length<1)
              msg+='You need to enter a value for Order ID.\n';
            else
              wstr+='##ordid^'+$('#ordid').val(); 
            break;
        case 'date':
            if(!/^(?=\d)(?:(?:(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})|(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))|(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2}))($|\ (?=\d)))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/.test($('#bdate').val()))
              msg+='Please use the valid format for From Date. mm/dd/yyyy\n';
            if(($('#edate').val().length>0)&&(!/^(?=\d)(?:(?:(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})|(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))|(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2}))($|\ (?=\d)))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/.test($('#edate').val())))
              msg+='Please use the valid format for To Date. mm/dd/yyyy\n';            
            if(msg.length<1){
              wstr+='##bdate^'+$('#bdate').val();
              wstr+=($('#edate').val().length>0)? '##edate^'+$('#edate').val() : '';
            }            
           break;        
        case 'status':
           break;        
        case 'conid':
           break;                                    
        default:
            msg+='Search option for '+$(this).val()+' is not defined.\n';
      }
      
      ckd=true;
     });
     
    if(!ckd)
      alert('You need to check at least 1 search parameter');
      
    if(msg.length > 0)
      alert(msg);
    
    if((ckd)&&(msg.length==0)&&(wstr.length>0)){
      $('#where').val(wstr);
      $('#srchform').submit();
    }
  });
  
  $('#resetbut').click(function() {
    $('input:checked').each(function() {
        $(this).attr('checked', false);
    });
    $('#ordid,#bdate,#edate').val('');
  });  
});


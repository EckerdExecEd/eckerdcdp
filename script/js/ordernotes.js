$(document).ready(function() {
  $('#addNote').click(function() {
    var subOK=true;
    if($('#ordnote').val().length < 5){
        alert('Please Add a note in the NOTE: field');
        subOK=false;
    }
    if($('#notetkr').val().length < 2){
       alert('Please enter name of Note creator');
       subOK=false;
    }
    if(subOK){
      $('#what').val('ADD');
      $('#notesfrm').submit();
    }
  });
});
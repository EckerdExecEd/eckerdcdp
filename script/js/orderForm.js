$(document).ready(function() {
  /* */ 
  var taxableStates = new Array("FL");
  var taxRates = new Array(.07, .675);
  var smsg = "";
   $("#cdpOrdFrm").submit(function() {
     //CHECK AND PREVENT double submit
     $('input[type=submit]', this).attr('disabled', 'disabled');  

     //CHECK to see if shipping is required
     var shipRequired = ($("input[name='noShip']:checked").val()=='Y')? true : false;
          
     //VALIDATE FORM DATA BERFORE SUBMITAL
     var ok2Sub = true;
      //Step 1 - Verify that all required SHIPPING Data is present 
        //Check Addr1
        if(!$.validateShipping()){
          smsg='Problem with shipping data';
          ok2Sub = false; 
        }
      //Step 2 - Check to see if the order should be tax exempt
      var taxExmpt = $('#taxExempt').val();
      var conState = $('#conState').val();
      var taxEIN = $('#taxExId').val();
      var StateTaxable = ($.inArray(conState,taxableStates)>-1)? true : false;
      //alert("taxExempt = "+taxExmpt+" and conState = "+conState+" and taxEIN = "+taxEIN);
      if((StateTaxable)&&(taxExmpt=='Y')&&(taxEIN.length!=10))
       {
        var taxEIN=prompt("You have not entered a Employer Identification Number required for tax exemption. Add one before completeing this order or taxes will be applied","Tax EIN"); 
        if((taxEIN!=null)&&(taxEIN!="")&&(taxEIN.length==10)){
          $('#taxExId').val(taxEIN);  
       }
      else if((taxEIN.length>0)&&(taxEIN.length!=10)&&(taxEIN!='Tax EIN'))
       {
          var useTax1=confirm("This order will be taxed. If you entered an EIN and still see this message, select cancel and try again.");
          if(useTax1!=true){
            $('input[type=submit]', this).attr('disabled', false);
            return false;
          }
       }
      else
       {
          var useTax2=confirm("This order will be taxed.");
          if(useTax2!=true){
            $('input[type=submit]', this).attr('disabled', false);
            return false;
          }        
        }
      }

      //Step 3 - Calculate shipping using Shipping Data
      var shipChk=$.calcOrderShipping();
      
      if($('#shipTTLamt').val()==0){
        ok2Sub =(!shipRequired)?  true : false;
        smsg=(!ok2Sub)? 'No shipping charges found' : ''; 
        //ok2Sub = false; 
      }
      
        
      if(!ok2Sub){
        if(smsg.length>0) alert(smsg);
        $('input[type=submit]', this).attr('disabled', false); 
        return false;
      }else{
        $('#cnfOrder').val('Y');
        //alert('We want to evaluate the submit');
        return true;
      }
   }); 
       
   //Purchase Licenses Fieldset
   $('.plfieldset,.dgfieldset,.shipfieldset,.ordTtlfieldset').addClass('highlight');
   $('.cdpOrderForm').addClass('orderForm');

   //default prices accordingly   
   $('#cdp360price1,#cdp360price50,#cdp360price100').fadeTo("slow", 1.00);   
   $('#cdpIprice1,#cdpIprice50,#cdpIprice100').fadeTo("slow", 1.00);
   $('#cdpIPPprice1,#cdpIPPprice50,#cdpIPPprice100').fadeTo("slow", 1.00);
   $('#cdpBCCprice1,#cdpBCCprice100').fadeTo("slow", 1.00);
      
   //disable Shipping fields until needed
   //$('#addr1,#addr2,#country,#city,#state,#zip,#attn,#phone,#email,#upsSrvc,.calcShip').attr('disabled', true); 
         
   //Ensure only numeric on following inputs
   $('.plInput').numeric();
   $('.fgInput').numeric();

   //OnChange code for fieldset CDP Licenses  
   $('#licCDP360,#licCDPI,#licCDPIPP,#licCDP360G,#licCDPIG,#licBCC').change(function(){
      var ttlPriceLic=0;
      var ttlWgtPL=0;
      //Check to see if there is a value, if not set of zero
      if(!$('#licCDP360').val().length){ $('#licCDP360').val(0); }
      if(!$('#licCDPI').val().length){ $('#licCDPI').val(0); } 
      if(!$('#licCDPIPP').val().length){ $('#licCDPIPP').val(0); }       
      if(!$('#licCDP360G').val().length){ $('#licCDP360G').val(0); }
      if(!$('#licCDPIG').val().length){ $('#licCDPIG').val(0); }
      if(!$('#licBCC').val().length){ $('#licBCC').val(0); } 
            
      //Set quantity variables for each field
      var qty360 = $('#licCDP360').val();
      var qtyI = $('#licCDPI').val();
      var qtyCDPIPP = $('#licCDPIPP').val();
      var qty360G = $('#licCDP360G').val(); 
      var qtyIG = $('#licCDPIG').val();
      var qtyBCC = $('#licBCC').val();
            
      //Set price variables for each field
      var cdp360price = $('#cdp360price1').attr('price');
      var cdp360wgt = $('#cdp360price1').attr('weight');
      if( $('#cdp360price50').length ){
         if(qty360 == 0){ //Set Qty Price Display to default
          $('#cdp360price1,#cdp360price50,#cdp360price100').fadeTo("slow", 1.00);
          $('#licCDP360key').val(0);
         }
         if(qty360 > 0 && qty360 <= 49){ 
          $('#cdp360price1').fadeTo("slow", 1.00);
          $('#licCDP360key').val($('#cdp360price1').attr('prdkey'));       
          $('#cdp360price50,#cdp360price100').fadeTo("slow", 0.25);    
         }
         if(qty360 >= 50 && qty360 <= 99){
          cdp360price = $('#cdp360price50').attr('price');
          $('#cdp360price1,#cdp360price100').fadeTo("slow", 0.25); 
          $('#cdp360price50').fadeTo("slow", 1.00);
          $('#licCDP360key').val($('#cdp360price50').attr('prdkey'));             
       
         }
         if(qty360 >= 100){
          cdp360price = $('#cdp360price100').attr('price');
          $('#cdp360price1,#cdp360price50').fadeTo("slow", 0.25);       
          $('#cdp360price100').fadeTo("slow", 1.00); 
          $('#licCDP360key').val($('#cdp360price100').attr('prdkey'));                         
         } 
      }else{
         if(qty360 > 0){
            //alert('Change product key');
            $('#licCDP360key').val($('#cdp360price1').attr('prdkey'));          
         }else
            $('#licCDP360key').val(0);
      }
      ttlPriceLic+=(qty360 * cdp360price);
      ttlWgtPL+=(qty360 * cdp360wgt);
      
      
      var cdpIprice = $('#cdpIprice1').attr('price');
      var cdpIwgt = $('#cdpIprice1').attr('weight');      
      if( $('#cdpIprice50').length ){
         if(qtyI == 0){
          $('#cdpIprice1,#cdpIprice50,#cdpIprice100').fadeTo("slow", 1.00);
          $('#licCDPIkey').val(0);
         }
         if(qtyI > 0 && qtyI <= 49){
          $('#cdpIprice1').fadeTo("slow", 1.00);       
          $('#licCDPIkey').val($('#cdpIprice1').attr('prdkey'));
          $('#cdpIprice50,#cdpIprice100').fadeTo("slow", 0.25);            
         }
         if(qtyI >= 50 && qtyI <= 99){
          cdpIprice = $('#cdpIprice50').attr('price');
          $('#cdpIprice50').fadeTo("slow", 1.00);
          $('#licCDPIkey').val($('#cdpIprice50').attr('prdkey'));          
          $('#cdpIprice1,#cdpIprice100').fadeTo("slow", 0.25);
         }
         if(qtyI >= 100){
          cdpIprice = $('#cdpIprice100').attr('price');
          $('#cdpIprice100').fadeTo("slow", 1.00);  
          $('#licCDPIkey').val($('#cdpIprice100').attr('prdkey'));          
          $('#cdpIprice1,#cdpIprice50').fadeTo("slow", 0.25);       
         }
      }else{
         if(qtyI > 0){
            $('#licCDPIkey').val($('#cdpIprice1').attr('prdkey'));         
         }else
            $('#licCDPIkey').val(0);
      }
      
      ttlPriceLic+=(qtyI * cdpIprice);
      ttlWgtPL+=(qtyI * cdpIwgt);

      var cdpIPPprice = $('#cdpIPPprice1').attr('price');
      var cdpIPPwgt = $('#cdpIPPprice1').attr('weight');      
      if( $('#cdpIPPprice50').length ){
         if(qtyCDPIPP == 0){
          $('#cdpIPPprice1,#cdpIPPprice50,#cdpIPPprice100').fadeTo("slow", 1.00);
          $('#licCDPIPPkey').val(0);
         }
         if(qtyCDPIPP > 0 && qtyCDPIPP <= 49){
          $('#cdpIPPprice1').fadeTo("slow", 1.00);       
          $('#licCDPIPPkey').val($('#cdpIPPprice1').attr('prdkey'));
          $('#cdpIPPprice50,#cdpIPPprice100').fadeTo("slow", 0.25);            
         }
         if(qtyCDPIPP >= 50 && qtyCDPIPP <= 99){
          cdpIPPprice = $('#cdpIPPprice50').attr('price');
          $('#cdpIPPprice50').fadeTo("slow", 1.00);
          $('#licCDPIPPkey').val($('#cdpIPPprice50').attr('prdkey'));          
          $('#cdpIPPprice1,#cdpIPPprice100').fadeTo("slow", 0.25);
         }
         if(qtyCDPIPP >= 100){
          cdpIPPprice = $('#cdpIPPprice100').attr('price');
          $('#cdpIPPprice100').fadeTo("slow", 1.00);  
          $('#licCDPIPPkey').val($('#cdpIPPprice100').attr('prdkey'));          
          $('#cdpIPPprice1,#cdpIPPprice50').fadeTo("slow", 0.25);       
         }
      }else{
         if(qtyCDPIPP > 0){
            $('#licCDPIPPkey').val($('#cdpIPPprice1').attr('prdkey'));         
         }else
            $('#licCDPIPPkey').val(0);
      }
      
      ttlPriceLic+=(qtyCDPIPP * cdpIPPprice);
      ttlWgtPL+=(qtyCDPIPP * cdpIPPwgt);

      // BCC Materials
      var cdpBCCprice = $('#cdpBCCprice1').attr('price');
      var cdpBCCwgt = $('#cdpBCCprice1').attr('weight');
      if( $('#cdpBCCprice100').length ){
         if(qtyBCC == 0){
          $('#cdpBCCprice1,#cdpBCCprice100').fadeTo("slow", 1.00);
          $('#licBCCkey').val(0);
         }
        if(qtyBCC > 0 && qtyBCC <= 99){
          $('#cdpBCCprice1').fadeTo("slow", 1.00);       
          $('#licBCCkey').val($('#cdpBCCprice1').attr('prdkey'));
          $('#cdpBCCprice100').fadeTo("slow", 0.25);            
         }
        if(qtyBCC >= 100){
          cdpBCCprice = $('#cdpBCCprice100').attr('price');
          $('#cdpBCCprice100').fadeTo("slow", 1.00);  
          $('#licBCCkey').val($('#cdpBCCprice100').attr('prdkey'));          
          $('#cdpBCCprice1').fadeTo("slow", 0.25);       
         }                              
      }else{
        if(qtyBCC > 0){
            $('#licBCCkey').val($('#cdpBCCprice1').attr('prdkey'));         
         }else
            $('#licBCCkey').val(0);      
      }
      
      ttlPriceLic+=(qtyBCC * cdpBCCprice);
      ttlWgtPL+=(qtyBCC * cdpBCCwgt);
           
      var cdp360Gprice = $('#cdp360Gprice1').attr('price');
      var cdp360Gwgt = $('#cdp360Gprice1').attr('weight');
      ttlPriceLic+=(qty360G * cdp360Gprice);
      if(qty360G==0){
        $('#licCDP360Gkey').val(0);        
      }else{
        $('#licCDP360Gkey').val($('#cdp360Gprice1').attr('prdkey'));
      }     
      ttlWgtPL+=(qty360G * cdp360Gwgt);   
         
      var cdpIGprice = $('#cdpIGprice1').attr('price');
      var cdpIGwgt = $('#cdpIGprice1').attr('weight');      
      ttlPriceLic+=(qtyIG * cdpIGprice); 
      if(qtyIG==0){
        $('#licCDPIGkey').val(0);        
      }else{
        $('#licCDPIGkey').val($('#cdpIGprice1').attr('prdkey'));
      }            
      ttlWgtPL+=(qtyIG * cdpIGwgt);       
     
       $('#plTTLck').val(ttlPriceLic);
       
       $('#shipPLwgt').val(ttlWgtPL);   
       $.calcOrderWgt()       
   })


   //OnChange code for fieldset Development Guides
  $('#fgCDP360,#fgCDPI,#fgCDPCTK,#fgCDPTTK,#fgCDPCTTK').change(function(){
    var ttlPriceFG=0;
    var ttlWgtFG=0;
    
    //Check to see if there is a value, if not set of zero
    if(!$('#fgCDP360').val().length){ $('#fgCDP360').val(0); }
    if(!$('#fgCDPI').val().length){ $('#fgCDPI').val(0); }
    if(!$('#fgCDPCTK').val().length){ $('#fgCDPCTK').val(0); }
    if(!$('#fgCDPTTK').val().length){ $('#fgCDPTTK').val(0); }                    
    if(!$('#fgCDPCTTK').val().length){ $('#fgCDPCTTK').val(0); }

    //Set quatity variables for each field
    var qty360fg = $('#fgCDP360').val();
    var qtyIfg = $('#fgCDPI').val();
    var qtyCTKfg = $('#fgCDPCTK').val();
    var qtyTTKfg = $('#fgCDPTTK').val();    
    var qtyCTTKfg = $('#fgCDPCTTK').val(); 
    
    
    var cdp360FGprice = $('#cdp360FGprice').attr('price');
    ttlPriceFG+=(qty360fg * cdp360FGprice);
    ttlWgtFG+=(qty360fg * ($('#cdp360FGprice').attr('weight')));
    var cdpIFGprice = $('#cdpIFGprice').attr('price');
    ttlPriceFG+=(qtyIfg * cdpIFGprice); 
    ttlWgtFG+=(qtyIfg * ($('#cdpIFGprice').attr('weight')));   
    var cdpCTKFGprice = $('#cdpCTKFGprice').attr('price');
    ttlPriceFG+=(qtyCTKfg * cdpCTKFGprice); 
    ttlWgtFG+=(qtyCTKfg * ($('#cdpCTKFGprice').attr('weight')));        
    var cdpTTKFGprice = $('#cdpTTKFGprice').attr('price');
    ttlPriceFG+=(qtyTTKfg * cdpTTKFGprice);  
    ttlWgtFG+=(qtyTTKfg * ($('#cdpTTKFGprice').attr('weight')));       
    var cdpCTTKFGprice = $('#cdpCTTKFGprice').attr('price');
    ttlPriceFG+=(qtyCTTKfg * cdpCTTKFGprice);
    ttlWgtFG+=(qtyCTTKfg * ($('#cdpCTTKFGprice').attr('weight')));
    
    $('#shipFGwgt').val(ttlWgtFG); 
    $.calcOrderWgt()
    
    if(parseInt(ttlPriceFG) > 0){
      //$('#addr1,#addr2,#country,#city,#state,#zip,#attn,#phone,#email,#upsSrvc,.calcShip').removeAttr('disabled');
    }else{
      //$('#addr1,#addr2,#country,#city,#state,#zip,#attn,#phone,#email,#upsSrvc,.calcShip').attr('disabled', true);
    }    
    
    $('#fgTotal').val(ttlPriceFG); 
    
    var fgTTL = $('#fgTotal').val();
    //alert('Test val = '+test);
    $('#fgTTLck').val(fgTTL);
       
  })
  
  $("input").change(function(){
  //Vars for Totals Fieldset
    //Vars for SUBTOTAL
     var ordsubTtl = 0;
     var aa = 0;
     var bb = 0;
    //Vars for TOTAL
    
    //Calculate SUBTOTAL ------------------------------------------------------- 
    if(!$('#plTTLck').val().length){
      alert('plTTLck has no length');
    }else{
      aa = $('#plTTLck').val();
    }
    if(!$('#fgTTLck').val().length){
      alert('fgTTLck has no length');
    }else{
      bb = $('#fgTTLck').val();
    }   
    ordSubTtl = (parseInt(aa) + parseInt(bb)).toFixed(2);
    $('#ordSubtotal').html('$'+ordSubTtl);
    $('#ordSubTTLamt').val(ordSubTtl);
    //--------------------------------------------------------------------------

    //Calculate TOTAL ----------------------------------------------------------
     $.calcformtotal();
    //--------------------------------------------------------------------------
    //alert('ordSubTtl = '+ordSubTtl);
  })
  
  $('#country').change(function(){
    var countryid = $('#country').val();
    var eleid = 'state';
     $.ajax({
         type: "POST",
         url: "../ajax/getStateProvince.php",
         data: "countryid="+countryid+"&eleid="+eleid,
         success: function(msg){
           $('#stateSEL').html(msg);
         }
       });  /**/  
  })

  //No shipping radiobutton=====================================================
  $("input[name='noShip']").click(function(){
    if($("input[name='noShip']:checked").val()=='N'){
        var noHandling = 0;
        alert('You have chosen to not have any printed field guides shipped.');
        $('#handTTLamt').val(parseFloat(noHandling).toFixed(2));
        $('#ordHandling').html('$'+noHandling.toFixed(2));
        $('#shipTTLamt').val(0);
        $('#ordShipping').html('$0.00');
        $.calcTaxes(); 
        $.calcformtotal();       
    }else{
        $.calcHandling();
        $.calcOrderShipping();
        $.calcformtotal();
    } 
  })
/* */ 
  //============================================================================

  //Calculate shipping rate=====================================================
  $('.calcShip').click(function(){
      $.calcOrderShipping();
  })
  //============================================================================
  
  //Reset Button functionality==================================================
  $('.resetBut').click(function(){
    var okReset=confirm('Are you sure you want to reset the order form?');
    if(okReset){
      //Reset all quantity inputs to zero    
      $('#licCDP360,#licCDP360key,#licCDPI,#licCDPIPP,#licCDPIkey,#licCDP360G,#licCDPIG,#licBCC').val(0);
      $('#cdp360price1,#cdp360price50,#cdp360price100').fadeTo("slow", 1.00);   
      $('#cdpIprice1,#cdpIprice50,#cdpIprice100').fadeTo("slow", 1.00);      
      $('#cdpIPPprice1,#cdpIPPprice50,#cdpIPPprice100').fadeTo("slow", 1.00); 
      
      $('#fgCDP360,#fgCDPI,#fgCDPCTK,#fgCDPTTK,#fgCDPCTTK').val(0);
     
      //$('#addr1,#addr2,#country,#city,#state,#zip,#attn,#phone,#email,#upsSrvc,.calcShip').attr('disabled', true);         
      
      $('#zip,#city').val('');
      
      $('#plTTLck,#fgTTLck,#shipTTLwgt,#ordSubTTLamt,#shipPLwgt,#shipFGwgt,#shipTTLwgt,#shipTTLamt,#handTTLamt,#taxTTLamt,#taxRate,#ordTTLamt').val(0);

      $('#ordSubtotal,#ordShipping,#ordHandling,#ordTaxes').html('$0.00');
      $.calcformtotal();
     }     
  })
  //============================================================================

  //Order Button functionality==================================================
/*
  $('.orderBut').click(function(){
    $("form#cdpOrdFrm").submit();
    
  })
*/
  //============================================================================

});

jQuery.calcOrderShipping = function(){
    //We need the following data in order to calculate shipping costs.
    var Ok2GetRate = true;
    var amsg='You need the following to calculate shipping:\n';
    
    var need2ship =($("input[name='noShip']:checked").val()=='Y')?  true : false;
    if(need2ship){
        //receipt zip
        if(!$.validateShipping()){
          Ok2GetRate = false;
        }else{
          var tocountry=$('#country').val();
          var tozip=$('#zip').val();
        }  
        //UPS service code
        if(!$('#upsSrvc').val().length){
          Ok2GetRate = false;
          amsg+='UPS Service\n';    
        }else
          var upssrv=$('#upsSrvc').val();
          
        //package weight
        var wgt=$('#shipTTLwgt').val();
        if(wgt==0){
          alert('No weight to calculate shipping! Please make sure\nthere is a quantity entered in the Licenses/Guides sections.');
          return false;
        }  
        //Make ajax call to getUPSrate.php
        if(Ok2GetRate){
          
          $.ajax({
             type: "POST",
             url: "../ajax/getUPSrate.php",
             async: false,             
             data: "tocountry="+tocountry+"&tozip="+tozip+"&upssrv="+upssrv+"&wgt="+wgt,
             success: function(msg){
               var amt = (parseFloat(msg, 10) * 1.05).toFixed(2);
               $('#shipTTLamt').val(amt);
               $('#ordShipping').html('$'+amt);
               $.calcTaxes();
               $.calcformtotal();
               alert( "Shipping rate: $" + amt);           
             }
           });
           return true;  
        }else{
          alert(amsg);
          return false;
        } 
      }else{ 
         //alert('We do not need to ship');                    
         $('#shipTTLamt').val(0);
         $('#ordShipping').html('$0.00');
         //Zero out handling here!! 
         $.calcTaxes();
         $.calcformtotal(); 
         return true;        
      }   
}

jQuery.calcformtotal = function(){
  var ordertotal = 0;
  var subttl = parseFloat($('#ordSubTTLamt').val());
  var shipttl = parseFloat($('#shipTTLamt').val());
  var handlingttl = parseFloat($('#handTTLamt').val());
  var taxamt = parseFloat($('#taxTTLamt').val());
  ordertotal=(subttl + shipttl + handlingttl + taxamt).toFixed(2);
  
  $('#ordTotal').html('$'+ordertotal);
  $('#ordTTLamt').val(ordertotal);
}

jQuery.calcOrderWgt = function(){
  var orderweight = 0;
  var plWgt = parseFloat($('#shipPLwgt').val());
  var fgWgt = parseFloat($('#shipFGwgt').val());  
  orderweight=(plWgt + fgWgt);
  $('#shipTTLwgt').val(orderweight);
  $.calcHandling();
  //alert('Total weight for shipping is '+orderweight+'Lbs');
}

jQuery.calcHandling = function(){
  var orderwgt=parseFloat($('#shipTTLwgt').val());
  var orderHandling = 0;
  var need2ship =($("input[name='noShip']:checked").val()=='Y')?  true : false;
  if((orderwgt > 0)&&(need2ship)){
    orderHandling=(orderwgt >= 10)? 10.00 : 4.50;
  }
  var qty = 0;
  $('.plInput').each(function() { qty += parseFloat($(this).val(), 10); });
  if ((qty > 0) && (need2ship)) {
    orderHandling += qty * 0.12;
    var qtyPP = parseFloat($('#licCDPIPP').val(), 10);
    if (qtyPP > 0) {
      orderHandling += qtyPP * 0.45;
    }
  }
  $('#handTTLamt').val(parseFloat(orderHandling).toFixed(2));
  
  $('#ordHandling').html('$'+orderHandling.toFixed(2)); 
}

jQuery.calcTaxes = function(){

  //var destinationState = $('#state').val();
  var conState = $('#conState').val();
  var taxExempt = $('#taxExempt').val();
  var orderSubtotal = $('#ordSubTTLamt').val();
  var taxes = 0;
  var rate = 0;
  //Taxable states
  if(conState=='FL'){
    if((taxExempt=='N')||$('#taxExId').val().length!=10){
      taxes = (parseFloat(orderSubtotal) * .07).toFixed(2);
      rate='7.0';    
    } 
  }  
  
  //taxes = taxes.toFixed(2);
  
  //alert('Con state is '+conState+'\\ntaxExempt is '+taxExempt+'\\nTaxes should be '+taxes);
  $('#ordTaxes').html('$'+taxes);
  $('#taxTTLamt').val(taxes);
  $('#taxRate').val(rate);
  
}

jQuery.validateShipping = function(){
  var formdataValid=true;
  var err="";
  var valstreet_address = new RegExp("[pP]{1}[.]*[oO]{1}[.]*[ ]*[bB]{1}[oO]{1}[xX]{1}");
  var cnt=1;
  var need2ship =($("input[name='noShip']:checked").val()=='Y')?  true : false;
  if(need2ship){
    //Make sure all required fields have values
      if($('#ship2').val().length < 2){
        formdataValid=false;
        err+=cnt+") Please provide a Ship To name.\n";
        cnt++;
      } 
      //Address 1 (has value, not a PO Box)
      if($('#addr1').val().length < 5){
        formdataValid=false;
        err+=cnt+") Invalid Street Address\n";
        cnt++;
      }
      if((formdataValid)&&(!$.nopobox())) {
        result = valstreet_address.exec($('#addr1').val());
        if(result && result.length > 0 && result[0].length > 0){
          formdataValid=false;  
          err+=cnt+") No PO boxes allowed\n";
          cnt++;
        }        
      }
      //Country Selected
      if(($('#country').val().length != 2)||(($('#country').val()!= 'US')&&($('#country').val()!= 'CA'))){
          formdataValid=false;
          err+=cnt+") Please select a country.\n";   
          cnt++;
      }else{ 
          //Zip codes
          if($('#country').val()== 'US'){
            if($('#zip').val().length != 5){
                formdataValid=false;
                err+=cnt+") Please enter 5 digit zip code.\n";   
                cnt++;    
            }        
          }
          if($('#country').val()== 'CA'){
            if($('#zip').val().length != 6){
                formdataValid=false;
                err+=cnt+") Please enter a 6 character zip code.\n";   
                cnt++;    
            }        
          }        
      } 
      //City (has value)
      if($('#city').val().length < 2){
          formdataValid=false;
          err+=cnt+") Please enter a city.\n";   
          cnt++;    
      }
      //State (has value)
      if($('#state').val().length != 2){
          formdataValid=false;
          err+=cnt+") Please select a state/province.\n";   
          cnt++;    
      }
  
      //Tax EIN
      if(($('#conState').val()=='FL')&&($('#taxExempt').val()=='Y')){
        if($('#taxExId').val().length!=10){
/*
          var taxOK=confirm('You have not entered a Employer Identification Number required for tax exemption. Do you wish to add one before completeing this order?');
          if(taxOK){
            formdataValid=false;
            return formdataValid;
          }
*/
          //err+=cnt+") Please enter an Employer Identification Number for tax exemption.\n";   
          //cnt++;      
        }
      }    
      //Phone
      if($('#phone').val().length < 10){
          formdataValid=false;
          err+=cnt+") Please enter a phone number with area code.\n";   
          cnt++;    
      }    
      
     if(!formdataValid){
      err="The following inputs must be corrected for shipping:\n"+err;
      alert(err);
     }
   }else{
      //if(formdataValid) alert('validateShipping returning true');
        
   }
   return formdataValid;
}

jQuery.nopobox = function(){
  var value = $('#addr1').val();
  return false;
}

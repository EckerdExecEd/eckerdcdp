$(document).ready(function() {
  $('#selectAll').click(function() {
     /*   alert('test');   */
    $('input[name=order]').attr('checked', true)
  });
  
  $('#selectNone').click(function() {
     /*   alert('test');   */
    $('input[name=order]').attr('checked', false)
  });

  $('#makeCSV').click(function() {
     var ordids='';
     var i=0;
     var num=0;
     $("input[name=order]").each(function(){
        if(this.checked){
          ordids+=(i>0)? ","+this.value : this.value;
          i++;
        }
     });
     if(ordids.length>0){
        var ck=confirm("Are you sure you want the following orders included in this upload file\n"+ordids);
        if(ck){
           $('#sOrderIds').val(ordids);
           $('#what').val('s');
           $('#bannerForm').submit();
        }
    
      }
  });  
 
});
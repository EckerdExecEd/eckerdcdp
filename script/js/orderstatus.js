$(document).ready(function() {
  $('#addStat').click(function() {
    var subOK=true;
    if($('#ordStatus').val().length < 6){
        alert('Please select a valid status');
        subOK=false;
    }
    if($('#statusdt').val().length < 18){
       alert('Please provide a valid date (mm/dd/yyyy hh:mmAM)\nValid date string is exactly 18 characters long.\n This string is only '+$('#statusdt').val().length+' characters long.');
       subOK=false;
    }
    if(subOK){
      $('#what').val('ADD');
      $('#statusfrm').submit();
    }
  });
});
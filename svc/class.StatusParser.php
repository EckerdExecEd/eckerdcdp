<?php
// Parser the CCL Initial request

// 11-30-2006: Jonathan says to use requestorkey and vendorkey across the board
// 12-11-2006: Jonathan gets 810 errors. They come from mismatches on requestorkey and vendorkey
// Changed query so as not to consider requestorkey

require_once "class.TagStack.php";
require_once "class.Credentials.php";
require_once "class.WaitTime.php";
require_once "../meta/dbfns.php";

class StatusParser{

	var $tags;
	var $creds;
	var $status=0;
	var $vendorkey;
	var $requestorkey;
	var $wait=0;
	
	function getWait(){
		return $this->wait;
	}
	
	function getStatus(){
		return $this->status;
	}

	function getRequestorkey(){
		return $this->requestorkey;
	}

	function getCDPkey(){
		return $this->vendorkey;
	}

	// constructor
	function StatusParser(){
		$this->tags= new TagStack();
		$this->creds=new Credentials();
	}
	
		// The three required parser functions
	function start_element($parser,$tag,$attributes){
		// as long aa we habve no errors
		if(0==$this->status){
			$this->tags->push($tag);
			$this->attr=$attributes;
			// debugging
//			echo "\nStart of: ".$this->tags->top()." nesting ".$this->tags->count();
		}
	}
	
	function end_element($parser,$tag){
		// as long as we have no errors
		if(0==$this->status){
			if("soap:Envelope"==$tag){
				// the entire message has been parsed
				// process the collected data
				$this->process();
			}
//			echo "\nEnd of: ".$this->tags->top()." nesting ".$this->tags->count();
			unset($this->attr);
			$this->tags->pop();
		}
	}
	
	function character_data($parser,$data){
		// as long aa we have no errors
		if(0==$this->status){
			$data=trim($data);
			$data=mysql_escape_string($data);
			$tag=$this->tags->top();
			if("Login"==$tag){
				$this->creds->setUID($data);
				$this->checkCredentials();
			}
			elseif("Password"==$tag){
				$this->creds->setPWD($data);
				$this->checkCredentials();
			}
			elseif("requestorkey"==$tag){
				$this->requestorkey=$data;
			}
			elseif("vendorkey"==$tag){
				$this->vendorkey=$data;
			}
		}
	}

	function checkCredentials(){
		if($this->creds->hasCredentials()){
			$this->status=$this->creds->verify();
		}
	}

	function process(){
		if($this->status==0){
			$conn=dbConnect();
			// 12/11/2006
//			$query="select count(*) from CANDIDATE where CID=".$this->vendorkey." and ALTID='".$this->requestorkey."'";
			$query="select count(*) from CANDIDATE where CID=".$this->vendorkey;
//			echo "\n$query\n";			
			$rs=mysql_query($query);
			if(false==$rs){
				$this->status=901;
			}
			else{
				$row=mysql_fetch_row($rs);
				if(false==$row){
					$this->status=902;
				}
				elseif(0==$row[0]){
					$this->status=810;
				}
			}
		}
	}

	function soapResponse(){
		if($this->status==0){
			// Set status to OK
			$this->status=1;
			// And get estimated wait time
			$wt=new WaitTime();
			$this->wait=$wt->getStatusWaitTime($this->vendorkey);
		}
		return "<?xml version=\"1.0\"?>
			<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
				<soap:Body>
					<Status_RequestResponse>
						<Status_RequestResult>
							<requestorkey>".$this->requestorkey."</requestorkey>
							<!-- CDP system generated unique key -->
							<vendorkey>".$this->vendorkey."</vendorkey> 
							<statuscode>".$this->status."</statuscode>
							<waittime>".$this->wait."</waittime>
						</Status_RequestResult>
					</Status_RequestResponse>
				</soap:Body>
			</soap:Envelope>";
	}
}
?>

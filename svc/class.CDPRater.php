<?php
require_once "../meta/rater.php";
require_once "../meta/survey.php";
class CDPRater{
	
	var $cid;
	var $demo;
	var $form;
	var $catid;
	var $err=0;
	var $tid;
	
	function getError(){
		return $this->err;
	}

	function CDPRater($cid,$tid,$demo,$form){
		$this->cid=$cid;
		$this->catid=$demo[0];
		$this->demo=$demo;
		$this->form=$form;
		$this->tid=$tid;
	}
/*
	function process(){
		// If this is anybody other than self we need to create a rater
		$rid=false;
		if(1!=$this->catid){
			// this is other than self
			// leave some fields blank since we don't have the data
			$ratData=array($this->cid,$this->catid,'','','',2);
			$rid=addRater($ratData);
		}
		else{
			// this is self
			// we already have the Self rater, so we don't have to add
			// her
			$rid=$this->cid;
		}
		if(false==$rid){
			$this->err=920;
			return false;
		}
		
		// we now have a valid rater
		// 1. Process demographics data
		if(false==$this->addDemogr($rid)){
			$this->err=921;
			return false;
		}
		
		// 2. Process the items
		if(false==$this->addResponse($rid)){
			$this->err=922;
			return false;
		}
		
		// 3. Process the comments
		if(false==$this->addComment($rid)){			
			$this->err=923;
			return false;
		}

		// Mark it as complete
		if(false==$this->expireTheRater($rid)){
			$this->err=924;
			return false;
		}
		
		return $rid;
	}
*/
	function process( $rid=false ){
      if (TEST_MSGS) echo "<li>CDPRater->process()";
      if (TEST_MSGS) { echo "<li>.... this->rid = "; var_dump($this->rid); }
      if (TEST_MSGS) { echo "<li>.... this->catid = "; var_dump($this->catid); }
		$this->rid=$rid;

		// If this is anybody other than self we need to create a rater

		if(1!=$this->catid){
			// this is other than self
			// leave some fields blank since we don't have the data
         if (TEST_MSGS) echo " <b>non-self</b>";
			$ratData=array($this->cid,$this->catid,'','','',2);
			$rid=addRater($ratData);
		}
		else{
			// this is self
			// we already have the Self rater, so we don't have to add
			// her
         if (TEST_MSGS) echo " <b>self</b>";
         $conn=dbConnect();
         $query = "SELECT COUNT(RID) FROM `RATER` WHERE CID = " . $this->cid;
         //echo "<br />QUERY: $query";
         $rs=mysql_query($query);
         $row=mysql_fetch_array($rs);
         if ($row["cnt"] == 0) {
            if (TEST_MSGS) echo ' <b style="color:green;">NEW</b>';
            $query = "SELECT FNAME, LNAME, EMAIL FROM `CANDIDATE` WHERE CID = " . $this->cid;
            $rs=mysql_query($query);
            if ($row=mysql_fetch_array($rs)) {
               if (TEST_MSGS) echo "<li>........ adding self rater " . $row["FNAME"] . " " . $row["LNAME"] . "  (" . $row["EMAIL"] . ")";
            	$ratData=array($this->cid,$this->catid,$row["FNAME"],$row["LNAME"],$row["EMAIL"],2);
            	$rid=addRater($ratData, $this->cid);
            } else {
               if (TEST_MSGS) echo "<li>........ adding self rater [no name]";
         	   $ratData=array($this->cid,$this->catid,'','','',2);
            	$rid=addRater($ratData, $this->cid);
            }
         	$this->rid = $rid;
         } else {			
            if (TEST_MSGS) echo ' <b style="color:red;">EXISTING</b>';
		     	$rid=$this->cid;
      		$this->rid = $rid;
         }
         if (TEST_MSGS) { echo "<li>.... rid = "; var_dump($rid); }
         if (TEST_MSGS) { echo "<li>.... this->rid = "; var_dump($this->rid); }
         if (TEST_MSGS) { echo "<li>.... this->catid = "; var_dump($this->catid); }
		}
		if(false==$rid){
         if (TEST_MSGS) echo '<h4 style="color:red;">ERR 920</h4>';
			$this->err=920;
			return false;
		}
		
		// we now have a valid rater
		// 1. Process demographics data
		if(false==$this->addDemogr($rid)){
         if (TEST_MSGS) echo '<h4 style="color:red;">ERR 921</h4>';
			$this->err=921;
			return false;
		}
		
		// 2. Process the items
		if(false==$this->addResponse($rid)){
         if (TEST_MSGS) echo '<h4 style="color:red;">ERR 922</h4>';
			$this->err=922;
			return false;
		}
		
		// 3. Process the comments
		if(false==$this->addComment($rid)){			
         if (TEST_MSGS) echo '<h4 style="color:red;">ERR 923</h4>';
			$this->err=923;
			return false;
		}

		// Mark it as complete
		if(false==$this->expireTheRater($rid)){
         if (TEST_MSGS) echo '<h4 style="color:red;">ERR 924</h4>';
			$this->err=924;
			return false;
		}

		return $rid;
	}	
	// Add Demographics
	function addDemogr($rid){
		$query="insert into ";
		if($this->catid==1){
			// Self
			$query=$query." DEMOGR (CID,DMID,TID,TXT) values ";
			$end=18;
		}
		else{
			// Other
			$query=$query." RATERDEMOGR (RID,DMID,TID,TXT) values ";
			$end=4;
		}
		$comma="";
		for($i=1;$i<=$end;$i++){
			$query=$query."$comma($rid,$i,".$this->tid.",'".$this->demo[$i]."') ";
			$comma=",";
		}
		$conn=dbConnect();
		return mysql_query($query);
	}
	
	// Add the responses to the Items
	function addResponse($rid){
		$end=78;
		if($this->catid==1){
			if($this->tid==3){
				$end=99;
			}
			else{
				$end=114;
			}
		}
		$query="insert into RATERRESP (ITEMID,TID,RID,VAL) values ";
		$comma="";
		for($i=1;$i<=$end;$i++){
			$query=$query."$comma($i,".$this->tid.",$rid,'".$this->form[$i]."') ";
			$comma=",";
		}
		$conn=dbConnect();
		return mysql_query($query);
	}
	
	function addComment($rid){
		// 2007/01/08 - Changed condition form ==2 to >=2
		// to mitigate defect 537.
		if($this->catid>=2){
			// Only for "real" raters
			$query="insert into RATERCMT (ITEMID,TID,RID,VAL) values ";
			$comma="";
			for($i=79;$i<=80;$i++){
				$query=$query."$comma($i,".$this->tid.",$rid,'".$this->form[$i]."') ";
				$comma=",";
			}
			$conn=dbConnect();
			return mysql_query($query);
		}
		return true;
	}
	
	function expireTheRater($rid){
		$conn=dbConnect();
		$query="update RATER set EXPIRED='Y', STARTDT=NOW(), ENDDT=NOW() where RID=$rid";
		return mysql_query($query);
	}
	
}
?>

<?php
require_once "../meta/program.php";
class CDPProgram{
	
	var $extid;
	var $extname;
	var $location;
	var $conid;
	var $dt;
	var $tid;
	var $pid;
	var $error=0;
	
	
	function getPID(){
		return $this->pid;
	}
	
	function getError(){
		return $this->error;
	}

	
	function CDPProgram($arg){
		$this->extid=$arg['EventID'];
		$this->extname=$arg['EventName'];
		$this->location=$arg['EventLocation'];
		$this->conid=$arg['CONID'];
		$this->dt=$arg['EventDate'];
		$this->tid=$arg['instrument_Type'];
	}
	
	function process(){
		$conn=dbConnect();
		// see if we already have the program
		$sql="select PID from WSProgram where EXTID=".$this->extid." and CONID=".$this->conid;
//		echo "\n$sql\n";
		$rs=mysql_query($sql);
		if(false==$rs){
			// an error occurred
			$this->pid=false;
			$this->error=807;
			return false;	
		}
		
		$rows=dbRes2Arr($rs);
		if(0<count($rows)){
			// we have at least one row - retrieve the PID
			$this->pid=$rows[0][0];
		}
		else{
			// We don't have this program:
			// Insert the program
			$progData=array($this->extname,$this->dt,$this->dt,$this->conid);
			$this->pid=pgmInsert($progData);
			if(false!=$this->pid){
				// Insert the instrument
				if(false==instrInsert($this->pid,$this->tid)){
					$this->pid=false;
				}
				else{
					// finally, insert the row
					$sql="insert WSProgram (CONID,EXTID,PID,EXTNAME,LOCATION,EXTDATE) values (".$this->conid.",".$this->extid.",".$this->pid.",'".$this->extname."','".$this->location."','".$this->dt."')";
//					echo "\n$sql\n";
					$rs=mysql_query($sql);
					if(false==$rs){
						// an error occurred
						$this->pid=false;
						$this->error=808;
						return false;	
					}
				}
			}
		}
		if(false==$this->pid){
			// lookup and/or creation failed
			$this->error=809;
		}
		return $this->pid;
	}
	
}
?>

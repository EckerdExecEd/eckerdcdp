<?php
if (!isset($_COOKIE['sessionid']) && $_SERVER['SERVER_NAME'] != 'www.onlinecdp.org') {
// redirect to your main site
header('Location: https://www.onlinecdp.org/');
}
include_once "admin/default.php"; //Determines if the logins will be available
$HTML=(!$systemON)? $systemMSG : "Please enter email address:
<br>
<input type=\"text\" name=\"uid\" value=\"\" maxlength=\"50\">
<br>
Password:
<br>
<input type=\"password\" value=\"\" name=\"pwd\" maxlength=\"20\">
<br>
<input type=\"submit\" value=\"Log In\">";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
  <head> 
    <title>Center for Conflict Dynamics - Welcome to the Center for Conflict Dynamics at Eckerd College</title> 
    <link href="https://www.onlinecdp.org/index_files/styles_backend.css" rel="stylesheet" type="text/css" />
  </head> 
  <body> 
    <div id="wrapper"> 
      <div id="main"> 
        <div id="banner"> 
          <h5><a href="index.php">CCD Home</a></h5> 
    		  <div id="features"> 
            <img alt="Center for Conflict Dynamics" class="right" height="178" src="https://www.onlinecdp.org/index_files/CCD-topright.png" title="Center for Conflict Dynamics" width="306"/> 
          </div><!-- end features --> 
        </div><!-- end banner --> 
      <div id="content"> 
    <div id="left">
<h1>Consultant Login</h1> 
<p>
<?php
  // for secure login...
	$path = $_SERVER['SCRIPT_FILENAME'];
	$server = 'www.onlinecdp.org'; //$_SERVER['SERVER_NAME'];
	$path = str_replace('/var/www/html', '', $path);
	$path = str_replace('index.php', 'cons/home.php', $path);
	$url  = "http://".$server.$path;
	$surl = "https://".$server.$path;
  
?>
<!-- <form name="frm1" action="cons/home.php" method=POST> -->
<form name="frm1" action="<?=$surl?>" method="post">

<input type="hidden" name="s" value="1">
<?php echo $HTML; ?>
</form></p>
    </div><!-- end left --> 
				<div id="right"> 
<div class="feature">
<h3>CDP Customer Service</h3> 
<p>Monday - Friday<br />
8:00 am - 4:00 pm</p>
<p>888-359-9906<br />
<a href="mailto:cdp@eckerd.edu">cdp@eckerd.edu</a></p> 
</div> 
			</div><!-- end right --> 
			</div><!-- end content --> 
		</div><!-- end main --> 
		<div id="footer"> 
<div id="extras"> 
<div id="copyright"> 
<p>Conflict Dynamics Profile is a registered trademark of Eckerd College</p> 
</div> 
<!-- end copyright --> 
<a href="https://www.eckerd.edu"><img alt="Eckerd College Home" class="left" height="66" src="https://www.onlinecdp.org/index_files/foot_logo.gif" title="Eckerd College Home" width="207"/></a> 
<p>4200 54th Avenue South<br /> 
 St. Petersburg, Florida 33711<br /> 
888-359-9906 | <a href="mailto:cdp@eckerd.edu">cdp@eckerd.edu</a></p> 
</div><!-- end extras --> 
		</div><!-- end footer -->	
	</div><!-- end wrapper --> 
</body> 
</html>

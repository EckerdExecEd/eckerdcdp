<?php
require_once "../meta/dbfns.php";
require_once "../svc/class.CDPCandidate.php";
require_once "../svc/class.CDPRater.php";

define('TEST_MODE', false);   // No updates/inserts performed (for debugging)
define('TEST_MSGS', false);   // Display test debugging messages

class DataReader {

	var $_pid;
	var $_tid;
	var $_conid;
	var $_filename;
	var $_data;
	var $_numRaters;
	var $_selfIdx = -1;
	var $_cid = -1;
  var $_lnames=array();	
	var $_dataErrorMsg;
	
  function DataReader( $dataOrFile='', $pid=-99){

		// constructor
		$this->_prog=array();
		$this->_cand=array();
		$this->_filename = '';
		$this->_data = array();
    $this->_dataErrorMsg="";
    
		// Get parameters
		$this->_pid = $pid;			if ($this->_pid == -99) $this->_pid = $_POST["pid"];
	  $ids=$this->getConTestIDs();
	  
	  $this->_conid=($ids)? $ids[0] : false;
	  $this->_tid=($ids)? $ids[1] : false;
    
		if (is_array($dataOrFile)) {
			// Data array passed
			$this->_data = $dataOrFile;
		} else {
			// Filename passed
			if (strlen($dataOrFile) > 0) {
				$this->_filename = $dataOrFile;
				$this->readDataFile();
			}
		}
   
    $this->_numRaters = count($this->_data);
    $this->_selfIdx = $this->findSelfIdx();
   
    $this->setLayout();                             // Define data layout		
		$this->verifyData();                            // Initial check of data
  
    foreach($this->_data as $i=>$vals){
        $this->_lnames[$i]=$this->getValue('lname', $i);
    }

  }

  function processCandidate(){
    /**
     * 1) Set Program and Candidate data
     * 2)
     **/ 
     
    $this->_prog['instrument_Type'] = 1;
    
    // Get cid (should come back as zero)
		$this->_cid = (int) $this->getValue('candidateid', $this->_selfIdx);

    $this->_cand['fname'] = $this->getValue('fname', $this->_selfIdx);
		$this->_cand['lname'] = $this->getValue('lname', $this->_selfIdx);
		$this->_cand['email'] = $this->getValue('email', $this->_selfIdx);
		$this->_cand['pid'] = $this->_pid;
		$this->_cand['language'] = 1;
		$this->_cand['clientidentification'] = $this->getValue('clientidentification', $this->_selfIdx);
    
    if(!$this->candidateExists()){
      if($this->insertCandidate()){
        $this->_cand['cid'] = $this->_cid;
        $this->insertCandcon();
        $this->insertRaterLang($this->_cid,$this->_pid,1);
      }
    }else{
      $this->_cand['cid'] = $this->_cid;
      $this->updateCandidate();
    }  
                          
  }

	function processRaters( ) {
		/**
		 *	Loop through and process all of the raters
		 **/

		$msg = '';
		$raterCount = 0;

		$readyToScore = true;

		// First process the Self, in order to get the Candidate ID (CID)
    if (TEST_MSGS) echo "<hr>";
		$rid = $this->processRater($this->_selfIdx);
		
		$msg .= '<br>Processing self rater';
		if ($rid == false) {
			$msg .= '<br><font color="#ff0000">Error 2: unable to process data for self rater</font>';
      $readyToScore = false;
		} else {
			$raterCount++;
			$msg .= '<br><font color="#006633">Candidate - OK!</font>';
		}

		// Process all the other raters
		for ($idx=0; $idx<$this->_numRaters; $idx++) {
			if ($idx != $this->_selfIdx) {
            if (TEST_MSGS) echo "<hr><h4>Processing rater $raterCount (idx = $idx, selfIdx = $this->_selfIdx) ...</h4>";
				$rid = $this->processRater($idx);
				$msg .= '<br>Processing rater ' . $raterCount;
					
				if ($rid == false) {
					//$msg .= '<br><font color="#ff0000">Error 2: unable to process data for rater ' . $raterCount . '</font>';
					$msg .= '<br><font color="#ff0000">Error 2: unable to process data for rater ' . $idx . '</font>';	// For debugging
					$readyToScore = false;
				} else {
					 $msg .= ' ('  . $rid . ') - OK!';		// Use for debugging
					//$msg .= ' - OK!';
				}
				$raterCount++;
			}
		}

		if ($readyToScore) {
			// Everything went well - we don't really score we just update the completion status
  			if (setComplete($this->_cid) == false) {
   			$msg .= '<br><font color="#ff0000">Error 3: unable to score - aborting</font>';
		   	$this->_cid = false;
	   	}
		} else {
			$msg .= '<br><font color=\"#ff0000\">Error 4: unable to score - aborting</font>';
			$this->_cid = false;
		}
/**/
      if (TEST_MSGS) echo "<h2>DONE PROCESSING RATERS</h2>";

		return $msg;
	}

	function processRater( $idx ) {
		/**
		 * Process a single rater that is specified by the index of that rater
		 **/
       if (TEST_MSGS) echo "<li>DataReader->processRater(idx = $idx)";
		// Get demographics
		$demographics = $this->getDemographics($idx);
		// Get rater responses
		$responses = $this->getResponses($idx);
		//die(print_r($demographics));
		$rat = new CDPRater($this->_cid, $this->_prog['instrument_Type'], $demographics, $responses);
		
 		if ($idx == $this->_selfIdx) {
 		   // Candidate, ie. self,  so rid is equal to the cid
         if (TEST_MSGS) echo " <b>SELF</b>";
   		$rid = $rat->process($this->_cid);
   	} else {
         if (TEST_MSGS) echo " <b>NON-SELF</b>";
   		$rid = $rat->process($rid);
   	}
      if (TEST_MSGS) echo "<li>.... <b>rid</b> = $rid";

		$conn = dbConnect();
  		if ($idx == $this->_selfIdx) {
   		// Self Rater
	   	// Set email address
  			$query = 'update RATER set EMAIL = "' . $rid . '" where CID = ' . $this->_cid . ' AND RID = ' . $rid;
           if (TEST_MSGS) echo "<li>.... <b>SELF : </b>$query";
   		$rs = mysql_query($query);
  		} else {
   		// Non Self Rater
	   	// Set LNAM to RID for non-self raters because we need some value for FNAM or LNAM
		   // Also set email to RID in case we need a value there for any reason
		   $lname=$this->_lnames[$idx];
  			$query = 'update RATER set LNAM = "' . $lname . '", EMAIL = "' . $rid . '" where CID = ' . $this->_cid . ' AND RID = ' . $rid;
           if (TEST_MSGS) echo "<li>.... <b>NON-SELF : </b>$query";
   		$rs = mysql_query($query);
   	}
		
		return $rid;

	}
    
  function getConTestIDs(){
    $pid=$this->_pid;
    $sql="SELECT a.CONID, b.TID FROM PROGCONS a, PROGINSTR b
           WHERE a.PID=b.PID
             AND a.PID = $pid
             AND b.TID IN (1,3)";
    $res=fetchOne($sql,'A');
    //die(print_r($res));
    if(count($res)==2){
      return $res;
    }else{
      $this->_dataErrorMsg.="Unable to find Consultant ID and/or Test ID for program $pid.<br>";
      return false;
    }
  }
  
	function readDataFile(){                          //Read the contents of a data file
		if (!strlen($this->_filename)) {                // No filename specified
			$this->_data = array();
		}else{                                          // Read data file
			$fp = fopen($this->_filename, 'r');
			$contents;
			while(false != ($str = fgets($fp,1024))){    // Discard empty CR-LF lines
				$str = trim($str);
				if (strlen($str) > 2){
					$contents[] = $str;
				}
			}
			fclose($fp);                                 // Close the temporary file
      $this->_data = $contents;
		}
	} 

	function findSelfIdx() {                        // Assumes the SELF is the longest row
		$selfIdx = -1;
		$selfLen = 0;
		for ($i=0; $i<$this->_numRaters; $i++) {
			if (strlen($this->_data[$i]) > $selfLen) {
				$selfIdx = $i;
				$selfLen = strlen($this->_data[$i]);
			}
		}
		$this->_selfIdx = $selfIdx;
		return $this->_selfIdx;
	}

	function setLayout( ) {
		/**
		 *	This function defines the layout of the data
		 *	Each entry consists of a name for the field and an array of initial position and last position
		 **/
		 $this->layout['age'] = array(83, 84);
		 $this->layout['clientidentification'] = array(41, 48);
		 $this->layout['degree'] = array(91, 91);
		 $this->layout['email'] = array(-1, -1);
		 $this->layout['ethnicity_nativeamerican'] = array(85, 85);
		 $this->layout['ethnicity_asian'] = array(86, 86);
		 $this->layout['ethnicity_black'] = array(87, 87);
		 $this->layout['ethnicity_hispanic'] = array(88, 88);
		 $this->layout['ethnicity_caucasian'] = array(89, 89);
		 $this->layout['ethnicity_other'] = array(90, 90);
		 $this->layout['ethnicityexplained'] = array(-1, -1);
		 $this->layout['familiarity'] = array(99, 99);
		 $this->layout['fname'] = array(64, 75);
		 $this->layout['function'] = array(96, 97);
		 $this->layout['functionspecific'] = array(-1, -1);
		 $this->layout['lname'] = array(49, 63);
		 $this->layout['middleinitial'] = array(76, 76);
		 $this->layout['orglevel'] = array(95, 95);
		 $this->layout['orgname'] = array(-1, -1);
		 $this->layout['orgtypea'] = array(92, 92);		// Profit/Not-for-profit
		 $this->layout['orgtypeb'] = array(93, 94);
		 $this->layout['relationship'] = array(98, 98);
		 $this->layout['responses'] = array(116, 193);
		 $this->layout['selfresponses'] = array(116, 229);
		 $this->layout['sex'] = array(77, 82);
		 $this->layout['workphone'] = array(-1, -1);

			// These are not used
		 $this->layout['seqnumber'] = array(1, 9);
		 $this->layout['unknown'] = array(10, 18);
		 $this->layout['type'] = array(19, 21);
		 $this->layout['y'] = array(23, 23);
		 $this->layout['num1'] = array(25, 28);
		 $this->layout['num2'] = array(30, 34);
		 $this->layout['n'] = array(39, 39);
		 $this->layout['candidateid'] = array(101, 110);
	}

	function getValue( $fieldname, $idx=0 ) {          
    /**
     *    Return the value from the data string
     **/      
		$pos1 = $this->layout[$fieldname][0];            // Get position of the field in the line
		if ($pos1 < 0) {                                 // The field is not in the data string
			$value = '';
		}else{                                           // Retrieve the field
			$pos1 = $pos1 - 1;
			$pos2 = $this->layout[$fieldname][1]; 
			$value = substr($this->_data[$idx], $pos1, $pos2 - $pos1);
		}
    $value = trim($value);
		
		switch ($fieldname) {                            // Special processing for some fields
			case "ethnicity" :
				$value = '';
				$dataStr = $this->_data[$idx];
				if (substr($dataStr, $this->layout['ethnicity_nativeamerican'][0], 1) == 1) $value = 6;
					elseif (substr($dataStr, $this->layout['ethnicity_asian'][0], 1) == 1) $value = 7;
					elseif (substr($dataStr, $this->layout['ethnicity_black'][0], 1) == 1) $value = 8;
					elseif (substr($dataStr, $this->layout['ethnicity_hispanic'][0], 1) == 1) $value = 9;
					elseif (substr($dataStr, $this->layout['ethnicity_caucasian'][0], 1) == 1) $value = 10;
					elseif (substr($dataStr, $this->layout['ethnicity_other'][0], 1) == 1) $value = 11;
					else $value = '';
				break;
      case "sex" :
				if ($value == "Female") $value = "F";
					elseif ($value == "Male") $value = "M";
					else $value = substr($value, 0, 1);
				break;
		}
		return mysql_escape_string($value);
  }

   function verifyData() {
      $errorMsg = "";

      // Verify Relationships
      // Make sure that only one rater does not have the relationship field filled in
      // (that should be self)
      $numSelfs = 0;
      for ($idx=0; $idx<count($this->_data); $idx++) {
         if (strlen(trim($this->getValue("relationship", $idx))) == 0) $numSelfs++;
      }
      if ($numSelfs > 0) $errorMsg .= '<p font="normal arial 10pt;color:red;margin:2px;">A value for `relationship` column is missing in at least one entry.<br/>(All entries must have a value for `relationship` except for the self.)</p>';

      $numBlank = 0;
      for ($idx=0; $idx<count($this->_data); $idx++) {
         if (strlen(trim($this->getValue("familiarity", $idx))) == 0) $numBlank++;
         if ($numBlank > 1) {
            $errorMsg .= '<p font="normal arial 10pt;color:red;margin:2px;">A value for `familiarity` column is missing in at least one entry.<br/>(All entries must have a value for `familiarity` except for the self.)</p>';
            break;
         }
      }
      $this->_dataErrorMsg.=$errorMsg;
  }   

  function candidateExists(){
    $pid=$this->_cand['pid'];
    $altid=$this->_cand['clientidentification'];
    
    $qry="SELECT CID from CANDIDATE WHERE PID = $pid AND ALTID = '$altid'";
    $cid=fetchOne($qry);
    if((is_numeric($cid))&&($cid>0)){
      $this->_cid=$cid; 
      return true;
    }else{
      return false;
    }
/**/ 
  }
  
  function insertCandidate(){
  
    $newCID=getKey("RATER","RID");
    $pid= $this->_cand['pid'];
    $altid=$this->_cand['clientidentification'];
    $fname=$this->_cand['fname'];
    $lname=$this->_cand['lname'];
    $email=$this->_cand['email'];
    $oth=($this->_tid==1)? 'Y' : 'N';
    
    $sql ="insert into CANDIDATE (CID,PID,ALTID,FNAME,LNAME,EMAIL,EXPIRED,ARCHFLAG,SELF,OTH)VALUES($newCID,$pid,";
    $sql.=(strlen($altid)<1)? "NULL," : "'$altid',";    
    $sql.=(strlen($fname)<1)? "NULL," : quote_smart($fname).",";
    $sql.=(strlen($lname)<1)? "NULL," : quote_smart($lname).",";
    $sql.=(strlen($email)<1)? "NULL," : quote_smart($email).",";
    $sql.="'N','N','Y','$oth')";
    //die($sql);

    if(false===mysql_query($sql)){
      $this->_dataErrorMsg.="Failed to insert new candidate.<br>";
      return false; 
    }else{
      $this->_cid=$newCID;
    }
    return true;
   
  }
  
  function insertCandcon(){
    $cid=$this->_cand['cid'];
    $conid=$this->_conid;
    $sql="insert into CANDCONS (CID,CONID) values ($cid,$conid)";
    if(false===mysql_query($sql)){
      $this->_dataErrorMsg.="Failed to Insert candidate-consultant record in CANDCON where CID = $cid and CONID = $conid.<br>"; 
      return false;
    }    
    return true;    
  }

  function insertRaterLang($rid,$pid,$lid=1){
    $sql="insert into RATERLANG (RID,PID,LID) values ($rid,$pid,$lid)";
    if(false===mysql_query($sql)){
      $this->_dataErrorMsg.="Failed to Insert record into RATERLANG where RID = $rid and PID = $pid.<br>"; 
      return false;
    }    
    return true;    
  }

    
/**/ 
  function updateCandidate(){
    
    $pid=$this->_cand['pid'];
    $altid=$this->_cand['clientidentification'];
    $cid=$this->_cand['cid'];
    $fname=(strlen($this->_cand['fname'])<1)? "NULL" : quote_smart($this->_cand['fname']);
    $lname=(strlen($this->_cand['lname'])<1)? "NULL" : quote_smart($this->_cand['lname']);
    $email=(strlen($this->_cand['email'])<1)? "NULL" : quote_smart($this->_cand['email']); 
      
    $sql ="update CANDIDATE set FNAME=".$fname.", LNAME=".$lname.", EMAIL=".$email." WHERE PID = $pid AND ALTID = '$altid' AND CID = $cid";
    if(!executeQurey($sql)){
      $this->_dataErrorMsg.="Failed to Update candidate where CID = $cid.<div style=\"color:red;\">$sql</div>"; 
      return false;
    }else{    
      return true;
    }
  }
  
	function getResponses( $idx ) {
		/**
		 *	Returns an array of rater responses  (question # => response)
		 **/

		if ($idx == $this->_selfIdx) {
			// Self raters usually have extra questions
			$responseStr = $this->getValue('selfresponses', $idx);
		} else {
			$responseStr = $this->getValue('responses', $idx);
		}
		$responses = array();
		
		$numResponses = strlen($responseStr);
		for ($i=0; $i<$numResponses; $i++) {
			$qNum = $i+1;
			$responses[$qNum] = substr($responseStr, $i, 1);
		}
	
		return $responses;
		
	}
	
	function getDemographics( $idx ) {
		/**
		 *	Returns the demographics array
		 **/

		$demo = array();

		if ($idx == $this->_selfIdx) {
			// ----------------
			// Self rater
			// ----------------
			
			// RaterType (self is 1)
			$demo[0] = 1;
			
			// Organization Name  (free form)
			$demo[1] = $this->getValue('orgname', $idx);
			
			// Work Phone  (valid phone number format)
			$demo[2] = $this->getValue('workphone', $idx);
			
			// Email Address  (valid email format)
			$demo[3] = $this->getValue('email', $idx);
			
			// Sex  (M/F)
			$demo[4] = $this->getValue('sex', $idx);

			// Age  (number)
			$demo[5] = (int) $this->getValue('age', $idx);

			// Ethnicity  (6  - Native American or Alaskan
			//             7  - Asian or pacific islander
			//             8  - Black or african american
			//             9  - Hispanic
			//             10 - Caucasian
			//             11 - Other)
			$demo[6] = $this->getValue('ethnicity', $idx);
	
			// Ethnicity explanation, Only if "Other" (free form)
			$demo[12] = $this->getValue('ethnicityexplained', $idx);
	
			// Highest degree earned  (1 - High school or less
			//                         2 - Associate's
			//                         3 - bachelor's
			//                         4 - masters
			//                         5 - doctorate professional)
			$demo[13] = (int) $this->getValue('degree', $idx);
	
			// Organization Level  (1 - top executive
			//                      2 - executive
			//                      3 - upper middle management
			//                      4 - middle office manager ...
			//                      5 - first level foreperson
			//                      6 - Hourly employee
			//                      7 - N/A)
			$demo[14] = (int) $this->getValue('orglevel', $idx);
	
			// Type of Organization - A  (1 - For Profit, 2 - Not For Profit)
			$demo[15] = (int) $this->getValue('orgtypea', $idx);
	
			// Type of Organization - B  (1  - Manufacturing
			//                            2  - Transportation/Communications/Utilities
			//                            3  - Wholesale/Retail Sales
			//                            4  - Finance/Insurance/Banking
			//                            5  - Personal Services/Legal
			//                            6  - Business Services/Consulting
			//                            7  - Health Services
			//                            8  - Eductation Services
			//                            9  - Social Services
			//                            10 - Public Administration/Legal
			//                            11 - Military
			$demo[16] = (int) $this->getValue('orgtypeb', $idx);

			// Your function  (1  - Accounting
			//                 2  - Administion
			//                        .
			//                        .
			//                        .
			//                 23 - yop managment
			//                 24 - Other
			$demo[17] = (int) $this->getValue('function', $idx);

			// Specification of function (free form)
			$demo[18] = $this->getValue('functionspecific', $idx);

		} else {
			// ----------------
			// Non-self rater
			// ----------------
		
			// RaterType  (1 - self
			//             2 - boss
			//             3 - peer
			//             4 - direct report)
			// Note: We have to add 1 to raterType that is read in
			
			$demo[0] = (int) $this->getValue('relationship', $idx) + 1;
			
			// Organization name of the person being rated (free form)
			$demo[1] = $this->getValue('orgname', $idx);

			// How well do you know the person being rated (1 - I hardly know the person
			//                                              2 - I know the person somewhat
			//                                              3 - I know the person well
			//                                              4 - I know the person very well
			$demo[2] = (int) $this->getValue('familiarity', $idx);

			// Sex (M/F)
			$demo[3] = $this->getValue('sex', $idx);

			// Age (Numeric)
			$demo[4] = (int) $this->getValue('age', $idx);

		}

		return $demo;

	}	

 }
 
function setComplete() {
	//??????? 
	// Copied from upload2.php (12/9/08)
	return true;
} 
?>
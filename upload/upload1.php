<?php
$msg="";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
require_once "../meta/upload.php";
require_once "../cons/consfn.php";
$tid=$_POST['tid'];
$conid=$_SESSION['conid'];
$pid="0";
if(isset($_POST['pid'])){
	$pid=$_POST['pid'];
}
$pgms=listTheUploadablePrograms($conid,$tid,$pid);
writeHead("Conflict Dynamics Profile - Consultants",false);
if ($tid == 1) $crumbs = array("Home"=>"../cons/home.php", "CDP 360"=>"../cons/cdp360.php", "Upload Scanned Surveys"=>"");
	else $crumbs = array("Home"=>"../cons/home.php", "CDP Individual"=>"../cons/cdpindividual.php", "Upload Scanned Surveys"=>"");
writeBody("Upload Scanned Surveys",$msg,$crumbs);
?>

<form name="frm1" action="upload2.php" method=POST enctype="multipart/form-data">
<input type="hidden" name="tid" value="<?=$tid?>">
<table border=0>
<tr>
<td>Select the Program you want<br>the Participant to belong to</td><td><select name="pid"><?=$pgms?></td></select></tr>
<tr><td>Select the file containing the<br>scoring data</td><td><input type="file" name="dataFile"></td></tr>
<tr>
<td><input type="submit" value="Upload"></td>
<td><input type="button" value="Cancel" onClick="javascript:frm1.action='../cons/home.php';frm1.submit();"></td>
</tr>
</table>
</form>
<?php
writeFooter(false);

function listTheUploadablePrograms($conid,$tid,$pid){
	$pgmList=getUploadablePrograms($conid,$tid);
	$rc="<option value=''>-- Select Program --</option>";
	foreach($pgmList as $row){
		$sel="";
		if($row[0]==$pid){
			$sel="selected";
		}
		$rc=$rc."<option value='$row[0]' $sel>$row[1]</option>";
	}
	return $rc;
}
?>
